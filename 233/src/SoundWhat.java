import java.awt.event.ActionEvent;

final class SoundWhat implements Runnable {

    volatile boolean running = false;
    volatile boolean stopped = false;
    volatile SoundPlayer[] soundPlayers = new SoundPlayer[2];
    CachePackageManager cpm;

    static void method499(Object var1, CachePackageManager var2) {
        if (var2.systemEventQueue != null) {
            for (int var3 = 0; var3 < 50 && var2.systemEventQueue.peekEvent() != null; ++var3) {
                GameApplet.sleep(1L, -17239);
            }

            try {
                if (var1 != null) {
                    var2.systemEventQueue.postEvent(new ActionEvent(var1, 1001, "dummy"));
                }
            } catch (Exception var4) {
                ;
            }
        }
    }


    public final void run() {
        this.running = true;

        try {
            while (!this.stopped) {
                for (int i = 0; i < 2; ++i) {
                    SoundPlayer sp = this.soundPlayers[i];
                    if (sp != null) {
                        sp.method494(79);
                    }
                }

                GameApplet.sleep(10L, -17239);
                method499(null, this.cpm);
            }
        } catch (Exception var8) {
            Utility.sendClientError(null, -257, var8);
        } finally {
            this.running = false;
        }
    }

}
