import java.awt.*;
import java.awt.image.ImageConsumer;
import java.awt.image.ImageObserver;
import java.awt.image.ImageProducer;
import java.util.Hashtable;

final class ImagePrubser implements ImageProducer, ImageObserver {

    static ImageConsumer anImageConsumer894;

    static void method364(byte[] var0, int var1) {
        if (anImageConsumer894 != null) {
            anImageConsumer894.setPixels(0, 0, ClientStream.anInt1698, ClientStream.anInt1697, Scene.aColorModel384, var0, 0, ClientStream.anInt1698);
            if (var1 < 2) {
                SoundPlayer.aSoundWhat_490 = null;
            }

            anImageConsumer894.imageComplete(3);
        }
    }

    public final boolean imageUpdate(Image var1, int var2, int var3, int var4, int var5, int var6) {
        return true;
    }

    public final void startProduction(ImageConsumer var1) {
        this.addConsumer(var1);
    }

    public final synchronized boolean isConsumer(ImageConsumer var1) {
        return var1 == anImageConsumer894;
    }

    public final void requestTopDownLeftRightResend(ImageConsumer var1) {
    }

    public final synchronized void removeConsumer(ImageConsumer var1) {
        if (var1 == anImageConsumer894) {
            anImageConsumer894 = null;
        }
    }

    public final synchronized void addConsumer(ImageConsumer var1) {
        anImageConsumer894 = var1;
        var1.setDimensions(ClientStream.anInt1698, ClientStream.anInt1697);
        var1.setProperties((Hashtable) null);
        var1.setColorModel(Scene.aColorModel384);
        var1.setHints(14);
    }

}
