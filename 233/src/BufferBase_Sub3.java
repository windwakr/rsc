import java.math.BigInteger;

class BufferBase_Sub3 extends BufferBase {

    static long aLong1640;
    static char[] unicodeChars = {
            '\u20AC', '\0'/**/, '\u201A', '\u0192', '\u201E', '\u2026', '\u2020', '\u2021', '\u02C6', '\u2030',
            '\u0160', '\u2039', '\u0152', '\0'/**/, '\u017D', '\0'/**/, '\0'/**/, '\u2018', '\u2019', '\u201C',
            '\u201D', '\u2022', '\u2013', '\u2014', '\u02DC', '\u2122', '\u0161', '\u203A', '\u0153', '\0'/**/,
            '\u017E', '\u0178'
    };
    static byte[][] aByteArrayArray820 = new byte[50][];
    static byte[][] aByteArrayArray806 = new byte[250][];
    static int[] anIntArray1091;
    int offset;
    byte[] buffer;


    BufferBase_Sub3(int var1) {
        this.offset = 0;
        this.buffer = Utility.method354(var1);
    }

    BufferBase_Sub3(byte[] var1) {
        this.offset = 0;
        this.buffer = var1;
    }

    final void addByte(int var2) {
        this.buffer[this.offset++] = (byte) var2;
    }

    final int getUnsignedInt2() {
        return this.buffer[this.offset] >= 0 ? this.getUnsignedShort() : Integer.MAX_VALUE & this.getUnsignedInt();
    }

    final void pjstr(String string) {
        int nul = string.indexOf('\0');
        if (nul >= 0) {
            throw new IllegalArgumentException("NUL character at " + nul + " - cannot pjstr");
        } else {
            this.offset += Utility.writeUnicodeString(string, 0, string.length(), buffer, offset);
            this.buffer[this.offset++] = 0;
        }
    }

    final byte getByte() {
        return this.buffer[this.offset++];
    }

    private void method208(int var1, byte[] var3, int var4) {
        for (int var5 = var1; var4 + var1 > var5; ++var5) {
            var3[var5] = this.buffer[this.offset++];
        }
    }

    final boolean value_same(int offset) {
        this.offset -= 4;
        int var2 = Utility.getnum(this.offset, offset, this.buffer);
        int var3 = this.getUnsignedInt();
        return var3 == var2;
    }

    final void setLengthShort(int offset, int length) {
        this.buffer[-length + this.offset - offset] = (byte) (length >> 8);
        this.buffer[-1 + (this.offset - length)] = (byte) length;
    }

    final int getUnsignedInt() {
        this.offset += 4;
        return (this.buffer[this.offset - 1] & 255) + ((255 & this.buffer[-4 + this.offset]) << 24) - (-(this.buffer[-3 + this.offset] << 16 & 16711680) - (this.buffer[-2 + this.offset] << 8 & '\uff00'));
    }

    final void encrypt(BigInteger exponent, BigInteger modulus) {
        int var4 = this.offset;
        this.offset = 0;
        byte[] var5 = new byte[var4];
        this.method208(0, var5, var4);
        BigInteger var6 = new BigInteger(var5);
        BigInteger encrypted = var6.modPow(exponent, modulus);
        byte[] var8 = encrypted.toByteArray();
        this.offset = 0;
        this.addShort(var8.length);
        this.addBytes(var8.length, var8, 0);
    }

    final String gjstr2() {
        byte nul = this.buffer[this.offset++];
        if (nul != 0) {
            throw new IllegalStateException("Bad version number in gjstr2");
        } else {
            int off = this.offset;
            while (this.buffer[this.offset++] != 0) ;
            int len = this.offset - off - 1;
            return len == 0 ? "" : Utility.readUnicodeString(this.buffer, off, len);
        }
    }

    final long method214(int var1) {
        long var2 = 4294967295L & (long) this.getUnsignedInt();
        if (var1 > -16) {
            return -50L;
        } else {
            long var4 = 4294967295L & (long) this.getUnsignedInt();
            return (var2 << 32) + var4;
        }
    }

    final void addInt(int var2) {
        this.buffer[this.offset++] = (byte) (var2 >> 24);
        this.buffer[this.offset++] = (byte) (var2 >> 16);
        this.buffer[this.offset++] = (byte) (var2 >> 8);
        this.buffer[this.offset++] = (byte) var2;
    }

    final byte[] method216(byte var1) {
        byte[] var2 = new byte[this.offset];
        int var3 = 0;
        if (var1 >= -84) {
            client.aStringArray1659 = null;
        }

        while (this.offset > var3) {
            var2[var3] = this.buffer[var3];
            ++var3;
        }

        return var2;
    }

    final void addInt3Byte(int var2) {
        this.buffer[this.offset++] = (byte) (var2 >> 16);
        this.buffer[this.offset++] = (byte) (var2 >> 8);
        this.buffer[this.offset++] = (byte) var2;
    }

    final int method218() {
        this.offset += 2;
        int var2 = ('\uff00' & this.buffer[-2 + this.offset] << 8) - -(255 & this.buffer[this.offset - 1]);
        if (var2 > 32767) {
            var2 -= 65536;
        }

        return var2;
    }

    final void addBytes(int var1, byte[] var3, int var4) {
        for (int var5 = var4; var4 - -var1 > var5; ++var5) {
            this.buffer[this.offset++] = var3[var5];
        }
    }

    final int method220() {
        int var2 = this.buffer[this.offset] & 255;
        return var2 < 128 ? this.getUnsignedByte() : this.getUnsignedShort() + -32768;
    }

    final void xtea_encrypt(int var1, int[] isaackeys, int var4) {
        int var5 = this.offset;
        this.offset = var1;
        int var6 = (-var1 + var4) / 8;

        for (int var7 = 0; var7 < var6; ++var7) {
            int var8 = this.getUnsignedInt();
            int var9 = this.getUnsignedInt();
            int var10 = 0;
            int var11 = -1640531527;

            for (int var12 = 32; var12-- > 0; var9 += (var8 >>> 5 ^ var8 << 4) + var8 ^ var10 - -isaackeys[var10 >>> 11 & 1356857347]) {
                var8 += var9 + (var9 << 4 ^ var9 >>> 5) ^ var10 - -isaackeys[3 & var10];
                var10 += var11;
            }

            this.offset -= 8;
            this.addInt(var8);
            this.addInt(var9);
        }

        this.offset = var5;
    }

    final int getUnsignedShort() {
        this.offset += 2;
        return (this.buffer[-1 + this.offset] & 255) + ((this.buffer[-2 + this.offset] & 255) << 8);
    }

    final void pjstr2(String string) {
        int nul = string.indexOf('\0');
        if (nul >= 0) {
            throw new IllegalArgumentException("NUL character at " + nul + " - cannot pjstr2");
        } else {
            this.buffer[this.offset++] = 0;
            this.offset += Utility.writeUnicodeString(string, 0, string.length(), buffer, offset);
            this.buffer[this.offset++] = 0;
        }
    }

    final void addShort2(int var2) {
        if (var2 >= 0 && var2 < 128) {
            this.addByte(var2);
        } else if (var2 >= 0 && var2 < '\u8000') {
            this.addShort(var2 + '\u8000');
        } else {
            throw new IllegalArgumentException();
        }
    }

    final void addShort(int var1) {
        this.buffer[this.offset++] = (byte) (var1 >> 8);
        this.buffer[this.offset++] = (byte) var1;
    }

    final int getUnsignedByte() {
        return this.buffer[this.offset++] & 255;
    }

}
