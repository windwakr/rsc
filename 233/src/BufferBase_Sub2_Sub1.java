final class BufferBase_Sub2_Sub1 extends BufferBase_Sub2 {

    private Class2 aClass2_1712 = new Class2();
    private Class2 aClass2_1713 = new Class2();
    private int anInt1714 = 0;
    private int anInt1715 = -1;

    static void method242(boolean var0, BufferBase var1, BufferBase var2) {
        if (var2.aBufferBase_188 != null) {
            var2.method160();
        }

        var2.aBufferBase_182 = var1;
        var2.aBufferBase_188 = var1.aBufferBase_188;
        if (var0) {
            var2.aBufferBase_188.aBufferBase_182 = var2;
            var2.aBufferBase_182.aBufferBase_188 = var2;
        }
    }


    final void method171(BufferBase_Sub4_Sub1 var1, int var2, int var3) {
        this.method172(BufferBase_Sub2_Sub2.method180(var1, var2, var3));
    }

    private synchronized void method172(BufferBase_Sub2 var1) {
        this.aClass2_1712.method153(118, var1);
    }

    private void method173(int var1) {
        for (BufferBase_Sub2 var2 = (BufferBase_Sub2) this.aClass2_1712.method152(27531); var2 != null; var2 = (BufferBase_Sub2) this.aClass2_1712.method151((byte) -75)) {
            var2.method170(var1);
        }

    }

    final int method167() {
        return 0;
    }

    final BufferBase_Sub2 method165() {
        return (BufferBase_Sub2) this.aClass2_1712.method151((byte) -76);
    }

    private void method174(BufferBase var1, BufferBase_Sub1 var2) {
        while (var1 != this.aClass2_1713.buffer && ((BufferBase_Sub1) var1).anInt1633 <= var2.anInt1633) {
            var1 = var1.aBufferBase_182;
        }

        method242(true, var1, var2);
        this.anInt1715 = ((BufferBase_Sub1) this.aClass2_1713.buffer.aBufferBase_182).anInt1633;
    }

    private void method175(BufferBase_Sub1 var1) {
        var1.method160();
        var1.method162();
        BufferBase var2 = this.aClass2_1713.buffer.aBufferBase_182;
        if (var2 == this.aClass2_1713.buffer) {
            this.anInt1715 = -1;
        } else {
            this.anInt1715 = ((BufferBase_Sub1) var2).anInt1633;
        }
    }

    final BufferBase_Sub2 method169() {
        return (BufferBase_Sub2) this.aClass2_1712.method152(27531);
    }

    final synchronized void method170(int var1) {
        do {
            if (this.anInt1715 < 0) {
                this.method173(var1);
                return;
            }

            if (this.anInt1714 + var1 < this.anInt1715) {
                this.anInt1714 += var1;
                this.method173(var1);
                return;
            }

            int var2 = this.anInt1715 - this.anInt1714;
            this.method173(var2);
            var1 -= var2;
            this.anInt1714 += var2;
            this.method176();
            BufferBase_Sub1 var3 = (BufferBase_Sub1) this.aClass2_1713.method152(27531);
            synchronized (var3) {
                int var5 = var3.method163(this);
                if (var5 < 0) {
                    var3.anInt1633 = 0;
                    this.method175(var3);
                } else {
                    var3.anInt1633 = var5;
                    this.method174(var3.aBufferBase_182, var3);
                }
            }
        } while (var1 != 0);

    }

    final synchronized void method164(int[] var1, int var2, int var3) {
        do {
            if (this.anInt1715 < 0) {
                this.method177(var1, var2, var3);
                return;
            }

            if (this.anInt1714 + var3 < this.anInt1715) {
                this.anInt1714 += var3;
                this.method177(var1, var2, var3);
                return;
            }

            int var4 = this.anInt1715 - this.anInt1714;
            this.method177(var1, var2, var4);
            var2 += var4;
            var3 -= var4;
            this.anInt1714 += var4;
            this.method176();
            BufferBase_Sub1 var5 = (BufferBase_Sub1) this.aClass2_1713.method152(27531);
            synchronized (var5) {
                int var7 = var5.method163(this);
                if (var7 < 0) {
                    var5.anInt1633 = 0;
                    this.method175(var5);
                } else {
                    var5.anInt1633 = var7;
                    this.method174(var5.aBufferBase_182, var5);
                }
            }
        } while (var3 != 0);

    }

    private void method176() {
        if (this.anInt1714 > 0) {
            for (BufferBase_Sub1 var1 = (BufferBase_Sub1) this.aClass2_1713.method152(27531); var1 != null; var1 = (BufferBase_Sub1) this.aClass2_1713.method151((byte) -103)) {
                var1.anInt1633 -= this.anInt1714;
            }

            this.anInt1715 -= this.anInt1714;
            this.anInt1714 = 0;
        }

    }

    private void method177(int[] var1, int var2, int var3) {
        for (BufferBase_Sub2 var4 = (BufferBase_Sub2) this.aClass2_1712.method152(27531); var4 != null; var4 = (BufferBase_Sub2) this.aClass2_1712.method151((byte) -104)) {
            var4.method166(var1, var2, var3);
        }

    }

}
