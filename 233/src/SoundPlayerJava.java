import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer.Info;
import javax.sound.sampled.SourceDataLine;
import java.awt.*;

final class SoundPlayerJava extends SoundPlayer {

    private AudioFormat audioFormat;
    private int bufferSize;
    private byte[] audioData;
    private SourceDataLine sourceDataLine;

    static int method236(int var0, int var1) {
        --var1;
        var1 |= var1 >>> 1;
        var1 |= var1 >>> 2;
        var1 |= var1 >>> 4;
        var1 |= var1 >>> 8;
        if (var0 != -1) {
            return 55;
        } else {
            var1 |= var1 >>> 16;
            return var1 - -1;
        }
    }


    final void init(Component comp) {
        Info[] mixerInfo = AudioSystem.getMixerInfo();
        if (mixerInfo != null) {
            for (int i = 0; mixerInfo.length > i; ++i) {
                Info info = mixerInfo[i];
                if (info != null) {
                    String var6 = info.getName();
                    if (var6 != null && 0 > var6.toLowerCase().indexOf("soundmax")) {
                        ;
                    }
                }
            }
        }

        this.audioFormat = new AudioFormat((float) sampleRate, 16, !stereo ? 1 : 2, true, false);
        this.audioData = new byte[256 << (!stereo ? 1 : 2)];
    }

    final void close() {
        if (null != this.sourceDataLine) {
            this.sourceDataLine.close();
            this.sourceDataLine = null;
        }

    }

    final int getPosition() {
        return this.bufferSize - (this.sourceDataLine.available() >> (!stereo ? 1 : 2));
    }

    final void flush() {
        int var1 = 256;
        if (stereo) {
            var1 <<= 1;
        }

        for (int i = 0; i < var1; ++i) {
            int var3 = this.anIntArray767[i];
            if ((var3 + 0x800000 & -0x1000000) != 0) {
                var3 = 0x7FFFFF ^ var3 >> 31;
            }

            this.audioData[i * 2] = (byte) (var3 >> 8);
            this.audioData[i * 2 + 1] = (byte) (var3 >> 16);
        }

        this.sourceDataLine.write(this.audioData, 0, var1 << 1);
    }

    final void start(int bufferSize) throws LineUnavailableException {
        try {
            javax.sound.sampled.DataLine.Info info = new javax.sound.sampled.DataLine.Info(SourceDataLine.class, this.audioFormat, bufferSize << (stereo ? 2 : 1));
            this.sourceDataLine = (SourceDataLine) AudioSystem.getLine(info);
            this.sourceDataLine.open();
            this.sourceDataLine.start();
            this.bufferSize = bufferSize;
        } catch (LineUnavailableException lue) {
            if (Scene.method311(bufferSize, true) == 1) {
                this.sourceDataLine = null;
                throw lue;
            } else {
                this.start(method236(-1, bufferSize));
            }
        }
    }

}
