import java.awt.*;

interface DirectSound {

    void method1(Component var1, int var2, int var3, boolean var4) throws Exception;

    void method2(int var1, int[] var2);

    void method3(byte var1, int var2);

    void method4(int var1, int var2, int var3) throws Exception;

    int method5(int var1, int var2);
}
