final class Character {

    static long aLong503;
    int damageTaken = 0;
    String accountName;
    int healthMax = 0;
    int stepCount;
    int colourTop;
    int messageTimeout = 0;
    int projectileRange = 0;
    int movingStep;
    int[] equippedItem = new int[12];
    int waypointCurrent;
    int animationCurrent;
    int skullVisible = 0;
    int incomingProjectileSprite = 0;
    int bubbleTimeout = 0;
    int colourBottom;
    String displayName;
    int attackingPlayerServerIndex = 0;
    int serverIndex;
    String message;
    int combatTimeout = 0;
    int currentY;
    int colourHair;
    int level = -1;
    int npcId;
    int attackingNpcServerIndex = 0;
    int[] waypointsY = new int[10];
    int colourSkin;
    int animationNext;
    int currentX;
    int bubbleItem;
    int healthCurrent = 0;
    int[] waypointsX = new int[10];


    static String method363(int var0, int var1) {
        String var2 = String.valueOf(var0);

        for (int var3 = var2.length() - 3; var3 > 0; var3 -= 3) {
            var2 = var2.substring(0, var3) + "," + var2.substring(var3);
        }

        if (var1 != 0) {
            SoundPlayer.aSoundWhat_490 = null;
        }

        if (var2.length() <= 8) {
            if (var2.length() > 4) {
                var2 = "@cya@" + var2.substring(0, -4 + var2.length()) + "K @whi@(" + var2 + ")";
            }
        } else {
            var2 = "@gre@" + var2.substring(0, -8 + var2.length()) + " million @whi@(" + var2 + ")";
        }

        return var2;
    }

}
