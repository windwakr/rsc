import java.applet.Applet;
import java.applet.AppletContext;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.IndexColorModel;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

public class GameApplet extends Applet implements Runnable, MouseListener, MouseMotionListener, KeyListener {

    static GameFrame gameFrameReference = null;
    static CacheFileManager aCacheFileManager_1042 = null;
    static CacheFileManager aCacheFileManager_742 = null;
    static URL appletCodeBase = null;
    static String loadingProgressText2 = "";
    static String aString197 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ";
    static CachePackageManager cachePackageManager;
    static int anInt131 = 0;
    static int anInt28 = 0;
    String dataDir = "./data233/";
    boolean appletMode = true;
    boolean aBoolean6 = false;
    int anInt7;
    String loadingProgressText = "Loading";
    int lastMouseAction = 0;
    long[] timings = new long[10];
    Thread appletThread = null;
    Font aFont51 = new Font("TimesRoman", 0, 15);
    int anInt58;
    String aString71 = null;
    Font aFont76 = new Font("Helvetica", 1, 13);
    Font aFont77 = new Font("Helvetica", 0, 12);
    boolean keyLeft = false;
    boolean shift_down = false;
    String inputTextFinal = "";
    String inputTextCurrent = "";
    boolean interlace = false;
    int mouseX = 0;
    int anInt84 = 0;
    int threadSleep = 1;
    int lastMouseButtonDown = 0;
    Image anImage88;
    String aString89 = "";
    boolean keyRight = false;
    String aString91 = "";
    int mouseY = 0;
    boolean ctrl_down = false;
    Graphics aGraphics94;
    private int loadingStep = 1;
    private int stopTimeout = 0;
    private int loadingProgressPercent = 0;
    private int anInt22 = 1000;
    private int anInt31 = 20;
    private int anInt32 = 512;
    private int anInt44 = 0;
    private boolean aBoolean55 = false;
    private int anInt72 = 384;
    private boolean aBoolean87 = false;
    /*public static int anInt95;
    public static boolean aBoolean96;
    public static boolean aBoolean97;
    public static boolean aBoolean98;
    public static int anInt99;
    public static boolean aBoolean100;*/

    static void sleep(long var0, int var2) {
        if (var0 > 0) {

            if (var0 % 10L == 0L) {
                Utility.sleep(-1L + var0);
                Utility.sleep(1L);
            } else {
                Utility.sleep(var0);
            }
        }
    }

    static void showLoadingProgress(String progressPrepend, int var2) {
        CacheFileManager.anGameApplet__299.showLoadingProgress(anInt131, progressPrepend + loadingProgressText2 + " - " + var2 + "%");
    }

    static byte[] method149(byte[] var0, boolean showProgress, int var2) {
        int var3 = (var2 & var0[2]) + (var0[1] << 8 & '\uff00') + (var0[0] << 16 & 16711680);
        int var4 = (255 & var0[5]) + (((var0[3] & 255) << 16) - -((255 & var0[4]) << 8));
        byte[] var5;
        if (var3 == var4) {
            var5 = new byte[-6 + var0.length];
            Utility.insertBytes(var0, 6, var5, 0, var5.length);
            return var5;
        } else {
            if (showProgress) {
                showLoadingProgress("Unpacking ", 0);
            }

            var5 = new byte[var3];
            BZLib.decompress(var5, var3, var0, var4, 6);
            return var5;
        }
    }

    static byte[] method555(int var0, String var1, int var2, GameApplet gameApplet) throws IOException {
        if (GameFrame.aByteArrayArray105[var2] != null) {
            return GameFrame.aByteArrayArray105[var2];
        } else {
            anInt131 = var0;
            loadingProgressText2 = var1;
            if (Cache.aCache_476 != null) {
                byte[] var4 = Cache.aCache_476.method351(var2, 14625);
                if (var4 != null && GameModel.method599(var4, var4.length) == Menu.anIntArray708[var2]) {
                    GameFrame.aByteArrayArray105[var2] = method149(var4, true, 255);
                    return GameFrame.aByteArrayArray105[var2];
                }
            }

            int var6 = 0;
            byte[] var5 = null;
            if (gameApplet.appletMode) {
                URL var11 = new URL(Menu.anURL707, "content" + var2 + "_" + Long.toHexString((long) Menu.anIntArray708[var2]));
                for (; var6 < 3; ++var6) {
                    try {
                        var5 = gameApplet.grabFile(0, var11, true);
                    } catch (IOException var9) {
                        if (var6 == 2) {
                            throw var9;
                        }
                    }
                }
            } else {
                try {
                    var5 = Files.readAllBytes(Paths.get(gameApplet.dataDir, "content" + var2 + "_" + Long.toHexString(Menu.anIntArray708[var2])));
                } catch (IOException var9) {
                    throw var9;
                }
            }

            if (GameModel.method599(var5, var5.length) == Menu.anIntArray708[var2]) {
                if (Cache.aCache_476 != null) {
                    Cache.aCache_476.method348(var2, var5.length, 998955376, var5);
                }

                GameFrame.aByteArrayArray105[var2] = method149(var5, true, 255);
                return GameFrame.aByteArrayArray105[var2];
            }

            if (var5 == null) {
                throw new IOException("Couldn\'t download file #" + var2 + ": crc=" + Menu.anIntArray708[var2]);
            } else {
                StringBuffer var7 = new StringBuffer("Couldn\'t download file #" + var2 + ": crc=" + Menu.anIntArray708[var2]);
                var7.append(" len=" + var5.length);

                for (int var8 = 0; var8 < var5.length && var8 < 5; ++var8) {
                    var7.append(" " + var5[var8]);
                }

                throw new IOException(var7.toString());
            }
        }
    }

    public static void provideLoaderApplet(Applet var0) {
        Isaac.anApplet445 = var0;
    }

    static void method14(int var0, int var1, int[] var2, int var3, int var4, int[] var5, int var6, int var7) {
        if (var0 < 0) {
            var7 <<= var1;
            var3 = var2[255 & var4 >> 8];
            var4 += var7;
            int var8 = var0 / 8;

            for (int var9 = var8; var9 < 0; ++var9) {
                var5[var6++] = var3;
                var5[var6++] = var3;
                var3 = var2[var4 >> 8 & 255];
                var4 += var7;
                var5[var6++] = var3;
                var5[var6++] = var3;
                var3 = var2[255 & var4 >> 8];
                var5[var6++] = var3;
                var4 += var7;
                var5[var6++] = var3;
                var3 = var2[var4 >> 8 & 255];
                var4 += var7;
                var5[var6++] = var3;
                var5[var6++] = var3;
                var3 = var2[255 & var4 >> 8];
                var4 += var7;
            }

            var8 = -(var0 % 8);

            for (int var10 = 0; var10 < var8; ++var10) {
                var5[var6++] = var3;
                if ((1 & var10) == 1) {
                    var3 = var2[var4 >> 8 & 255];
                    var4 += var7;
                }
            }

        }
    }

    private static Image createImage(Component var0, int var1, byte[] var2) {
        ClientStream.anInt1697 = var2[14] + 256 * var2[15];
        ClientStream.anInt1698 = var2[12] + 256 * var2[13];
        byte[] var3 = new byte[256];
        byte[] var4 = new byte[256];
        byte[] var5 = new byte[256];

        for (int var6 = 0; var6 < 256; ++var6) {
            var3[var6] = var2[20 + var6 * 3];
            var4[var6] = var2[19 + var6 * 3];
            var5[var6] = var2[3 * var6 + 18];
        }

        Scene.aColorModel384 = new IndexColorModel(8, 256, var3, var4, var5);
        byte[] var7 = new byte[ClientStream.anInt1697 * ClientStream.anInt1698];
        int var8 = 0;
        if (var1 != 198) {
            method14(-66, -98, (int[]) null, 34, 59, (int[]) null, 99, 104);
        }

        for (int var9 = ClientStream.anInt1697 + -1; var9 >= 0; --var9) {
            for (int var10 = 0; ClientStream.anInt1698 > var10; ++var10) {
                var7[var8++] = var2[ClientStream.anInt1698 * var9 + var10 + 786];
            }
        }

        ImagePrubser imagePrubser = new ImagePrubser();
        Image var12 = var0.createImage(imagePrubser);
        ImagePrubser.method364(var7, 8);
        var0.prepareImage(var12, imagePrubser);
        ImagePrubser.method364(var7, 31);
        var0.prepareImage(var12, imagePrubser);
        ImagePrubser.method364(var7, 53);
        var0.prepareImage(var12, imagePrubser);
        return var12;
    }

    final void grabContentCrcs(URL var0, GameApplet var2) throws IOException {
        CacheFileManager.anGameApplet__299 = var2;
        Menu.anURL707 = var0;
        loadingProgressText2 = "Checking for new content";
        byte[] bytesCrc = null;
        if (var2.appletMode) {
            URL urlCrc = new URL(Menu.anURL707, "contentcrcs" + Long.toHexString(Utility.method347(85)));

            bytesCrc = grabFile(0, urlCrc, true);
        } else {
            bytesCrc = Files.readAllBytes(Paths.get(var2.dataDir, "contentcrcs"));
        }
        BufferBase_Sub3 buf = new BufferBase_Sub3(bytesCrc);

        for (int var6 = 0; var6 < 12; ++var6) {
            Menu.anIntArray708[var6] = buf.getUnsignedInt();
        }

        buf.getUnsignedInt();
        if (!buf.value_same(0)) {
            throw new IOException("Invalid CRC in CRC check file");
        } else {
            try {
                if (cachePackageManager.cacheMainDat2 != null) {
                    aCacheFileManager_1042 = new CacheFileManager(cachePackageManager.cacheMainDat2, 5200, 0);
                    aCacheFileManager_742 = new CacheFileManager(cachePackageManager.cacheMainIdx255, 6000, 0);
                    Cache.aCache_476 = new Cache(0, aCacheFileManager_1042, aCacheFileManager_742, 1000000);
                    cachePackageManager.cacheMainDat2 = null;
                    cachePackageManager.cacheMainIdx255 = null;
                }
            } catch (IOException var7) {
                aCacheFileManager_1042 = null;
                aCacheFileManager_742 = null;
            }
        }
    }

    final byte[] grabFile(int var0, URL var1, boolean var2) throws IOException {
        JagGrab var3 = new JagGrab(cachePackageManager, var1, 2000000);
        if (var2) {
            showLoadingProgress("", 0);
        }

        while (!var3.method605(var0 ^ -97)) {
            sleep(50L, -17239);
        }

        BufferBase_Sub3 var4 = var3.method604(var0 + 48);
        if (var4 == null) {
            throw new IOException("Couldn\'t download file");
        } else {
            if (var2) {
                showLoadingProgress("", 100);
            }

            return var0 != 0 ? null : var4.method216((byte) -87);
        }
    }

    public final Image createImage(int var1, int var2) {
        return gameFrameReference != null ? gameFrameReference.createImage(var1, var2) : (Isaac.anApplet445 != null ? Isaac.anApplet445.createImage(var1, var2) : super.createImage(var1, var2));
    }

    public final void stop() {
        if (this.stopTimeout >= 0) {
            this.stopTimeout = 4000 / this.anInt31;
        }
    }

    private Image method8(byte var1, byte[] var2) {
        if (var1 > -113) {
            this.registerInputModifiers((byte) 61, (InputEvent) null);
        }

        return createImage(this, 198, var2);
    }

    final void method9(int var1, int var2, int var3, Graphics var4, Font var5, String var6) {
        if (var3 == 5061) {
            Object var7;
            if (gameFrameReference != null) {
                var7 = gameFrameReference;
            } else {
                var7 = this;
            }

            FontMetrics var8 = ((Component) var7).getFontMetrics(var5);
            var8.stringWidth(var6);
            var4.setFont(var5);
            var4.drawString(var6, var1 - var8.stringWidth(var6) / 2, var2 - -(var8.getHeight() / 4));
        }
    }

    private boolean method11(byte var1) {
        if (var1 < 41) {
            this.handleInputs(-52);
        }

        return gameFrameReference != null ? gameFrameReference.getPeer() != null : (Isaac.anApplet445 != null ? Isaac.anApplet445.getPeer() != null : super.getPeer() != null);
    }

    private boolean loadJagex(int var1) {
        byte[] var2 = this.readDataFile(3, 0, "Jagex library", -10);
        if (var2 == null) {
            return false;
        } else {
            byte[] var3 = Utility.loadData(var2, 0, "logo.tga");
            this.anImage88 = this.method8((byte) -114, var3);
            if (var1 >= -28) {
                this.keyRight = true;
            }
            // todo proper fonts
            if (!Utility.createFont("h11p", 0, this)) {
                return false;
            }
            if (!Utility.createFont("h12b", 1, this)) {
                return false;
            }
            if (!Utility.createFont("h12p", 2, this)) {
                return false;
            }
            if (!Utility.createFont("h13b", 3, this)) {
                return false;
            }
            if (!Utility.createFont("h14b", 4, this)) {
                return false;
            }
            if (!Utility.createFont("h16b", 5, this)) {
                return false;
            }
            if (!Utility.createFont("h20b", 6, this)) {
                return false;
            }
            if (!Utility.createFont("h24b", 7, this)) {
                return false;
            }
            return true;
        }
    }

    public final void mouseEntered(MouseEvent var1) {
        this.registerInputModifiers((byte) -17, var1);
    }

    void startThread(boolean var1, Runnable var2) {
        if (var1) {
            this.anInt7 = 44;
        }

        Thread thread = new Thread(var2);
        thread.setDaemon(true);
        thread.start();
    }

    public final synchronized void mouseMoved(MouseEvent var1) {
        this.registerInputModifiers((byte) -30, var1);
        this.mouseX = var1.getX() - this.anInt58;
        this.mouseY = var1.getY() - this.anInt7;
        this.lastMouseAction = 0;
        this.anInt84 = 0;
    }

    public final synchronized void mousePressed(MouseEvent var1) {
        this.registerInputModifiers((byte) 124, var1);
        this.mouseX = var1.getX() - this.anInt58;
        this.mouseY = var1.getY() + -this.anInt7;
        if (var1.isMetaDown()) {
            this.anInt84 = 2;
        } else {
            this.anInt84 = 1;
        }

        this.lastMouseButtonDown = this.anInt84;
        this.lastMouseAction = 0;
        this.method27(this.mouseY, 8191, this.anInt84, this.mouseX);
    }

    public final AppletContext getAppletContext() {
        return gameFrameReference != null ? null : (Isaac.anApplet445 != null ? Isaac.anApplet445.getAppletContext() : super.getAppletContext());
    }

    final byte[] readDataFile(int var1, int var2, String var3, int var4) {
        try {
            return var4 != -10 ? null : method555(var2, var3, var1, this);
        } catch (IOException var6) {
            Utility.sendClientError("Unable to load content pack " + var1, var4 + -247, var6);
            return null;
        }
    }

    public final Graphics getGraphics() {
        return gameFrameReference != null ? gameFrameReference.getGraphics() : (Isaac.anApplet445 != null ? Isaac.anApplet445.getGraphics() : super.getGraphics());
    }

    final void startApplication(int var1, String var2, int var3, boolean var4, int var5, int modeId, int var7, String gameName, int var9) {
        try {
            System.out.println("Started application");
            this.anInt32 = var3;
            this.anInt72 = var1;
            gameFrameReference = new GameFrame(this, 800, 600, var2, var4, false);

            try {
                gameFrameReference.getClass().getMethod("setFocusTraversalKeysEnabled", new Class[]{Boolean.TYPE}).invoke(gameFrameReference, new Object[]{Boolean.FALSE});
            } catch (Exception var12) {
                ;
            }

            ClientStream.anInt1693 = var9;
            this.loadingStep = 1;
            Class11.aCachePackageManager_205 = cachePackageManager = new CachePackageManager(modeId, gameName, var7, true);

            try {
                grabContentCrcs(new URL("http", "127.0.0.1", var5, ""), this);
            } catch (IOException var11) {
                Utility.sendClientError((String) null, var7 + -257, var11);
            }

            this.appletThread = new Thread(this);
            this.appletThread.start();
            this.appletThread.setPriority(1);
        } catch (Exception var13) {
            Utility.sendClientError((String) null, -257, var13);
        }
    }

    void method17(byte var1) {
        if (var1 <= 18) {
            this.anInt32 = 16;
        }
    }

    public final URL getDocumentBase() {
        return gameFrameReference != null ? null : (Isaac.anApplet445 != null ? Isaac.anApplet445.getDocumentBase() : super.getDocumentBase());
    }

    public final void keyTyped(KeyEvent var1) {
        this.registerInputModifiers((byte) 107, var1);
    }

    public final URL getCodeBase() {
        return gameFrameReference != null ? null : (Isaac.anApplet445 != null ? Isaac.anApplet445.getCodeBase() : super.getCodeBase());
    }

    void startGame(byte var1) {
        int var2 = -99 / ((-19 - var1) / 43);
    }

    public final synchronized void mouseDragged(MouseEvent var1) {
        this.registerInputModifiers((byte) 119, var1);
        this.mouseX = var1.getX() + -this.anInt58;
        this.mouseY = var1.getY() - this.anInt7;
        if (!var1.isMetaDown()) {
            this.anInt84 = 1;
        } else {
            this.anInt84 = 2;
        }
    }

    public final void update(Graphics var1) {
        this.paint(var1);
    }

    synchronized void draw(int var1) {
    }

    public final void paint(Graphics var1) {
        this.aBoolean6 = true;
        if (this.loadingStep == 2 && this.anImage88 != null) {
            this.drawLoadingScreen(8, this.loadingProgressPercent, this.loadingProgressText);
        } else if (this.loadingStep == 0) {
            this.method32(112);
        }
    }

    public final void run() {
        try {
            if (this.loadingStep == 1) {
                for (this.loadingStep = 2; !this.method11((byte) 108) && this.stopTimeout >= 0; sleep((long) this.anInt31, -17239)) {
                    if (this.stopTimeout > 0) {
                        --this.stopTimeout;
                        if (this.stopTimeout == 0) {
                            this.closeProgram((byte) 25);
                            this.appletThread = null;
                            return;
                        }
                    }
                }

                if (this.stopTimeout < 0) {
                    if (this.stopTimeout == -1) {
                        this.closeProgram((byte) 111);
                    }

                    this.appletThread = null;
                    return;
                }

                if (!this.loadJagex(-50)) {
                    if (this.stopTimeout != -2) {
                        this.closeProgram((byte) 106);
                    }

                    this.appletThread = null;
                    return;
                }

                this.startGame((byte) -91);
                this.loadingStep = 0;
            }

            if (gameFrameReference != null) {
                gameFrameReference.addMouseListener(this);
                gameFrameReference.addMouseMotionListener(this);
                gameFrameReference.addKeyListener(this);
            } else if (Isaac.anApplet445 == null) {
                this.addMouseListener(this);
                this.addMouseMotionListener(this);
                this.addKeyListener(this);
            } else {
                Isaac.anApplet445.addMouseListener(this);
                Isaac.anApplet445.addMouseMotionListener(this);
                Isaac.anApplet445.addKeyListener(this);
            }

            int var1 = 0;
            int var2 = 256;
            int sleep = 1;
            int var4 = 0;

            for (int var5 = 0; var5 < 10; ++var5) {
                this.timings[var5] = Utility.method347(96);
            }

            long var6 = Utility.method347(-39);

            while (this.stopTimeout >= 0) {
                if (this.stopTimeout > 0) {
                    --this.stopTimeout;
                    if (this.stopTimeout == 0) {
                        this.closeProgram((byte) 35);
                        this.appletThread = null;
                        return;
                    }
                }

                int var8 = var2;
                var2 = 300;
                int var9 = sleep;
                sleep = 1;
                var6 = Utility.method347(-62);
                if (this.timings[var1] != 0L) {
                    if (this.timings[var1] < var6) {
                        var2 = (int) ((long) (this.anInt31 * 2560) / (-this.timings[var1] + var6));
                    }
                } else {
                    sleep = var9;
                    var2 = var8;
                }

                if (var2 < 25) {
                    var2 = 25;
                }

                if (var2 > 256) {
                    sleep = (int) ((long) this.anInt31 - (var6 + -this.timings[var1]) / 10L);
                    var2 = 256;
                    if (this.threadSleep > sleep) {
                        sleep = this.threadSleep;
                    }
                }

                sleep((long) sleep, -17239);
                this.timings[var1] = var6;
                int var10;
                if (sleep > 1) {
                    for (var10 = 0; var10 < 10; ++var10) {
                        if (this.timings[var10] != 0) {
                            this.timings[var10] += (long) sleep;
                        }
                    }
                }

                var1 = (var1 + 1) % 10;
                var10 = 0;

                while (true) {
                    if (var4 < 256) {
                        this.handleInputs(-126);
                        ++var10;
                        var4 += var2;
                        if (this.anInt22 >= var10) {
                            continue;
                        }

                        var4 = 0;
                        this.anInt44 += 6;
                        if (this.anInt44 > 25) {
                            this.anInt44 = 0;
                            this.interlace = true;
                        }
                    }

                    --this.anInt44;
                    var4 &= 255;
                    this.draw(16586);
                    break;
                }
            }

            if (this.stopTimeout == -1) {
                this.closeProgram((byte) 113);
            }

            this.appletThread = null;
        } catch (Exception var12) {
            Utility.sendClientError((String) null, -257, var12);
            this.showGameErrorPage("crash", -56);
        }
    }

    void handleKeyPress(byte var1, int var2) {
        int var3 = 118 % ((var1 - -68) / 58);
    }

    final boolean drawLoading(int var1) {
        Graphics var2 = this.getGraphics();
        if (var2 == null) {
            return false;
        } else {
            this.aGraphics94 = var2.create();
            this.aGraphics94.translate(this.anInt58, this.anInt7);
            this.aGraphics94.setColor(Color.black);
            this.aGraphics94.fillRect(0, 0, this.anInt32, this.anInt72);
            this.drawLoadingScreen(8, var1, "Loading...");
            return true;
        }
    }

    private void drawLoadingScreen(int var1, int var2, String var3) {
        try {
            int var4 = (-281 + this.anInt32) / 2;
            int var5 = (-148 + this.anInt72) / 2;
            this.aGraphics94.setColor(Color.black);
            this.aGraphics94.fillRect(0, 0, this.anInt32, this.anInt72);
            if (!this.aBoolean55) {
                this.aGraphics94.drawImage(this.anImage88, var4, var5, this);
            }

            this.loadingProgressPercent = var2;
            var5 += 90;
            var4 += 2;
            this.loadingProgressText = var3;
            this.aGraphics94.setColor(new Color(132, 132, 132));
            if (this.aBoolean55) {
                this.aGraphics94.setColor(new Color(220, 0, 0));
            }

            this.aGraphics94.drawRect(-2 + var4, -2 + var5, 280, 23);
            this.aGraphics94.fillRect(var4, var5, var2 * 277 / 100, 20);
            this.aGraphics94.setColor(new Color(198, 198, 198));
            if (this.aBoolean55) {
                this.aGraphics94.setColor(new Color(255, 255, 255));
            }

            this.method9(138 + var4, var5 - -10, 5061, this.aGraphics94, this.aFont51, var3);
            if (this.aBoolean55) {
                this.aGraphics94.setColor(new Color(132, 132, 152));
                this.method9(138 + var4, this.anInt72 - 20, 5061, this.aGraphics94, this.aFont77, "© 2001-2009 Jagex Ltd");
            } else {
                this.method9(138 + var4, 30 + var5, 5061, this.aGraphics94, this.aFont76, "Created by JAGeX - visit www.jagex.com");
                this.method9(var4 - -138, var5 - -44, 5061, this.aGraphics94, this.aFont76, "© 2001-2009 Jagex Ltd");
            }

            if (this.aString71 != null) {
                this.aGraphics94.setColor(Color.white);
                this.method9(138 + var4, var5 - 120, 5061, this.aGraphics94, this.aFont76, this.aString71);
            }
        } catch (Exception var6) {
            ;
        }

        if (var1 != 8) {
            this.closeProgram((byte) 101);
        }
    }

    final void startApplet(int var1, int var2, int var3, int var4, int var5) {
        try {
            System.out.println("Started applet");
            this.loadingStep = 1;
            this.anInt72 = var2;
            this.anInt32 = var5;
            appletCodeBase = this.getCodeBase();
            ClientStream.anInt1693 = var4;
            if (cachePackageManager == null) {
                Class11.aCachePackageManager_205 = cachePackageManager = new CachePackageManager(var1, (String) null, 0, Isaac.anApplet445 != null);
            }

            if (Isaac.anApplet445 != null) {
                Method var6 = CachePackageManager.methodFocusCycle;
                if (var6 != null) {
                    try {
                        var6.invoke(Isaac.anApplet445, new Object[]{Boolean.TRUE});
                    } catch (Throwable var10) {
                        ;
                    }
                }

                Method var7 = CachePackageManager.methodFocusTraversal;
                if (var7 != null) {
                    try {
                        var7.invoke(Isaac.anApplet445, new Object[]{Boolean.FALSE});
                    } catch (Throwable var9) {
                        ;
                    }
                }
            }

            try {
                grabContentCrcs(this.getCodeBase(), this);
                if (var3 != 23527) {
                    this.anInt58 = -84;
                }
            } catch (IOException var8) {
                var8.printStackTrace();
            }

            this.startThread(false, this);
        } catch (Exception var11) {
            Utility.sendClientError((String) null, var3 ^ -23272, var11);
            this.showGameErrorPage("crash", -117);
        }
    }

    final void resetTimings(int var1) {
        if (var1 == -10) {

            for (int var2 = 0; var2 < 10; ++var2) {
                this.timings[var2] = 0L;
            }

        }
    }

    public final String getParameter(String var1) {
        return gameFrameReference != null ? null : (Isaac.anApplet445 != null ? Isaac.anApplet445.getParameter(var1) : super.getParameter(var1));
    }

    private void showGameErrorPage(String var1, int var2) {
        if (!this.aBoolean87) {
            if (var2 >= -36) {
                this.getGraphics();
            }

            this.aBoolean87 = true;
            System.out.println("error_game_" + var1);

            try {
                if (Isaac.anApplet445 == null) {
                    //Class35.method498("loggedout", (byte)89, this);
                } else {
                    //Class35.method498("loggedout", (byte)89, Isaac.anApplet445);
                }
            } catch (Throwable var5) {
                ;
            }

            try {
                this.getAppletContext().showDocument(new URL(this.getCodeBase(), "error_game_" + var1 + ".ws"), "_top");
            } catch (Exception var4) {
                ;
            }
        }
    }

    void method27(int var1, int var2, int var3, int var4) {
        if (var2 != 8191) {
            this.loadJagex(99);
        }
    }

    private void registerInputModifiers(byte var1, InputEvent var2) {
        //int var3 = -83 / ((71 - var1) / 36);
        int var4 = var2.getModifiers();
        this.shift_down = (var4 & 1) != 0;
        this.ctrl_down = (var4 & 2) != 0;
    }

    public final void mouseExited(MouseEvent var1) {
        this.registerInputModifiers((byte) 115, var1);
    }

    synchronized void handleInputs(int var1) {
        if (var1 <= -30) {
        }
    }

    final void showLoadingProgress(int percent, String text) {
        try {
            int var4 = (this.anInt32 + -281) / 2;
            int var5 = (this.anInt72 + -148) / 2;
            var4 += 2;
            this.loadingProgressText = text;
            this.loadingProgressPercent = percent;
            var5 += 90;
            int var6 = percent * 277 / 100;
            this.aGraphics94.setColor(new Color(132, 132, 132));
            if (this.aBoolean55) {
                this.aGraphics94.setColor(new Color(220, 0, 0));
            }

            this.aGraphics94.fillRect(var4, var5, var6, 20);
            this.aGraphics94.setColor(Color.black);
            this.aGraphics94.fillRect(var6 + var4, var5, -var6 + 277, 20);
            this.aGraphics94.setColor(new Color(198, 198, 198));
            if (this.aBoolean55) {
                this.aGraphics94.setColor(new Color(255, 255, 255));
            }

            this.method9(138 + var4, 10 + var5, 5061, this.aGraphics94, this.aFont51, text);
        } catch (Exception var7) {
            ;
        }
    }

    public final synchronized void keyReleased(KeyEvent var1) {
        this.registerInputModifiers((byte) -57, var1);
        char var2 = var1.getKeyChar();
        int var3 = var1.getKeyCode();
        if (var3 == 37) {
            this.keyLeft = false;
        }

        if (var3 == 39) {
            this.keyRight = false;
        }

        boolean var10000;
        if ((char) var2 != 110 && ((char) var2) != 109) {
            var10000 = false;
        } else {
            var10000 = true;
        }

        if ((char) var2 != 78 && ((char) var2) != 77) {
            var10000 = false;
        } else {
            var10000 = true;
        }
        ;
    }

    final void setFPS(byte var1, int var2) {
        this.anInt31 = 1000 / var2;
        int var3 = 104 / ((92 - var1) / 34);
    }

    public final Dimension getSize() {
        return gameFrameReference != null ? gameFrameReference.getSize() : (Isaac.anApplet445 != null ? Isaac.anApplet445.getSize() : super.getSize());
    }

    public final void destroy() {
        this.stopTimeout = -1;
        sleep(5000L, -17239);
        if (this.stopTimeout == -1) {
            System.out.println("5 seconds expired, forcing kill");
            this.closeProgram((byte) 37);
            if (this.appletThread != null) {
                this.appletThread.stop();
                this.appletThread = null;
                return;
            }
        }
    }

    public final void start() {
        if (this.stopTimeout >= 0) {
            this.stopTimeout = 0;
        }
    }

    public final synchronized void keyPressed(KeyEvent var1) {
        this.registerInputModifiers((byte) -105, var1);
        char var2 = var1.getKeyChar();
        int var3 = var1.getKeyCode();
        this.handleKeyPress((byte) 96, var2);
        this.lastMouseAction = 0;
        if (var3 == 112) {
            this.interlace = !this.interlace;
        }

        if (var3 == 39) {
            this.keyRight = true;
        }

        boolean var10000;
        if ((char) var2 != 110 && ((char) var2) != 109) {
            var10000 = false;
        } else {
            var10000 = true;
        }

        if (var3 == 37) {
            this.keyLeft = true;
        }

        if ((char) var2 != 78 && (char) var2 != 77) {
            var10000 = false;
        } else {
            var10000 = true;
        }

        boolean var4 = false;

        for (int var5 = 0; var5 < aString197.length(); ++var5) {
            if (var2 == aString197.charAt(var5)) {
                var4 = true;
                break;
            }
        }

        if (var4 && this.inputTextCurrent.length() < 20) {
            this.inputTextCurrent = this.inputTextCurrent + (char) var2;
        }

        if (var4 && this.aString91.length() < 80) {
            this.aString91 = this.aString91 + (char) var2;
        }

        if (var2 == 8 && this.inputTextCurrent.length() > 0) {
            this.inputTextCurrent = this.inputTextCurrent.substring(0, this.inputTextCurrent.length() + -1);
        }

        if (var2 == 8 && this.aString91.length() > 0) {
            this.aString91 = this.aString91.substring(0, this.aString91.length() + -1);
        }

        if (var2 == 10 || var2 == 13) {
            this.inputTextFinal = this.inputTextCurrent;
            this.aString89 = this.aString91;
        }
        ;
    }

    public final void mouseClicked(MouseEvent var1) {
        this.registerInputModifiers((byte) 109, var1);
    }

    public final synchronized void mouseReleased(MouseEvent var1) {
        this.registerInputModifiers((byte) -12, var1);
        this.mouseX = var1.getX() - this.anInt58;
        this.mouseY = var1.getY() - this.anInt7;
        this.anInt84 = 0;
    }

    final void method32(int var1) {
        if (var1 <= 109) {
            this.lastMouseAction = -61;
        }
    }

    private void closeProgram(byte var1) {
        this.stopTimeout = -2;
        System.out.println("Closing program");
        this.method17((byte) 117);
        sleep(1000L, -17239);
        if (gameFrameReference != null) {
            gameFrameReference.dispose();
            System.exit(0);
        }

        if (var1 <= 3) {
            this.aFont51 = null;
        }
    }

}
