final class GameModeWhat {

    static GameModeWhat RC = new GameModeWhat("RC", 1);
    static GameModeWhat WIP = new GameModeWhat("WIP", 2);
    static GameModeWhat LIVE = new GameModeWhat("LIVE", 0);

    int id;

    GameModeWhat(String name, int id) {
        this.id = id;
    }

    static GameModeWhat[] method343(int var0) {
        if (var0 != 17062) {
            SoundPlayer.stereo = true;
        }

        return new GameModeWhat[]{LIVE, RC, WIP};
    }

    static GameModeWhat method337(int var0, boolean var1) {
        GameModeWhat[] var2 = method343(17062);

        for (int var3 = 0; var3 < var2.length; ++var3) {
            GameModeWhat var4 = var2[var3];
            if (var0 == var4.id) {
                return var4;
            }
        }

        if (!var1) {
            Utility.sendClientError((String) null, 11, (Throwable) null);
        }

        return null;
    }

    public final String toString() {
        throw new IllegalStateException();
    }

}
