import java.io.DataInputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStream;

final class Panel {

    static boolean drawBackgroundArrow = true;
    static int anInt809 = 176;
    static int textListEntryHeightMod = 0;
    private static int anInt867 = 114;
    int[] anIntArray824;
    int[] controlFlashText;
    private Surface surface;
    private int colourRoundedBoxMid;
    private int colourRoundedBoxOut;
    private int colourBoxLeftNRight;
    private String[][] aStringArrayArray839;
    private int controlCount = 0;
    private int colourScrollbarTop;
    private boolean[] aBooleanArray843;
    private int[] anIntArray847;
    private int colourScrollbarHandleMid;
    private int colourScrollbarHandleRight;
    private int mouseLastButtonDown = 0;
    private int colourBoxTopNBottom;
    private int[] controlInputMaxLen;
    private int colourRoundedBoxIn;
    private int[][] anIntArrayArray860;
    private int[] controlType;
    private int mouseY = 0;
    private int mouseButtonDown = 0;
    private int[] controlListEntryMouseOver;
    private int[] anIntArray866;
    private int colourScrollbarBottom;
    private String[][] aStringArrayArray871;
    private int colourBoxLeftNRight2;
    private String[][] aStringArrayArray876;
    private int anInt878 = 0;
    private int[] anIntArray879;
    private boolean[] controlClicked;
    private int focusControlIndex = -1;
    private int[] controlListEntryMouseButtonDown;
    private int colourScrollbarHandleLeft;
    private String[] controlText;
    private int colourBoxTopNBottom2;
    private int[] anIntArray896;
    private boolean[] controlListScrollbarHandleDragged;
    private int[] anIntArray903;
    private boolean aBoolean904 = true;
    private boolean[] controlUseAlternativeColour;
    private int mouseX = 0;
    private boolean[] controlShown;


    Panel(Surface var1, int var2) {
        this.anIntArrayArray860 = new int[var2][];
        this.controlListEntryMouseOver = new int[var2];
        this.anIntArray903 = new int[var2];
        this.controlListEntryMouseButtonDown = new int[var2];
        this.aStringArrayArray839 = new String[var2][];
        this.controlClicked = new boolean[var2];
        this.anIntArray896 = new int[var2];
        this.controlListScrollbarHandleDragged = new boolean[var2];
        this.anIntArray824 = new int[var2];
        this.surface = var1;
        this.controlType = new int[var2];
        this.anIntArray866 = new int[var2];
        this.aBooleanArray843 = new boolean[var2];
        this.aStringArrayArray871 = new String[var2][];
        this.aStringArrayArray876 = new String[var2][];
        this.anIntArray879 = new int[var2];
        this.controlInputMaxLen = new int[var2];
        this.anIntArray847 = new int[var2];
        this.controlFlashText = new int[var2];
        this.controlShown = new boolean[var2];
        this.controlText = new String[var2];
        this.controlUseAlternativeColour = new boolean[var2];
        this.colourScrollbarTop = this.rgb2longMod(176, 114, -12, 114);
        this.colourScrollbarBottom = this.rgb2longMod(62, 14, -12, 14);
        this.colourScrollbarHandleLeft = this.rgb2longMod(232, 208, -12, 200);
        this.colourScrollbarHandleMid = this.rgb2longMod(184, 129, -12, 96);
        this.colourScrollbarHandleRight = this.rgb2longMod(115, 95, -12, 53);
        this.colourRoundedBoxOut = this.rgb2longMod(171, 142, -12, 117);
        this.colourRoundedBoxMid = this.rgb2longMod(158, 122, -12, 98);
        this.colourRoundedBoxIn = this.rgb2longMod(136, 100, -12, 86);
        this.colourBoxTopNBottom = this.rgb2longMod(179, 146, -12, 135);
        this.colourBoxTopNBottom2 = this.rgb2longMod(151, 112, -12, 97);
        this.colourBoxLeftNRight2 = this.rgb2longMod(136, 102, -12, 88);
        this.colourBoxLeftNRight = this.rgb2longMod(120, 93, -12, 84);
    }

    static void method543(byte var0, String var1, int var2, byte[] var3) throws IOException {
        if (var0 < 95) {
            GameData.spellCount = 16;
        }

        InputStream var4 = Utility.method545(var1, -32341);
        DataInputStream var5 = new DataInputStream(var4);

        try {
            var5.readFully(var3, 0, var2);
        } catch (EOFException var6) {
            ;
        }

        var5.close();
    }

    private void drawListContainer(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        int var8 = -12 + var7 + var3;
        this.surface.drawBoxEdge(var2, 12, false, var8, var5, 0);
        this.surface.drawSprite((byte) 61, var2 + 1, client.baseSpriteStart, 1 + var8);
        this.surface.drawSprite((byte) 61, var5 + var2 - 12, client.baseSpriteStart + 1, 1 + var8);
        this.surface.drawLineHoriz(var8, 0, -9986, var6, 13 + var2);
        this.surface.drawLineHoriz(var8, 0, -9986, 12, var5 + var2 - 13);
        this.surface.drawGradient(1 + var8, this.colourScrollbarBottom, var2 + 14, var5 - 27, -120, this.colourScrollbarTop, 11);
        this.surface.drawBox(var2 + var4 - -14, this.colourScrollbarHandleMid, 7, true, 3 + var8, var1);
        this.surface.drawLineVert(this.colourScrollbarHandleLeft, var8 + 2, 14 + var4 + var2, -19766, var1);
        this.surface.drawLineVert(this.colourScrollbarHandleRight, var8 + 2 - -8, var2 + (var4 - -14), -19766, var1);
    }

    private void drawTextListInteractive(int var1, int var2, int var3, int var4, int var5, byte var6, int var7, int var8, int textSize, String[] var10, int[] var11) {
        int var12 = var1 / this.surface.textHeight(textSize, true);
        int var13;
        int var14;
        int var15;
        int var16;
        if (var3 <= var12) {
            var2 = 0;
            this.controlFlashText[var4] = 0;
        } else {
            var13 = var8 + var7 - 12;
            var14 = var12 * (var1 + -27) / var3;
            if (var14 < 6) {
                var14 = 6;
            }

            int var10000 = var2 * (-var14 + -27 + var1) / (-var12 + var3);
            if (this.mouseButtonDown == 1 && var13 <= this.mouseX && 12 + var13 >= this.mouseX) {
                if (var5 < this.mouseY && this.mouseY < 12 + var5 && var2 > 0) {
                    --var2;
                }

                if ((-12 + var5 + var1) < this.mouseY && this.mouseY < (var1 + var5) && var2 < -var12 + var3) {
                    ++var2;
                }

                this.controlFlashText[var4] = var2;
            }

            if (this.mouseButtonDown == 1 && (var13 <= this.mouseX && var13 - -12 >= this.mouseX || this.mouseX >= (var13 + -12) && this.mouseX <= (24 + var13) && this.controlListScrollbarHandleDragged[var4])) {
                if (this.mouseY > 12 + var5 && this.mouseY < (-12 + var1 + var5)) {
                    this.controlListScrollbarHandleDragged[var4] = true;
                    var16 = -12 + -var5 + this.mouseY - var14 / 2;
                    var2 = var16 * var3 / (var1 - 24);
                    if (var2 < 0) {
                        var2 = 0;
                    }

                    if ((var3 + -var12) < var2) {
                        var2 = -var12 + var3;
                    }

                    this.controlFlashText[var4] = var2;
                }
            } else {
                this.controlListScrollbarHandleDragged[var4] = false;
            }

            var15 = var2 * (var1 - (27 + var14)) / (-var12 + var3);
            this.drawListContainer(var14, var5, var7, var15, var1, 12, var8);
        }

        this.controlListEntryMouseOver[var4] = -1;
        var13 = var1 - this.surface.textHeight(textSize, true) * var12;
        if (var6 <= 43) {
            this.show(-68, (byte) -36);
        }

        var14 = 5 * this.surface.textHeight(textSize, true) / 6 + var5 + var13 / 2;

        for (var15 = var2; var3 > var15; ++var15) {
            if (!this.controlUseAlternativeColour[var4]) {
                var16 = 0;
            } else {
                var16 = 16777215;
            }

            if ((var7 - -2) <= this.mouseX && this.mouseX <= this.surface.textWidth(textSize, -127, var10[var15]) + (var7 - -2) && (this.mouseY + -2) <= var14 && -2 + this.mouseY > var14 - this.surface.textHeight(textSize, true)) {
                if (!this.controlUseAlternativeColour[var4]) {
                    var16 = 16777215;
                } else {
                    var16 = 8421504;
                }

                this.controlListEntryMouseOver[var4] = var15;
                if (this.mouseLastButtonDown == 1) {
                    this.controlListEntryMouseButtonDown[var4] = var15;
                    this.controlClicked[var4] = true;
                }
            }

            if (var15 == this.controlListEntryMouseButtonDown[var4] && this.aBoolean904) {
                var16 = 16711680;
            }

            this.surface.drawstring(var14, var10[var15], 2 + var7, textSize, var11[var15], -117, var16);
            var14 += this.surface.textHeight(textSize, true);
            if (var1 + var5 <= var14) {
                return;
            }
        }
        ;
    }

    final void method506(int var1, int var2) {
        if (var1 != -22757) {
            this.anIntArray903 = null;
        }

        this.controlFlashText[var2] = 0;
        this.controlListEntryMouseOver[var2] = -1;
    }

    final String method507(int var1, byte var2, int var3) {
        if (var2 != -93) {
            this.anIntArray847 = null;
        }

        return this.aStringArrayArray876[var1][var3];
    }

    final int method508(int var1, int var2, byte var3, int var4, int var5) {
        this.controlType[this.controlCount] = 11;
        if (var3 != -2) {
            this.anIntArray896 = null;
        }

        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = false;
        this.anIntArray866[this.controlCount] = -(var5 / 2) + var2;
        this.anIntArray896[this.controlCount] = var1 + -(var4 / 2);
        this.anIntArray879[this.controlCount] = var5;
        this.anIntArray903[this.controlCount] = var4;
        return this.controlCount++;
    }

    private void method509(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.surface.drawBox(var4, 16777215, var2, true, var5, var6);
        if (var3 == 14) {
            this.surface.drawLineHoriz(var5, this.colourBoxTopNBottom, -9986, var2, var4);
            this.surface.drawLineVert(this.colourBoxTopNBottom, var5, var4, -19766, var6);
            this.surface.drawLineHoriz(var5, this.colourBoxLeftNRight, -9986, var2, var4 - (-var6 - -1));
            this.surface.drawLineVert(this.colourBoxLeftNRight, var2 + (var5 - 1), var4, var3 ^ -19772, var6);
            if (this.controlListEntryMouseButtonDown[var1] == 1) {
                for (int var7 = 0; var7 < var6; ++var7) {
                    this.surface.drawLineHoriz(var5 - -var7, 0, var3 ^ -10000, 1, var7 + var4);
                    this.surface.drawLineHoriz(-1 + var2 + var5 + -var7, 0, -9986, 1, var4 - -var7);
                }

            }
        }
    }

    final void hide(int var1, int var2) {
        this.controlShown[var1] = false;
        if (var2 != -12) {
            this.method538(56, 68, -124, -40, false, 66, -119, true);
        }
    }

    final void drawPanel(int var1) {
        int var2 = 0;
        if (var1 > 41) {
            for (; this.controlCount > var2; ++var2) {
                if (this.controlShown[var2]) {
                    if (this.controlType[var2] != 0) {
                        if (this.controlType[var2] != 1) {
                            if (this.controlType[var2] != 2) {
                                if (this.controlType[var2] != 3) {
                                    if (this.controlType[var2] == 4) {
                                        this.method542(this.anIntArray866[var2], this.anIntArray896[var2], this.anIntArray903[var2], this.anIntArray879[var2], this.anIntArray847[var2], this.anIntArray824[var2], 10, this.aStringArrayArray871[var2], var2, this.anIntArrayArray860[var2], this.controlFlashText[var2]);
                                    } else if (this.controlType[var2] != 5 && this.controlType[var2] != 6) {
                                        if (this.controlType[var2] == 7) {
                                            this.method527(this.anIntArray847[var2], this.anIntArray896[var2], var2, this.anIntArray866[var2], false, this.aStringArrayArray871[var2]);
                                        } else if (this.controlType[var2] != 8) {
                                            if (this.controlType[var2] != 9) {
                                                if (this.controlType[var2] != 11) {
                                                    if (this.controlType[var2] != 12) {
                                                        if (this.controlType[var2] == 14) {
                                                            this.method509(var2, this.anIntArray879[var2], 14, this.anIntArray896[var2], this.anIntArray866[var2], this.anIntArray903[var2]);
                                                        }
                                                    } else {
                                                        this.method534(this.anIntArray847[var2], (byte) -12, this.anIntArray866[var2], this.anIntArray896[var2]);
                                                    }
                                                } else {
                                                    this.method514(this.anIntArray896[var2], this.anIntArray903[var2], (byte) 35, this.anIntArray879[var2], this.anIntArray866[var2]);
                                                }
                                            } else {
                                                this.drawTextListInteractive(this.anIntArray903[var2], this.controlFlashText[var2], this.anIntArray824[var2], var2, this.anIntArray896[var2], (byte) 120, this.anIntArray866[var2], this.anIntArray879[var2], this.anIntArray847[var2], this.aStringArrayArray871[var2], this.anIntArrayArray860[var2]);
                                            }
                                        } else {
                                            this.method536(this.anIntArray896[var2], this.anIntArray866[var2], this.anIntArray847[var2], this.aStringArrayArray871[var2], (byte) -15, var2);
                                        }
                                    } else {
                                        this.method532(this.anIntArray879[var2], this.anIntArray896[var2], var2, this.anIntArray903[var2], this.anIntArray866[var2], this.controlText[var2], (byte) 125, this.anIntArray847[var2]);
                                    }
                                } else {
                                    this.method525(this.anIntArray879[var2], (byte) 99, this.anIntArray896[var2], this.anIntArray866[var2]);
                                }
                            } else {
                                this.method522(false, this.anIntArray866[var2], this.anIntArray903[var2], this.anIntArray896[var2], this.anIntArray879[var2]);
                            }
                        } else {
                            this.method512(var2, (byte) -85, this.anIntArray866[var2] + -(this.surface.textWidth(this.anIntArray847[var2], -127, this.controlText[var2]) / 2), this.anIntArray896[var2], this.controlText[var2], 0, this.anIntArray847[var2]);
                        }
                    } else {
                        this.method512(var2, (byte) -76, this.anIntArray866[var2], this.anIntArray896[var2], this.controlText[var2], 0, this.anIntArray847[var2]);
                    }
                }
            }

            this.mouseLastButtonDown = 0;
        }
    }

    private void method512(int var1, byte var2, int var3, int var4, String var5, int var6, int var7) {
        int var8 = var4 + this.surface.textHeight(var7, true) / 3;
        if (var2 < -67) {
            this.method541(var8, var6, var3, var1, var5, var7, false);
        }
    }

    final boolean isClicked(int var1, int var2) {
        if (var1 != 3) {
            return true;
        } else {
            if (this.controlShown[var2] && this.controlClicked[var2]) {
                this.controlClicked[var2] = false;
                return true;
            } else {
                return false;
            }
        }
    }

    private void method514(int var1, int var2, byte var3, int var4, int var5) {
        if (var3 != 35) {
            this.anIntArray903 = null;
        }

        this.surface.drawBox(var1, 0, var4, true, var5, var2);
        this.surface.drawBoxEdge(var1, var4, false, var5, var2, this.colourRoundedBoxOut);
        this.surface.drawBoxEdge(var1 + 1, var4 - 2, false, 1 + var5, var2 + -2, this.colourRoundedBoxMid);
        this.surface.drawBoxEdge(var1 + 2, var4 - 4, false, 2 + var5, -4 + var2, this.colourRoundedBoxIn);
        this.surface.drawSprite((byte) 61, var1, client.baseSpriteStart + 2, var5);
        this.surface.drawSprite((byte) 61, var1, 3 - -client.baseSpriteStart, var4 + var5 + -7);
        this.surface.drawSprite((byte) 61, var2 + var1 + -7, client.baseSpriteStart + 4, var5);
        this.surface.drawSprite((byte) 61, -7 + var2 + var1, 5 + client.baseSpriteStart, -7 + var4 + var5);
    }

    final void show(int var1, byte var2) {
        if (var2 <= -57) {
            this.controlShown[var1] = true;
        }
    }

    final int addButton(int var1, int var2, int var3, int var4, int var5) {
        this.controlType[this.controlCount] = 10;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = false;
        this.anIntArray866[this.controlCount] = -(var1 / 2) + var4;
        this.anIntArray896[this.controlCount] = -(var2 / 2) + var3;
        this.anIntArray879[this.controlCount] = var1;
        this.anIntArray903[this.controlCount] = var2;
        if (var5 < 122) {
            this.method507(81, (byte) -42, 92);
        }

        return this.controlCount++;
    }

    final String method517(int var1, int var2, int var3) {
        if (var1 >= -71) {
            this.method512(-118, (byte) 21, 92, 29, (String) null, -52, 100);
        }

        return this.aStringArrayArray839[var2][var3];
    }

    final int addButtonBackground(int var1, int var2, int var3, int var4, byte var5) {
        this.controlType[this.controlCount] = 2;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = false;
        if (var5 != -8) {
            this.method507(78, (byte) 93, -111);
        }

        this.anIntArray866[this.controlCount] = var1 + -(var2 / 2);
        this.anIntArray896[this.controlCount] = -(var4 / 2) + var3;
        this.anIntArray879[this.controlCount] = var2;
        this.anIntArray903[this.controlCount] = var4;
        return this.controlCount++;
    }

    final int addText(boolean var1, boolean var2, int var3, int var4, int var5, String var6) {
        this.controlType[this.controlCount] = 1;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = var2;
        this.anIntArray847[this.controlCount] = var3;
        this.controlUseAlternativeColour[this.controlCount] = var1;
        this.anIntArray866[this.controlCount] = var5;
        this.anIntArray896[this.controlCount] = var4;
        this.controlText[this.controlCount] = var6;
        return this.controlCount++;
    }

    final void method520(String var1, int var2, String var3, boolean var4, String var5, byte var6, int var7) {
        if (var6 > -75) {
            this.method517(-102, -128, -83);
        }

        int var8 = this.anIntArray824[var2]++;
        if (this.controlInputMaxLen[var2] <= var8) {
            --var8;
            --this.anIntArray824[var2];

            for (int var9 = 0; var8 > var9; ++var9) {
                this.aStringArrayArray871[var2][var9] = this.aStringArrayArray871[var2][var9 - -1];
                this.anIntArrayArray860[var2][var9] = this.anIntArrayArray860[var2][var9 + 1];
                this.aStringArrayArray876[var2][var9] = this.aStringArrayArray876[var2][1 + var9];
                this.aStringArrayArray839[var2][var9] = this.aStringArrayArray839[var2][1 + var9];
            }
        }

        this.aStringArrayArray871[var2][var8] = var3;
        this.anIntArrayArray860[var2][var8] = var7;
        this.aStringArrayArray876[var2][var8] = var1;
        this.aStringArrayArray839[var2][var8] = var5;
        if (var4) {
            this.controlFlashText[var2] = 999999;
        }
        ;
    }

    final void setFocus(byte var1, int var2) {
        int var3 = -7 % ((21 - var1) / 48);
        this.focusControlIndex = var2;
    }

    private void method522(boolean var1, int var2, int var3, int var4, int var5) {
        this.surface.setBounds(var3 + var4, var2, 16777215, var4, var2 + var5);
        this.surface.drawGradient(var2, this.colourBoxTopNBottom, var4, var3, -126, this.colourBoxLeftNRight, var5);
        if (drawBackgroundArrow) {
            for (int var6 = var2 + -(63 & var4); var2 - -var5 > var6; var6 += 128) {
                for (int var7 = -(var4 & 31) + var4; var3 + var4 > var7; var7 += 128) {
                    this.surface.method411(var7, 6 + client.baseSpriteStart, (byte) -53, 128, var6);
                }
            }
        }

        this.surface.drawLineHoriz(var2, this.colourBoxTopNBottom, -9986, var5, var4);
        this.surface.drawLineHoriz(1 + var2, this.colourBoxTopNBottom, -9986, var5 + -2, 1 + var4);
        this.surface.drawLineHoriz(var2 - -2, this.colourBoxTopNBottom2, -9986, var5 - 4, 2 + var4);
        this.surface.drawLineVert(this.colourBoxTopNBottom, var2, var4, -19766, var3);
        this.surface.drawLineVert(this.colourBoxTopNBottom, 1 + var2, var4 - -1, -19766, var3 + -2);
        this.surface.drawLineVert(this.colourBoxTopNBottom2, 2 + var2, var4 - -2, -19766, var3 + -4);
        this.surface.drawLineHoriz(var2, this.colourBoxLeftNRight, -9986, var5, -1 + var4 + var3);
        this.surface.drawLineHoriz(1 + var2, this.colourBoxLeftNRight, -9986, var5 - 2, var3 + (var4 - 2));
        if (!var1) {
            this.surface.drawLineHoriz(2 + var2, this.colourBoxLeftNRight2, -9986, var5 - 4, var4 - -var3 + -3);
            this.surface.drawLineVert(this.colourBoxLeftNRight, var2 - -var5 - 1, var4, -19766, var3);
            this.surface.drawLineVert(this.colourBoxLeftNRight, var5 + (var2 - 2), var4 - -1, -19766, var3 - 2);
            this.surface.drawLineVert(this.colourBoxLeftNRight2, var2 + var5 + -3, var4 - -2, -19766, var3 - 4);
            this.surface.method408((byte) 10);
        }
    }

    final String method523(int var1, int var2) {
        if (var1 != -17054) {
            this.addButtonBackground(-103, -83, 101, -54, (byte) -51);
        }

        return this.controlText[var2] == null ? "null" : this.controlText[var2];
    }

    final void handleMouse(int var1, int var2, int var3, int var4, int var5) {
        if (var2 != 0) {
            this.mouseLastButtonDown = var2;
        }

        this.mouseX = var3;
        this.mouseY = var5;
        this.mouseButtonDown = var4;
        int var6;
        if (var2 == 1) {
            for (var6 = 0; var6 < this.controlCount; ++var6) {
                if (this.controlShown[var6] && this.controlType[var6] == 10 && this.anIntArray866[var6] <= this.mouseX && this.anIntArray896[var6] <= this.mouseY && (this.anIntArray879[var6] + this.anIntArray866[var6]) >= this.mouseX && this.mouseY <= this.anIntArray903[var6] + this.anIntArray896[var6]) {
                    this.controlClicked[var6] = true;
                }

                if (this.controlShown[var6] && this.controlType[var6] == 14 && this.anIntArray866[var6] <= this.mouseX && this.mouseY >= this.anIntArray896[var6] && this.mouseX <= (this.anIntArray866[var6] - -this.anIntArray879[var6]) && this.anIntArray896[var6] - -this.anIntArray903[var6] >= this.mouseY) {
                    this.controlListEntryMouseButtonDown[var6] = 1 - this.controlListEntryMouseButtonDown[var6];
                }
            }
        }

        if (var4 == 1) {
            ++this.anInt878;
        } else {
            this.anInt878 = 0;
        }

        if (var2 == 1 || this.anInt878 > 20) {
            for (var6 = 0; this.controlCount > var6; ++var6) {
                if (this.controlShown[var6] && this.controlType[var6] == 15 && this.anIntArray866[var6] <= this.mouseX && this.mouseY >= this.anIntArray896[var6] && this.anIntArray866[var6] + this.anIntArray879[var6] >= this.mouseX && this.mouseY <= this.anIntArray903[var6] + this.anIntArray896[var6]) {
                    this.controlClicked[var6] = true;
                }
            }

            this.anInt878 -= 5;
        }

        if (var1 >= -73) {
            this.colourRoundedBoxIn = -112;
        }
    }

    private void method525(int var1, byte var2, int var3, int var4) {
        int var5 = 9 / ((-74 - var2) / 51);
        this.surface.drawLineHoriz(var4, 0, -9986, var1, var3);
    }

    final int method526(int var1, int var2) {
        if (var2 != 2) {
            this.method535(72, 90, true, false, 77, -85, 52, -51, 23);
        }

        int var3 = this.controlListEntryMouseOver[var1];
        return var3;
    }

    private void method527(int var1, int var2, int var3, int var4, boolean var5, String[] var6) {
        int var7 = 0;
        int var8 = var6.length;

        for (int var9 = 0; var8 > var9; ++var9) {
            var7 += this.surface.textWidth(var1, -127, var6[var9]);
            if (-1 + var8 > var9) {
                var7 += this.surface.textWidth(var1, -127, "  ");
            }
        }

        int var10 = -(var7 / 2) + var4;
        int var11 = var2 - -(this.surface.textHeight(var1, true) / 3);
        if (var5) {
            this.method536(-33, 4, -32, (String[]) null, (byte) -32, -3);
        }

        for (int var12 = 0; var12 < var8; ++var12) {
            int var13;
            if (!this.controlUseAlternativeColour[var3]) {
                var13 = 0;
            } else {
                var13 = 16777215;
            }

            if (var10 <= this.mouseX && this.mouseX <= (var10 - -this.surface.textWidth(var1, -127, var6[var12])) && var11 >= this.mouseY && this.mouseY > -this.surface.textHeight(var1, true) + var11) {
                if (!this.controlUseAlternativeColour[var3]) {
                    var13 = 16777215;
                } else {
                    var13 = 8421504;
                }

                if (this.mouseLastButtonDown == 1) {
                    this.controlListEntryMouseButtonDown[var3] = var12;
                    this.controlClicked[var3] = true;
                }
            }

            if (this.controlListEntryMouseButtonDown[var3] == var12) {
                if (this.controlUseAlternativeColour[var3]) {
                    var13 = 16711680;
                } else {
                    var13 = 12582912;
                }
            }

            this.surface.drawstring(var11, var6[var12], var10, var1, 0, -123, var13);
            var10 += this.surface.textWidth(var1, -127, var6[var12] + "  ");
        }
        ;
    }

    final int method528(int var1, boolean var2, int var3, int var4) {
        int var5 = this.surface.anIntArray600[var1];
        int var6 = this.surface.anIntArray577[var1];
        this.controlType[this.controlCount] = 12;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = var2;
        this.anIntArray866[this.controlCount] = var3 - var5 / 2;
        this.anIntArray896[this.controlCount] = var4 - var6 / 2;
        this.anIntArray879[this.controlCount] = var5;
        this.anIntArray903[this.controlCount] = var6;
        this.anIntArray847[this.controlCount] = var1;
        return this.controlCount++;
    }

    private int rgb2longMod(int var1, int var2, int var3, int var4) {
        return var3 != -12 ? -102 : Surface.rgb2long(var1 * anInt809 / 176, ClientStreamBase.anInt739 * var2 / 114, anInt867 * var4 / 114);
    }

    final int method530(boolean var1, int var2, byte var3, int var4, int var5, int var6, int var7, boolean var8, int var9) {
        if (var3 != 91) {
            this.method523(24, 37);
        }

        this.controlType[this.controlCount] = 5;
        this.controlShown[this.controlCount] = true;
        this.aBooleanArray843[this.controlCount] = var8;
        this.controlClicked[this.controlCount] = false;
        this.anIntArray847[this.controlCount] = var5;
        this.controlUseAlternativeColour[this.controlCount] = var1;
        this.anIntArray866[this.controlCount] = var4;
        this.anIntArray896[this.controlCount] = var2;
        this.anIntArray879[this.controlCount] = var9;
        this.anIntArray903[this.controlCount] = var7;
        this.controlInputMaxLen[this.controlCount] = var6;
        this.controlText[this.controlCount] = "";
        return this.controlCount++;
    }

    final void addListEntry(int var1, String var2, int var3, String var4, byte var5, int var6, String var7) {
        if (var5 != 81) {
            this.method542(16, -101, -14, 16, 105, -48, -21, (String[]) null, -110, (int[]) null, 15);
        }

        this.aStringArrayArray871[var1][var3] = var7;
        this.anIntArrayArray860[var1][var3] = var6;
        this.aStringArrayArray876[var1][var3] = var2;
        this.aStringArrayArray839[var1][var3] = var4;
        if (this.anIntArray824[var1] < 1 + var3) {
            this.anIntArray824[var1] = var3 + 1;
        }
    }

    private void method532(int var1, int var2, int var3, int var4, int var5, String var6, byte var7, int var8) {
        int var9;
        int var10;
        if (this.aBooleanArray843[var3]) {
            var9 = var6.length();
            var6 = "";

            for (var10 = 0; var9 > var10; ++var10) {
                var6 = var6 + "X";
            }
        }

        if (this.controlType[var3] != 5) {
            if (this.controlType[var3] == 6) {
                if (this.mouseLastButtonDown == 1 && this.mouseX >= -(var1 / 2) + var5 && -(var4 / 2) + var2 <= this.mouseY && this.mouseX <= var5 - -(var1 / 2) && var4 / 2 + var2 >= this.mouseY) {
                    this.focusControlIndex = var3;
                }

                var5 -= this.surface.textWidth(var8, -127, var6) / 2;
            }
        } else if (this.mouseLastButtonDown == 1 && this.mouseX >= var5 && this.mouseY >= (-(var4 / 2) + var2) && var1 + var5 >= this.mouseX && this.mouseY <= (var4 / 2 + var2)) {
            this.focusControlIndex = var3;
        }

        if (var3 == this.focusControlIndex) {
            var6 = var6 + "*";
        }

        var10 = -1 % ((var7 - 35) / 41);
        var9 = this.surface.textHeight(var8, true) / 3 + var2;
        this.method541(var9, 0, var5, var3, var6, var8, false);
    }

    final void updateText(int var1, String var2, int var3) {
        int var4 = -19 % ((var3 - -10) / 35);
        this.controlText[var1] = var2;
    }

    private void method534(int var1, byte var2, int var3, int var4) {
        this.surface.drawSprite((byte) 61, var4, var1, var3);
        if (var2 == -12) {
        }
    }

    final int method535(int var1, int var2, boolean var3, boolean var4, int var5, int var6, int var7, int var8, int var9) {
        this.controlType[this.controlCount] = 6;
        this.controlShown[this.controlCount] = true;
        this.aBooleanArray843[this.controlCount] = var3;
        this.controlClicked[this.controlCount] = false;
        this.anIntArray847[this.controlCount] = var6;
        this.controlUseAlternativeColour[this.controlCount] = var4;
        this.anIntArray866[this.controlCount] = var1;
        this.anIntArray896[this.controlCount] = var9;
        this.anIntArray879[this.controlCount] = var5;
        this.anIntArray903[this.controlCount] = var8;
        this.controlInputMaxLen[this.controlCount] = var7;
        if (var2 >= -6) {
            this.method514(14, 17, (byte) -111, -92, -62);
        }

        this.controlText[this.controlCount] = "";
        return this.controlCount++;
    }

    private void method536(int var1, int var2, int var3, String[] var4, byte var5, int var6) {
        if (var5 != -15) {
            this.aStringArrayArray839 = null;
        }

        int var7 = var4.length;
        int var8 = var1 - this.surface.textHeight(var3, true) * (var7 + -1) / 2;

        for (int var9 = 0; var7 > var9; ++var9) {
            int var10;
            if (!this.controlUseAlternativeColour[var6]) {
                var10 = 0;
            } else {
                var10 = 16777215;
            }

            int var11 = this.surface.textWidth(var3, -127, var4[var9]);
            if (this.mouseX >= (-(var11 / 2) + var2) && this.mouseX <= var2 + var11 / 2 && var8 >= (this.mouseY + -2) && (this.mouseY + -2) > (var8 - this.surface.textHeight(var3, true))) {
                if (!this.controlUseAlternativeColour[var6]) {
                    var10 = 16777215;
                } else {
                    var10 = 8421504;
                }

                if (this.mouseLastButtonDown == 1) {
                    this.controlListEntryMouseButtonDown[var6] = var9;
                    this.controlClicked[var6] = true;
                }
            }

            if (this.controlListEntryMouseButtonDown[var6] == var9) {
                if (!this.controlUseAlternativeColour[var6]) {
                    var10 = 12582912;
                } else {
                    var10 = 16711680;
                }
            }

            this.surface.drawstring(var8, var4[var9], -(var11 / 2) + var2, var3, 0, -117, var10);
            var8 += this.surface.textHeight(var3, true);
        }
        ;
    }

    final void keyPress(int var1, int key) {
        if (key == 0) {
            return;
        }
        if (focusControlIndex != -1 && controlText[focusControlIndex] != null && controlShown[focusControlIndex]) {
            int inputLen = controlText[focusControlIndex].length();
            if (key == 8 && inputLen > 0) {
                controlText[focusControlIndex] = controlText[focusControlIndex].substring(0, inputLen - 1);
            }
            if ((key == 10 || key == 13) && inputLen > 0) {
                controlClicked[focusControlIndex] = true;
            }
            String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ";
            if (inputLen < controlInputMaxLen[focusControlIndex]) {
                for (int i = 0; i < s.length(); i++) {
                    if (key == s.charAt(i)) {
                        controlText[focusControlIndex] += (char) key;
                    }
                }
            }
            if (key == 9) {
                for (; controlType[focusControlIndex] != 5 && controlType[focusControlIndex] != 6;
                     focusControlIndex = (focusControlIndex + 1) % controlCount)
                    ;
            }
        } else if (focusControlIndex != -1 && controlShown[focusControlIndex]) {
            if (key == 10 || key == 13) {
                controlClicked[focusControlIndex] = true;
            }
        }
    }

    final int method538(int var1, int var2, int var3, int var4, boolean var5, int var6, int var7, boolean var8) {
        this.controlType[this.controlCount] = 9;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = var8;
        this.anIntArray847[this.controlCount] = var1;
        this.controlUseAlternativeColour[this.controlCount] = var5;
        this.anIntArray866[this.controlCount] = var2;
        this.anIntArray896[this.controlCount] = var7;
        this.anIntArray879[this.controlCount] = var3;
        this.anIntArray903[this.controlCount] = var4;
        this.controlInputMaxLen[this.controlCount] = var6;
        this.aStringArrayArray871[this.controlCount] = new String[var6];
        this.anIntArrayArray860[this.controlCount] = new int[var6];
        this.aStringArrayArray876[this.controlCount] = new String[var6];
        this.aStringArrayArray839[this.controlCount] = new String[var6];
        this.anIntArray824[this.controlCount] = 0;
        this.controlFlashText[this.controlCount] = 0;
        this.controlListEntryMouseButtonDown[this.controlCount] = -1;
        this.controlListEntryMouseOver[this.controlCount] = -1;
        return this.controlCount++;
    }

    final int method539(int var1, int var2, int var3, boolean var4, boolean var5, int var6, int var7, int var8) {
        this.controlType[this.controlCount] = 4;
        this.controlShown[this.controlCount] = true;
        this.controlClicked[this.controlCount] = var5;
        this.anIntArray866[this.controlCount] = var3;
        this.anIntArray896[this.controlCount] = var6;
        this.anIntArray879[this.controlCount] = var8;
        this.anIntArray903[this.controlCount] = var1;
        this.controlUseAlternativeColour[this.controlCount] = var4;
        this.anIntArray847[this.controlCount] = var2;
        this.controlInputMaxLen[this.controlCount] = var7;
        this.anIntArray824[this.controlCount] = 0;
        this.controlFlashText[this.controlCount] = 0;
        this.aStringArrayArray871[this.controlCount] = new String[var7];
        this.anIntArrayArray860[this.controlCount] = new int[var7];
        this.aStringArrayArray876[this.controlCount] = new String[var7];
        this.aStringArrayArray839[this.controlCount] = new String[var7];
        return this.controlCount++;
    }

    final void clearList(boolean var1, int var2) {
        if (var1) {
            this.method526(-121, -87);
        }

        this.anIntArray824[var2] = 0;
    }

    private void method541(int var1, int var2, int var3, int var4, String var5, int var6, boolean var7) {
        int var8;
        if (!this.controlUseAlternativeColour[var4]) {
            var8 = 0;
        } else {
            var8 = 16777215;
        }

        this.surface.drawstring(var1, var5, var3, var6, var2, -125, var8);
        if (var7) {
            this.method523(102, -39);
        }
    }

    private void method542(int var1, int var2, int var3, int var4, int var5, int var6, int var7, String[] var8, int var9, int[] var10, int var11) {
        int var12 = var3 / this.surface.textHeight(var5, true);
        if (var11 > -var12 + var6) {
            var11 = -var12 + var6;
        }

        if (var11 < 0) {
            var11 = 0;
        }

        this.controlFlashText[var9] = var11;
        int var13;
        int var14;
        int var15;
        int var16;
        if (var6 > var12) {
            var13 = -12 + var4 + var1;
            var14 = var12 * (-27 + var3) / var6;
            if (var14 < 6) {
                var14 = 6;
            }

            var15 = (-var14 + -27 + var3) * var11 / (var6 + -var12);
            if (this.mouseButtonDown == 1 && var13 <= this.mouseX && 12 + var13 >= this.mouseX) {
                if (this.mouseY > var2 && (var2 + 12) > this.mouseY && var11 > 0) {
                    --var11;
                }

                if (this.mouseY > -12 + var2 + var3 && this.mouseY < var2 + var3 && var11 < (-var12 + var6)) {
                    ++var11;
                }

                this.controlFlashText[var9] = var11;
            }

            if (this.mouseButtonDown == 1 && (this.mouseX >= var13 && this.mouseX <= var13 - -12 || this.mouseX >= -12 + var13 && (24 + var13) >= this.mouseX && this.controlListScrollbarHandleDragged[var9])) {
                if (this.mouseY > (var2 + 12) && -12 + var3 + var2 > this.mouseY) {
                    this.controlListScrollbarHandleDragged[var9] = true;
                    var16 = -(var14 / 2) + (-12 + this.mouseY - var2);
                    var11 = var16 * var6 / (var3 - 24);
                    if (var6 + -var12 < var11) {
                        var11 = var6 + -var12;
                    }

                    if (var11 < 0) {
                        var11 = 0;
                    }

                    this.controlFlashText[var9] = var11;
                }
            } else {
                this.controlListScrollbarHandleDragged[var9] = false;
            }

            var15 = (-27 + (var3 - var14)) * var11 / (-var12 + var6);
            this.drawListContainer(var14, var2, var1, var15, var3, 12, var4);
        }

        var13 = -(this.surface.textHeight(var5, true) * var12) + var3;
        var14 = this.surface.textHeight(var5, true) * 5 / 6 + var2 - -(var13 / 2);
        var16 = 117 / ((80 - var7) / 40);

        for (var15 = var11; var15 < var6; ++var15) {
            if (this.mouseLastButtonDown != 0 && this.mouseX >= var1 + 2 && this.mouseX <= this.surface.textWidth(var5, -127, var8[var15]) + (var1 - -2) && var14 >= (-2 + this.mouseY) && (this.mouseY - 2) > (var14 - this.surface.textHeight(var5, true))) {
                this.controlClicked[var9] = true;
                this.controlListEntryMouseButtonDown[var9] = Utility.bitwiseOr(var15, this.mouseLastButtonDown << 16);
            }

            this.method541(var14, var10[var15], var1 - -2, var9, var8[var15], var5, false);
            var14 += this.surface.textHeight(var5, true) - textListEntryHeightMod;
            if ((var2 + var3) <= var14) {
                return;
            }
        }
    }

    final int method544(int var1, int var2) {
        if (var1 >= -123) {
            this.mouseLastButtonDown = -13;
        }

        return this.controlListEntryMouseButtonDown[var2];
    }

}
