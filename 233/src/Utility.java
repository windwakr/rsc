import java.awt.*;
import java.awt.image.PixelGrabber;
import java.io.*;
import java.net.URL;

/**
 * rsc
 * 02.01.2014
 */
public class Utility {

    static int[] data = new int[256];
    static byte[] gameFontData = new byte[100000];

    static {
        for (int var0 = 0; var0 < 256; ++var0) {
            int var1 = var0;

            for (int var2 = 0; var2 < 8; ++var2) {
                if ((var1 & 1) == 1) {
                    var1 = -306674912 ^ var1 >>> 1;
                } else {
                    var1 >>>= 1;
                }
            }

            data[var0] = var1;
        }
    }

    static int gameFontSize = 0;
    static byte[][][] aByteArrayArrayArray775;
    static int[] anIntArray446;

    static byte[] loadData(byte[] var1, int var2, String var3) {
        return unpackData(var1, var3, (byte[]) null, var2);
    }

    private static byte[] unpackData(byte[] archiveData, String filename, byte[] fileData, int var3) {
        int entryCount = (archiveData[0] & 255) * 256 + (255 & archiveData[1]);
        filename = filename.toUpperCase();
        int wantedHash = 0;

        for (int var7 = 0; var7 < filename.length(); ++var7) {
            wantedHash = 61 * wantedHash - (-filename.charAt(var7) - -32);
        }

        int offset = 2 - -(10 * entryCount);

        for (int var9 = 0; var9 < entryCount; ++var9) {
            int var10 = (archiveData[5 + var9 * 10] & 255) + 65536 * (255 & archiveData[10 * var9 + 3]) + (255 & archiveData[2 + 10 * var9]) * 16777216 + (archiveData[4 + 10 * var9] & 255) * 256;
            int var11 = 65536 * (255 & archiveData[6 + var9 * 10]) + 256 * (255 & archiveData[7 + var9 * 10]) + (archiveData[8 + var9 * 10] & 255);
            int var12 = 256 * (255 & archiveData[10 + var9 * 10]) + (65536 * (255 & archiveData[9 + 10 * var9]) - -(archiveData[11 + var9 * 10] & 255));
            if (wantedHash == var10) {
                if (fileData == null) {
                    fileData = new byte[var3 + var11];
                }

                if (var12 != var11) {
                    BZLib.decompress(fileData, var11, archiveData, var12, offset);
                } else {
                    for (int var13 = 0; var13 < var11; ++var13) {
                        fileData[var13] = archiveData[offset + var13];
                    }
                }

                return fileData;
            }

            offset += var12;
        }

        return null;
    }

    static int getUnsignedShort(int var0, byte[] var1, int var2) {
        if (var2 < 40) {
            getUnsignedShort(79, (byte[]) null, -33);
        }

        return (var1[var0 - -1] & 255) + ('\uff00' & var1[var0] << 8);
    }

    static void sendClientError(String description, int unused, Throwable error) {

        if (true) {
            System.out.println("sendClientError: " + description);
            error.printStackTrace();
            return;
        }

        try {
            String encDesc = "";
            if (error != null) {
                encDesc = throwable2string(error);
            }

            if (description != null) {
                if (error != null) {
                    encDesc = encDesc + " | ";
                }

                encDesc = encDesc + description;
            }

            printError(encDesc);
            encDesc = encode("%3a", ":", encDesc);
            encDesc = encode("%40", "@", encDesc);
            encDesc = encode("%26", "&", encDesc);
            encDesc = encode("%23", "#", encDesc);
            if (Isaac.anApplet445 != null) {
                CacheState var4 = Class11.aCachePackageManager_205.openURL(new URL(Isaac.anApplet445.getCodeBase(), "clienterror.ws?c=" + ClientStream.anInt1693 + "&u=" + (Scene.aString378 != null ? Scene.aString378 : String.valueOf(Character.aLong503)) + "&v1=" + CachePackageManager.javaVendor + "&v2=" + CachePackageManager.javaVersion + "&e=" + encDesc));

                while (var4.state == 0) {
                    GameApplet.sleep(1L, -17239);
                }

                if (var4.state == 1) {
                    DataInputStream var5 = (DataInputStream) var4.stateObject;
                    var5.read();
                    var5.close();
                }
            }
        } catch (Exception var6) {
            ;
        }
    }

    private static String throwable2string(Throwable var1) throws IOException {
        String var2;
        if (!(var1 instanceof GameException)) {
            var2 = "";
        } else {
            GameException var3 = (GameException) var1;
            var2 = var3.errorMessage + " | ";
            var1 = var3.errorThrowable;
        }

        StringWriter var13 = new StringWriter();
        PrintWriter var4 = new PrintWriter(var13);
        var1.printStackTrace(var4);
        var4.close();
        String var5 = var13.toString();
        BufferedReader var6 = new BufferedReader(new StringReader(var5));
        String var7 = var6.readLine();

        while (true) {
            String var8 = var6.readLine();
            if (var8 == null) {
                var2 = var2 + "| " + var7;
                return var2;
            }

            int var9 = var8.indexOf(40);
            int var10 = var8.indexOf(41, var9 - -1);
            String var11;
            if (var9 != -1) {
                var11 = var8.substring(0, var9);
            } else {
                var11 = var8;
            }

            var11 = var11.trim();
            var11 = var11.substring(1 + var11.lastIndexOf(32));
            var11 = var11.substring(var11.lastIndexOf(9) + 1);
            var2 = var2 + var11;
            if (var9 != -1 && var10 != -1) {
                int var12 = var8.indexOf(".java:", var9);
                if (var12 >= 0) {
                    var2 = var2 + var8.substring(var12 + 5, var10);
                }
            }

            var2 = var2 + ' ';
        }
    }

    private static void printError(String var1) {
        System.out.println("Error: " + encode("\n", "%0a", var1));
    }

    private static String encode(String to, String from, String unenc) {
        int var4 = unenc.indexOf(from);

        while (var4 != -1) {
            unenc = unenc.substring(0, var4) + to + unenc.substring(from.length() + var4);
            var4 = unenc.indexOf(from, to.length() + var4);
        }

        return unenc;
    }

    static int bitwiseOr(int var0, int var1) {
        return var0 | var1;
    }

    static int bitwiseAnd(int var0, int var1) {
        return var0 & var1;
    }

    static int bitwiseXor(int var0, int var1) {
        return var0 ^ var1;
    }

    static int getDataFileOffset(String filename, byte[] data) {
        int numEntries = getUnsignedShort(0, data, 76);
        filename = filename.toUpperCase();
        int wantedHash = 0;

        for (int var5 = 0; var5 < filename.length(); ++var5) {
            wantedHash = filename.charAt(var5) + wantedHash * 61 + -32;
        }

        int offset = numEntries * 10 + 2;

        for (int var7 = 0; var7 < numEntries; ++var7) {
            int fileHash = (data[5 + var7 * 10] & 255) + 256 * (255 & data[var7 * 10 - -4]) + ((255 & data[2 + 10 * var7]) * 16777216 - -(65536 * (255 & data[var7 * 10 - -3])));
            int fileSize = (data[9 + var7 * 10] & 255) * 65536 + ((data[10 + 10 * var7] & 255) * 256 - -(255 & data[11 + 10 * var7]));
            if (wantedHash == fileHash) {
                return offset;
            }

            offset += fileSize;
        }

        return 0;
    }

    static InputStream method545(String var0, int var1) throws IOException {
        if (var1 != -32341) {
            return null;
        } else {
            Object var2;
            if (GameApplet.appletCodeBase == null) {
                var2 = new BufferedInputStream(new FileInputStream(var0));
            } else {
                URL var3 = new URL(GameApplet.appletCodeBase, var0);
                var2 = var3.openStream();
            }

            return (InputStream) var2;
        }
    }

    static void insertBytes(byte[] in, int offIn, byte[] out, int offOut, int var4) {
        if (in == out) {
            if (offIn == offOut) {
                return;
            }

            if (offOut > offIn && offOut < offIn + var4) {
                --var4;
                offIn += var4;
                offOut += var4;
                var4 = offIn - var4;

                for (var4 += 7; offIn >= var4; out[offOut--] = in[offIn--]) {
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                    out[offOut--] = in[offIn--];
                }

                for (var4 -= 7; offIn >= var4; out[offOut--] = in[offIn--]) {
                    ;
                }

                return;
            }
        }

        var4 += offIn;

        for (var4 -= 7; offIn < var4; out[offOut++] = in[offIn++]) {
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
            out[offOut++] = in[offIn++];
        }

        for (var4 += 7; offIn < var4; out[offOut++] = in[offIn++]) {
            ;
        }

    }

    static String method500(String var0, int unused) {
        if (var0 == null) {
            return null;
        } else {
            int var2 = 0;

            int var3;
            for (var3 = var0.length(); var2 < var3 && method483(var0.charAt(var2), (byte) 48); ++var2) {
                ;
            }

            while (var2 < var3 && method483(var0.charAt(-1 + var3), (byte) 53)) {
                --var3;
            }

            int var4 = -var2 + var3;
            if (var4 >= 1 && var4 <= 12) {
                StringBuffer var5 = new StringBuffer(var4);

                for (int var6 = var2; var6 < var3; ++var6) {
                    char var7 = var0.charAt(var6);
                    if (GameFrame.method147(var7, 0)) {
                        char var8 = ClientStream.method478(var7);
                        if (var8 != 0) {
                            var5.append(var8);
                        }
                    }
                }

                if (var5.length() == 0) {
                    return null;
                } else {
                    return var5.toString();
                }
            } else {
                return null;
            }
        }
    }

    static boolean method483(char var0, byte var1) {
        return var1 <= 8 ? true : var0 == 160 || var0 == 32 || var0 == 95 || var0 == 45;
    }

    static int getUnsignedInt(byte[] var0, boolean var1, int var2) {
        if (var1) {
            getUnsignedInt((byte[]) null, true, -81);
        }

        return (var0[var2 + 3] & 255) + (16711680 & var0[var2 - -1] << 16) + ((255 & var0[var2]) << 24) + (var0[2 + var2] << 8 & '\uff00');
    }

    static synchronized long method347(int var0) {
        int var1 = 101 % ((25 - var0) / 52);
        long var2 = System.currentTimeMillis();
        if (BufferBase_Sub3.aLong1640 > var2) {
            Isaac.aLong449 += -var2 + BufferBase_Sub3.aLong1640;
        }

        BufferBase_Sub3.aLong1640 = var2;
        return Isaac.aLong449 + var2;
    }

    static int getDataFileLength(String filename, byte[] data) {
        int numEntries = getUnsignedShort(0, data, 53);
        int wantedHash = 0;
        filename = filename.toUpperCase();

        for (int var5 = 0; var5 < filename.length(); ++var5) {
            wantedHash = -32 + 61 * wantedHash + filename.charAt(var5);
        }

        for (int var6 = 0; var6 < numEntries; ++var6) {
            int fileHash = (data[10 * var6 + 5] & 255) + 256 * (data[4 + 10 * var6] & 255) + (data[var6 * 10 + 2] & 255) * 16777216 - -(65536 * (255 & data[10 * var6 + 3]));
            int fileSize = (data[7 + 10 * var6] & 255) * 256 + 65536 * (255 & data[10 * var6 - -6]) - -(255 & data[10 * var6 + 8]);
            if (wantedHash == fileHash) {
                return fileSize;
            }
        }

        return 0;
    }

    static int getnum(int length, int offset, byte[] bytes) {
        int var4 = -1;

        for (int var5 = offset; var5 < length; ++var5) {
            var4 = var4 >>> 8 ^ data[(bytes[var5] ^ var4) & 255];
        }

        var4 = ~var4;

        return var4;
    }

    static int writeUnicodeString(CharSequence charseq, int seqoff, int seqlen, byte[] buf, int bufoff) {
        int j1 = seqlen - seqoff;
        for (int k1 = 0; k1 < j1; k1++) {
            char c = charseq.charAt(seqoff + k1);
            if (c > 0 && c < '\200' || c >= '\240' && c <= '\377') {
                buf[bufoff + k1] = (byte) c;
                continue;
            }
            if (c == '\u20AC') {
                buf[bufoff + k1] = -128;
                continue;
            }
            if (c == '\u201A') {
                buf[bufoff + k1] = -126;
                continue;
            }
            if (c == '\u0192') {
                buf[bufoff + k1] = -125;
                continue;
            }
            if (c == '\u201E') {
                buf[bufoff + k1] = -124;
                continue;
            }
            if (c == '\u2026') {
                buf[bufoff + k1] = -123;
                continue;
            }
            if (c == '\u2020') {
                buf[bufoff + k1] = -122;
                continue;
            }
            if (c == '\u2021') {
                buf[bufoff + k1] = -121;
                continue;
            }
            if (c == '\u02C6') {
                buf[bufoff + k1] = -120;
                continue;
            }
            if (c == '\u2030') {
                buf[bufoff + k1] = -119;
                continue;
            }
            if (c == '\u0160') {
                buf[bufoff + k1] = -118;
                continue;
            }
            if (c == '\u2039') {
                buf[bufoff + k1] = -117;
                continue;
            }
            if (c == '\u0152') {
                buf[bufoff + k1] = -116;
                continue;
            }
            if (c == '\u017D') {
                buf[bufoff + k1] = -114;
                continue;
            }
            if (c == '\u2018') {
                buf[bufoff + k1] = -111;
                continue;
            }
            if (c == '\u2019') {
                buf[bufoff + k1] = -110;
                continue;
            }
            if (c == '\u201C') {
                buf[bufoff + k1] = -109;
                continue;
            }
            if (c == '\u201D') {
                buf[bufoff + k1] = -108;
                continue;
            }
            if (c == '\u2022') {
                buf[bufoff + k1] = -107;
                continue;
            }
            if (c == '\u2013') {
                buf[bufoff + k1] = -106;
                continue;
            }
            if (c == '\u2014') {
                buf[bufoff + k1] = -105;
                continue;
            }
            if (c == '\u02DC') {
                buf[bufoff + k1] = -104;
                continue;
            }
            if (c == '\u2122') {
                buf[bufoff + k1] = -103;
                continue;
            }
            if (c == '\u0161') {
                buf[bufoff + k1] = -102;
                continue;
            }
            if (c == '\u203A') {
                buf[bufoff + k1] = -101;
                continue;
            }
            if (c == '\u0153') {
                buf[bufoff + k1] = -100;
                continue;
            }
            if (c == '\u017E') {
                buf[bufoff + k1] = -98;
                continue;
            }
            if (c == '\u0178')
                buf[bufoff + k1] = -97;
            else
                buf[bufoff + k1] = '?';
        }

        return j1;
    }

    static String readUnicodeString(byte[] buf, int bufoff, int len) {
        char[] chars = new char[len];
        int off = 0;
        for (int j = 0; j < len; j++) {
            int uchar = buf[bufoff + j] & 0xff;
            if (uchar == 0) {
                continue;
            }
            if (uchar >= 128 && uchar < 160) {
                char c = BufferBase_Sub3.unicodeChars[uchar - 128];
                if (c == '\0') {
                    c = '?';
                }
                uchar = c;
            }
            chars[off++] = (char) uchar;
        }
        return new String(chars, 0, off);
    }

    static void sleep(long var0) {
        try {
            Thread.sleep(var0);
        } catch (InterruptedException var4) {
            ;
        }
    }

    static byte[] stringToUnicode(String str) {
        int strlen = str.length();
        byte[] buf = new byte[strlen];
        for (int i = 0; i < strlen; i++) {
            char c = str.charAt(i);
            if (c > 0 && c < '\200' || c >= '\240' && c <= '\377') {
                buf[i] = (byte) c;
                continue;
            }
            if (c == '\u20AC') {
                buf[i] = -128;
                continue;
            }
            if (c == '\u201A') {
                buf[i] = -126;
                continue;
            }
            if (c == '\u0192') {
                buf[i] = -125;
                continue;
            }
            if (c == '\u201E') {
                buf[i] = -124;
                continue;
            }
            if (c == '\u2026') {
                buf[i] = -123;
                continue;
            }
            if (c == '\u2020') {
                buf[i] = -122;
                continue;
            }
            if (c == '\u2021') {
                buf[i] = -121;
                continue;
            }
            if (c == '\u02C6') {
                buf[i] = -120;
                continue;
            }
            if (c == '\u2030') {
                buf[i] = -119;
                continue;
            }
            if (c == '\u0160') {
                buf[i] = -118;
                continue;
            }
            if (c == '\u2039') {
                buf[i] = -117;
                continue;
            }
            if (c == '\u0152') {
                buf[i] = -116;
                continue;
            }
            if (c == '\u017D') {
                buf[i] = -114;
                continue;
            }
            if (c == '\u2018') {
                buf[i] = -111;
                continue;
            }
            if (c == '\u2019') {
                buf[i] = -110;
                continue;
            }
            if (c == '\u201C') {
                buf[i] = -109;
                continue;
            }
            if (c == '\u201D') {
                buf[i] = -108;
                continue;
            }
            if (c == '\u2022') {
                buf[i] = -107;
                continue;
            }
            if (c == '\u2013') {
                buf[i] = -106;
                continue;
            }
            if (c == '\u2014') {
                buf[i] = -105;
                continue;
            }
            if (c == '\u02DC') {
                buf[i] = -104;
                continue;
            }
            if (c == '\u2122') {
                buf[+i] = -103;
                continue;
            }
            if (c == '\u0161') {
                buf[i] = -102;
                continue;
            }
            if (c == '\u203A') {
                buf[i] = -101;
                continue;
            }
            if (c == '\u0153') {
                buf[i] = -100;
                continue;
            }
            if (c == '\u017E') {
                buf[i] = -98;
                continue;
            }
            if (c == '\u0178')
                buf[i] = -97;
            else
                buf[i] = '?';
        }

        return buf;
    }

    static boolean createFont(String var0, int var2, GameApplet var3) {
        boolean var4 = false;
        boolean var5 = false;
        var0 = var0.toLowerCase();
        if (var0.startsWith("helvetica")) {
            var0 = var0.substring(9);
        }

        if (var0.startsWith("h")) {
            var0 = var0.substring(1);
        }

        if (var0.startsWith("f")) {
            var0 = var0.substring(1);
            var4 = true;
        }

        if (var0.startsWith("d")) {
            var0 = var0.substring(1);
            var5 = true;
        }

        if (var0.endsWith(".jf")) {
            var0 = var0.substring(0, var0.length() - 3);
        }

        byte var6 = 0;
        if (var0.endsWith("b")) {
            var6 = 1;
            var0 = var0.substring(0, var0.length() - 1);
        }

        if (var0.endsWith("p")) {
            var0 = var0.substring(0, -1 + var0.length());
        }

        int var7 = Integer.parseInt(var0);
        Font var8 = new Font("Helvetica", var6, var7);
        FontMetrics var9 = var3.getFontMetrics(var8);
        gameFontSize = 855;
        String var10 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:\'@#~,<.>/?\\| ";

        for (int var12 = 0; var12 < 95; ++var12) {
            if (!readFont(var2, var12, var8, var10.charAt(var12), var9, var3, (byte) 126, var5)) {
                return false;
            }
        }

        Surface.gameFonts[var2] = new byte[gameFontSize];

        for (int var13 = 0; var13 < gameFontSize; ++var13) {
            Surface.gameFonts[var2][var13] = gameFontData[var13];
        }

        if (var6 == 1 && Scene.aBooleanArray418[var2]) {
            Scene.aBooleanArray418[var2] = false;
            if (!createFont("f" + var7 + "p", var2, var3)) {
                return false;
            }
        }

        if (var4 && !Scene.aBooleanArray418[var2]) {
            Scene.aBooleanArray418[var2] = false;
            if (!createFont("d" + var7 + "p", var2, var3)) {
                return false;
            }
        }

        return true;
    }

    private static boolean readFont(int var0, int var1, Font var2, char var3, FontMetrics var4, GameApplet var5, byte var6, boolean var7) {
        int var8 = var4.charWidth(var3);
        int var9 = var8;
        if (var7) {
            try {
                if (var3 == 102 || var3 == 116 || var3 == 119 || var3 == 118 || var3 == 107 || var3 == 120 || var3 == 121 || var3 == 65 || var3 == 86 || var3 == 87) {
                    ++var8;
                }

                if (var3 == 47) {
                    var7 = false;
                }
            } catch (Exception var30) {
                ;
            }
        }

        int var10 = var4.getMaxAscent();
        int var11 = var4.getMaxAscent() + var4.getMaxDescent();
        int var12 = var4.getHeight();
        Image var13 = var5.createImage(var8, var11);
        if (var13 == null) {
            return false;
        } else {
            Graphics var14 = var13.getGraphics();
            var14.setColor(Color.black);
            var14.fillRect(0, 0, var8, var11);
            var14.setColor(Color.white);
            var14.setFont(var2);
            var14.drawString(String.valueOf(var3), 0, var10);
            if (var7) {
                var14.drawString(String.valueOf(var3), 1, var10);
            }

            int[] var15 = new int[var8 * var11];
            PixelGrabber var16 = new PixelGrabber(var13, 0, 0, var8, var11, var15, 0, var8);

            try {
                var16.grabPixels();
            } catch (InterruptedException var29) {
                return false;
            }

            var13.flush();
            var13 = null;
            int var17 = 0;
            int var18 = 0;
            int var19 = var8;

            int var21;
            int var22;
            label213:
            for (int var20 = 0; var20 < var11; ++var20) {
                for (var21 = 0; var8 > var21; ++var21) {
                    var22 = var15[var20 * var8 + var21];
                    if ((0xffffff & var22) != 0) {
                        var18 = var20;
                        break label213;
                    }
                }
            }

            var21 = 115 % ((-26 - var6) / 39);
            int var23 = var11;

            int var25;
            int var24;
            label199:
            for (var22 = 0; var8 > var22; ++var22) {
                for (var24 = 0; var24 < var11; ++var24) {
                    var25 = var15[var24 * var8 + var22];
                    if ((var25 & 0xffffff) != 0) {
                        var17 = var22;
                        break label199;
                    }
                }
            }

            int var26;
            label185:
            for (var24 = var11 + -1; var24 >= 0; --var24) {
                for (var25 = 0; var8 > var25; ++var25) {
                    var26 = var15[var8 * var24 + var25];
                    if ((var26 & 0xffffff) != 0) {
                        var23 = 1 + var24;
                        break label185;
                    }
                }
            }

            int var27;
            label171:
            for (var25 = var8 - 1; var25 >= 0; --var25) {
                for (var26 = 0; var26 < var11; ++var26) {
                    var27 = var15[var8 * var26 + var25];
                    if ((var27 & 0xffffff) != 0) {
                        var19 = 1 + var25;
                        break label171;
                    }
                }
            }

            gameFontData[9 * var1] = (byte) (gameFontSize >> 14);
            gameFontData[var1 * 9 + 1] = (byte) bitwiseAnd(127, gameFontSize >> 7);
            gameFontData[var1 * 9 + 2] = (byte) bitwiseAnd(127, gameFontSize);
            gameFontData[9 * var1 + 3] = (byte) (-var17 + var19);
            gameFontData[4 + 9 * var1] = (byte) (var23 + -var18);
            gameFontData[9 * var1 - -5] = (byte) var17;
            gameFontData[9 * var1 + 6] = (byte) (var10 + -var18);
            gameFontData[var1 * 9 - -7] = (byte) var9;
            gameFontData[8 + 9 * var1] = (byte) var12;

            for (var26 = var18; var23 > var26; ++var26) {
                for (var27 = var17; var19 > var27; ++var27) {
                    int var28 = 255 & var15[var26 * var8 + var27];
                    if (var28 > 30 && var28 < 230) {
                        Scene.aBooleanArray418[var0] = true;
                    }

                    gameFontData[gameFontSize++] = (byte) var28;
                }
            }

            return true;
        }
    }

    static String format(int var0, String var1, byte var2) {
        int var3 = -109 / ((-78 - var2) / 46);
        String var4 = "";

        for (int var5 = 0; var5 < var0; ++var5) {
            if (var5 >= var1.length()) {
                var4 = var4 + " ";
            } else {
                char var6 = var1.charAt(var5);
                if (var6 >= 97 && var6 <= 122) {
                    var4 = var4 + var6;
                } else if (var6 >= 65 && var6 <= 90) {
                    var4 = var4 + var6;
                } else if (var6 >= 48 && var6 <= 57) {
                    var4 = var4 + var6;
                } else {
                    var4 = var4 + '_';
                }
            }
        }

        return var4;
    }

    static synchronized byte[] method354(int var1) {
        byte[] var5;
        if (var1 == 100 && GameApplet.anInt28 > 0) {
            var5 = Isaac.aByteArrayArray438[--GameApplet.anInt28];
            Isaac.aByteArrayArray438[GameApplet.anInt28] = null;
            return var5;
        } else if (var1 == 5000 && ClientStream.anInt1695 > 0) {
            var5 = BufferBase_Sub3.aByteArrayArray806[--ClientStream.anInt1695];
            BufferBase_Sub3.aByteArrayArray806[ClientStream.anInt1695] = null;
            return var5;
        } else if (var1 == 30000 && GameFrame.anInt108 > 0) {
            var5 = BufferBase_Sub3.aByteArrayArray820[--GameFrame.anInt108];
            BufferBase_Sub3.aByteArrayArray820[GameFrame.anInt108] = null;
            return var5;
        } else {
            int var2;
            if (aByteArrayArrayArray775 != null) {
                for (var2 = 0; var2 < BufferBase_Sub3.anIntArray1091.length; ++var2) {
                    if (var1 == BufferBase_Sub3.anIntArray1091[var2] && anIntArray446[var2] > 0) {
                        byte[] var3 = aByteArrayArrayArray775[var2][--anIntArray446[var2]];
                        aByteArrayArrayArray775[var2][anIntArray446[var2]] = null;
                        return var3;
                    }
                }
            }
            return new byte[var1];
        }
    }
}
