import java.awt.*;

class SoundPlayer {

    static int anInt769;
    static int sampleRate;
    static boolean stereo;
    static int channels;
    static SoundWhat aSoundWhat_490;
    int[] anIntArray767;
    int anInt789;
    int anInt796;
    private boolean aBoolean771 = false;
    private int anInt772 = 32;
    private BufferBase_Sub2 aClass8_Sub2_776;
    private long aLong783 = Utility.method347(-115);
    private BufferBase_Sub2[] aClass8_Sub2Array786 = new BufferBase_Sub2[8];
    private long aLong787 = 0L;
    private int anInt788;
    private BufferBase_Sub2[] aClass8_Sub2Array790 = new BufferBase_Sub2[8];
    private int anInt791 = 0;
    private long aLong792 = 0L;
    private int anInt793 = 0;
    private int anInt794 = 0;
    private int anInt795 = 0;
    private boolean aBoolean797 = true;

    static void method502(int[] var0, int var1, int var2) {
        for (var2 = var1 + var2 - 7; var1 < var2; var0[var1++] = 0) {
            var0[var1++] = 0;
            var0[var1++] = 0;
            var0[var1++] = 0;
            var0[var1++] = 0;
            var0[var1++] = 0;
            var0[var1++] = 0;
            var0[var1++] = 0;
        }

        for (var2 += 7; var1 < var2; var0[var1++] = 0) {
            ;
        }

    }

    static void method346(BufferBase_Sub2 var0, int var1) {
        if (var1 <= -4) {
            var0.aBoolean1637 = false;
            if (var0.aClass8_Sub4_1634 != null) {
                var0.aClass8_Sub4_1634.anInt1669 = 0;
            }

            for (BufferBase_Sub2 var2 = var0.method169(); var2 != null; var2 = var0.method165()) {
                method346(var2, -117);
            }

        }
    }

    static void setSettings(int var0, int channels, boolean stereo, int sampleRate) {
        if (sampleRate >= 8000 && sampleRate <= 48000) {
            if (var0 == 50000) {
                SoundPlayer.stereo = stereo;
                SoundPlayer.channels = channels;
                SoundPlayer.sampleRate = sampleRate;
            }
        } else {
            throw new IllegalArgumentException();
        }
    }


    private void method485(int var1, int var2) {
        this.anInt793 -= var2;
        if (~this.anInt793 > var1) {
            this.anInt793 = 0;
        }

        if (this.aClass8_Sub2_776 != null) {
            this.aClass8_Sub2_776.method170(var2);
        }
    }

    void close() {
    }

    void start(int var1) throws Exception {
    }

    private void method489(int[] var1, int var2) {
        int var3 = var2;
        if (stereo) {
            var3 = var2 << 1;
        }

        method502(var1, 0, var3);
        this.anInt793 -= var2;
        if (this.aClass8_Sub2_776 != null && this.anInt793 <= 0) {
            this.anInt793 += sampleRate >> 4;
            method346(this.aClass8_Sub2_776, -82);
            this.method492(this.aClass8_Sub2_776, this.aClass8_Sub2_776.method168(), 10124);
            int var4 = 0;
            int var5 = 255;

            int var7;
            label112:
            for (int var6 = 7; var5 != 0; --var6) {
                int var8;
                if (var6 < 0) {
                    var7 = var6 & 3;
                    var8 = -(var6 >> 2);
                } else {
                    var7 = var6;
                    var8 = 0;
                }

                for (int var9 = var5 >>> var7 & 286331153; var9 != 0; var9 >>>= 4) {
                    if ((var9 & 1) != 0) {
                        var5 &= ~(1 << var7);
                        BufferBase_Sub2 var10 = null;
                        BufferBase_Sub2 var11 = this.aClass8_Sub2Array786[var7];

                        while (var11 != null) {
                            BufferBase_Sub4 var12 = var11.aClass8_Sub4_1634;
                            if (var12 != null && var12.anInt1669 > var8) {
                                var5 |= 1 << var7;
                                var10 = var11;
                                var11 = var11.aClass8_Sub2_1636;
                            } else {
                                var11.aBoolean1637 = true;
                                int var13 = var11.method167();
                                var4 += var13;
                                if (var12 != null) {
                                    var12.anInt1669 += var13;
                                }

                                if (var4 >= this.anInt772) {
                                    break label112;
                                }

                                BufferBase_Sub2 var14 = var11.method169();
                                if (var14 != null) {
                                    for (int var15 = var11.anInt1635; var14 != null; var14 = var11.method165()) {
                                        this.method492(var14, var15 * var14.method168() >> 8, 10124);
                                    }
                                }

                                BufferBase_Sub2 var18 = var11.aClass8_Sub2_1636;
                                var11.aClass8_Sub2_1636 = null;
                                if (var10 == null) {
                                    this.aClass8_Sub2Array786[var7] = var18;
                                } else {
                                    var10.aClass8_Sub2_1636 = var18;
                                }

                                if (var18 == null) {
                                    this.aClass8_Sub2Array790[var7] = var10;
                                }

                                var11 = var18;
                            }
                        }
                    }

                    var7 += 4;
                    ++var8;
                }
            }

            for (var7 = 0; var7 < 8; ++var7) {
                BufferBase_Sub2 var17 = this.aClass8_Sub2Array786[var7];

                BufferBase_Sub2 var16;
                for (this.aClass8_Sub2Array786[var7] = this.aClass8_Sub2Array790[var7] = null; var17 != null; var17 = var16) {
                    var16 = var17.aClass8_Sub2_1636;
                    var17.aClass8_Sub2_1636 = null;
                }
            }
        }

        if (this.anInt793 < 0) {
            this.anInt793 = 0;
        }

        if (this.aClass8_Sub2_776 != null) {
            this.aClass8_Sub2_776.method164(var1, 0, var2);
        }

        this.aLong783 = Utility.method347(92);
    }

    final synchronized void method490(BufferBase_Sub2 var1, int var2) {
        if (var2 != 16385) {
            this.method494(-41);
        }

        this.aClass8_Sub2_776 = var1;
    }

    void init(Component var1) throws Exception {
    }

    private void method492(BufferBase_Sub2 var1, int var2, int var3) {
        int var4 = var2 >> 5;
        BufferBase_Sub2 var5 = this.aClass8_Sub2Array790[var4];
        if (var3 != 10124) {
            this.method493((byte) -92);
        }

        if (var5 != null) {
            var5.aClass8_Sub2_1636 = var1;
        } else {
            this.aClass8_Sub2Array786[var4] = var1;
        }

        this.aClass8_Sub2Array790[var4] = var1;
        var1.anInt1635 = var2;
    }

    final synchronized void method493(byte var1) {
        if (aSoundWhat_490 != null) {
            boolean var2 = true;

            for (int var3 = 0; var3 < 2; ++var3) {
                if (aSoundWhat_490.soundPlayers[var3] == this) {
                    aSoundWhat_490.soundPlayers[var3] = null;
                }

                if (aSoundWhat_490.soundPlayers[var3] != null) {
                    var2 = false;
                }
            }

            if (var2) {
                aSoundWhat_490.stopped = true;

                while (aSoundWhat_490.running) {
                    GameApplet.sleep(50L, -17239);
                }

                aSoundWhat_490 = null;
            }
        }

        this.close();
        this.anIntArray767 = null;
        if (var1 < 110) {
            this.method494(-66);
        }

        this.aBoolean771 = true;
    }

    final synchronized void method494(int var1) {
        if (!this.aBoolean771) {
            long var2 = Utility.method347(-126);

            try {
                if ((this.aLong783 - -6000L) < var2) {
                    this.aLong783 = -6000L + var2;
                }

                while (var2 > (5000L + this.aLong783)) {
                    this.method485(-1, 256);
                    this.aLong783 += (long) (256000 / sampleRate);
                    var2 = Utility.method347(-104);
                }
            } catch (Exception var8) {
                this.aLong783 = var2;
            }

            if (this.anIntArray767 != null) {
                try {
                    if (this.aLong792 != 0L) {
                        if (this.aLong792 > var2) {
                            return;
                        }

                        this.start(this.anInt789);
                        this.aBoolean797 = true;
                        this.aLong792 = 0L;
                    }

                    int var4 = this.getPosition();
                    if (this.anInt795 < -var4 + this.anInt791) {
                        this.anInt795 = this.anInt791 + -var4;
                    }

                    int var5 = this.anInt796 + this.anInt788;
                    if (var5 + 256 > 16384) {
                        var5 = 16128;
                    }

                    if (var5 + 256 > this.anInt789) {
                        this.anInt789 += 1024;
                        if (this.anInt789 > 16384) {
                            this.anInt789 = 16384;
                        }

                        this.close();
                        var4 = 0;
                        this.start(this.anInt789);
                        this.aBoolean797 = true;
                        if (this.anInt789 < 256 + var5) {
                            var5 = this.anInt789 + -256;
                            this.anInt788 = var5 + -this.anInt796;
                        }
                    }

                    while (var4 < var5) {
                        this.method489(this.anIntArray767, 256);
                        this.flush();
                        var4 += 256;
                    }

                    if (this.aLong787 < var2) {
                        if (this.aBoolean797) {
                            this.aBoolean797 = false;
                        } else {
                            if (this.anInt795 == 0 && this.anInt794 == 0) {
                                this.close();
                                this.aLong792 = 2000L + var2;
                                return;
                            }

                            this.anInt788 = Math.min(this.anInt794, this.anInt795);
                            this.anInt794 = this.anInt795;
                        }

                        this.anInt795 = 0;
                        this.aLong787 = 2000L + var2;
                    }

                    this.anInt791 = var4;
                } catch (Exception var7) {
                    this.close();
                    this.aLong792 = 2000L + var2;
                }

                if (var1 <= 20) {
                    this.aLong787 = -30L;
                }
            }
        }
    }

    int getPosition() throws Exception {
        return this.anInt789;
    }

    void flush() throws Exception {
    }

}
