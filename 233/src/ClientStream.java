import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

final class ClientStream extends ClientStreamBase implements Runnable {

    static int anInt1697;
    static int anInt1698;
    static int anInt1693;
    static int[] anIntArray1679 = new int[512];
    static String[] aStringArray1683 = new String[]{"Enter number of items to remove and press enter"};
    static int[] anIntArray1685 = new int[256];
    static int[] anIntArray1687 = new int[2048];
    static int version = 233;
    static int anInt1695 = 0;
    private static byte[] aByteArray1682 = new byte[64];
    private boolean aBoolean1684 = false;
    private InputStream anInputStream1690;
    private OutputStream anOutputStream1691;
    private Socket aSocket1699;
    private int anInt1700 = 0;
    private byte[] aByteArray1701 = new byte[1];
    private byte[] aByteArray1702;
    private int anInt1703 = 0;
    private boolean aBoolean1704 = true;


    ClientStream(Socket var1, GameApplet var2) throws IOException {
        this.aSocket1699 = var1;
        this.anInputStream1690 = var1.getInputStream();
        this.anOutputStream1691 = var1.getOutputStream();
        this.aBoolean1704 = false;
        var2.startThread(false, this);
    }

    static {
        for (int var0 = 0; var0 < 256; ++var0) {
            anIntArray1679[var0] = (int) (32768.0D * Math.sin(0.02454369D * (double) var0));
            anIntArray1679[256 + var0] = (int) (32768.0D * Math.cos(0.02454369D * (double) var0));
        }

        for (int var1 = 0; var1 < 1024; ++var1) {
            anIntArray1687[var1] = (int) (Math.sin((double) var1 * 0.00613592315D) * 32768.0D);
            anIntArray1687[var1 + 1024] = (int) (Math.cos((double) var1 * 0.00613592315D) * 32768.0D);
        }

        for (int var2 = 0; var2 < 10; ++var2) {
            aByteArray1682[var2] = (byte) (48 + var2);
        }

        for (int var3 = 0; var3 < 26; ++var3) {
            aByteArray1682[var3 + 10] = (byte) (65 + var3);
        }

        for (int var4 = 0; var4 < 26; ++var4) {
            aByteArray1682[var4 + 36] = (byte) (97 + var4);
        }

        aByteArray1682[63] = 36;
        aByteArray1682[62] = -93;

        for (int var5 = 0; var5 < 10; anIntArray1685[48 + var5] = var5++) {
            ;
        }

        for (int var6 = 0; var6 < 26; ++var6) {
            anIntArray1685[var6 + 65] = var6 + 10;
        }

        for (int var7 = 0; var7 < 26; ++var7) {
            anIntArray1685[var7 + 97] = 36 + var7;
        }

        anIntArray1685[36] = 63;
        anIntArray1685[163] = 62;
    }

    static char method478(char c) {
        if (c == 32 || c == 160 || c == 95 || c == 45) {
            return '_';
        }
        if (c == 91 || c == 93 || c == 35) {
            return c;
        }
        if ((c >= 224 && c <= 228) || (c >= 192 && c <= 196)) {
            return 'a';
        }
        if ((c >= 232 && c <= 235) || (c >= 201 && c <= 203)) {
            return 'e';
        }
        if ((c >= 237 && c <= 239) || (c >= 205 && c <= 207)) {
            return 'i';
        }
        if ((c >= 242 && c <= 246) || (c >= 210 && c <= 214)) {
            return 'o';
        }
        if ((c >= 249 && c <= 252) || (c >= 217 && c <= 220)) {
            return 'u';
        }
        if (c == 231 || c == 199) {
            return 'c';
        }
        if (c == 255 || c == 376) {
            return 'y';
        }
        if (c == 241 || c == 209) {
            return 'n';
        }
        if (c == 223) {
            return 'b';
        }
        return java.lang.Character.toLowerCase(c);
    }

    final void method471(int var1, int var2, byte[] var3) throws IOException {
        if (!this.aBoolean1684) {
            int var5 = 0;

            int var8;
            for (boolean var6 = false; var5 < var2; var5 += var8) {
                if ((var8 = this.anInputStream1690.read(var3, var1 + var5, -var5 + var2)) <= 0) {
                    throw new IOException("EOF");
                }
            }

        }
    }

    public final void run() {
        while (!this.aBoolean1704) {
            int var2;
            int var3;
            synchronized (this) {
                if (this.anInt1700 == this.anInt1703) {
                    try {
                        this.wait();
                    } catch (InterruptedException var10) {

                    }
                }

                if (aBoolean1704) {
                    /*
                    was:
                    if(!aBoolean1704) { ... break label }
                     */
                    return;
                }
                var2 = this.anInt1703;
                if (this.anInt1700 < this.anInt1703) {
                    var3 = -this.anInt1703 + 5000;
                } else {
                    var3 = -this.anInt1703 + this.anInt1700;
                }
            }

            if (var3 > 0) {
                try {
                    this.anOutputStream1691.write(this.aByteArray1702, var2, var3);
                } catch (IOException var12) {
                    super.socketException = true;
                    super.socketExceptionMessage = "Twriter:" + var12;
                }

                this.anInt1703 = (this.anInt1703 - -var3) % 5000;

                try {
                    if (this.anInt1703 == this.anInt1700) {
                        this.anOutputStream1691.flush();
                    }
                } catch (IOException var11) {
                    super.socketException = true;
                    super.socketExceptionMessage = "Twriter:" + var11;
                }
            }
        }
    }

    final void closeStream() {
        super.closeStream();
        this.aBoolean1684 = true;

        try {
            if (this.anInputStream1690 != null) {
                this.anInputStream1690.close();
            }

            if (this.anOutputStream1691 != null) {
                this.anOutputStream1691.close();
            }

            if (this.aSocket1699 != null) {
                this.aSocket1699.close();
            }
        } catch (IOException var7) {
            System.out.println("Error closing stream");
        }

        this.aBoolean1704 = true;
        synchronized (this) {
            this.notify();
        }

        this.aByteArray1702 = null;
    }

    final int availableStream() throws IOException {
        return this.aBoolean1684 ? 0 : this.anInputStream1690.available();
    }

    final int read() throws IOException {
        if (this.aBoolean1684) {
            return 0;
        } else {
            this.method471(0, 1, this.aByteArray1701);
            return 255 & this.aByteArray1701[0];
        }
    }

    final void method476(int var1, byte[] var2, int var4) throws IOException {
        if (!this.aBoolean1684) {
            if (this.aByteArray1702 == null) {
                this.aByteArray1702 = new byte[5000];
            }

            synchronized (this) {
                for (int var6 = 0; var1 > var6; ++var6) {
                    this.aByteArray1702[this.anInt1700] = var2[var4 + var6];
                    this.anInt1700 = (1 + this.anInt1700) % 5000;
                    if (this.anInt1700 == ((4900 + this.anInt1703) % 5000)) {
                        throw new IOException("buffer overflow");
                    }
                }

                this.notify();
                return;
            }

        }
    }
}
