import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;

final class GameModel {

    static int anInt1031;
    boolean aBoolean922 = false;
    int[] faceFillFront;
    int[][] faceVertices;
    boolean aBoolean936 = true;
    boolean aBoolean938 = false;
    int[] anIntArray939;
    byte[] aByteArray942;
    int[] anIntArray950;
    int[] faceNumVertices;
    byte[] aByteArray956;
    int numFaces = 1;
    int[] anIntArray961;
    boolean transparent = false;
    int[] anIntArray973;
    int numVertices;
    int[] faceIntensity;
    int[] vertexZ;
    int anInt985 = 32;
    int transformState;
    int[] anIntArray992;
    int[] anIntArray994;
    int[] vertexX;
    int[] faceFillBack;
    int[] anIntArray1001;
    int[] anIntArray1003;
    int key = -1;
    int[] anIntArray1008;
    int anInt1009 = 0;
    private int[] anIntArray915;
    private boolean aBoolean916 = false;
    private int anInt917;
    private int anInt918;
    private int anInt919;
    private int anInt921;
    private int[] anIntArray923;
    private int anInt926;
    private int anInt928;
    private int anInt929;
    private int[] anIntArray930;
    private int[] anIntArray931;
    private int[] anIntArray933;
    private int anInt934;
    private int anInt937;
    private int[] anIntArray941;
    private int anInt943 = 12345678;
    private boolean aBoolean944 = false;
    private int[] vertexY;
    private int[][] anIntArrayArray948;
    private int anInt951;
    private int anInt952 = 256;
    private int anInt954 = 95;
    private int anInt960 = 180;
    private int anInt962 = 512;
    private int anInt964;
    private int[] anIntArray965;
    private int anInt967;
    private int anInt970;
    private int anInt971;
    private int anInt972 = 155;
    private int anInt976;
    private int magic = 12345678;
    private boolean aBoolean978 = false;
    private int[] anIntArray979;
    private int[] anIntArray982;
    private boolean aBoolean984 = false;
    private int anInt986;
    private int anInt987;
    private int anInt990;
    private int[] anIntArray991;
    private int anInt1005;
    private int anInt1010;
    private int[] anIntArray1011;
    private int anInt1012;
    private int anInt1017;
    private int anInt1019;
    private int[] anIntArray1020;
    private int anInt1026;
    private int anInt1027;

    GameModel(int var1, int var2) {
        this.allocate(22745, var2, var1);
        this.anIntArrayArray948 = new int[var2][1];

        for (int var3 = 0; var3 < var2; this.anIntArrayArray948[var3][0] = var3++) {
            ;
        }
    }

    GameModel(int var1, int var2, boolean var3, boolean var4, boolean var5, boolean var6, boolean var7) {
        this.aBoolean944 = var4;
        this.aBoolean978 = var7;
        this.aBoolean984 = var3;
        this.aBoolean922 = var6;
        this.aBoolean916 = var5;
        this.allocate(22745, var2, var1);
    }

    GameModel(byte[] data, int offset, boolean unused) {
        int var4 = Utility.getUnsignedShort(offset, data, 55);
        offset += 2;
        int var5 = Utility.getUnsignedShort(offset, data, 113);
        this.allocate(22745, var5, var4);
        offset += 2;
        this.anIntArrayArray948 = new int[var5][1];

        for (int var6 = 0; var6 < var4; ++var6) {
            this.vertexX[var6] = getSignedShort(data, (byte) -20, offset);
            offset += 2;
        }

        for (int var7 = 0; var7 < var4; ++var7) {
            this.vertexY[var7] = getSignedShort(data, (byte) -20, offset);
            offset += 2;
        }

        for (int var8 = 0; var4 > var8; ++var8) {
            this.vertexZ[var8] = getSignedShort(data, (byte) -20, offset);
            offset += 2;
        }

        this.numVertices = var4;

        for (int var9 = 0; var5 > var9; ++var9) {
            this.faceNumVertices[var9] = Utility.bitwiseAnd(255, data[offset++]);
        }

        for (int var10 = 0; var5 > var10; ++var10) {
            this.faceFillFront[var10] = getSignedShort(data, (byte) -20, offset);
            offset += 2;
            if (this.faceFillFront[var10] == 32767) {
                this.faceFillFront[var10] = this.magic;
            }
        }

        for (int var11 = 0; var5 > var11; ++var11) {
            this.faceFillBack[var11] = getSignedShort(data, (byte) -20, offset);
            if (this.faceFillBack[var11] == 32767) {
                this.faceFillBack[var11] = this.magic;
            }

            offset += 2;
        }

        int var13;
        for (int var12 = 0; var5 > var12; ++var12) {
            var13 = 255 & data[offset++];
            if (var13 == 0) {
                this.faceIntensity[var12] = 0;
            } else {
                this.faceIntensity[var12] = this.magic;
            }
        }

        for (var13 = 0; var5 > var13; ++var13) {
            this.faceVertices[var13] = new int[this.faceNumVertices[var13]];

            for (int var14 = 0; this.faceNumVertices[var13] > var14; ++var14) {
                if (var4 >= 256) {
                    this.faceVertices[var13][var14] = Utility.getUnsignedShort(offset, data, 121);
                    offset += 2;
                } else {
                    this.faceVertices[var13][var14] = Utility.bitwiseAnd(data[offset++], 255);
                }
            }
        }

        this.numFaces = 1;
        this.transformState = var5;
    }


    GameModel(String var1) {
        boolean var2 = false;
        boolean var3 = false;
        Object var4 = null;

        byte[] var26;
        try {
            InputStream var5 = Utility.method545(var1, -32341);
            DataInputStream var6 = new DataInputStream(var5);
            var26 = new byte[3];
            this.anInt1027 = 0;

            int var24;
            for (var24 = 0; var24 < 3; var24 += var6.read(var26, var24, 3 - var24)) {
                ;
            }

            int var25 = this.method579(var26, (byte) -27);
            this.anInt1027 = 0;
            var26 = new byte[var25];
            var24 = 0;

            while (true) {
                if (var25 <= var24) {
                    var6.close();
                    break;
                }

                var24 += var6.read(var26, var24, -var24 + var25);
            }
        } catch (IOException var22) {
            this.numVertices = 0;
            this.transformState = 0;
            return;
        }

        int var27 = this.method579(var26, (byte) -27);
        int var28 = this.method579(var26, (byte) -27);
        this.allocate(22745, var28, var27);
        boolean var7 = false;
        this.anIntArrayArray948 = new int[var28][];

        for (int var8 = 0; var8 < var27; ++var8) {
            int var9 = this.method579(var26, (byte) -27);
            int var10 = this.method579(var26, (byte) -27);
            int var11 = this.method579(var26, (byte) -27);
            this.method561(var9, (byte) 126, var10, var11);
        }

        for (int var12 = 0; var12 < var28; ++var12) {
            int var13 = this.method579(var26, (byte) -27);
            int var14 = this.method579(var26, (byte) -27);
            int var15 = this.method579(var26, (byte) -27);
            int var16 = this.method579(var26, (byte) -27);
            this.anInt962 = this.method579(var26, (byte) -27);
            this.anInt985 = this.method579(var26, (byte) -27);
            int var29 = this.method579(var26, (byte) -27);
            int[] var17 = new int[var13];

            for (int var18 = 0; var13 > var18; ++var18) {
                var17[var18] = this.method579(var26, (byte) -27);
            }

            int[] var19 = new int[var16];

            for (int var20 = 0; var16 > var20; ++var20) {
                var19[var20] = this.method579(var26, (byte) -27);
            }

            int var21 = this.method549(var13, var15, (byte) -104, var17, var14);
            this.anIntArrayArray948[var12] = var19;
            if (var29 == 0) {
                this.faceIntensity[var21] = 0;
            } else {
                this.faceIntensity[var21] = this.magic;
            }
        }

        this.numFaces = 1;
    }

    private GameModel(GameModel[] var1, int var2, boolean var3, boolean var4, boolean var5, boolean var6) {
        this.aBoolean916 = var5;
        this.aBoolean984 = var3;
        this.aBoolean944 = var4;
        this.aBoolean922 = var6;
        this.method577(false, var1, 1, var2);
    }

    private GameModel(GameModel[] var1, int var2) {
        this.method577(true, var1, 1, var2);
    }

    static int getSignedShort(byte[] var0, byte var1, int var2) {
        int var3 = method345(var0[var2], var1 ^ 96) * 256 - -method345(var0[1 + var2], var1 ^ 101);
        if (var3 > 32767) {
            var3 -= 65536;
        }

        return var3;
    }

    static int method599(byte[] var0, int var2) {
        return Utility.getnum(var2, 0, var0);
    }

    static int method345(byte var0, int var1) {
        if (var1 >= -115) {
            GameApplet.loadingProgressText2 = null;
        }

        return 255 & var0;
    }

    private void method546(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        for (int var8 = 0; var8 < this.numVertices; ++var8) {
            if (var7 != 0) {
                this.anIntArray915[var8] += this.anIntArray991[var8] * var7 >> 8;
            }

            if (var1 != 0) {
                this.anIntArray982[var8] += this.anIntArray991[var8] * var1 >> 8;
            }

            if (var2 != 0) {
                this.anIntArray915[var8] += this.anIntArray982[var8] * var2 >> 8;
            }

            if (var3 != 0) {
                this.anIntArray991[var8] += this.anIntArray982[var8] * var3 >> 8;
            }

            if (var5 != 0) {
                this.anIntArray982[var8] += this.anIntArray915[var8] * var5 >> 8;
            }

            if (var6 != 0) {
                this.anIntArray991[var8] += var6 * this.anIntArray915[var8] >> 8;
            }
        }

        if (var4 == 2) {
        }
    }

    final void method548(int var1, int var2, byte var3, int var4, int var5, int var6, int var7, int var8, int var9) {
        if (var3 != 76) {
            this.method565(11, (byte) -113, 76, 118);
        }

        this.method571(false);
        if (this.anInt987 <= World.anInt273 && anInt1031 <= this.anInt986 && this.anInt951 <= BufferBase_Sub3_Sub1.anInt1741 && Scene.anInt762 <= this.anInt971 && SoundPlayer.anInt769 >= this.anInt964 && this.anInt1012 >= Scene.anInt465) {
            this.aBoolean936 = true;
            int var10 = 0;
            int var11 = 0;
            int var12 = 0;
            int var13 = 0;
            int var14 = 0;
            int var15 = 0;
            if (var7 != 0) {
                var10 = ClientStream.anIntArray1687[var7];
                var11 = ClientStream.anIntArray1687[1024 + var7];
            }

            if (var8 != 0) {
                var12 = ClientStream.anIntArray1687[var8];
                var13 = ClientStream.anIntArray1687[var8 - -1024];
            }

            if (var6 != 0) {
                var14 = ClientStream.anIntArray1687[var6];
                var15 = ClientStream.anIntArray1687[1024 + var6];
            }

            for (int var16 = 0; var16 < this.numVertices; ++var16) {
                int var17 = -var4 + this.anIntArray915[var16];
                int var18 = -var5 + this.anIntArray991[var16];
                int var19;
                if (var7 != 0) {
                    var19 = var10 * var18 - -(var17 * var11) >> 15;
                    var18 = var18 * var11 - var10 * var17 >> 15;
                    var17 = var19;
                }

                int var20 = this.anIntArray982[var16] - var2;
                if (var6 != 0) {
                    var19 = var17 * var15 + var20 * var14 >> 15;
                    var20 = -(var17 * var14) + var15 * var20 >> 15;
                    var17 = var19;
                }

                if (var8 != 0) {
                    var19 = -(var12 * var20) + var13 * var18 >> 15;
                    var20 = var13 * var20 + var12 * var18 >> 15;
                    var18 = var19;
                }

                if (var1 > var20) {
                    this.anIntArray1001[var16] = var17 << var9;
                } else {
                    this.anIntArray1001[var16] = (var17 << var9) / var20;
                }

                if (var20 < var1) {
                    this.anIntArray994[var16] = var18 << var9;
                } else {
                    this.anIntArray994[var16] = (var18 << var9) / var20;
                }

                this.anIntArray961[var16] = var17;
                this.anIntArray992[var16] = var18;
                this.anIntArray973[var16] = var20;
            }

        } else {
            this.aBoolean936 = false;
        }
    }

    final int method549(int var1, int var2, byte var3, int[] var4, int var5) {
        if (this.anInt1010 <= this.transformState) {
            return -1;
        } else {
            this.faceNumVertices[this.transformState] = var1;
            this.faceVertices[this.transformState] = var4;
            this.faceFillFront[this.transformState] = var5;
            this.faceFillBack[this.transformState] = var2;
            this.numFaces = 1;
            return var3 > -90 ? -53 : this.transformState++;
        }
    }

    private void allocate(int var1, int var2, int var3) {
        this.anIntArray1003 = new int[var2];
        this.vertexX = new int[var3];
        if (!this.aBoolean978) {
            this.anIntArray973 = new int[var3];
            this.anIntArray961 = new int[var3];
            this.anIntArray994 = new int[var3];
            this.anIntArray1001 = new int[var3];
            this.anIntArray992 = new int[var3];
        }

        this.aByteArray942 = new byte[var3];
        if (var1 != 22745) {
            this.method556((byte) 114);
        }

        this.faceFillBack = new int[var2];
        this.vertexY = new int[var3];
        this.faceNumVertices = new int[var2];
        this.anIntArray939 = new int[var3];
        this.anIntArray1008 = new int[var2];
        this.faceIntensity = new int[var2];
        this.faceVertices = new int[var2][];
        this.vertexZ = new int[var3];
        if (!this.aBoolean922) {
            this.anIntArray950 = new int[var2];
            this.aByteArray956 = new byte[var2];
        }

        this.faceFillFront = new int[var2];
        this.anInt1017 = var3;
        this.anInt934 = this.anInt919 = this.anInt1019 = 0;
        this.anInt1005 = 0;
        if (!this.aBoolean944) {
            this.anIntArray1020 = new int[var2];
            this.anIntArray930 = new int[var2];
            this.anIntArray933 = new int[var2];
            this.anIntArray965 = new int[var2];
            this.anIntArray931 = new int[var2];
            this.anIntArray941 = new int[var2];
        }

        this.anInt1010 = var2;
        this.anInt928 = this.anInt970 = this.anInt967 = 0;
        this.anInt917 = this.anInt1026 = this.anInt921 = this.anInt918 = this.anInt990 = this.anInt937 = 256;
        if (!this.aBoolean916 || !this.aBoolean944) {
            this.anIntArray923 = new int[var2];
            this.anIntArray979 = new int[var2];
            this.anIntArray1011 = new int[var2];
        }

        this.transformState = 0;
        if (this.aBoolean984) {
            this.anIntArray991 = this.vertexY;
            this.anIntArray982 = this.vertexZ;
            this.anIntArray915 = this.vertexX;
        } else {
            this.anIntArray982 = new int[var3];
            this.anIntArray991 = new int[var3];
            this.anIntArray915 = new int[var3];
        }

        this.anInt929 = this.anInt976 = this.anInt926 = 256;
        this.numVertices = 0;
    }

    private void method551(int var1, int var2, int var3, int var4) {
        if (var3 != -28982) {
            this.anIntArray941 = null;
        }

        for (int var5 = 0; this.numVertices > var5; ++var5) {
            this.anIntArray915[var5] = var4 * this.anIntArray915[var5] >> 8;
            this.anIntArray991[var5] = this.anIntArray991[var5] * var1 >> 8;
            this.anIntArray982[var5] = this.anIntArray982[var5] * var2 >> 8;
        }
    }

    private void method552(byte var1) {
        if (var1 < 15) {
            this.method549(-74, 113, (byte) -102, (int[]) null, 14);
        }

        if (!this.aBoolean916) {
            int var2 = this.anInt962 * this.anInt952 >> 8;

            for (int var3 = 0; var3 < this.transformState; ++var3) {
                if (this.magic != this.faceIntensity[var3]) {
                    this.faceIntensity[var3] = (this.anInt960 * this.anIntArray923[var3] - -(this.anInt972 * this.anIntArray1011[var3]) + this.anInt954 * this.anIntArray979[var3]) / var2;
                }
            }

            int[] var4 = new int[this.numVertices];
            int[] var5 = new int[this.numVertices];
            int[] var6 = new int[this.numVertices];
            int[] var7 = new int[this.numVertices];

            for (int var8 = 0; this.numVertices > var8; ++var8) {
                var4[var8] = 0;
                var5[var8] = 0;
                var6[var8] = 0;
                var7[var8] = 0;
            }

            int var10;
            for (int var9 = 0; this.transformState > var9; ++var9) {
                if (this.magic == this.faceIntensity[var9]) {
                    for (var10 = 0; var10 < this.faceNumVertices[var9]; ++var10) {
                        int var11 = this.faceVertices[var9][var10];
                        var4[var11] += this.anIntArray923[var9];
                        var5[var11] += this.anIntArray1011[var9];
                        var6[var11] += this.anIntArray979[var9];
                        ++var7[var11];
                    }
                }
            }

            for (var10 = 0; this.numVertices > var10; ++var10) {
                if (var7[var10] > 0) {
                    this.anIntArray939[var10] = (this.anInt954 * var6[var10] + this.anInt960 * var4[var10] + var5[var10] * this.anInt972) / (var2 * var7[var10]);
                }
            }

        }
    }

    private void method553(byte var1) {
        if (var1 == -80) {
            this.method571(false);

            for (int var2 = 0; var2 < this.numVertices; ++var2) {
                this.vertexX[var2] = this.anIntArray915[var2];
                this.vertexY[var2] = this.anIntArray991[var2];
                this.vertexZ[var2] = this.anIntArray982[var2];
            }

            this.anInt1005 = 0;
            this.anInt928 = this.anInt970 = this.anInt967 = 0;
            this.anInt934 = this.anInt919 = this.anInt1019 = 0;
            this.anInt917 = this.anInt1026 = this.anInt921 = this.anInt918 = this.anInt990 = this.anInt937 = 256;
            this.anInt929 = this.anInt976 = this.anInt926 = 256;
        }
    }

    final void method554(int var1, int var2, int var3, int var4) {
        if (!this.aBoolean916) {
            if (var3 != -243635764) {
                this.anIntArray965 = null;
            }

            this.anInt972 = var1;
            this.anInt960 = var4;
            this.anInt954 = var2;
            this.anInt952 = (int) Math.sqrt((double) (var2 * var2 + var4 * var4 + var1 * var1));
            this.method552((byte) 26);
        }
    }

    private void method556(byte var1) {
        if (!this.aBoolean916 || !this.aBoolean944) {
            for (int var2 = 0; var2 < this.transformState; ++var2) {
                int[] var3 = this.faceVertices[var2];
                int var4 = this.anIntArray915[var3[0]];
                int var5 = this.anIntArray991[var3[0]];
                int var6 = this.anIntArray982[var3[0]];
                int var7 = -var4 + this.anIntArray915[var3[1]];
                int var8 = -var5 + this.anIntArray991[var3[1]];
                int var9 = this.anIntArray982[var3[1]] + -var6;
                int var10 = -var4 + this.anIntArray915[var3[2]];
                int var11 = -var5 + this.anIntArray991[var3[2]];
                int var12 = this.anIntArray982[var3[2]] + -var6;
                int var13 = -(var11 * var9) + var12 * var8;
                int var14 = -(var7 * var12) + var9 * var10;

                int var15;
                for (var15 = -(var10 * var8) + var11 * var7; var13 > 8192 || var14 > 8192 || var15 > 8192 || var13 < -8192 || var14 < -8192 || var15 < -8192; var13 >>= 1) {
                    var14 >>= 1;
                    var15 >>= 1;
                }

                int var16 = (int) (Math.sqrt((double) (var15 * var15 + var14 * var14 + var13 * var13)) * 256.0D);
                if (var16 <= 0) {
                    var16 = 1;
                }

                this.anIntArray923[var2] = var13 * 65536 / var16;
                this.anIntArray1011[var2] = var14 * 65536 / var16;
                this.anIntArray979[var2] = '\uffff' * var15 / var16;
                this.anIntArray1003[var2] = -1;
            }

            this.method552((byte) 87);
            if (var1 != 54) {
                this.method580((byte) 78);
            }
        }
    }

    final GameModel[] method557(int var1, int var2, int var3, int var4, boolean var5, int var6, int var7, int var8, int var9) {
        this.method553((byte) -80);
        int[] var10 = new int[var7];
        int[] var11 = new int[var7];

        for (int var12 = 0; var7 > var12; ++var12) {
            var10[var12] = 0;
            var11[var12] = 0;
        }

        int var15;
        int var16;
        int var19;
        int var18;
        for (int var13 = 0; var13 < this.transformState; ++var13) {
            int var14 = 0;
            var15 = 0;
            var16 = this.faceNumVertices[var13];
            int[] var17 = this.faceVertices[var13];

            for (var18 = 0; var18 < var16; ++var18) {
                var15 += this.vertexZ[var17[var18]];
                var14 += this.vertexX[var17[var18]];
            }

            var19 = var15 / (var16 * var9) * var8 + var14 / (var4 * var16);
            var10[var19] += var16;
            ++var11[var19];
        }

        GameModel[] var24 = new GameModel[var7];

        for (var15 = var6; var7 > var15; ++var15) {
            if (var3 < var10[var15]) {
                var10[var15] = var3;
            }

            var24[var15] = new GameModel(var10[var15], var11[var15], true, true, true, var5, true);
            var24[var15].anInt962 = this.anInt962;
            var24[var15].anInt985 = this.anInt985;
        }

        int var25;
        for (var16 = 0; var16 < this.transformState; ++var16) {
            var25 = 0;
            var18 = 0;
            var19 = this.faceNumVertices[var16];
            int[] var20 = this.faceVertices[var16];

            for (int var21 = 0; var21 < var19; ++var21) {
                var25 += this.vertexX[var20[var21]];
                var18 += this.vertexZ[var20[var21]];
            }

            int var22 = var18 / (var9 * var19) * var8 + var25 / (var19 * var4);
            this.method564(var19, var16, var6 ^ 45, var20, var24[var22]);
        }

        for (var25 = 0; var25 < var7; ++var25) {
            var24[var25].method566(256);
        }

        return var24;
    }

    final void method558(int var1, int var2, int var3, boolean var4) {
        this.anInt967 = var3 & 255;
        this.anInt928 = var1 & 255;
        this.anInt970 = 255 & var2;
        this.method575(1);
        this.numFaces = 1;
        if (var4) {
            this.method548(73, 24, (byte) -82, -69, 45, 90, -31, -115, 34);
        }
    }

    private void method559(int var1, int var2, int var3, int var4) {
        if (var3 != 63) {
            this.method549(15, -90, (byte) -113, (int[]) null, 35);
        }

        for (int var5 = 0; this.numVertices > var5; ++var5) {
            int var6;
            int var7;
            int var8;
            if (var1 != 0) {
                var6 = ClientStream.anIntArray1679[var1];
                var7 = ClientStream.anIntArray1679[var1 + 256];
                var8 = var6 * this.anIntArray991[var5] - -(var7 * this.anIntArray915[var5]) >> 15;
                this.anIntArray991[var5] = -(this.anIntArray915[var5] * var6) + var7 * this.anIntArray991[var5] >> 15;
                this.anIntArray915[var5] = var8;
            }

            if (var4 != 0) {
                var7 = ClientStream.anIntArray1679[256 + var4];
                var6 = ClientStream.anIntArray1679[var4];
                var8 = -(this.anIntArray982[var5] * var6) + var7 * this.anIntArray991[var5] >> 15;
                this.anIntArray982[var5] = var6 * this.anIntArray991[var5] - -(this.anIntArray982[var5] * var7) >> 15;
                this.anIntArray991[var5] = var8;
            }

            if (var2 != 0) {
                var7 = ClientStream.anIntArray1679[256 + var2];
                var6 = ClientStream.anIntArray1679[var2];
                var8 = var6 * this.anIntArray982[var5] - -(this.anIntArray915[var5] * var7) >> 15;
                this.anIntArray982[var5] = var7 * this.anIntArray982[var5] - var6 * this.anIntArray915[var5] >> 15;
                this.anIntArray915[var5] = var8;
            }
        }
    }

    final void method560(int var1, int var2, boolean var3) {
        this.aByteArray942[var2] = (byte) var1;
        if (!var3) {
            this.method570(-70, 49, 81);
        }
    }

    final int method561(int var1, byte var2, int var3, int var4) {
        for (int var5 = 0; this.numVertices > var5; ++var5) {
            if (var1 == this.vertexX[var5] && var3 == this.vertexY[var5] && this.vertexZ[var5] == var4) {
                return var5;
            }
        }

        if (this.numVertices >= this.anInt1017) {
            return -1;
        } else {
            this.vertexX[this.numVertices] = var1;
            this.vertexY[this.numVertices] = var3;
            this.vertexZ[this.numVertices] = var4;
            if (var2 < 80) {
                this.anIntArray994 = null;
            }

            return this.numVertices++;
        }
    }

    final void setLight(int var1, int var2, boolean var3, byte var4, int var5, int var6, int var7) {
        this.anInt962 = (64 - var5) * 16 + 128;
        this.anInt985 = -(4 * var2) + 256;
        if (!this.aBoolean916) {
            int var8 = 0;
            if (var4 > -9) {
                this.method566(92);
            }

            for (; var8 < this.transformState; ++var8) {
                if (!var3) {
                    this.faceIntensity[var8] = 0;
                } else {
                    this.faceIntensity[var8] = this.magic;
                }
            }

            this.anInt960 = var6;
            this.anInt954 = var7;
            this.anInt972 = var1;
            this.anInt952 = (int) Math.sqrt((double) (var1 * var1 + var6 * var6 + var7 * var7));
            this.method552((byte) 41);
        }
    }

    private void method563(int var1) {
        if (var1 == 1) {
            this.anInt943 = this.anInt971 = this.anInt1012 = this.anInt986 = -999999;
            this.anInt951 = this.anInt964 = this.anInt987 = 999999;

            for (int var2 = 0; var2 < this.transformState; ++var2) {
                int[] var3 = this.faceVertices[var2];
                int var4 = this.faceNumVertices[var2];
                int var5 = var3[0];
                int var6;
                int var7 = var6 = this.anIntArray915[var5];
                int var8;
                int var9 = var8 = this.anIntArray991[var5];
                int var10;
                int var11 = var10 = this.anIntArray982[var5];

                for (int var12 = 0; var12 < var4; ++var12) {
                    var5 = var3[var12];
                    if (var6 > this.anIntArray915[var5]) {
                        var6 = this.anIntArray915[var5];
                    } else if (var7 < this.anIntArray915[var5]) {
                        var7 = this.anIntArray915[var5];
                    }

                    if (var10 > this.anIntArray982[var5]) {
                        var10 = this.anIntArray982[var5];
                    } else if (this.anIntArray982[var5] > var11) {
                        var11 = this.anIntArray982[var5];
                    }

                    if (var8 > this.anIntArray991[var5]) {
                        var8 = this.anIntArray991[var5];
                    } else if (this.anIntArray991[var5] > var9) {
                        var9 = this.anIntArray991[var5];
                    }
                }

                if (!this.aBoolean944) {
                    this.anIntArray930[var2] = var6;
                    this.anIntArray941[var2] = var7;
                    this.anIntArray933[var2] = var8;
                    this.anIntArray931[var2] = var9;
                    this.anIntArray965[var2] = var10;
                    this.anIntArray1020[var2] = var11;
                }

                if (var7 + -var6 > this.anInt943) {
                    this.anInt943 = -var6 + var7;
                }

                if ((-var8 + var9) > this.anInt943) {
                    this.anInt943 = var9 + -var8;
                }

                if (this.anInt987 > var10) {
                    this.anInt987 = var10;
                }

                if (this.anInt964 > var8) {
                    this.anInt964 = var8;
                }

                if (var7 > this.anInt971) {
                    this.anInt971 = var7;
                }

                if ((var11 - var10) > this.anInt943) {
                    this.anInt943 = var11 + -var10;
                }

                if (this.anInt986 < var11) {
                    this.anInt986 = var11;
                }

                if (var9 > this.anInt1012) {
                    this.anInt1012 = var9;
                }

                if (var6 < this.anInt951) {
                    this.anInt951 = var6;
                }
            }

        }
    }

    private void method564(int var1, int var2, int var3, int[] var4, GameModel var5) {
        int[] var6 = new int[var1];

        int var8;
        for (int var7 = 0; var1 > var7; ++var7) {
            var8 = var6[var7] = var5.method561(this.vertexX[var4[var7]], (byte) 109, this.vertexY[var4[var7]], this.vertexZ[var4[var7]]);
            var5.anIntArray939[var8] = this.anIntArray939[var4[var7]];
            var5.aByteArray942[var8] = this.aByteArray942[var4[var7]];
        }

        if (var3 > 16) {
            var8 = var5.method549(var1, this.faceFillBack[var2], (byte) -123, var6, this.faceFillFront[var2]);
            if (!var5.aBoolean922 && !this.aBoolean922) {
                var5.anIntArray950[var8] = this.anIntArray950[var2];
            }

            var5.faceIntensity[var8] = this.faceIntensity[var2];
            var5.anIntArray1003[var8] = this.anIntArray1003[var2];
            var5.anIntArray1008[var8] = this.anIntArray1008[var2];
        }
        ;
    }

    private void method565(int var1, byte var2, int var3, int var4) {
        int var5 = -122 % ((-8 - var2) / 63);

        for (int var6 = 0; var6 < this.numVertices; ++var6) {
            this.anIntArray915[var6] += var4;
            this.anIntArray991[var6] += var1;
            this.anIntArray982[var6] += var3;
        }
    }

    private void method566(int var1) {
        if (var1 == 256) {
            this.anIntArray961 = new int[this.numVertices];
            this.anIntArray1001 = new int[this.numVertices];
            this.anIntArray973 = new int[this.numVertices];
            this.anIntArray992 = new int[this.numVertices];
            this.anIntArray994 = new int[this.numVertices];
        }
    }

    final void method567(int var1, boolean var2, int var3, int var4, int var5, int var6) {
        this.anInt985 = -(4 * var4) + 256;
        this.anInt962 = -(16 * var6) + 1152;
        if (!this.aBoolean916) {
            this.anInt972 = var1;
            if (!var2) {
                this.setLight(31, -104, false, (byte) 23, 104, 112, 101);
            }

            this.anInt954 = var3;
            this.anInt960 = var5;
            this.anInt952 = (int) Math.sqrt((double) (var3 * var3 + var5 * var5 - -(var1 * var1)));
            this.method552((byte) 28);
        }
    }

    final void translate(int var1, int var2, byte var3, int var4) {
        this.anInt919 += var1;
        int var5 = 46 % ((var3 - -29) / 62);
        this.anInt934 += var4;
        this.anInt1019 += var2;
        this.method575(1);
        this.numFaces = 1;
    }

    final GameModel method569(boolean var1, byte var2, boolean var3, boolean var4, boolean var5) {
        GameModel[] var6 = new GameModel[]{this};
        GameModel var7 = new GameModel(var6, 1, var4, var1, var3, var5);
        var7.anInt1009 = this.anInt1009;
        int var8 = 64 / ((var2 - 20) / 59);
        return var7;
    }

    final void method570(int var1, int var2, int var3) {
        this.transformState -= var1;
        if (this.transformState < 0) {
            this.transformState = 0;
        }

        if (var3 != -23817) {
            this.anInt934 = -102;
        }

        this.numVertices -= var2;
        if (this.numVertices < 0) {
            this.numVertices = 0;
        }
    }

    private void method571(boolean var1) {
        if (var1) {
            this.method558(79, 66, 8, true);
        }

        int var2;
        if (this.numFaces == 2) {
            this.numFaces = 0;

            for (var2 = 0; this.numVertices > var2; ++var2) {
                this.anIntArray915[var2] = this.vertexX[var2];
                this.anIntArray991[var2] = this.vertexY[var2];
                this.anIntArray982[var2] = this.vertexZ[var2];
            }

            this.anInt943 = this.anInt971 = this.anInt1012 = this.anInt986 = 9999999;
            this.anInt951 = this.anInt964 = this.anInt987 = -9999999;
        } else if (this.numFaces == 1) {
            this.numFaces = 0;

            for (var2 = 0; var2 < this.numVertices; ++var2) {
                this.anIntArray915[var2] = this.vertexX[var2];
                this.anIntArray991[var2] = this.vertexY[var2];
                this.anIntArray982[var2] = this.vertexZ[var2];
            }

            if (this.anInt1005 >= 2) {
                this.method559(this.anInt967, this.anInt970, 63, this.anInt928);
            }

            if (this.anInt1005 >= 3) {
                this.method551(this.anInt976, this.anInt926, -28982, this.anInt929);
            }

            if (this.anInt1005 >= 4) {
                this.method546(this.anInt1026, this.anInt921, this.anInt918, 2, this.anInt990, this.anInt937, this.anInt917);
            }

            if (this.anInt1005 >= 1) {
                this.method565(this.anInt919, (byte) -87, this.anInt1019, this.anInt934);
            }

            this.method563(1);
            this.method556((byte) 54);
        }
        ;
    }

    final void rotate(int var1, int var2, int var3, int var4) {
        if (var1 < 39) {
            this.method567(124, true, 95, 47, -63, 105);
        }

        this.anInt970 = 255 & var4 + this.anInt970;
        this.anInt967 = 255 & var3 + this.anInt967;
        this.anInt928 = 255 & var2 + this.anInt928;
        this.method575(1);
        this.numFaces = 1;
    }

    final void method573(int var1, int var2, int var3, byte var4) {
        this.anInt1019 = var1;
        this.anInt934 = var2;
        this.anInt919 = var3;
        this.method575(1);
        this.numFaces = 1;
        if (var4 != -83) {
            this.method577(true, (GameModel[]) null, -45, -96);
        }
    }

    final int method574(int var1, int var2, int var3, int var4) {
        if (this.numVertices >= this.anInt1017) {
            return -1;
        } else {
            this.vertexX[this.numVertices] = var2;
            this.vertexY[this.numVertices] = var3;
            this.vertexZ[this.numVertices] = var1;
            if (var4 != -1) {
                this.aBoolean916 = true;
            }

            return this.numVertices++;
        }
    }

    private void method575(int var1) {
        if (var1 == 1) {
            if (this.anInt917 == 256 && this.anInt1026 == 256 && this.anInt921 == 256 && this.anInt918 == 256 && this.anInt990 == 256 && this.anInt937 == 256) {
                if (this.anInt929 == 256 && this.anInt976 == 256 && this.anInt926 == 256) {
                    if (this.anInt928 == 0 && this.anInt970 == 0 && this.anInt967 == 0) {
                        if (this.anInt934 == 0 && this.anInt919 == 0 && this.anInt1019 == 0) {
                            this.anInt1005 = 0;
                        } else {
                            this.anInt1005 = 1;
                        }
                    } else {
                        this.anInt1005 = 2;
                    }
                } else {
                    this.anInt1005 = 3;
                }
            } else {
                this.anInt1005 = 4;
            }
        }
    }

    final GameModel copy(int var1) {
        GameModel[] var2 = new GameModel[var1];
        var2[0] = this;
        GameModel var3 = new GameModel(var2, 1);
        var3.transparent = this.transparent;
        var3.anInt1009 = this.anInt1009;
        return var3;
    }

    private void method577(boolean var1, GameModel[] var2, int var3, int var4) {
        int var5 = 0;
        int var6 = 0;

        for (int var7 = 0; var4 > var7; ++var7) {
            var6 += var2[var7].numVertices;
            var5 += var2[var7].transformState;
        }

        this.allocate(22745, var5, var6);
        if (var1) {
            this.anIntArrayArray948 = new int[var5][];
        }

        for (int var8 = 0; var8 < var4; ++var8) {
            GameModel var9 = var2[var8];
            var9.method553((byte) -80);
            this.anInt954 = var9.anInt954;
            this.anInt962 = var9.anInt962;
            this.anInt985 = var9.anInt985;
            this.anInt972 = var9.anInt972;
            this.anInt952 = var9.anInt952;
            this.anInt960 = var9.anInt960;

            for (int var10 = 0; var9.transformState > var10; ++var10) {
                int[] var11 = new int[var9.faceNumVertices[var10]];
                int[] var12 = var9.faceVertices[var10];

                for (int var13 = 0; var13 < var9.faceNumVertices[var10]; ++var13) {
                    var11[var13] = this.method561(var9.vertexX[var12[var13]], (byte) 85, var9.vertexY[var12[var13]], var9.vertexZ[var12[var13]]);
                }

                int var14 = this.method549(var9.faceNumVertices[var10], var9.faceFillBack[var10], (byte) -122, var11, var9.faceFillFront[var10]);
                this.faceIntensity[var14] = var9.faceIntensity[var10];
                this.anIntArray1003[var14] = var9.anIntArray1003[var10];
                this.anIntArray1008[var14] = var9.anIntArray1008[var10];
                if (var1) {
                    int var15;
                    if (var4 <= 1) {
                        this.anIntArrayArray948[var14] = new int[var9.anIntArrayArray948[var10].length];

                        for (var15 = 0; var9.anIntArrayArray948[var10].length > var15; ++var15) {
                            this.anIntArrayArray948[var14][var15] = var9.anIntArrayArray948[var10][var15];
                        }
                    } else {
                        this.anIntArrayArray948[var14] = new int[var9.anIntArrayArray948[var10].length + 1];
                        this.anIntArrayArray948[var14][0] = var8;

                        for (var15 = 0; var9.anIntArrayArray948[var10].length > var15; ++var15) {
                            this.anIntArrayArray948[var14][1 + var15] = var9.anIntArrayArray948[var10][var15];
                        }
                    }
                }
            }
        }

        this.numFaces = var3;
    }

    final void method578(boolean var1, GameModel var2) {
        this.anInt1019 = var2.anInt1019;
        this.anInt967 = var2.anInt967;
        this.anInt928 = var2.anInt928;
        this.anInt934 = var2.anInt934;
        this.anInt919 = var2.anInt919;
        if (var1) {
            this.aBoolean936 = true;
        }

        this.anInt970 = var2.anInt970;
        this.method575(1);
        this.numFaces = 1;
    }

    private int method579(byte[] var1, byte var2) {
        while (var1[this.anInt1027] == 10 || var1[this.anInt1027] == 13) {
            ++this.anInt1027;
        }

        if (var2 != -27) {
            this.allocate(-34, 39, 126);
        }

        int var3 = ClientStream.anIntArray1685[var1[this.anInt1027++] & 255];
        int var4 = ClientStream.anIntArray1685[var1[this.anInt1027++] & 255];
        int var5 = ClientStream.anIntArray1685[255 & var1[this.anInt1027++]];
        int var6 = -131072 + (var5 + var3 * 4096 - -(64 * var4));
        if (var6 == 123456) {
            var6 = this.magic;
        }

        return var6;
    }

    final void method580(byte var1) {
        if (var1 >= 89) {
            this.numVertices = 0;
            this.transformState = 0;
        }
    }

}
