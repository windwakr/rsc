/**
 * rsc
 * 30.12.2013
 */
public class GameData {
    static int objectCount;
    static String[] wallObjectCommand2;
    static byte[] dataString;
    static int stringOffset;
    static byte[] dataInteger;
    static int integerOffset;
    static int itemCount;
    static String[] itemDescription;
    static int[] itemUnused;
    static int[] itemPicture;
    static int[] itemSpecial;
    static int[] itemMask;
    static int[] itemStackable;
    static String[] itemName;
    static String[] itemCommand;
    static int[] itemBasePrice;
    static int[] itemWearable;
    static int[] itemMembers;
    static int npcCount;
    static int[] npcWalkModel;
    static int[] npcCombatModel;
    static int[][] npcSprite;
    static int[] npcColourBottom;
    static int[] npcDefense;
    static int[] npcHeight;
    static int[] npcAttackable;
    static int[] npcAttack;
    static String[] npcName;
    static int[] npcCombatAnimation;
    static int[] npcColourSkin;
    static int[] npcColourTop;
    static int[] npcWidth;
    static String[] npcDescription;
    static int[] npcStrength;
    static String[] npcCommand;
    static int[] npcColourHair;
    static int[] npcHits;
    static int textureCount = 0;
    static String[] textureSubtypeName;
    static String[] textureName;
    static int animationCount;
    static int[] animationNumber;
    static int[] animationHasA;
    static int[] animationCharacterColour;
    static int[] animationHasF;
    static String[] animationName;
    static int[] animationSomething;
    static int[] objectElevation;
    static String[] objectName;
    static int[] objectType;
    static int[] objectModelIndex;
    static String[] objectCommand2;
    static String[] objectDescription;
    static int[] objectHeight;
    static int[] objectWidth;
    static String[] objectCommand1;
    static int wallObjectCount;
    static int[] wallObjectHeight;
    static String[] wallObjectDescription;
    static String[] wallObjectCommand1;
    static int[] wallObjectAdjacent;
    static int[] wallObjectInvisible;
    static String[] wallObjectName;
    static int[] wallObjectTextureFront;
    static int[] wallObjectTextureBack;
    static int roofCount;
    static int[] roofNumVertice;
    static int[] roofHeight;
    static int tileCount;
    static int[] tileType;
    static int[] tileAdjacent;
    static int[] tileDecoration;
    static int anInt199 = 0;
    static int spellCount;
    static int[] spellRunesRequired;
    static String[] spellName;
    static String[] spellDescription;
    static int[][] spellRunesId;
    static int[] spellLevel;
    static int[] spellType;
    static int[][] spellRunesCount;
    static int prayerCount;
    static int[] prayerDrain;
    static String[] prayerName;
    static int[] prayerLevel;
    static String[] prayerDescription;
    static String[] modelName = new String[5000];
    static int modelCount = 0;
    static int itemSpriteCount = 0;

    static void method41(byte var0, boolean members, byte[] buffer) {
        dataString = Utility.loadData(buffer, 0, "string.dat");
        stringOffset = 0;
        dataInteger = Utility.loadData(buffer, 0, "integer.dat");
        integerOffset = 0;
        itemCount = getUnsignedShort(true);
        itemDescription = new String[itemCount];
        itemUnused = new int[itemCount];
        itemPicture = new int[itemCount];
        itemSpecial = new int[itemCount];
        itemMask = new int[itemCount];
        itemName = new String[itemCount];
        itemStackable = new int[itemCount];
        itemCommand = new String[itemCount];
        itemBasePrice = new int[itemCount];
        itemWearable = new int[itemCount];
        itemMembers = new int[itemCount];

        for (int var3 = 0; itemCount > var3; ++var3) {
            itemName[var3] = getString(71);
        }

        for (int var4 = 0; itemCount > var4; ++var4) {
            itemDescription[var4] = getString(56);
        }

        for (int var5 = 0; var5 < itemCount; ++var5) {
            itemCommand[var5] = getString(122);
        }

        for (int var6 = 0; var6 < itemCount; ++var6) {
            itemPicture[var6] = getUnsignedShort(true);
            if (itemSpriteCount < 1 + itemPicture[var6]) {
                itemSpriteCount = 1 + itemPicture[var6];
            }
        }

        for (int var7 = 0; var7 < itemCount; ++var7) {
            itemBasePrice[var7] = getUnsignedInt(4);
        }

        for (int var8 = 0; var8 < itemCount; ++var8) {
            itemStackable[var8] = getUnsignedByte(true);
        }

        int var9 = 4 / ((64 - var0) / 39);

        for (int var10 = 0; var10 < itemCount; ++var10) {
            itemUnused[var10] = getUnsignedByte(true);
        }

        for (int var11 = 0; var11 < itemCount; ++var11) {
            itemWearable[var11] = getUnsignedShort(true);
        }

        for (int var12 = 0; itemCount > var12; ++var12) {
            itemMask[var12] = getUnsignedInt(4);
        }

        for (int var13 = 0; var13 < itemCount; ++var13) {
            itemSpecial[var13] = getUnsignedByte(true);
        }

        for (int var14 = 0; itemCount > var14; ++var14) {
            itemMembers[var14] = getUnsignedByte(true);
        }

        for (int var15 = 0; var15 < itemCount; ++var15) {
            if (!members && itemMembers[var15] == 1) {
                itemName[var15] = "Members object";
                itemDescription[var15] = "You need to be a member to use this object";
                itemBasePrice[var15] = 0;
                itemCommand[var15] = "";
                itemUnused[0] = 0;
                itemWearable[var15] = 0;
                itemSpecial[var15] = 1;
            }
        }

        npcCount = getUnsignedShort(true);
        npcWalkModel = new int[npcCount];
        npcCombatModel = new int[npcCount];
        npcSprite = new int[npcCount][12];
        npcColourBottom = new int[npcCount];
        npcDefense = new int[npcCount];
        npcHeight = new int[npcCount];
        npcAttackable = new int[npcCount];
        npcAttack = new int[npcCount];
        npcName = new String[npcCount];
        npcCombatAnimation = new int[npcCount];
        npcColourSkin = new int[npcCount];
        npcColourTop = new int[npcCount];
        npcWidth = new int[npcCount];
        npcDescription = new String[npcCount];
        npcStrength = new int[npcCount];
        npcCommand = new String[npcCount];
        npcColourHair = new int[npcCount];
        npcHits = new int[npcCount];

        for (int var16 = 0; npcCount > var16; ++var16) {
            npcName[var16] = getString(64);
        }

        for (int var17 = 0; var17 < npcCount; ++var17) {
            npcDescription[var17] = getString(52);
        }

        for (int var18 = 0; var18 < npcCount; ++var18) {
            npcAttack[var18] = getUnsignedByte(true);
        }

        for (int var19 = 0; var19 < npcCount; ++var19) {
            npcStrength[var19] = getUnsignedByte(true);
        }

        for (int var20 = 0; var20 < npcCount; ++var20) {
            npcHits[var20] = getUnsignedByte(true);
        }

        for (int var21 = 0; var21 < npcCount; ++var21) {
            npcDefense[var21] = getUnsignedByte(true);
        }

        for (int var22 = 0; var22 < npcCount; ++var22) {
            npcAttackable[var22] = getUnsignedByte(true);
        }

        int var24;
        for (int var23 = 0; npcCount > var23; ++var23) {
            for (var24 = 0; var24 < 12; ++var24) {
                npcSprite[var23][var24] = getUnsignedByte(true);
                if (npcSprite[var23][var24] == 255) {
                    npcSprite[var23][var24] = -1;
                }
            }
        }

        for (var24 = 0; var24 < npcCount; ++var24) {
            npcColourHair[var24] = getUnsignedInt(4);
        }

        for (int var25 = 0; var25 < npcCount; ++var25) {
            npcColourTop[var25] = getUnsignedInt(4);
        }

        for (int var26 = 0; var26 < npcCount; ++var26) {
            npcColourBottom[var26] = getUnsignedInt(4);
        }

        for (int var27 = 0; var27 < npcCount; ++var27) {
            npcColourSkin[var27] = getUnsignedInt(4);
        }

        for (int var28 = 0; npcCount > var28; ++var28) {
            npcWidth[var28] = getUnsignedShort(true);
        }

        for (int var29 = 0; var29 < npcCount; ++var29) {
            npcHeight[var29] = getUnsignedShort(true);
        }

        for (int var30 = 0; npcCount > var30; ++var30) {
            npcWalkModel[var30] = getUnsignedByte(true);
        }

        for (int var31 = 0; var31 < npcCount; ++var31) {
            npcCombatModel[var31] = getUnsignedByte(true);
        }

        for (int var32 = 0; npcCount > var32; ++var32) {
            npcCombatAnimation[var32] = getUnsignedByte(true);
        }

        for (int var33 = 0; npcCount > var33; ++var33) {
            npcCommand[var33] = getString(35);
        }

        textureCount = getUnsignedShort(true);
        textureSubtypeName = new String[textureCount];
        textureName = new String[textureCount];

        for (int var34 = 0; var34 < textureCount; ++var34) {
            textureName[var34] = getString(67);
        }

        for (int var35 = 0; textureCount > var35; ++var35) {
            textureSubtypeName[var35] = getString(1);
        }

        animationCount = getUnsignedShort(true);
        animationNumber = new int[animationCount];
        animationHasA = new int[animationCount];
        animationCharacterColour = new int[animationCount];
        animationHasF = new int[animationCount];
        animationName = new String[animationCount];
        animationSomething = new int[animationCount];

        for (int var36 = 0; animationCount > var36; ++var36) {
            animationName[var36] = getString(-101);
        }

        for (int var37 = 0; animationCount > var37; ++var37) {
            animationCharacterColour[var37] = getUnsignedInt(4);
        }

        for (int var38 = 0; animationCount > var38; ++var38) {
            animationSomething[var38] = getUnsignedByte(true);
        }

        for (int var39 = 0; var39 < animationCount; ++var39) {
            animationHasA[var39] = getUnsignedByte(true);
        }

        for (int var40 = 0; var40 < animationCount; ++var40) {
            animationHasF[var40] = getUnsignedByte(true);
        }

        for (int var41 = 0; animationCount > var41; ++var41) {
            animationNumber[var41] = getUnsignedByte(true);
        }

        objectCount = getUnsignedShort(true);
        objectElevation = new int[objectCount];
        objectName = new String[objectCount];
        objectType = new int[objectCount];
        objectModelIndex = new int[objectCount];
        objectCommand2 = new String[objectCount];
        objectDescription = new String[objectCount];
        objectHeight = new int[objectCount];
        objectWidth = new int[objectCount];
        objectCommand1 = new String[objectCount];

        for (int var42 = 0; var42 < objectCount; ++var42) {
            objectName[var42] = getString(-101);
        }

        for (int var43 = 0; var43 < objectCount; ++var43) {
            objectDescription[var43] = getString(18);
        }

        for (int var44 = 0; objectCount > var44; ++var44) {
            objectCommand1[var44] = getString(-127);
        }

        for (int var45 = 0; objectCount > var45; ++var45) {
            objectCommand2[var45] = getString(65);
        }

        for (int var46 = 0; var46 < objectCount; ++var46) {
            objectModelIndex[var46] = getModelIndex((byte) 95, getString(-107));
        }

        for (int var47 = 0; objectCount > var47; ++var47) {
            objectWidth[var47] = getUnsignedByte(true);
        }

        for (int var48 = 0; var48 < objectCount; ++var48) {
            objectHeight[var48] = getUnsignedByte(true);
        }

        for (int var49 = 0; var49 < objectCount; ++var49) {
            objectType[var49] = getUnsignedByte(true);
        }

        for (int var50 = 0; var50 < objectCount; ++var50) {
            objectElevation[var50] = getUnsignedByte(true);
        }

        wallObjectCount = getUnsignedShort(true);
        wallObjectHeight = new int[wallObjectCount];
        wallObjectDescription = new String[wallObjectCount];
        wallObjectCommand1 = new String[wallObjectCount];
        wallObjectAdjacent = new int[wallObjectCount];
        wallObjectInvisible = new int[wallObjectCount];
        wallObjectName = new String[wallObjectCount];
        wallObjectCommand2 = new String[wallObjectCount];
        wallObjectTextureFront = new int[wallObjectCount];
        wallObjectTextureBack = new int[wallObjectCount];

        for (int var51 = 0; wallObjectCount > var51; ++var51) {
            wallObjectName[var51] = getString(-99);
        }

        for (int var52 = 0; var52 < wallObjectCount; ++var52) {
            wallObjectDescription[var52] = getString(46);
        }

        for (int var53 = 0; wallObjectCount > var53; ++var53) {
            wallObjectCommand1[var53] = getString(-117);
        }

        for (int var54 = 0; var54 < wallObjectCount; ++var54) {
            wallObjectCommand2[var54] = getString(-100);
        }

        for (int var55 = 0; var55 < wallObjectCount; ++var55) {
            wallObjectHeight[var55] = getUnsignedShort(true);
        }

        for (int var56 = 0; wallObjectCount > var56; ++var56) {
            wallObjectTextureFront[var56] = getUnsignedInt(4);
        }

        for (int var57 = 0; var57 < wallObjectCount; ++var57) {
            wallObjectTextureBack[var57] = getUnsignedInt(4);
        }

        for (int var58 = 0; var58 < wallObjectCount; ++var58) {
            wallObjectAdjacent[var58] = getUnsignedByte(true);
        }

        for (int var59 = 0; var59 < wallObjectCount; ++var59) {
            wallObjectInvisible[var59] = getUnsignedByte(true);
        }

        roofCount = getUnsignedShort(true);
        roofNumVertice = new int[roofCount];
        roofHeight = new int[roofCount];

        for (int var60 = 0; roofCount > var60; ++var60) {
            roofHeight[var60] = getUnsignedByte(true);
        }

        for (int var61 = 0; var61 < roofCount; ++var61) {
            roofNumVertice[var61] = getUnsignedByte(true);
        }

        tileCount = getUnsignedShort(true);
        tileType = new int[tileCount];
        tileAdjacent = new int[tileCount];
        tileDecoration = new int[tileCount];

        for (int var62 = 0; tileCount > var62; ++var62) {
            tileDecoration[var62] = getUnsignedInt(4);
        }

        for (int var63 = 0; var63 < tileCount; ++var63) {
            tileType[var63] = getUnsignedByte(true);
        }

        for (int var64 = 0; tileCount > var64; ++var64) {
            tileAdjacent[var64] = getUnsignedByte(true);
        }

        anInt199 = getUnsignedShort(true);
        spellCount = getUnsignedShort(true);
        spellRunesRequired = new int[spellCount];
        spellName = new String[spellCount];
        spellDescription = new String[spellCount];
        spellRunesId = new int[spellCount][];
        spellLevel = new int[spellCount];
        spellType = new int[spellCount];
        spellRunesCount = new int[spellCount][];

        for (int var65 = 0; spellCount > var65; ++var65) {
            spellName[var65] = getString(-106);
        }

        for (int var66 = 0; spellCount > var66; ++var66) {
            spellDescription[var66] = getString(29);
        }

        for (int var67 = 0; var67 < spellCount; ++var67) {
            spellLevel[var67] = getUnsignedByte(true);
        }

        for (int var68 = 0; spellCount > var68; ++var68) {
            spellRunesRequired[var68] = getUnsignedByte(true);
        }

        for (int var69 = 0; var69 < spellCount; ++var69) {
            spellType[var69] = getUnsignedByte(true);
        }

        int var71;
        int var72;
        for (int var70 = 0; spellCount > var70; ++var70) {
            var71 = getUnsignedByte(true);
            spellRunesId[var70] = new int[var71];

            for (var72 = 0; var71 > var72; ++var72) {
                spellRunesId[var70][var72] = getUnsignedShort(true);
            }
        }

        int var73;
        for (var71 = 0; var71 < spellCount; ++var71) {
            var72 = getUnsignedByte(true);
            spellRunesCount[var71] = new int[var72];

            for (var73 = 0; var73 < var72; ++var73) {
                spellRunesCount[var71][var73] = getUnsignedByte(true);
            }
        }

        prayerCount = getUnsignedShort(true);
        prayerDrain = new int[prayerCount];
        prayerName = new String[prayerCount];
        prayerLevel = new int[prayerCount];
        prayerDescription = new String[prayerCount];

        for (var72 = 0; var72 < prayerCount; ++var72) {
            prayerName[var72] = getString(112);
        }

        for (var73 = 0; prayerCount > var73; ++var73) {
            prayerDescription[var73] = getString(-95);
        }

        for (int var74 = 0; prayerCount > var74; ++var74) {
            prayerLevel[var74] = getUnsignedByte(true);
        }

        for (int var75 = 0; prayerCount > var75; ++var75) {
            prayerDrain[var75] = getUnsignedByte(true);
        }

        dataInteger = null;
        dataString = null;
    }

    static int getUnsignedShort(boolean var0) {
        if (!var0) {
            getUnsignedShort(false);
        }

        int var1 = Utility.getUnsignedShort(integerOffset, dataInteger, 102);
        integerOffset += 2;
        return var1;
    }

    static String getString(int var0) {
        String var1;
        for (var1 = ""; dataString[stringOffset] != 0; var1 = var1 + (char) dataString[stringOffset++]) {
            ;
        }

        ++stringOffset;
        int var2 = 77 % ((-52 - var0) / 39);
        return var1;
    }

    static int getUnsignedInt(int var0) {
        int var1 = Utility.getUnsignedInt(dataInteger, false, integerOffset);
        integerOffset += var0;
        if (var1 > 99999999) {
            var1 = 99999999 - var1;
        }

        return var1;
    }

    static int getUnsignedByte(boolean var0) {
        int var1 = 255 & dataInteger[integerOffset];
        ++integerOffset;
        if (!var0) {
            Utility.format(75, (String) null, (byte) -127);
        }

        return var1;
    }

    static int getModelIndex(byte var0, String var1) {
        if (var1.equalsIgnoreCase("na")) {
            return 0;
        } else {
            if (var0 <= 38) {
                getModelIndex((byte) -56, (String) null);
            }

            for (int var2 = 0; modelCount > var2; ++var2) {
                if (modelName[var2].equalsIgnoreCase(var1)) {
                    return var2;
                }
            }

            modelName[modelCount++] = var1;
            return modelCount + -1;
        }
    }

}
