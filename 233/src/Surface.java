import java.awt.*;
import java.awt.image.*;
import java.util.Hashtable;

class Surface implements ImageProducer, ImageObserver {

    static byte[][] gameFonts = new byte[50][];
    static int anInt196 = 0;
    static int anInt1129 = 0;
    static int anInt751 = 0;
    int[] anIntArray577;
    int[][] surfaceSetPixels;
    int[][] spriteColours;
    int[] anIntArray600;
    int[] anIntArray607;
    int[] anIntArray627;
    int width2;
    int[] anIntArray649;
    byte[][] spriteColoursUsed;
    int anInt661;
    boolean interlace = false;
    boolean loggedIn = false;
    private int[] anIntArray566;
    private Image anImage578;
    private int[] anIntArray579;
    private boolean[] aBooleanArray581;
    private int anInt584;
    private ImageConsumer anImageConsumer593;
    private int[] anIntArray609;
    private int[] anIntArray610;
    private int[] anIntArray614;
    private int anInt621 = 0;
    private int[] anIntArray631;
    private ColorModel aColorModel634;
    private int anInt636 = 0;
    private int[] anIntArray637;
    private int anInt651 = 0;
    private int[] anIntArray653;
    private int anInt660 = 0;
    private int[] anIntArray664;

    Surface(int var1, int var2, int var3, Component var4) {
        this.anIntArray627 = new int[var3];
        this.anInt661 = var2;
        this.spriteColoursUsed = new byte[var3][];
        this.aBooleanArray581 = new boolean[var3];
        this.anInt636 = var2;
        this.anIntArray631 = new int[var3];
        this.anInt660 = var1;
        this.width2 = var1;
        this.anIntArray600 = new int[var3];
        this.anIntArray577 = new int[var3];
        this.anIntArray609 = new int[var3];
        this.surfaceSetPixels = new int[var3][];
        this.anIntArray607 = new int[var2 * var1];
        this.spriteColours = new int[var3][];
        this.anIntArray649 = new int[var3];
        if (var1 > 1 && var2 > 1 && var4 != null) {
            this.aColorModel634 = new DirectColorModel(32, 16711680, '\uff00', 255);
            int var5 = this.width2 * this.anInt661;

            for (int var6 = 0; var5 > var6; ++var6) {
                this.anIntArray607[var6] = 0;
            }

            this.anImage578 = var4.createImage(this);
            this.method390(false);
            var4.prepareImage(this.anImage578, var4);
            this.method390(false);
            var4.prepareImage(this.anImage578, var4);
            this.method390(false);
            var4.prepareImage(this.anImage578, var4);
        }
    }

    static int rgb2long(int var1, int var2, int var3) {
        return (var3 << 16) - (-(var2 << 8) + -var1);
    }

    private void method372(int var1, int var2, int var3, int var4, byte var5, int var6, int[] var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, int var15, int[] var16) {
        int var17 = 125 % ((-35 - var5) / 41);
        int var18 = (var12 & 16765487) >> 16;
        int var19 = var12 >> 8 & 255;
        int var20 = var12 & 255;

        try {
            int var21 = var10;

            for (int var22 = -var1; var22 < 0; ++var22) {
                int var23 = (var15 >> 16) * var4;
                int var24 = var13 >> 16;
                int var25 = var9;
                int var26;
                if (var24 < this.anInt621) {
                    var26 = this.anInt621 - var24;
                    var25 = var9 - var26;
                    var10 += var2 * var26;
                    var24 = this.anInt621;
                }

                if (this.anInt660 <= (var24 + var25)) {
                    var26 = -this.anInt660 + var25 + var24;
                    var25 -= var26;
                }

                var6 = -var6 + 1;
                if (var6 != 0) {
                    for (var26 = var24; (var25 + var24) > var26; ++var26) {
                        var8 = var16[(var10 >> 16) - -var23];
                        var10 += var2;
                        if (var8 != 0) {
                            int var27 = (var8 & '\uff97') >> 8;
                            int var28 = var8 & 255;
                            int var29 = (16720564 & var8) >> 16;
                            if (var29 == var27 && var27 == var28) {
                                var7[var26 + var14] = (var20 * var28 >> 8) + (var19 * var27 >> 8 << 8) + (var18 * var29 >> 8 << 16);
                            } else {
                                var7[var26 + var14] = var8;
                            }
                        }
                    }
                }

                var15 += var3;
                var14 += this.width2;
                var13 += var11;
                var10 = var21;
            }

        } catch (Exception var30) {
            System.out.println("error in transparent sprite plot routine");
        }
        ;
    }

    public final synchronized void removeConsumer(ImageConsumer var1) {
        if (this.anImageConsumer593 == var1) {
            this.anImageConsumer593 = null;
        }
    }

    final void setBounds(int var1, int var2, int var3, int var4, int var5) {
        if (var5 > this.width2) {
            var5 = this.width2;
        }

        if (var2 < 0) {
            var2 = 0;
        }

        if (var4 < 0) {
            var4 = 0;
        }

        if (this.anInt661 < var1) {
            var1 = this.anInt661;
        }

        this.anInt660 = var5;
        if (var3 != 16777215) {
            this.anInt636 = 6;
        }

        this.anInt621 = var2;
        this.anInt651 = var4;
        this.anInt636 = var1;
    }

    final void drawGradient(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        if (var1 < this.anInt621) {
            var7 -= -var1 + this.anInt621;
            var1 = this.anInt621;
        }

        if ((var7 + var1) > this.anInt660) {
            var7 = -var1 + this.anInt660;
        }

        int var8 = (var2 & 16761084) >> 16;
        int var9 = ('\uffbb' & var2) >> 8;
        int var10 = var2 & 255;
        int var11 = 255 & var6 >> 16;
        int var12 = 255 & var6 >> 8;
        int var13 = var6 & 255;
        if (var5 > -111) {
            this.anIntArray579 = null;
        }

        int var14 = this.width2 - var7;
        byte var15 = 1;
        if (this.interlace) {
            var14 += this.width2;
            var15 = 2;
            if ((var3 & 1) != 0) {
                ++var3;
                --var4;
            }
        }

        int var16 = var1 + var3 * this.width2;

        for (int var17 = 0; var4 > var17; var17 += var15) {
            if (this.anInt651 <= var3 + var17 && this.anInt636 > (var17 + var3)) {
                int var18 = ((var17 * var9 + (-var17 + var4) * var12) / var4 << 8) + (((var4 + -var17) * var11 + var8 * var17) / var4 << 16) - -((var17 * var10 - -((-var17 + var4) * var13)) / var4);

                for (int var19 = -var7; var19 < 0; ++var19) {
                    this.anIntArray607[var16++] = var18;
                }

                var16 += var14;
            } else {
                var16 += this.width2;
            }
        }
    }

    final void preloadSprite(int var1, int var2) {
        if (this.spriteColoursUsed[var2] != null) {
            int var3 = this.anIntArray577[var2] * this.anIntArray600[var2];
            byte[] var4 = this.spriteColoursUsed[var2];
            int[] var5 = this.spriteColours[var2];
            int[] var6 = new int[var3];
            if (var1 != 1) {
                this.aColorModel634 = null;
            }

            for (int var7 = 0; var3 > var7; ++var7) {
                int var8 = var5[var4[var7] & 255];
                if (var8 != 0) {
                    if (var8 == 16711935) {
                        var8 = 0;
                    }
                } else {
                    var8 = 1;
                }

                var6[var7] = var8;
            }

            this.surfaceSetPixels[var2] = var6;
            this.spriteColoursUsed[var2] = null;
            this.spriteColours[var2] = null;
        }
    }

    public final void requestTopDownLeftRightResend(ImageConsumer var1) {
        System.out.println("TDLR");
    }

    private void method376(int var1, int var2, int var3, int var4, int[] var5, int var6, int var7, int var8, int var9, int var10, int[] var11) {
        if (var4 == -10642) {
            int var12 = -(var1 >> 2);
            var1 = -(var1 & 3);

            for (int var13 = -var9; var13 < 0; var13 += var8) {
                for (int var14 = var12; var14 < 0; ++var14) {
                    var7 = var11[var10++];
                    if (var7 != 0) {
                        var5[var3++] = var7;
                    } else {
                        ++var3;
                    }

                    var7 = var11[var10++];
                    if (var7 != 0) {
                        var5[var3++] = var7;
                    } else {
                        ++var3;
                    }

                    var7 = var11[var10++];
                    if (var7 == 0) {
                        ++var3;
                    } else {
                        var5[var3++] = var7;
                    }

                    var7 = var11[var10++];
                    if (var7 != 0) {
                        var5[var3++] = var7;
                    } else {
                        ++var3;
                    }
                }

                for (int var15 = var1; var15 < 0; ++var15) {
                    var7 = var11[var10++];
                    if (var7 == 0) {
                        ++var3;
                    } else {
                        var5[var3++] = var7;
                    }
                }

                var3 += var2;
                var10 += var6;
            }

        }
    }

    private void method377(int var1, boolean var2, int var3, int var4, int var5, int var6, String var7) {
        this.drawstring(var1, var7, -this.textWidth(var3, -127, var7) + var6, var3, var5, -120, var4);
        if (!var2) {
            this.method382(-30, -61, (byte[]) null, (int[]) null, -18, 66, 47, -54, 76, -22, -39, -17, 65, -100, 99, (int[]) null, 94);
        }
    }

    final int textHeight(int var1, boolean var2) {
        return var1 == 0 ? 12 : (var1 == 1 ? 14 : (var1 == 2 ? 14 : (!var2 ? 78 : (var1 == 3 ? 15 : (var1 == 4 ? 15 : (var1 == 5 ? 19 : (var1 == 6 ? 24 : (var1 == 7 ? 29 : this.method393((byte) -120, var1)))))))));
    }

    final void drawBoxEdge(int var1, int var2, boolean var3, int var4, int var5, int var6) {
        this.drawLineHoriz(var4, var6, -9986, var2, var1);
        this.drawLineHoriz(var4, var6, -9986, var2, -1 + var5 + var1);
        if (var3) {
            this.anIntArray631 = null;
        }

        this.drawLineVert(var6, var4, var1, -19766, var5);
        this.drawLineVert(var6, -1 + (var4 - -var2), var1, -19766, var5);
    }

    final void drawMinimapSprite(int var1, int var2, int var3, int var4, boolean var5, int var6) {
        int var7 = this.width2;
        int var8 = this.anInt661;
        int var9;
        if (this.anIntArray637 == null) {
            this.anIntArray637 = new int[512];

            for (var9 = 0; var9 < 256; ++var9) {
                this.anIntArray637[var9] = (int) (32768.0D * Math.sin((double) var9 * 0.02454369D));
                this.anIntArray637[var9 + 256] = (int) (Math.cos((double) var9 * 0.02454369D) * 32768.0D);
            }
        }

        var9 = -this.anIntArray627[var3] / 2;
        int var10 = -this.anIntArray649[var3] / 2;
        if (this.aBooleanArray581[var3]) {
            var10 += this.anIntArray609[var3];
            var9 += this.anIntArray631[var3];
        }

        int var11 = this.anIntArray600[var3] + var9;
        int var12 = this.anIntArray577[var3] + var10;
        var6 &= 255;
        int var17 = this.anIntArray637[var6] * var2;
        int var18 = this.anIntArray637[var6 + 256] * var2;
        int var19 = var1 + (var9 * var18 + var17 * var10 >> 22);
        int var20 = (-(var9 * var17) + var18 * var10 >> 22) + var4;
        int var21 = (var10 * var17 - -(var11 * var18) >> 22) + var1;
        int var22 = (-(var17 * var11) + var18 * var10 >> 22) + var4;
        int var23 = (var12 * var17 + var11 * var18 >> 22) + var1;
        int var24 = var4 + (-(var17 * var11) + var12 * var18 >> 22);
        int var25 = (var9 * var18 + var17 * var12 >> 22) + var1;
        int var26 = (var18 * var12 - var17 * var9 >> 22) + var4;
        if (var2 == 192 && (63 & anInt196) == (var6 & 63)) {
            ++anInt1129;
        } else if (var2 == 128) {
            anInt196 = var6;
        } else {
            ++anInt751;
        }

        int var27 = var20;
        int var28 = var20;
        if (var20 <= var22) {
            if (var20 < var22) {
                var28 = var22;
            }
        } else {
            var27 = var22;
        }

        if (var27 > var24) {
            var27 = var24;
        } else if (var28 < var24) {
            var28 = var24;
        }

        if (!var5) {
            this.method399(-93, (int[]) null, 127, (int[]) null, true, -113, 41, -68, 55, 9, -125);
        }

        if (var27 <= var26) {
            if (var26 > var28) {
                var28 = var26;
            }
        } else {
            var27 = var26;
        }

        if (this.anIntArray653 == null || this.anIntArray653.length != (var8 + 1)) {
            this.anIntArray610 = new int[1 + var8];
            this.anIntArray566 = new int[1 + var8];
            this.anIntArray664 = new int[1 + var8];
            this.anIntArray653 = new int[var8 - -1];
            this.anIntArray614 = new int[1 + var8];
            this.anIntArray579 = new int[var8 - -1];
        }

        if (var28 > this.anInt636) {
            var28 = this.anInt636;
        }

        if (this.anInt651 > var27) {
            var27 = this.anInt651;
        }

        for (int var29 = var27; var28 >= var29; ++var29) {
            this.anIntArray653[var29] = 99999999;
            this.anIntArray614[var29] = -99999999;
        }

        int var30 = 0;
        int var31 = 0;
        int var32 = 0;
        int var33 = this.anIntArray600[var3];
        int var13 = var33 - 1;
        byte var54 = 0;
        int var34 = this.anIntArray577[var3];
        var11 = var33 - 1;
        byte var14 = 0;
        byte var55 = 0;
        byte var15 = 0;
        var12 = -1 + var34;
        int var16 = var34 + -1;
        int var35;
        int var38;
        int var36;
        int var37;
        if (var26 >= var20) {
            var35 = var19 << 8;
            var36 = var54 << 8;
            var37 = var20;
            var38 = var26;
        } else {
            var38 = var20;
            var36 = var16 << 8;
            var37 = var26;
            var35 = var25 << 8;
        }

        if (var26 != var20) {
            var30 = (-var19 + var25 << 8) / (-var20 + var26);
            var32 = (-var54 + var16 << 8) / (var26 + -var20);
        }

        if (var38 > var8 + -1) {
            var38 = -1 + var8;
        }

        if (var37 < 0) {
            var35 -= var37 * var30;
            var36 -= var32 * var37;
            var37 = 0;
        }

        for (int var39 = var37; var38 >= var39; ++var39) {
            this.anIntArray653[var39] = this.anIntArray614[var39] = var35;
            this.anIntArray664[var39] = this.anIntArray566[var39] = 0;
            var35 += var30;
            this.anIntArray610[var39] = this.anIntArray579[var39] = var36;
            var36 += var32;
        }

        int var40;
        if (var22 >= var20) {
            var40 = var55 << 8;
            var35 = var19 << 8;
            var37 = var20;
            var38 = var22;
        } else {
            var37 = var22;
            var35 = var21 << 8;
            var40 = var13 << 8;
            var38 = var20;
        }

        if (var20 != var22) {
            var31 = (var13 + -var55 << 8) / (-var20 + var22);
            var30 = (-var19 + var21 << 8) / (var22 + -var20);
        }

        if (var37 < 0) {
            var35 -= var30 * var37;
            var40 -= var31 * var37;
            var37 = 0;
        }

        if (var38 > -1 + var8) {
            var38 = -1 + var8;
        }

        for (int var41 = var37; var38 >= var41; ++var41) {
            if (var35 < this.anIntArray653[var41]) {
                this.anIntArray653[var41] = var35;
                this.anIntArray664[var41] = var40;
                this.anIntArray610[var41] = 0;
            }

            if (var35 > this.anIntArray614[var41]) {
                this.anIntArray614[var41] = var35;
                this.anIntArray566[var41] = var40;
                this.anIntArray579[var41] = 0;
            }

            var35 += var30;
            var40 += var31;
        }

        if (var24 >= var22) {
            var38 = var24;
            var36 = var14 << 8;
            var37 = var22;
            var35 = var21 << 8;
            var40 = var13 << 8;
        } else {
            var38 = var22;
            var36 = var12 << 8;
            var40 = var11 << 8;
            var35 = var23 << 8;
            var37 = var24;
        }

        if (var22 != var24) {
            var32 = (-var14 + var12 << 8) / (var24 - var22);
            var30 = (var23 - var21 << 8) / (-var22 + var24);
        }

        if ((-1 + var8) < var38) {
            var38 = var8 - 1;
        }

        if (var37 < 0) {
            var36 -= var37 * var32;
            var35 -= var37 * var30;
            var37 = 0;
        }

        for (int var42 = var37; var38 >= var42; ++var42) {
            if (this.anIntArray653[var42] > var35) {
                this.anIntArray653[var42] = var35;
                this.anIntArray664[var42] = var40;
                this.anIntArray610[var42] = var36;
            }

            if (var35 > this.anIntArray614[var42]) {
                this.anIntArray614[var42] = var35;
                this.anIntArray566[var42] = var40;
                this.anIntArray579[var42] = var36;
            }

            var36 += var32;
            var35 += var30;
        }

        if (var24 != var26) {
            var30 = (var25 - var23 << 8) / (var26 - var24);
            var31 = (-var11 + var15 << 8) / (var26 + -var24);
        }

        if (var26 < var24) {
            var40 = var15 << 8;
            var37 = var26;
            var38 = var24;
            var35 = var25 << 8;
            var36 = var16 << 8;
        } else {
            var35 = var23 << 8;
            var40 = var11 << 8;
            var37 = var24;
            var38 = var26;
            var36 = var12 << 8;
        }

        if (var37 < 0) {
            var35 -= var37 * var30;
            var40 -= var37 * var31;
            var37 = 0;
        }

        if (-1 + var8 < var38) {
            var38 = var8 + -1;
        }

        for (int var43 = var37; var43 <= var38; ++var43) {
            if (this.anIntArray653[var43] > var35) {
                this.anIntArray653[var43] = var35;
                this.anIntArray664[var43] = var40;
                this.anIntArray610[var43] = var36;
            }

            if (this.anIntArray614[var43] < var35) {
                this.anIntArray614[var43] = var35;
                this.anIntArray566[var43] = var40;
                this.anIntArray579[var43] = var36;
            }

            var40 += var31;
            var35 += var30;
        }

        int var44 = var27 * var7;
        int[] var45 = this.surfaceSetPixels[var3];

        for (int var46 = var27; var28 > var46; ++var46) {
            int var47 = this.anIntArray653[var46] >> 8;
            int var48 = this.anIntArray614[var46] >> 8;
            if (-var47 + var48 <= 0) {
                var44 += var7;
            } else {
                int var49 = this.anIntArray664[var46] << 9;
                int var50 = (-var49 + (this.anIntArray566[var46] << 9)) / (var48 - var47);
                int var51 = this.anIntArray610[var46] << 9;
                int var52 = (-var51 + (this.anIntArray579[var46] << 9)) / (var48 - var47);
                if (this.anInt621 > var47) {
                    var51 += (this.anInt621 + -var47) * var52;
                    var49 += var50 * (-var47 + this.anInt621);
                    var47 = this.anInt621;
                }

                if (var48 > this.anInt660) {
                    var48 = this.anInt660;
                }

                if (!this.interlace || (var46 & 1) == 0) {
                    if (this.aBooleanArray581[var3]) {
                        this.method410(var33, this.anIntArray607, var51, var45, var49, -var48 + var47, var47 + var44, 0, var52, var50, 185018513);
                    } else {
                        this.method399(var33, var45, var44 + var47, this.anIntArray607, true, var50, 0, var47 - var48, var51, var52, var49);
                    }
                }

                var44 += var7;
            }
        }
        ;
    }

    final void drawLineVert(int var1, int var2, int var3, int var4, int var5) {
        if (this.anInt621 <= var2 && var2 < this.anInt660) {
            if (this.anInt651 > var3) {
                var5 -= this.anInt651 - var3;
                var3 = this.anInt651;
            }

            if (var3 - -var5 > this.anInt636) {
                var5 = -var3 + this.anInt636;
            }

            if (var5 > 0) {
                if (var4 != -19766) {
                    this.method396((int[]) null, 42, (byte[]) null, 117, -49, 104, -62, 33, 22, 77);
                }

                int var6 = var2 - -(this.width2 * var3);

                for (int var7 = 0; var7 < var5; ++var7) {
                    this.anIntArray607[var6 + this.width2 * var7] = var1;
                }

            }
        }
    }

    private void method382(int var1, int var2, byte[] var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, int var14, int var15, int[] var16, int var17) {
        int var18 = (16757176 & var7) >> 16;
        int var19 = ('\uff11' & var7) >> 8;
        if (var11 != -1321763928) {
            this.anIntArray566 = null;
        }

        int var20 = var7 & 255;

        try {
            int var21 = var9;

            for (int var22 = -var14; var22 < 0; ++var22) {
                int var23 = var12 * (var17 >> 16);
                int var24 = var6 >> 16;
                int var25 = var5;
                int var26;
                if (this.anInt621 > var24) {
                    var26 = this.anInt621 + -var24;
                    var9 += var15 * var26;
                    var24 = this.anInt621;
                    var25 = var5 - var26;
                }

                var2 = -var2 + 1;
                if (this.anInt660 <= var25 + var24) {
                    var26 = var24 - (-var25 - -this.anInt660);
                    var25 -= var26;
                }

                var17 += var8;
                if (var2 != 0) {
                    for (var26 = var24; var25 + var24 > var26; ++var26) {
                        var1 = 255 & var3[(var9 >> 16) + var23];
                        var9 += var15;
                        if (var1 != 0) {
                            var1 = var16[var1];
                            int var27 = (var1 & 16741842) >> 16;
                            int var28 = var1 >> 8 & 255;
                            int var29 = var1 & 255;
                            if (var28 == var27 && var29 == var28) {
                                var4[var26 - -var13] = (var20 * var29 >> 8) + (var19 * var28 >> 8 << 8) + (var18 * var27 >> 8 << 16);
                            } else {
                                var4[var13 + var26] = var1;
                            }
                        }
                    }
                }

                var9 = var21;
                var13 += this.width2;
                var6 += var10;
            }

        } catch (Exception var30) {
            System.out.println("error in transparent sprite plot routine");
        }
        ;
    }

    final void drawstring(int var1, String var2, int var3, int var4, int var5, int var6, int var7) {
        try {
            if (var5 > 0) {
                int var8 = this.anInt584 - (-var5 - -1);
                if (this.spriteColoursUsed[var8] != null) {
                    this.drawSprite((byte) 61, var1 - this.anIntArray577[var8], var8, var3);
                    var3 += this.anIntArray600[var8] - -5;
                }
            }

            if (var6 > -113) {
                this.method398(60, 69, 115, 42, -7, -127, -53);
            }

            byte[] var15 = gameFonts[var4];

            for (int var9 = 0; var9 < var2.length(); ++var9) {
                if (var2.charAt(var9) == 64 && 4 + var9 < var2.length() && var2.charAt(4 + var9) == 64) {
                    if (var2.substring(1 + var9, var9 + 4).equalsIgnoreCase("red")) {
                        var7 = 16711680;
                    } else if (!var2.substring(1 + var9, 4 + var9).equalsIgnoreCase("lre")) {
                        if (var2.substring(1 + var9, var9 + 4).equalsIgnoreCase("yel")) {
                            var7 = 16776960;
                        } else if (var2.substring(var9 + 1, 4 + var9).equalsIgnoreCase("gre")) {
                            var7 = '\uff00';
                        } else if (!var2.substring(1 + var9, 4 + var9).equalsIgnoreCase("blu")) {
                            if (!var2.substring(1 + var9, 4 + var9).equalsIgnoreCase("cya")) {
                                if (!var2.substring(1 + var9, 4 + var9).equalsIgnoreCase("mag")) {
                                    if (var2.substring(var9 - -1, 4 + var9).equalsIgnoreCase("whi")) {
                                        var7 = 16777215;
                                    } else if (var2.substring(var9 + 1, var9 + 4).equalsIgnoreCase("bla")) {
                                        var7 = 0;
                                    } else if (var2.substring(1 + var9, var9 + 4).equalsIgnoreCase("dre")) {
                                        var7 = 12582912;
                                    } else if (!var2.substring(var9 + 1, var9 + 4).equalsIgnoreCase("ora")) {
                                        if (!var2.substring(var9 + 1, 4 + var9).equalsIgnoreCase("ran")) {
                                            if (!var2.substring(1 + var9, 4 + var9).equalsIgnoreCase("or1")) {
                                                if (!var2.substring(1 + var9, var9 + 4).equalsIgnoreCase("or2")) {
                                                    if (!var2.substring(1 + var9, 4 + var9).equalsIgnoreCase("or3")) {
                                                        if (!var2.substring(1 + var9, 4 + var9).equalsIgnoreCase("gr1")) {
                                                            if (var2.substring(var9 + 1, 4 + var9).equalsIgnoreCase("gr2")) {
                                                                var7 = 8453888;
                                                            } else if (var2.substring(1 + var9, var9 - -4).equalsIgnoreCase("gr3")) {
                                                                var7 = 4259584;
                                                            }
                                                        } else {
                                                            var7 = 12648192;
                                                        }
                                                    } else {
                                                        var7 = 16723968;
                                                    }
                                                } else {
                                                    var7 = 16740352;
                                                }
                                            } else {
                                                var7 = 16756736;
                                            }
                                        } else {
                                            var7 = (int) (Math.random() * 1.6777215E7D);
                                        }
                                    } else {
                                        var7 = 16748608;
                                    }
                                } else {
                                    var7 = 16711935;
                                }
                            } else {
                                var7 = '\uffff';
                            }
                        } else {
                            var7 = 255;
                        }
                    } else {
                        var7 = 16748608;
                    }

                    var9 += 4;
                } else {
                    char var10;
                    if (var2.charAt(var9) == 126 && (4 + var9) < var2.length() && var2.charAt(var9 - -4) == 126) {
                        var10 = var2.charAt(1 + var9);
                        char var16 = var2.charAt(var9 + 2);
                        char var12 = var2.charAt(3 + var9);
                        if (var10 >= 48 && var10 <= 57 && var16 >= 48 && var16 <= 57 && var12 >= 48 && var12 <= 57) {
                            var3 = Integer.parseInt(var2.substring(1 + var9, 4 + var9));
                        }

                        var9 += 4;
                    } else {
                        var10 = var2.charAt(var9);
                        if (var10 == 160) {
                            var10 = 32;
                        }

                        if (var10 < 0 || JagGrab.anIntArray1124.length <= var10) {
                            var10 = 32;
                        }

                        int var11 = JagGrab.anIntArray1124[var10];
                        if (this.loggedIn && !Scene.aBooleanArray418[var4] && var7 != 0) {
                            this.method419(var11, 0, var1, Scene.aBooleanArray418[var4], (byte) 90, var3 + 1, var15);
                        }

                        if (this.loggedIn && !Scene.aBooleanArray418[var4] && var7 != 0) {
                            this.method419(var11, 0, var1 - -1, Scene.aBooleanArray418[var4], (byte) 104, var3, var15);
                        }

                        this.method419(var11, var7, var1, Scene.aBooleanArray418[var4], (byte) 78, var3, var15);
                        var3 += var15[7 + var11];
                    }
                }
            }
        } catch (Exception var13) {
            System.out.println("drawstring: " + var13);
            var13.printStackTrace();
        }
    }

    final void loadSprite(byte[] var1, int var2, byte var3, byte[] var4, int var5) {
        if (var3 <= 66) {
            this.method411(-9, -7, (byte) -38, -45, -16);
        }

        int var6 = Utility.getUnsignedShort(0, var4, 117);
        int var7 = Utility.getUnsignedShort(var6, var1, 118);
        var6 += 2;
        int var8 = Utility.getUnsignedShort(var6, var1, 98);
        var6 += 2;
        int var9 = var1[var6++] & 255;
        int[] var10 = new int[var9];
        var10[0] = 16711935;

        for (int var11 = 0; var11 < (var9 - 1); ++var11) {
            var10[1 + var11] = Utility.bitwiseAnd(255, var1[var6 - -2]) + Utility.bitwiseAnd('\uff00', var1[1 + var6] << 8) + (Utility.bitwiseAnd(var1[var6], 255) << 16);
            var6 += 3;
        }

        int var12 = 2;

        for (int var13 = var2; var5 + var2 > var13; ++var13) {
            this.anIntArray631[var13] = Utility.bitwiseAnd(255, var1[var6++]);
            this.anIntArray609[var13] = Utility.bitwiseAnd(var1[var6++], 255);
            this.anIntArray600[var13] = Utility.getUnsignedShort(var6, var1, 98);
            var6 += 2;
            this.anIntArray577[var13] = Utility.getUnsignedShort(var6, var1, 85);
            var6 += 2;
            int var14 = var1[var6++] & 255;
            int var15 = this.anIntArray577[var13] * this.anIntArray600[var13];
            this.spriteColoursUsed[var13] = new byte[var15];
            this.spriteColours[var13] = var10;
            this.anIntArray627[var13] = var7;
            this.anIntArray649[var13] = var8;
            this.surfaceSetPixels[var13] = null;
            this.aBooleanArray581[var13] = false;
            if (this.anIntArray631[var13] != 0 || this.anIntArray609[var13] != 0) {
                this.aBooleanArray581[var13] = true;
            }

            int var16;
            if (var14 == 0) {
                for (var16 = 0; var15 > var16; ++var16) {
                    this.spriteColoursUsed[var13][var16] = var4[var12++];
                    if (this.spriteColoursUsed[var13][var16] == 0) {
                        this.aBooleanArray581[var13] = true;
                    }
                }
            } else if (var14 == 1) {
                for (var16 = 0; this.anIntArray600[var13] > var16; ++var16) {
                    for (int var17 = 0; this.anIntArray577[var13] > var17; ++var17) {
                        this.spriteColoursUsed[var13][var16 - -(var17 * this.anIntArray600[var13])] = var4[var12++];
                        if (this.spriteColoursUsed[var13][var16 + this.anIntArray600[var13] * var17] == 0) {
                            this.aBooleanArray581[var13] = true;
                        }
                    }
                }
            }
        }
        ;
    }

    final void blackScreen(byte var1) {
        if (var1 != 127) {
            this.removeConsumer((ImageConsumer) null);
        }

        int var2 = this.width2 * this.anInt661;
        int var3;
        if (!this.interlace) {
            for (var3 = 0; var3 < var2; ++var3) {
                this.anIntArray607[var3] = 0;
            }

        } else {
            var3 = 0;

            for (int var4 = -this.anInt661; var4 < 0; var4 += 2) {
                for (int var5 = -this.width2; var5 < 0; ++var5) {
                    this.anIntArray607[var3++] = 0;
                }

                var3 += this.width2;
            }

        }
        ;
    }

    final void drawSprite(byte var1, int var2, int var3, int var4) {
        if (this.aBooleanArray581[var3]) {
            var4 += this.anIntArray631[var3];
            var2 += this.anIntArray609[var3];
        }

        int var5 = var2 * this.width2 + var4;
        int var6 = 0;
        int var7 = this.anIntArray577[var3];
        int var8 = this.anIntArray600[var3];
        if (var1 != 61) {
            this.anIntArray631 = null;
        }

        int var9 = -var8 + this.width2;
        int var10;
        if (this.anInt651 > var2) {
            var10 = this.anInt651 - var2;
            var2 = this.anInt651;
            var6 += var10 * var8;
            var5 += var10 * this.width2;
            var7 -= var10;
        }

        int var11 = 0;
        if (var4 < this.anInt621) {
            var10 = -var4 + this.anInt621;
            var6 += var10;
            var8 -= var10;
            var11 += var10;
            var5 += var10;
            var9 += var10;
            var4 = this.anInt621;
        }

        if ((var7 + var2) >= this.anInt636) {
            var7 -= 1 + var7 + (var2 - this.anInt636);
        }

        if (var8 + var4 >= this.anInt660) {
            var10 = var8 + var4 - (this.anInt660 - 1);
            var11 += var10;
            var9 += var10;
            var8 -= var10;
        }

        if (var8 > 0 && var7 > 0) {
            byte var13 = 1;
            if (this.interlace) {
                var11 += this.anIntArray600[var3];
                var9 += this.width2;
                if ((1 & var2) != 0) {
                    var5 += this.width2;
                    --var7;
                }

                var13 = 2;
            }

            if (this.surfaceSetPixels[var3] == null) {
                this.method413(var11, this.anIntArray607, var8, this.spriteColours[var3], var13, var5, (byte) 64, var6, this.spriteColoursUsed[var3], var7, var9);
            } else {
                this.method376(var8, var9, var5, -10642, this.anIntArray607, var11, 0, var13, var7, var6, this.surfaceSetPixels[var3]);
            }
        }
        ;
    }

    final void drawBoxAlpha(int colour, int y, int width, byte var4, int height, int alpha, int x) {
        if (y < this.anInt651) {
            height -= -y + this.anInt651;
            y = this.anInt651;
        }

        if (this.anInt621 > x) {
            width -= -x + this.anInt621;
            x = this.anInt621;
        }

        if (y + height > this.anInt636) {
            height = -y + this.anInt636;
        }

        if (this.anInt660 < x - -width) {
            width = -x + this.anInt660;
        }

        int var9 = 256 + -alpha;
        int var10 = (colour >> 16 & 255) * alpha;
        int var11 = ((colour & '\uffc4') >> 8) * alpha;
        int var12 = (255 & colour) * alpha;
        int var13 = -width + this.width2;
        byte var14 = 1;
        if (this.interlace) {
            var13 += this.width2;
            var14 = 2;
            if ((y & 1) != 0) {
                ++y;
                --height;
            }
        }

        int var15 = this.width2 * y + x;

        for (int var16 = 0; var16 < height; var16 += var14) {
            for (int var17 = -width; var17 < 0; ++var17) {
                int var18 = var9 * (255 & this.anIntArray607[var15]);
                int var19 = var9 * (255 & this.anIntArray607[var15] >> 8);
                int var20 = var9 * (this.anIntArray607[var15] >> 16 & 255);
                int var21 = (var18 + var12 >> 8) + (var10 - -var20 >> 8 << 16) + (var11 - -var19 >> 8 << 8);
                this.anIntArray607[var15++] = var21;
            }

            var15 += var13;
        }
    }

    final void method388(byte var1, int var2, int var3, int var4, int var5, int var6) {
        this.anIntArray600[var3] = var4;
        this.anIntArray577[var3] = var2;
        this.aBooleanArray581[var3] = false;
        int var7 = -15 % ((-75 - var1) / 43);
        this.anIntArray631[var3] = 0;
        this.anIntArray609[var3] = 0;
        this.anIntArray627[var3] = var4;
        this.anIntArray649[var3] = var2;
        int var8 = var2 * var4;
        this.surfaceSetPixels[var3] = new int[var8];
        int var9 = 0;

        for (int var10 = var5; var10 < var5 + var2; ++var10) {
            for (int var11 = var6; var6 - -var4 > var11; ++var11) {
                this.surfaceSetPixels[var3][var9++] = this.anIntArray607[var11 + this.width2 * var10];
            }
        }
    }

    final void drawstringCenter(String var1, int var3, int var4, int var5, int var6) {
        this.method417(var4, var6, var5, var3, 2, var1, 0);
    }

    private synchronized void method390(boolean var1) {
        if (!var1) {
            if (this.anImageConsumer593 != null) {
                this.anImageConsumer593.setPixels(0, 0, this.width2, this.anInt661, this.aColorModel634, this.anIntArray607, 0, this.width2);
                this.anImageConsumer593.imageComplete(2);
            }
        }
    }

    final void readSleepWord(int var1, byte var2, byte[] var3) {
        int[] var4 = this.surfaceSetPixels[var1] = new int[10200];
        this.anIntArray600[var1] = 255;
        this.anIntArray577[var1] = 40;
        this.anIntArray631[var1] = 0;
        this.anIntArray609[var1] = 0;
        this.anIntArray627[var1] = 255;
        this.anIntArray649[var1] = 40;
        this.aBooleanArray581[var1] = false;
        int var5 = 0;
        int var6 = 1;

        int var7;
        int var8;
        int var9;
        for (var7 = 0; var7 < 255; var5 = -var5 + 16777215) {
            var8 = 255 & var3[var6++];

            for (var9 = 0; var9 < var8; ++var9) {
                var4[var7++] = var5;
            }
        }

        var8 = 1;
        if (var2 == -120) {
            while (var8 < 40) {
                var9 = 0;

                while (var9 < 255) {
                    int var10 = 255 & var3[var6++];

                    for (int var11 = 0; var11 < var10; ++var11) {
                        var4[var7] = var4[-255 + var7];
                        ++var9;
                        ++var7;
                    }

                    if (var9 < 255) {
                        var4[var7] = -var4[-255 + var7] + 16777215;
                        ++var9;
                        ++var7;
                    }
                }

                ++var8;
            }

        }
    }

    private void method392(int var1, int var2, int[] var3, int[] var4, int var5, int var6, int var7, int var8, int var9, byte var10, int var11, int var12, int var13, int var14, int var15) {
        int var16 = 255 & var6 >> 16;
        int var17 = 255 & var6 >> 8;
        if (var10 <= 67) {
            this.fade2black(27);
        }

        int var18 = var6 & 255;

        try {
            int var19 = var15;

            for (int var20 = -var2; var20 < 0; var20 += var13) {
                int var21 = (var5 >> 16) * var7;

                for (int var22 = -var12; var22 < 0; ++var22) {
                    var1 = var4[var21 + (var15 >> 16)];
                    if (var1 == 0) {
                        ++var9;
                    } else {
                        int var23 = 255 & var1 >> 16;
                        int var24 = 255 & var1 >> 8;
                        int var25 = var1 & 255;
                        if (var23 == var24 && var25 == var24) {
                            var3[var9++] = (var24 * var17 >> 8 << 8) + (var16 * var23 >> 8 << 16) - -(var18 * var25 >> 8);
                        } else {
                            var3[var9++] = var1;
                        }
                    }

                    var15 += var8;
                }

                var5 += var14;
                var9 += var11;
                var15 = var19;
            }

        } catch (Exception var26) {
            System.out.println("error in plot_scale");
        }
        ;
    }

    private int method393(byte var1, int var2) {
        if (var1 >= -112) {
            this.anIntArray649 = null;
        }

        return var2 == 0 ? -2 + gameFonts[var2][8] : -1 + gameFonts[var2][8];
    }

    final void draw(int var1, Graphics var2, int var3, int var4) {
        this.method390(false);
        var2.drawImage(this.anImage578, var3, var4, this);
        if (var1 != -2020315800) {
            this.anIntArray609 = null;
        }
    }

    final void method395(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        try {
            int var8 = this.anIntArray600[var3];
            int var9 = this.anIntArray577[var3];
            int var10 = 0;
            int var11 = 0;
            int var12 = (var8 << 16) / var2;
            int var13 = (var9 << 16) / var5;
            if (var4 == 1536) {
                int var14;
                int var15;
                if (this.aBooleanArray581[var3]) {
                    var14 = this.anIntArray627[var3];
                    var15 = this.anIntArray649[var3];
                    if (var14 == 0 || var15 == 0) {
                        return;
                    }

                    var12 = (var14 << 16) / var2;
                    if ((var2 * this.anIntArray631[var3] % var14) != 0) {
                        var10 = (var14 - var2 * this.anIntArray631[var3] % var14 << 16) / var2;
                    }

                    var7 += (-1 + var14 + var2 * this.anIntArray631[var3]) / var14;
                    if (this.anIntArray609[var3] * var5 % var15 != 0) {
                        var11 = (-(var5 * this.anIntArray609[var3] % var15) + var15 << 16) / var5;
                    }

                    var13 = (var15 << 16) / var5;
                    var6 += (-1 + this.anIntArray609[var3] * var5 - -var15) / var15;
                    var5 = var5 * (-(var11 >> 16) + this.anIntArray577[var3]) / var15;
                    var2 = var2 * (-(var10 >> 16) + this.anIntArray600[var3]) / var14;
                }

                var14 = var7 - -(this.width2 * var6);
                var15 = this.width2 + -var2;
                int var16;
                if (this.anInt651 > var6) {
                    var16 = this.anInt651 - var6;
                    var11 += var16 * var13;
                    var6 = 0;
                    var14 += this.width2 * var16;
                    var5 -= var16;
                }

                if (this.anInt636 <= (var5 + var6)) {
                    var5 -= -this.anInt636 + var5 + var6 + 1;
                }

                if (var7 < this.anInt621) {
                    var16 = this.anInt621 + -var7;
                    var15 += var16;
                    var14 += var16;
                    var10 += var12 * var16;
                    var2 -= var16;
                    var7 = 0;
                }

                if (this.anInt660 <= var7 + var2) {
                    var16 = 1 + -this.anInt660 + var2 + var7;
                    var2 -= var16;
                    var15 += var16;
                }

                byte var19 = 1;
                if (this.interlace) {
                    var19 = 2;
                    if ((var6 & 1) != 0) {
                        --var5;
                        var14 += this.width2;
                    }

                    var15 += this.width2;
                    var13 += var13;
                }

                this.method392(0, var5, this.anIntArray607, this.surfaceSetPixels[var3], var11, var1, var8, var12, var14, (byte) 121, var15, var2, var19, var13, var10);
            }
        } catch (Exception var17) {
            System.out.println("error in sprite clipping routine");
        }
    }

    private void method396(int[] var1, int var2, byte[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10) {
        if (var4 <= 9) {
            this.anInt660 = -8;
        }

        for (int var11 = -var5; var11 < 0; ++var11) {
            for (int var12 = -var8; var12 < 0; ++var12) {
                int var13 = var3[var6++] & 255;
                if (var13 <= 30) {
                    ++var10;
                } else if (var13 >= 230) {
                    var1[var10++] = var7;
                } else {
                    int var14 = var1[var10];
                    var1[var10++] = Utility.bitwiseAnd(Utility.bitwiseAnd(16711935, var7) * var13 + (256 - var13) * Utility.bitwiseAnd(var14, 16711935), -16711936) + Utility.bitwiseAnd(16711680, Utility.bitwiseAnd(var14, '\uff00') * (-var13 + 256) + var13 * Utility.bitwiseAnd(var7, '\uff00')) >> 8;
                }
            }

            var10 += var2;
            var6 += var9;
        }
    }

    private void method397(int var1, int var2, boolean var3, int var4, byte[] var5, int[] var6, int var7, int var8, int var9, int var10) {
        try {
            int var11 = -(var10 >> 2);
            var10 = -(var10 & 3);
            int var12 = -var9;
            if (var3) {
                this.surfaceSetPixels = null;
            }

            while (var12 < 0) {
                for (int var13 = var11; var13 < 0; ++var13) {
                    if (var5[var7++] != 0) {
                        var6[var8++] = var4;
                    } else {
                        ++var8;
                    }

                    if (var5[var7++] != 0) {
                        var6[var8++] = var4;
                    } else {
                        ++var8;
                    }

                    if (var5[var7++] == 0) {
                        ++var8;
                    } else {
                        var6[var8++] = var4;
                    }

                    if (var5[var7++] == 0) {
                        ++var8;
                    } else {
                        var6[var8++] = var4;
                    }
                }

                for (int var14 = var10; var14 < 0; ++var14) {
                    if (var5[var7++] == 0) {
                        ++var8;
                    } else {
                        var6[var8++] = var4;
                    }
                }

                var8 += var1;
                var7 += var2;
                ++var12;
            }

        } catch (Exception var15) {
            System.out.println("plotletter: " + var15);
            var15.printStackTrace();
        }
    }

    final void method398(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        int var8 = var2;

        for (int var9 = -70 % ((var1 - -64) / 37); (var7 + var2) > var8; ++var8) {
            for (int var10 = var4; var10 < var6 + var4; ++var10) {
                int var11 = 0;
                int var12 = 0;
                int var13 = 0;
                int var14 = 0;

                for (int var15 = var8 + -var5; var15 <= var5 + var8; ++var15) {
                    if (var15 >= 0 && var15 < this.width2) {
                        for (int var16 = -var3 + var10; var10 + var3 >= var16; ++var16) {
                            if (var16 >= 0 && this.anInt661 > var16) {
                                int var17 = this.anIntArray607[this.width2 * var16 + var15];
                                ++var14;
                                var11 += var17 >> 16 & 255;
                                var13 += 255 & var17;
                                var12 += ('\uffc3' & var17) >> 8;
                            }
                        }
                    }
                }

                this.anIntArray607[var10 * this.width2 + var8] = var13 / var14 + (var12 / var14 << 8) + (var11 / var14 << 16);
            }
        }
    }

    private void method399(int var1, int[] var2, int var3, int[] var4, boolean var5, int var6, int var7, int var8, int var9, int var10, int var11) {
        for (var7 = var8; var7 < 0; ++var7) {
            this.anIntArray607[var3++] = var2[(var9 >> 17) * var1 + (var11 >> 17)];
            var11 += var6;
            var9 += var10;
        }

        if (!var5) {
            this.anIntArray566 = null;
        }
    }

    final void drawBox(int var1, int var2, int var3, boolean var4, int var5, int var6) {
        if (var5 < this.anInt621) {
            var3 -= -var5 + this.anInt621;
            var5 = this.anInt621;
        }

        if (var4) {
            if (this.anInt651 > var1) {
                var6 -= this.anInt651 - var1;
                var1 = this.anInt651;
            }

            if ((var5 + var3) > this.anInt660) {
                var3 = -var5 + this.anInt660;
            }

            if ((var6 + var1) > this.anInt636) {
                var6 = -var1 + this.anInt636;
            }

            int var7 = this.width2 + -var3;
            byte var8 = 1;
            if (this.interlace) {
                var8 = 2;
                var7 += this.width2;
                if ((var1 & 1) != 0) {
                    ++var1;
                    --var6;
                }
            }

            int var9 = var5 + this.width2 * var1;

            for (int var10 = -var6; var10 < 0; var10 += var8) {
                for (int var11 = -var3; var11 < 0; ++var11) {
                    this.anIntArray607[var9++] = var2;
                }

                var9 += var7;
            }

        }
    }

    final void method401(byte var1, int var2, int var3, int var4) {
        if (this.anInt621 <= var3 && var2 >= this.anInt651 && this.anInt660 > var3 && this.anInt636 > var2) {
            this.anIntArray607[var3 + var2 * this.width2] = var4;
            if (var1 != 118) {
                this.drawWorld(16, 86);
            }
        }
    }

    final void drawLineHoriz(int var1, int var2, int var3, int var4, int var5) {
        if (var5 >= this.anInt651 && var5 < this.anInt636) {
            if (var1 < this.anInt621) {
                var4 -= -var1 + this.anInt621;
                var1 = this.anInt621;
            }

            if ((var4 + var1) > this.anInt660) {
                var4 = this.anInt660 - var1;
            }

            if (var4 > 0) {
                if (var3 != -9986) {
                    this.drawstring(-10, (String) null, -112, 4, -121, -43, 81);
                }

                int var6 = var5 * this.width2 + var1;

                for (int var7 = 0; var4 > var7; ++var7) {
                    this.anIntArray607[var7 + var6] = var2;
                }

            }
        }
    }

    public final synchronized boolean isConsumer(ImageConsumer var1) {
        return var1 == this.anImageConsumer593;
    }

    private void method403(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, int[] var9, int[] var10, int var11, int var12) {
        int var13 = 116 % ((var12 - -37) / 60);
        int var14 = 256 - var3;

        for (int var15 = -var8; var15 < 0; var15 += var2) {
            for (int var16 = -var6; var16 < 0; ++var16) {
                var4 = var10[var11++];
                if (var4 != 0) {
                    int var17 = var9[var1];
                    var9[var1++] = Utility.bitwiseAnd(16711680, var3 * Utility.bitwiseAnd(var4, '\uff00') - -(var14 * Utility.bitwiseAnd(var17, '\uff00'))) + Utility.bitwiseAnd(var14 * Utility.bitwiseAnd(16711935, var17) + Utility.bitwiseAnd(var4, 16711935) * var3, -16711936) >> 8;
                } else {
                    ++var1;
                }
            }

            var1 += var7;
            var11 += var5;
        }
        ;
    }

    final void drawWorld(int var1, int var2) {
        int var3 = this.anIntArray600[var2] * this.anIntArray577[var2];
        if (var1 == -257) {
            int[] var4 = this.surfaceSetPixels[var2];
            int[] var5 = new int['\u8000'];

            for (int var6 = 0; var6 < var3; ++var6) {
                int var7 = var4[var6];
                ++var5[(var7 >> 3 & 31) + (('\uf800' & var7) >> 6) + ((var7 & 16252928) >> 9)];
            }

            int[] var26 = new int[256];
            var26[0] = 16711935;
            int[] var8 = new int[256];

            int var11;
            int var12;
            for (int var9 = 0; var9 < '\u8000'; ++var9) {
                int var10 = var5[var9];
                if (var10 > var8[255]) {
                    for (var11 = 1; var11 < 256; ++var11) {
                        if (var8[var11] < var10) {
                            for (var12 = 255; var12 > var11; --var12) {
                                var26[var12] = var26[-1 + var12];
                                var8[var12] = var8[var12 + -1];
                            }

                            var26[var11] = (Utility.bitwiseAnd(31744, var9) << 9) + Utility.bitwiseAnd('\uf800', var9 << 6) + Utility.bitwiseAnd(248, var9 << 3) + 263172;
                            var8[var11] = var10;
                            break;
                        }
                    }
                }

                var5[var9] = -1;
            }

            byte[] var27 = new byte[var3];

            for (var11 = 0; var11 < var3; ++var11) {
                var12 = var4[var11];
                int var13 = (('\uf800' & var12) >> 6) + ((16252928 & var12) >> 9) + (var12 >> 3 & 31);
                int var14 = var5[var13];
                if (var14 == -1) {
                    int var15 = 999999999;
                    int var16 = var12 >> 16 & 255;
                    int var17 = ('\uff8f' & var12) >> 8;
                    int var18 = var12 & 255;

                    for (int var19 = 0; var19 < 256; ++var19) {
                        int var20 = var26[var19];
                        int var21 = (16740634 & var20) >> 16;
                        int var22 = (var20 & '\uff0b') >> 8;
                        int var23 = 255 & var20;
                        int var24 = (var18 + -var23) * (-var23 + var18) + (-var21 + var16) * (var16 - var21) + (var17 - var22) * (var17 - var22);
                        if (var15 > var24) {
                            var15 = var24;
                            var14 = var19;
                        }
                    }

                    var5[var13] = var14;
                }

                var27[var11] = (byte) var14;
            }

            this.spriteColoursUsed[var2] = var27;
            this.spriteColours[var2] = var26;
            this.surfaceSetPixels[var2] = null;
        }
    }

    private void method405(int var1, int[] var2, int[] var3, int var4, int var5, int var6, int var7, int var8, int var9, int var10, byte var11, int var12, int var13, int var14, int var15, int var16, int var17) {
        int var18 = (16720276 & var1) >> 16;
        if (var11 > -96) {
            this.drawBoxEdge(-13, 119, false, -98, -64, -14);
        }

        int var19 = 255 & var1 >> 8;
        int var20 = var1 & 255;
        int var21 = (16724553 & var16) >> 16;
        int var22 = ('\uff53' & var16) >> 8;
        int var23 = 255 & var16;

        try {
            int var24 = var5;

            for (int var25 = -var4; var25 < 0; ++var25) {
                int var26 = var17 * (var8 >> 16);
                int var27 = var10 >> 16;
                int var28 = var13;
                int var29;
                if (var27 < this.anInt621) {
                    var29 = -var27 + this.anInt621;
                    var27 = this.anInt621;
                    var5 += var29 * var15;
                    var28 = var13 - var29;
                }

                var14 = 1 + -var14;
                if ((var28 + var27) >= this.anInt660) {
                    var29 = var27 - (-var28 - -this.anInt660);
                    var28 -= var29;
                }

                var8 += var6;
                if (var14 != 0) {
                    for (var29 = var27; var27 - -var28 > var29; ++var29) {
                        var9 = var3[var26 + (var5 >> 16)];
                        var5 += var15;
                        if (var9 != 0) {
                            int var30 = 255 & var9 >> 16;
                            int var31 = var9 & 255;
                            int var32 = (var9 & '\uffae') >> 8;
                            if (var32 == var30 && var32 == var31) {
                                var2[var12 + var29] = (var31 * var20 >> 8) + ((var18 * var30 >> 8 << 16) - -(var32 * var19 >> 8 << 8));
                            } else if (var30 == 255 && var32 == var31) {
                                var2[var12 + var29] = (var31 * var23 >> 8) + (var21 * var30 >> 8 << 16) + (var32 * var22 >> 8 << 8);
                            } else {
                                var2[var29 + var12] = var9;
                            }
                        }
                    }
                }

                var10 += var7;
                var12 += this.width2;
                var5 = var24;
            }

        } catch (Exception var33) {
            System.out.println("error in transparent sprite plot routine");
        }
    }

    final void drawCircle(int var1, int var2, int var3, int var4, int var5, int var6) {
        if (var4 != -1385529104) {
            this.anIntArray579 = null;
        }

        int var7 = 256 - var6;
        int var8 = var6 * (255 & var5 >> 16);
        int var9 = var6 * ((var5 & '\uff98') >> 8);
        int var10 = (255 & var5) * var6;
        int var11 = var3 + -var1;
        if (var11 < 0) {
            var11 = 0;
        }

        int var12 = var3 - -var1;
        if (this.anInt661 <= var12) {
            var12 = this.anInt661 - 1;
        }

        byte var13 = 1;
        if (this.interlace) {
            if ((1 & var11) != 0) {
                ++var11;
            }

            var13 = 2;
        }

        for (int var14 = var11; var14 <= var12; var14 += var13) {
            int var15 = var14 - var3;
            int var16 = (int) Math.sqrt((double) (var1 * var1 + -(var15 * var15)));
            int var17 = var2 + -var16;
            if (var17 < 0) {
                var17 = 0;
            }

            int var18 = var2 - -var16;
            if (var18 >= this.width2) {
                var18 = this.width2 + -1;
            }

            int var19 = var17 - -(var14 * this.width2);

            for (int var20 = var17; var18 >= var20; ++var20) {
                int var21 = ((16750808 & this.anIntArray607[var19]) >> 16) * var7;
                int var22 = var7 * ((this.anIntArray607[var19] & '\uff80') >> 8);
                int var23 = (this.anIntArray607[var19] & 255) * var7;
                int var24 = (var23 + var10 >> 8) + (var21 + var8 >> 8 << 16) + (var22 + var9 >> 8 << 8);
                this.anIntArray607[var19++] = var24;
            }
        }
        ;
    }

    final void method407(int var1, int var2) {
        if (var2 > -63) {
            this.anIntArray653 = null;
        }

        this.anInt584 = var1;
    }

    final void method408(byte var1) {
        if (var1 != 10) {
            this.drawstring(23, (String) null, -38, 36, -41, -111, 79);
        }

        this.anInt660 = this.width2;
        this.anInt621 = 0;
        this.anInt651 = 0;
        this.anInt636 = this.anInt661;
    }

    final void method409(int var1, int var2, int var3, int var4, int var5, int var6, int var7, int var8, boolean var9, int var10) {
        try {
            if (var6 == 0) {
                var6 = 16777215;
            }

            if (var1 == 0) {
                var1 = 16777215;
            }

            int var11 = this.anIntArray600[var2];
            int var12 = this.anIntArray577[var2];
            int var13 = 0;
            int var14 = 0;
            int var15 = var10 << 16;
            int var16 = (var11 << 16) / var7;
            int var17 = (var12 << 16) / var5;
            int var18 = -(var10 << 16) / var5;
            int var19;
            int var20;
            if (this.aBooleanArray581[var2]) {
                var19 = this.anIntArray627[var2];
                var20 = this.anIntArray649[var2];
                if (var19 == 0 || var20 == 0) {
                    return;
                }

                var16 = (var19 << 16) / var7;
                var17 = (var20 << 16) / var5;
                int var21 = this.anIntArray631[var2];
                if (var9) {
                    var21 = var19 + -this.anIntArray600[var2] - var21;
                }

                int var22 = this.anIntArray609[var2];
                var8 += (var19 + (var7 * var21 - 1)) / var19;
                int var23 = (-1 + var20 + var22 * var5) / var20;
                var15 += var23 * var18;
                var3 += var23;
                if (var22 * var5 % var20 != 0) {
                    var14 = (var20 - var5 * var22 % var20 << 16) / var5;
                }

                if ((var21 * var7 % var19) != 0) {
                    var13 = (-(var21 * var7 % var19) + var19 << 16) / var7;
                }

                var7 = ((this.anIntArray600[var2] << 16) - (var13 + -var16 + 1)) / var16;
                var5 = (-1 + var17 + -var14 + (this.anIntArray577[var2] << 16)) / var17;
            }

            var19 = var3 * this.width2;
            var15 += var8 << 16;
            if (var3 < this.anInt651) {
                var20 = this.anInt651 + -var3;
                var3 = this.anInt651;
                var5 -= var20;
                var14 += var20 * var17;
                var15 += var18 * var20;
                var19 += var20 * this.width2;
            }

            if (this.anInt636 <= var5 + var3) {
                var5 -= 1 + -this.anInt636 + var3 + var5;
            }

            var20 = var19 / this.width2 & 1;
            if (!this.interlace) {
                var20 = 2;
            }

            if (var6 == 16777215) {
                if (this.surfaceSetPixels[var2] != null) {
                    if (!var9) {
                        this.method372(var5, var16, var17, var11, (byte) -93, var20, this.anIntArray607, 0, var7, var13, var18, var1, var15, var19, var14, this.surfaceSetPixels[var2]);
                    } else {
                        this.method372(var5, -var16, var17, var11, (byte) -88, var20, this.anIntArray607, 0, var7, -1 + ((this.anIntArray600[var2] << 16) - var13), var18, var1, var15, var19, var14, this.surfaceSetPixels[var2]);
                    }
                } else if (var9) {
                    this.method382(0, var20, this.spriteColoursUsed[var2], this.anIntArray607, var7, var15, var1, var17, -var13 + ((this.anIntArray600[var2] << 16) - 1), var18, -1321763928, var11, var19, var5, -var16, this.spriteColours[var2], var14);
                } else {
                    this.method382(0, var20, this.spriteColoursUsed[var2], this.anIntArray607, var7, var15, var1, var17, var13, var18, -1321763928, var11, var19, var5, var16, this.spriteColours[var2], var14);
                }
            } else if (this.surfaceSetPixels[var2] != null) {
                if (var9) {
                    this.method405(var1, this.anIntArray607, this.surfaceSetPixels[var2], var5, -var13 + (this.anIntArray600[var2] << 16) + -1, var17, var18, var14, 0, var15, (byte) -127, var19, var7, var20, -var16, var6, var11);
                } else {
                    this.method405(var1, this.anIntArray607, this.surfaceSetPixels[var2], var5, var13, var17, var18, var14, 0, var15, (byte) -119, var19, var7, var20, var16, var6, var11);
                }
            } else if (var9) {
                this.method418(0, this.spriteColours[var2], var1, var18, this.spriteColoursUsed[var2], var5, var7, this.anIntArray607, var20, var11, (byte) -21, var6, (this.anIntArray600[var2] << 16) + -var13 - 1, var19, var17, var15, -var16, var14);
            } else {
                this.method418(0, this.spriteColours[var2], var1, var18, this.spriteColoursUsed[var2], var5, var7, this.anIntArray607, var20, var11, (byte) -21, var6, var13, var19, var17, var15, var16, var14);
            }

            if (var4 >= -27) {
                this.method426(-99, -110, -43, 30, (byte) -29, -96);
            }
        } catch (Exception var24) {
            System.out.println("error in sprite clipping routine");
        }
    }

    public final boolean imageUpdate(Image var1, int var2, int var3, int var4, int var5, int var6) {
        return true;
    }

    private void method410(int var1, int[] var2, int var3, int[] var4, int var5, int var6, int var7, int var8, int var9, int var10, int var11) {
        if (var11 != 185018513) {
            this.method419(-121, 113, -121, true, (byte) 23, 65, (byte[]) null);
        }

        for (int var12 = var6; var12 < 0; ++var12) {
            var8 = var4[(var5 >> 17) - -((var3 >> 17) * var1)];
            var5 += var10;
            if (var8 != 0) {
                this.anIntArray607[var7++] = var8;
            } else {
                ++var7;
            }

            var3 += var9;
        }
    }

    public final synchronized void addConsumer(ImageConsumer var1) {
        this.anImageConsumer593 = var1;
        var1.setDimensions(this.width2, this.anInt661);
        var1.setProperties((Hashtable) null);
        var1.setColorModel(this.aColorModel634);
        var1.setHints(14);
    }

    public final void startProduction(ImageConsumer var1) {
        this.addConsumer(var1);
    }

    final void method411(int var1, int var2, byte var3, int var4, int var5) {
        if (this.aBooleanArray581[var2]) {
            var1 += this.anIntArray609[var2];
            var5 += this.anIntArray631[var2];
        }

        int var6 = this.width2 * var1 + var5;
        int var7 = 0;
        int var8 = this.anIntArray577[var2];
        int var9 = this.anIntArray600[var2];
        int var10 = -var9 + this.width2;
        int var11;
        if (this.anInt651 > var1) {
            var11 = this.anInt651 - var1;
            var1 = this.anInt651;
            var8 -= var11;
            var7 += var11 * var9;
            var6 += var11 * this.width2;
        }

        int var12 = 0;
        if (var1 - -var8 >= this.anInt636) {
            var8 -= 1 + -this.anInt636 + var1 + var8;
        }

        if (var5 < this.anInt621) {
            var11 = -var5 + this.anInt621;
            var12 += var11;
            var10 += var11;
            var6 += var11;
            var5 = this.anInt621;
            var9 -= var11;
            var7 += var11;
        }

        if (var5 + var9 >= this.anInt660) {
            var11 = var5 + var9 + (-this.anInt660 - -1);
            var10 += var11;
            var9 -= var11;
            var12 += var11;
        }

        if (var9 > 0 && var8 > 0) {
            byte var14 = 1;
            if (this.interlace) {
                var14 = 2;
                var10 += this.width2;
                var12 += this.anIntArray600[var2];
                if ((var1 & 1) != 0) {
                    --var8;
                    var6 += this.width2;
                }
            }

            if (var3 < -40) {
                if (this.surfaceSetPixels[var2] != null) {
                    this.method403(var6, var14, var4, 0, var12, var9, var10, var8, this.anIntArray607, this.surfaceSetPixels[var2], var7, 28);
                } else {
                    this.method427(this.spriteColoursUsed[var2], var10, false, var7, var4, var14, this.anIntArray607, var9, var8, var12, this.spriteColours[var2], var6);
                }
            }
        }
        ;
    }

    final int textWidth(int var1, int var2, String var3) {
        int var4 = 0;
        byte[] var5 = gameFonts[var1];
        if (var2 != -127) {
            GameData.npcWidth = null;
        }

        for (int var6 = 0; var3.length() > var6; ++var6) {
            if (var3.charAt(var6) == 64 && var6 - -4 < var3.length() && var3.charAt(4 + var6) == 64) {
                var6 += 4;
            } else if (var3.charAt(var6) == 126 && (4 + var6) < var3.length() && var3.charAt(4 + var6) == 126) {
                var6 += 4;
            } else {
                char var7 = var3.charAt(var6);
                if (var7 < 0 || var7 >= JagGrab.anIntArray1124.length) {
                    var7 = 32;
                }

                var4 += var5[7 + JagGrab.anIntArray1124[var7]];
            }
        }

        return var4;
    }

    private void method413(int var1, int[] var2, int var3, int[] var4, int var5, int var6, byte var7, int var8, byte[] var9, int var10, int var11) {
        int var12 = 40 / ((var7 - -20) / 49);
        int var13 = -(var3 >> 2);
        var3 = -(var3 & 3);

        for (int var14 = -var10; var14 < 0; var14 += var5) {
            for (int var15 = var13; var15 < 0; ++var15) {
                byte var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(var16, 255)];
                } else {
                    ++var6;
                }

                var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(var16, 255)];
                } else {
                    ++var6;
                }

                var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(255, var16)];
                } else {
                    ++var6;
                }

                var16 = var9[var8++];
                if (var16 != 0) {
                    var2[var6++] = var4[Utility.bitwiseAnd(255, var16)];
                } else {
                    ++var6;
                }
            }

            for (int var19 = var3; var19 < 0; ++var19) {
                byte var17 = var9[var8++];
                if (var17 == 0) {
                    ++var6;
                } else {
                    var2[var6++] = var4[Utility.bitwiseAnd(255, var17)];
                }
            }

            var6 += var11;
            var8 += var1;
        }
        ;
    }

    final void method414(int var1, int var2, int var3, int var4, String var5, int var6) {
        this.method377(var1, true, var4, var2, 0, var3, var5);
        if (var6 == -5169) {
        }
    }

    private void method415(int[] var1, int var2, int var3, int var4, int[] var5, int var6, int var7, int var8, int var9, int var10, int var11, int var12, int var13, boolean var14, int var15) {
        if (var14) {
            GameData.itemBasePrice = null;
        }

        int var16 = -var15 + 256;

        try {
            int var17 = var13;

            for (int var18 = -var9; var18 < 0; var18 += var11) {
                int var19 = var4 * (var12 >> 16);
                var12 += var2;

                for (int var20 = -var6; var20 < 0; ++var20) {
                    var10 = var5[(var13 >> 16) + var19];
                    var13 += var8;
                    if (var10 == 0) {
                        ++var7;
                    } else {
                        int var21 = var1[var7];
                        var1[var7++] = Utility.bitwiseAnd(-16711936, Utility.bitwiseAnd(var21, 16711935) * var16 + Utility.bitwiseAnd(var10, 16711935) * var15) + Utility.bitwiseAnd(16711680, Utility.bitwiseAnd('\uff00', var21) * var16 + var15 * Utility.bitwiseAnd('\uff00', var10)) >> 8;
                    }
                }

                var13 = var17;
                var7 += var3;
            }

        } catch (Exception var22) {
            System.out.println("error in tran_scale");
        }
        ;
    }

    final void fade2black(int var1) {
        if (var1 != -915682524) {
            this.anImage578 = null;
        }

        int var2 = this.anInt661 * this.width2;

        for (int var3 = 0; var2 > var3; ++var3) {
            int var4 = this.anIntArray607[var3] & 16777215;
            this.anIntArray607[var3] = Utility.bitwiseAnd(var4 >>> 4, -2146496753) + ((Utility.bitwiseAnd(var4, 16711423) >>> 1) - -Utility.bitwiseAnd(-1069596865, var4 >>> 2)) + (Utility.bitwiseAnd(var4, 16316664) >>> 3);
        }
        ;
    }

    private void method417(int var1, int var2, int var3, int var4, int var5, String var6, int var7) {
        this.drawstring(var1, var6, var3 - this.textWidth(var4, -127, var6) / var5, var4, var7, -117, var2);
    }

    private void method418(int var1, int[] var2, int var3, int var4, byte[] var5, int var6, int var7, int[] var8, int var9, int var10, byte var11, int var12, int var13, int var14, int var15, int var16, int var17, int var18) {
        int var19 = 255 & var3 >> 16;
        int var20 = 255 & var3 >> 8;
        int var21 = 255 & var3;
        if (var11 == -21) {
            int var22 = var12 >> 16 & 255;
            int var23 = (var12 & '\uff6d') >> 8;
            int var24 = 255 & var12;

            try {
                int var25 = var13;

                for (int var26 = -var6; var26 < 0; ++var26) {
                    int var27 = var10 * (var18 >> 16);
                    int var28 = var16 >> 16;
                    int var29 = var7;
                    int var30;
                    if (var28 < this.anInt621) {
                        var30 = -var28 + this.anInt621;
                        var29 = var7 - var30;
                        var13 += var17 * var30;
                        var28 = this.anInt621;
                    }

                    if (var28 - -var29 >= this.anInt660) {
                        var30 = var29 + (var28 - this.anInt660);
                        var29 -= var30;
                    }

                    var9 = -var9 + 1;
                    if (var9 != 0) {
                        for (var30 = var28; var30 < (var29 + var28); ++var30) {
                            var1 = var5[(var13 >> 16) - -var27] & 255;
                            var13 += var17;
                            if (var1 != 0) {
                                var1 = var2[var1];
                                int var31 = var1 & 255;
                                int var32 = var1 >> 8 & 255;
                                int var33 = (var1 & 16739633) >> 16;
                                if (var33 == var32 && var32 == var31) {
                                    var8[var14 + var30] = (var32 * var20 >> 8 << 8) + (var19 * var33 >> 8 << 16) + (var21 * var31 >> 8);
                                } else if (var33 == 255 && var32 == var31) {
                                    var8[var30 - -var14] = (var31 * var24 >> 8) + (var22 * var33 >> 8 << 16) + (var32 * var23 >> 8 << 8);
                                } else {
                                    var8[var30 + var14] = var1;
                                }
                            }
                        }
                    }

                    var18 += var15;
                    var13 = var25;
                    var14 += this.width2;
                    var16 += var4;
                }

            } catch (Exception var34) {
                System.out.println("error in transparent sprite plot routine");
            }
        }
        ;
    }

    private void method419(int var1, int var2, int var3, boolean var4, byte var5, int var6, byte[] var7) {
        if (var5 <= 54) {
            this.removeConsumer((ImageConsumer) null);
        }

        int var8 = var6 - -var7[var1 - -5];
        int var9 = var3 + -var7[6 + var1];
        int var10 = var7[3 + var1];
        int var11 = var7[var1 - -4];
        int var12 = var7[2 + var1] + 128 * var7[1 + var1] + var7[var1] * 16384;
        int var13 = var9 * this.width2 + var8;
        int var14 = -var10 + this.width2;
        int var15;
        if (var9 < this.anInt651) {
            var15 = -var9 + this.anInt651;
            var13 += this.width2 * var15;
            var9 = this.anInt651;
            var12 += var10 * var15;
            var11 -= var15;
        }

        int var16 = 0;
        if (var11 + var9 >= this.anInt636) {
            var11 -= -this.anInt636 + var9 + var11 + 1;
        }

        if (this.anInt621 > var8) {
            var15 = this.anInt621 + -var8;
            var16 += var15;
            var13 += var15;
            var12 += var15;
            var10 -= var15;
            var14 += var15;
            var8 = this.anInt621;
        }

        if (this.anInt660 <= var10 + var8) {
            var15 = 1 + -this.anInt660 + var8 + var10;
            var10 -= var15;
            var16 += var15;
            var14 += var15;
        }

        if (var10 > 0 && var11 > 0) {
            if (!var4) {
                this.method397(var14, var16, false, var2, var7, this.anIntArray607, var12, var13, var11, var10);
            } else {
                this.method396(this.anIntArray607, var14, var7, 51, var11, var12, var2, var10, var16, var13);
            }
        }
        ;
    }

    void method420(int var1, int var2, int var3, int var4, int var5, byte var6, int var7, int var8) {
        if (var6 >= -53) {
            this.textWidth(99, 44, (String) null);
        }

        this.method426(var8, var3, var1, var4, (byte) -92, var2);
    }

    final void drawstring(int var1, int var2, int var3, int var4, String var5) {
        this.drawstring(var2, var5, var4, var1, 0, -124, var3);
    }

    private void plot_scale(int var1, int var2, int var3, int var4, int[] var5, int var6, int var7, int var8, int var9, int var10, int[] var11, int var12, int var13, int var14) {
        try {
            int var15 = var8;
            int var16 = -var1;
            if (var6 == -15265) {
                while (var16 < 0) {
                    int var17 = (var12 >> 16) * var4;
                    var12 += var13;

                    for (int var18 = -var10; var18 < 0; ++var18) {
                        var2 = var11[var17 + (var8 >> 16)];
                        var8 += var7;
                        if (var2 == 0) {
                            ++var14;
                        } else {
                            var5[var14++] = var2;
                        }
                    }

                    var8 = var15;
                    var14 += var3;
                    var16 += var9;
                }

            }
        } catch (Exception var19) {
            System.out.println("error in plot_scale");
        }
    }

    final void centrepara(int var1, boolean var2, boolean var3, int var4, int var5, String var6, int var7, int var8) {
        try {
            int var9 = 0;
            byte[] var10 = gameFonts[var4];
            int var11 = 0;
            int var12 = 0;

            for (int var13 = 0; var13 < var6.length(); ++var13) {
                if (var6.charAt(var13) == 64 && (4 + var13) < var6.length() && var6.charAt(var13 + 4) == 64) {
                    var13 += 4;
                } else if (var6.charAt(var13) == 126 && var6.length() > (var13 - -4) && var6.charAt(4 + var13) == 126) {
                    var13 += 4;
                } else {
                    char var14 = var6.charAt(var13);
                    if (var14 < 0 || var14 >= JagGrab.anIntArray1124.length) {
                        var14 = 32;
                    }

                    var9 += var10[JagGrab.anIntArray1124[var14] - -7];
                }

                if (var6.charAt(var13) == 32) {
                    var12 = var13;
                }

                if (var6.charAt(var13) == 37 && var3) {
                    var9 = 1000;
                    var12 = var13;
                }

                if (var9 > var7) {
                    if (var11 >= var12) {
                        var12 = var13;
                    }

                    var9 = 0;
                    this.method417(var8, var5, var1, var4, 2, var6.substring(var11, var12), 0);
                    var8 += this.textHeight(var4, true);
                    var11 = var13 = var12 + 1;
                }
            }

            if (var9 > 0) {
                this.method417(var8, var5, var1, var4, 2, var6.substring(var11), 0);
            }
        } catch (Exception var15) {
            System.out.println("centrepara: " + var15);
            var15.printStackTrace();
        }

        if (!var2) {
        }
    }

    final void method424(int var1, int var2, int var3, int var4, int var5, int var6) {
        this.anIntArray600[var5] = var2;
        this.anIntArray577[var5] = var1;
        this.aBooleanArray581[var5] = false;
        this.anIntArray631[var5] = 0;
        this.anIntArray609[var5] = 0;
        this.anIntArray627[var5] = var2;
        this.anIntArray649[var5] = var1;
        int var7 = var2 * var1;
        this.surfaceSetPixels[var5] = new int[var7];
        int var8 = var4;

        for (int var9 = var6; var9 < (var2 + var6); ++var9) {
            for (int var10 = var3; var1 + var3 > var10; ++var10) {
                this.surfaceSetPixels[var5][var8++] = this.anIntArray607[this.width2 * var10 + var9];
            }
        }
    }

    final void method425(int var1, int var2, int var3, int var4, int var5, int var6, int var7) {
        try {
            int var8 = this.anIntArray600[var4];
            int var9 = this.anIntArray577[var4];
            int var10 = 0;
            if (var5 > -38) {
                this.anIntArray610 = null;
            }

            int var11 = 0;
            int var12 = (var8 << 16) / var1;
            int var13 = (var9 << 16) / var6;
            int var14;
            int var15;
            if (this.aBooleanArray581[var4]) {
                var14 = this.anIntArray627[var4];
                var15 = this.anIntArray649[var4];
                if (var14 == 0 || var15 == 0) {
                    return;
                }

                var7 += (var6 * this.anIntArray609[var4] - -var15 - 1) / var15;
                if (this.anIntArray631[var4] * var1 % var14 != 0) {
                    var10 = (-(this.anIntArray631[var4] * var1 % var14) + var14 << 16) / var1;
                }

                var2 += (-1 + (var1 * this.anIntArray631[var4] - -var14)) / var14;
                var13 = (var15 << 16) / var6;
                var12 = (var14 << 16) / var1;
                if ((this.anIntArray609[var4] * var6 % var15) != 0) {
                    var11 = (var15 + -(this.anIntArray609[var4] * var6 % var15) << 16) / var6;
                }

                var1 = var1 * (-(var10 >> 16) + this.anIntArray600[var4]) / var14;
                var6 = var6 * (this.anIntArray577[var4] + -(var11 >> 16)) / var15;
            }

            var14 = var2 - -(this.width2 * var7);
            int var16;
            if (var7 < this.anInt651) {
                var16 = this.anInt651 - var7;
                var6 -= var16;
                var7 = 0;
                var11 += var13 * var16;
                var14 += var16 * this.width2;
            }

            var15 = this.width2 + -var1;
            if (var2 < this.anInt621) {
                var16 = this.anInt621 - var2;
                var10 += var12 * var16;
                var1 -= var16;
                var2 = 0;
                var14 += var16;
                var15 += var16;
            }

            if (this.anInt636 <= var7 + var6) {
                var6 -= var6 + var7 + -this.anInt636 + 1;
            }

            if ((var2 - -var1) >= this.anInt660) {
                var16 = -this.anInt660 + var2 + var1 + 1;
                var15 += var16;
                var1 -= var16;
            }

            byte var19 = 1;
            if (this.interlace) {
                var15 += this.width2;
                if ((1 & var7) != 0) {
                    var14 += this.width2;
                    --var6;
                }

                var19 = 2;
                var13 += var13;
            }

            this.method415(this.anIntArray607, var13, var15, var8, this.surfaceSetPixels[var4], var1, var14, var12, var6, 0, var19, var11, var10, false, var3);
        } catch (Exception var17) {
            System.out.println("error in sprite clipping routine");
        }
    }

    final void method426(int var1, int var2, int var3, int var4, byte var5, int var6) {
        try {
            int var7 = this.anIntArray600[var4];
            int var8 = this.anIntArray577[var4];
            int var9 = 0;
            int var10 = 36 % ((var5 - -36) / 36);
            int var11 = 0;
            int var12 = (var7 << 16) / var6;
            int var13 = (var8 << 16) / var2;
            int var14;
            int var15;
            if (this.aBooleanArray581[var4]) {
                var14 = this.anIntArray627[var4];
                var15 = this.anIntArray649[var4];
                if (var14 == 0 || var15 == 0) {
                    return;
                }

                var12 = (var14 << 16) / var6;
                var13 = (var15 << 16) / var2;
                if (this.anIntArray609[var4] * var2 % var15 != 0) {
                    var11 = (-(var2 * this.anIntArray609[var4] % var15) + var15 << 16) / var2;
                }

                var3 += (-1 + var6 * this.anIntArray631[var4] + var14) / var14;
                if ((var6 * this.anIntArray631[var4] % var14) != 0) {
                    var9 = (-(this.anIntArray631[var4] * var6 % var14) + var14 << 16) / var6;
                }

                var1 += (var15 + var2 * this.anIntArray609[var4] + -1) / var15;
                var6 = var6 * (-(var9 >> 16) + this.anIntArray600[var4]) / var14;
                var2 = (-(var11 >> 16) + this.anIntArray577[var4]) * var2 / var15;
            }

            var14 = var3 - -(this.width2 * var1);
            int var16;
            if (this.anInt651 > var1) {
                var16 = -var1 + this.anInt651;
                var11 += var13 * var16;
                var2 -= var16;
                var14 += var16 * this.width2;
                var1 = 0;
            }

            var15 = -var6 + this.width2;
            if (var3 < this.anInt621) {
                var16 = -var3 + this.anInt621;
                var15 += var16;
                var6 -= var16;
                var3 = 0;
                var14 += var16;
                var9 += var12 * var16;
            }

            if (this.anInt636 <= var2 + var1) {
                var2 -= 1 + (var1 + var2 - this.anInt636);
            }

            if (this.anInt660 <= (var3 - -var6)) {
                var16 = -this.anInt660 + (var3 - -var6) - -1;
                var6 -= var16;
                var15 += var16;
            }

            byte var19 = 1;
            if (this.interlace) {
                var13 += var13;
                var15 += this.width2;
                var19 = 2;
                if ((1 & var1) != 0) {
                    --var2;
                    var14 += this.width2;
                }
            }

            this.plot_scale(var2, 0, var15, var7, this.anIntArray607, -15265, var12, var9, var19, var6, this.surfaceSetPixels[var4], var11, var13, var14);
        } catch (Exception var17) {
            System.out.println("error in sprite clipping routine");
        }
    }

    private void method427(byte[] var1, int var2, boolean var3, int var4, int var5, int var6, int[] var7, int var8, int var9, int var10, int[] var11, int var12) {
        int var13 = -var5 + 256;
        int var14 = -var9;
        if (var3) {
            this.anIntArray649 = null;
        }

        while (var14 < 0) {
            for (int var15 = -var8; var15 < 0; ++var15) {
                byte var16 = var1[var4++];
                if (var16 != 0) {
                    int var19 = var11[var16 & 255];
                    int var17 = var7[var12];
                    var7[var12++] = Utility.bitwiseAnd(var13 * Utility.bitwiseAnd(16711935, var17) + Utility.bitwiseAnd(16711935, var19) * var5, -16711936) + Utility.bitwiseAnd(var5 * Utility.bitwiseAnd(var19, '\uff00') + Utility.bitwiseAnd(var17, '\uff00') * var13, 16711680) >> 8;
                } else {
                    ++var12;
                }
            }

            var4 += var10;
            var12 += var2;
            var14 += var6;
        }
        ;
    }

}
