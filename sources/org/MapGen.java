package org;

import org.jaglib.Archive;
import org.jaglib.File;
import org.jaglib.Util;
import org.jaglib.io.ArchiveManager;
import org.jaglib.io.ReadBuffer;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

/**
 * rsc
 * 12-01-2014
 */
public class MapGen {

    private String path;
    private Archive landjag;
    private Archive landmem;
    private Archive mapsjag;
    private Archive mapsmem;
    private byte[] terrainHeight;
    private byte[] terrainColour;
    private byte[] wallsNorthSouth;
    private byte[] wallsEastWest;
    private int[] wallsDiagonal;
    private byte[] wallsRoof;
    private byte[] tileDecoration;
    private byte[] tileDirection;
    private int[] colours;

    public MapGen() {
        terrainHeight = new byte[2304];
        colours = new int[256];
        load();
    }

    public static int method305(int i, int j, int k) {
        return -1 - (i / 8) * 1024 - (j / 8) * 32 - k / 8;
    }

    public void load() {
        terrainHeight = terrainColour = wallsNorthSouth = wallsEastWest = wallsRoof
                = tileDecoration = tileDirection = new byte[2304];
        wallsDiagonal = new int[2304];

        for(int i = 0; i < 64; i++) {
            colours[i] = method305(255 - i * 4, 255 - (int)(i * 1.75D), 255 - i * 4);
        }
        for(int i = 0; i < 64; i++) {
            colours[i + 64] = method305(i * 3, 144, 0);
        }
        for(int i = 0; i < 64; i++) {
            colours[i + 128] = method305(192 - (int)(i * 1.5D), 144 - (int)(i * 1.5D), 0);
        }
        for(int i = 0; i < 64; i++) {
            colours[i + 192] = method305(96 - (int)(i * 1.5D), 48 + (int)(i * 1.5D), 0);
        }

        landjag = new ArchiveManager().loadArchive("data204/land63.jag");
        landmem = new ArchiveManager().loadArchive("data204/land63.mem");
        mapsjag = new ArchiveManager().loadArchive("data204/maps63.jag");
        mapsmem = new ArchiveManager().loadArchive("data204/maps63.mem");
        BufferedImage bi = new BufferedImage(100 * 48, 100 * 48 * 4, BufferedImage.TYPE_INT_RGB);
        for(int plane = 0; plane < 4; plane++) {
            for(int x = 0; x < 100; x++) {
                for(int y = 0; y < 100; y++) {
                    String mapname = "m" + plane + x / 10 + x % 10 + y / 10 + y % 10;
                    File file = landjag.getFile(Util.calcHash(mapname + ".hei"));
                    if(file == null) {
                        file = landmem.getFile(Util.calcHash(mapname + ".hei"));
                    }
                    if (file != null) {
                        System.out.println(mapname);
                        ReadBuffer buf = file.getBuffer();
                        int caret = 0;
                        int lastval = 0;
                        for (int tile = 0; tile < 2304; ) {
                            int val = buf.readUnsignedByte();
                            if (val < 128) {
                                terrainHeight[tile++] = (byte) val;
                                lastval = val;
                            }
                            if (val >= 128) {
                                for (int i = 0; i < val - 128; i++)
                                    terrainHeight[tile++] = (byte) lastval;

                            }
                        }

                        lastval = 64;
                        for (int tileY = 0; tileY < 48; tileY++) {
                            for (int tileX = 0; tileX < 48; tileX++) {
                                lastval = terrainHeight[tileX * 48 + tileY] + lastval & 0x7f;
                                terrainHeight[tileX * 48 + tileY] = (byte) (lastval * 2);
                            }

                        }

                        lastval = 0;
                        for (int tile = 0; tile < 2304; ) {
                            int val = buf.readUnsignedByte();
                            if (val < 128) {
                                terrainColour[tile++] = (byte) val;
                                lastval = val;
                            }
                            if (val >= 128) {
                                for (int i = 0; i < val - 128; i++)
                                    terrainColour[tile++] = (byte) lastval;

                            }
                            System.out.println(terrainColour[tile - 1]);
                        }

                        lastval = 35;
                        for (int tileY = 0; tileY < 48; tileY++) {
                            for (int tileX = 0; tileX < 48; tileX++) {
                                lastval = terrainColour[tileX * 48 + tileY] + lastval & 0x7f;// ??? wat
                                terrainColour[tileX * 48 + tileY] = (byte) (lastval * 2);
                            }

                        }

                    } else {
                        for (int tile = 0; tile < 2304; tile++) {
                            terrainHeight[tile] = 0;
                            terrainColour[tile] = 0;
                        }

                    }
                    file = mapsjag.getFile(Util.calcHash(mapname + ".dat"));
                    if (file == null)
                        file = mapsmem.getFile(Util.calcHash(mapname + ".dat"));
                    if(file != null) {
                        ReadBuffer buf = file.getBuffer();
                        int caret = 0;
                        for (int tile = 0; tile < 2304; tile++)
                            wallsNorthSouth[tile] = buf.readByte();

                        for (int tile = 0; tile < 2304; tile++)
                            wallsEastWest[tile] = buf.readByte();

                        for (int tile = 0; tile < 2304; tile++)
                            wallsDiagonal[tile] = buf.readUnsignedByte();

                        for (int tile = 0; tile < 2304; tile++) {
                            int val = buf.readUnsignedByte();
                            if (val > 0)
                                wallsDiagonal[tile] = val + 12000;// why??
                        }

                        for (int tile = 0; tile < 2304; ) {
                            int val = buf.readUnsignedByte();
                            if (val < 128) {
                                wallsRoof[tile++] = (byte) val;
                            } else {
                                for (int i = 0; i < val - 128; i++)
                                    wallsRoof[tile++] = 0;

                            }
                        }

                        int lastval = 0;
                        for (int tile = 0; tile < 2304; ) {
                            int val = buf.readUnsignedByte();
                            if (val < 128) {
                                tileDecoration[tile++] = (byte) val;
                                lastval = val;
                            } else {
                                for (int i = 0; i < val - 128; i++)
                                    tileDecoration[tile++] = (byte) lastval;

                            }
                        }

                        for (int tile = 0; tile < 2304; ) {
                            int val = buf.readUnsignedByte();
                            if (val < 128) {
                                tileDirection[tile++] = (byte) val;
                            } else {
                                for (int j11 = 0; j11 < val - 128; j11++)
                                    tileDirection[tile++] = 0;

                            }
                        }
                    }

                    file = mapsjag.getFile(Util.calcHash(mapname + ".loc"));
                    if (file != null) {
                        ReadBuffer buf = file.getBuffer();
                        int caret1 = 0;
                        for (int tile = 0; tile < 2304; ) {
                            int val = buf.readUnsignedByte();
                            if (val < 128)
                                wallsDiagonal[tile++] = val + 48000;
                            else
                                tile += val - 128;
                        }

                    }

                    try {
                        if(x == 50 && y == 50) {
                            BufferedImage im = new BufferedImage(48, 48, BufferedImage.TYPE_INT_RGB);
                            // 100 * 48, 100 * 48 * 4
                            for(int tileX = 0; tileX < 48; tileX++) {
                                for(int tileY = 0; tileY < 48; tileY++) {
                                    // tileX * 48 + tileY
                                    int ix = x * 48 + tileX;
                                    int iy = y * 48 * plane + tileY;
                                    //System.out.println(colours[terrainColour[tileY * 48 + tileX]]);
                                    bi.setRGB(tileX, tileY, colours[terrainColour[tileX * 48 + tileY] & 0xff]);
                                }
                            }
                            ImageIO.write(im, "png", new java.io.File(mapname + ".png"));
                            return;
                        }
                    } catch(Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }
        System.out.println("Ok");
        /*try {
            ImageIO.write(bi, "png", new java.io.File("map.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }*/
    }
}
