package org.jaglib;

import org.jaglib.io.BZLib;
import org.jaglib.io.ReadBuffer;
import org.jaglib.io.CBZip2OutputStream;
import org.jaglib.io.WriteBuffer;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * rsc
 * 03-01-2014
 */
public class Archive {

    private ReadBuffer readBuffer;
    private WriteBuffer writeBuffer;
    private int archiveSize;
    private int archiveSizeCompressed;
    private List<File> files;
    private boolean unpacked, loaded;

    public Archive(byte[] buffer) {
        this.readBuffer = new ReadBuffer(buffer);
        files = new ArrayList<>();
    }

    public Archive unpack() {
        if (unpacked) {
            return this;
        }
        ReadBuffer header = readBuffer.copy(0, 6);
        archiveSize = header.readUnsigned3Byte();
        archiveSizeCompressed = header.readUnsigned3Byte();
        if (archiveSizeCompressed != archiveSize) {
            byte[] decompressed = new byte[archiveSize];
            BZLib.decompress(decompressed, archiveSize, readBuffer.cut(6).getBuffer(), archiveSizeCompressed, 0);
            readBuffer = new ReadBuffer(decompressed);
        } else {
            readBuffer = readBuffer.cut(6);
        }
        unpacked = true;
        return this;
    }

    public byte[] pack() {
        try {
            String SPAGHETTI = "CODE";
            WriteBuffer w = new WriteBuffer();
            CBZip2OutputStream c = new CBZip2OutputStream(w, 1);
            c.write(readBuffer.getBuffer());
            c.close();
            w = new WriteBuffer().write3Byte(archiveSize).write3Byte(archiveSizeCompressed)
                    .writeBytes(w.getBuffer(), 2);//? ?? ? ? ?
            return w.getBuffer();

            /*ByteArrayOutputStream baus = new ByteArrayOutputStream();
            CBZip2OutputStream cbou = new CBZip2OutputStream(baus, 1);
            cbou.write(readBuffer.getBuffer());
            cbou.flush();
            cbou.close();
            WriteBuffer buf = new WriteBuffer();
            buf.write3Byte(archiveSize);
            buf.write3Byte(archiveSizeCompressed);
            buf.writeBytes(baus.toByteArray(), 2);// ?? ? ? +
            return buf.getBuffer();*/
        } catch (IOException e) {
            System.err.println("hey what the fuck");
            return null;
        }
    }

    public void load() {
        if (loaded) {
            return;
        }
        int numEntries = readBuffer.getUnsignedShort();
        int offset = 2 + 10 * numEntries;
        for (int entry = 0; entry < numEntries; entry++) {
            int hash = readBuffer.getUnsignedInt(2 + 10 * entry);
            int size = readBuffer.getUnsigned3Byte(6 + 10 * entry);
            int sizec = readBuffer.getUnsigned3Byte(9 + 10 * entry);
            byte[] bytes = new byte[size];
            if (size != sizec) {
                BZLib.decompress(bytes, size, readBuffer.getBuffer(), sizec, offset);
            } else {
                for (int i = 0; i < size; i++) {
                    bytes[i] = readBuffer.getByte(offset + i);
                }
            }
            addFile(new File(bytes, hash, size, sizec));
            offset += sizec;
        }
        loaded = true;
    }

    public boolean addFile(File file) {
        if (!files.contains(file)) {
            files.add(file);
            return true;
        }
        return false;
    }

    public boolean removeFile(File file) {
        if (files.contains(file)) {
            files.remove(file);
            return true;
        }
        return false;
    }

    public boolean removeFile(int hash) {
        for (File f : files) {
            if (f.getHash() == hash) {
                return removeFile(f);
            }
        }
        return false;
    }

    public File[] getFiles() {
        return files.toArray(new File[0]);
    }

    public File getFile(int hash) {
        for(File f : files) {
            if(f.getHash() == hash) {
                return f;
            }
        }
        return null;
    }

    public byte[] getBuffer() {
        return readBuffer.getBuffer();
    }

    public String toString() {
        return String.format("Archive[size=%d sizec=%d filec=%d buffer=%s]", archiveSize, archiveSizeCompressed, files.size(), readBuffer.toString());
    }
}
