package org.jaglib;

import org.jaglib.io.ReadBuffer;

/**
 * rsc
 * 09-01-2014
 */
public class File {

    private int hash;
    private int size;
    private int sizeCompressed;
    private ReadBuffer readBuffer;

    public File(byte[] b, int hash, int size, int sizeCompressed) {
        readBuffer = new ReadBuffer(b);
        this.hash = hash;
        this.size = size;
        this.sizeCompressed = sizeCompressed;
    }

    public boolean equals(Object o) {
        if (!(o instanceof File)) {
            return false;
        }
        File f = (File) o;
        return hash == f.hash && size == f.size && sizeCompressed == f.sizeCompressed;
    }

    public String toString() {
        return "File[hash=" + hash + " size=" + size
                + " sizec=" + sizeCompressed + " len=" + readBuffer.getBuffer().length + "]";
    }

    public int getHash() {
        return hash;
    }

    public int getSize() {
        return size;
    }

    public int getSizeCompressed() {
        return sizeCompressed;
    }

    public ReadBuffer getBuffer() {
        return readBuffer;
    }
}
