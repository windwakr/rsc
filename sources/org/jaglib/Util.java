package org.jaglib;

/**
 * rsc
 * 07.01.2014
 */
public class Util {

    public static int calcHash(String s) {
        int hash = 0;
        s = s.toUpperCase();
        for (int l = 0; l < s.length(); l++)
            hash = (hash * 61 + s.charAt(l)) - 32;
        return hash;
    }
}
