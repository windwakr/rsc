package org.jaglib.io;

import java.io.OutputStream;
import java.util.Arrays;

/**
 * rsc
 * 08.01.2014
 */
public class WriteBuffer extends OutputStream {

    private byte[] buffer;
    private int caret;

    public WriteBuffer() {
        buffer = new byte[0];
        caret = 0;
    }

    public WriteBuffer reset() {
        caret = 0;
        buffer = new byte[0];
        return this;
    }

    private void grow(int by) {
        if (buffer.length - 1 < caret + by) {
            buffer = Arrays.copyOf(buffer, caret + by);
        }
    }

    @Override
    public void write(int b) {
        System.out.println("write");
        writeByte(b);
    }

    public WriteBuffer writeByte(int b) {
        grow(1);
        buffer[caret++] = (byte) b;
        return this;
    }

    public WriteBuffer writeShort(int s) {
        grow(2);
        buffer[caret++] = (byte) (s >> 8);
        buffer[caret++] = (byte) s;
        return this;
    }

    public WriteBuffer write3Byte(int i) {
        grow(3);
        buffer[caret++] = (byte) (i >> 16);
        buffer[caret++] = (byte) (i >> 8);
        buffer[caret++] = (byte) i;
        return this;
    }

    public WriteBuffer writeInt(int i) {
        grow(4);
        buffer[caret++] = (byte) (i >> 24);
        buffer[caret++] = (byte) (i >> 16);
        buffer[caret++] = (byte) (i >> 8);
        buffer[caret++] = (byte) i;
        return this;
    }

    public WriteBuffer writeBytes(byte[] b) {
        writeBytes(b, 0);
        return this;
    }

    public WriteBuffer writeBytes(byte[] b, int o) {
        grow(b.length - o);
        System.arraycopy(b, o, buffer, caret, b.length - o);
        return this;
    }

    public WriteBuffer putByte(int o, int b) {
        buffer[o] = (byte) b;
        return this;
    }

    public WriteBuffer putShort(int o, int s) {
        buffer[o] = (byte) (s >> 8);
        buffer[o + 1] = (byte) s;
        return this;
    }

    public WriteBuffer put3Byte(int o, int i) {
        buffer[o] = (byte) (i >> 16);
        buffer[o + 1] = (byte) (i >> 8);
        buffer[o + 2] = (byte) i;
        return this;
    }

    public WriteBuffer putInt(int o, int i) {
        buffer[o] = (byte) (i >> 24);
        buffer[o + 1] = (byte) (i >> 16);
        buffer[o + 2] = (byte) (i >> 8);
        buffer[o + 3] = (byte) i;
        return this;
    }

    public WriteBuffer putBytes(int o, byte[] bytes) {
        System.arraycopy(bytes, 0, buffer, o, bytes.length);
        return this;
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public WriteBuffer copy(int offset, int length) {
        WriteBuffer inputBuffer = new WriteBuffer();
        System.arraycopy(this.buffer, offset, inputBuffer.buffer, 0, length);
        return inputBuffer;
    }

    public WriteBuffer copy() {
        return copy(0, buffer.length);
    }

    public String toString() {
        return String.format("WriteBuffer[length=%d caret=%d]", buffer.length, caret);
    }
}
