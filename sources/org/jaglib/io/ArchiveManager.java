package org.jaglib.io;

import org.jaglib.Archive;
import org.jaglib.File;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashMap;

/**
 * rsc
 * 07.01.2014
 */
public class ArchiveManager {

    private HashMap<String, Archive> archives;

    public ArchiveManager() {
        archives = new HashMap<>();
    }

    public Archive loadArchive(String filename) {
        return loadArchive(Paths.get(filename));
    }

    public Archive loadArchive(Path path) {
        try {
            Archive archive = new Archive(Files.readAllBytes(path));
            archive.unpack();
            archive.load();
            archives.put(path.getFileName().toString(), archive);
            return archive;
        } catch (IOException e) {
            System.err.println("error loading archive " + path);
            e.printStackTrace();
            return null;
        }
    }

    public Archive getArchive(String filename) {
        return archives.get(filename);
    }

    public void dumpArchive(Archive archive, String outfilename, boolean pack) {
        dumpArchive(archive, Paths.get(outfilename), pack);
    }

    public void dumpArchive(String filename, Path outpath, boolean pack) {
        dumpArchive(archives.get(filename), outpath, pack);
    }

    public void dumpArchive(String filename, String outfilename, boolean pack) {
        dumpArchive(archives.get(filename), Paths.get(outfilename), pack);
    }

    public void dumpArchive(Archive archive, Path outpath, boolean pack) {
        try {
            Files.write(outpath, pack ? archive.pack() : archive.getBuffer(), StandardOpenOption.CREATE);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void dumpFile(File file, Path outpath) {
        try {
            Files.write(outpath, file.getBuffer().getBuffer(), StandardOpenOption.CREATE);
        } catch(IOException e) {
            e.printStackTrace();
        }
    }
}
