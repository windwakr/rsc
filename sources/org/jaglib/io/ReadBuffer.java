package org.jaglib.io;

/**
 * rsc
 * 08.01.2014
 */
public class ReadBuffer {

    private byte[] buffer;
    private int caret;

    public ReadBuffer(byte[] buffer) {
        this.buffer = buffer;
        caret = 0;
    }

    public void reset() {
        caret = 0;
    }

    public int getCaret() {
        return caret;
    }

    public void setCaret(int caret) {
        this.caret = caret;
    }

    public byte getByte() {
        return buffer[caret];
    }

    public byte getByte(int offset) {
        return buffer[offset];
    }

    public int getShort() {
        return getByte() * 256 + getByte();
    }

    public int getUnsignedByte() {
        return buffer[caret] & 255;
    }

    public int getUnsignedByte(int offset) {
        return buffer[offset] & 255;
    }

    public int getUnsignedShort() {
        return ((buffer[caret] & 255) << 8) + (buffer[caret + 1] & 255);
    }

    public int getUnsigned3Byte() {
        return ((buffer[caret] & 255) << 16) + ((buffer[caret + 1] & 255) << 8) + (buffer[caret + 2] & 255);
    }

    public int getUnsigned3Byte(int offset) {
        return ((buffer[offset] & 255) << 16) + ((buffer[offset + 1] & 255) << 8) + (buffer[offset + 2] & 255);
    }

    public int getUnsignedInt() {
        return ((buffer[caret] & 255) << 24) + ((buffer[caret + 1] & 255) << 16) + ((buffer[caret + 2] & 255) << 8) + (buffer[caret + 3] & 255);
    }

    public int getUnsignedInt(int offset) {
        return ((buffer[offset] & 255) << 24) + ((buffer[offset + 1] & 255) << 16) + ((buffer[offset + 2] & 255) << 8) + (buffer[offset + 3] & 255);
    }

    public byte readByte() {
        byte b = getByte();
        caret++;
        return b;
    }

    public int readUnsignedByte() {
        int i = getUnsignedByte();
        caret++;
        return i;
    }

    public int readUnsignedShort() {
        int i = getUnsignedShort();
        caret += 2;
        return i;
    }

    public int readUnsigned3Byte() {
        int i = getUnsigned3Byte();
        caret += 3;
        return i;
    }

    public int readUnsignedInt() {
        int i = getUnsignedInt();
        caret += 4;
        return i;
    }

    public byte[] getBuffer() {
        return buffer;
    }

    public byte[] getBytes(int offset, int length) {
        byte[] bytes = new byte[length];
        System.arraycopy(buffer, offset, bytes, 0, length);
        return bytes;
    }

    public ReadBuffer copy(int offset, int length) {
        ReadBuffer readBuffer = new ReadBuffer(new byte[length]);
        System.arraycopy(this.buffer, offset, readBuffer.buffer, 0, length);
        return readBuffer;
    }

    public ReadBuffer copy() {
        return copy(0, buffer.length);
    }

    public ReadBuffer cut(int start, int end) {
        ReadBuffer readBuffer = new ReadBuffer(new byte[end - start]);
        System.arraycopy(this.buffer, start, readBuffer.buffer, 0, end - start);
        return readBuffer;
    }

    public ReadBuffer cut(int start) {
        return cut(start, buffer.length);
    }

    public String toString() {
        return String.format("ReadBuffer[length=%d caret=%d]", buffer.length, caret);
    }
}
