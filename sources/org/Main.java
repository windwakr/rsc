package org;

import org.jaglib.Archive;
import org.jaglib.io.ArchiveManager;

import java.io.IOException;
import java.math.BigInteger;

/**
 * rsc
 * 03-01-2014
 */
public class Main {

    private ArchiveManager am;

    public static void main(String[] args) throws IOException {
        //MapGen mg = new MapGen();
    }

    private Main() throws IOException {
        am = new ArchiveManager();
        Archive a = am.loadArchive("excl/bztest/sounds.mem");

    }

    static BigInteger modulus = new BigInteger("58778699976184461502525193738213253649000149147835990136706041084440742975821");
    static BigInteger exponent = new BigInteger("7162900525229798032761816791230527296329313291232324290237849263501208207972894053929065636522363163621000728841182238772712427862772219676577293600221789");

}
