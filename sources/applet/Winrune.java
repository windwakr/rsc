package applet;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.html.HTMLEditorKit;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * rsc
 * 02.01.2014
 */
public class Winrune extends Applet implements Runnable {

    private JEditorPane editorPane;
    private String loading;
    private String downloading;
    private String launching;
    private String server = "classic2";
    private String client = "client233.client";
    private Applet gameApplet;
    private Map<String, String> appletParams;
    private JFrame frame;
    private String cacheid = ".file_store_32";
    private String nicename[] = {
            "main game", "config", "monster graphics", "jagex library", "landscape", "maps", "object graphics",
            "3d models", "textures", /*"chat system",*/ "members monsters", "members land", "members maps",
            "sound effects"
    };
    private String internetname[] = {
            "rsclassic.jar", "content0_", "content1_", "content3_", "content6_", "content4_", "content8_",
            "content9_", "content11_", /*"",*/ "content2_", "content7_", "content5_",
            "content10_"
    };
    private int size[] = {
            289871, 58819, 244467, 4990, 142219, 37613, 98857,
            289822, 63685, /*0,*/ 48212, 155110, 60002,
            114375
    };
    private String[] crc = {
            "", "229aa476", "1c9fa8c3", "5181c9f5", "ffffffffe997514b", "ffffffffaaca2b0d", "ffffffffb03e2a0c",
            "ffffffffe0e19e2c", "7d5437c5", /*"",*/ "2fdddb3c", "3fc5d9e3", "6a1d6b00",
            "ffffffffa95e7195"
    };

    public static void main(String[] args) throws Exception {
        Winrune wr = new Winrune();
        wr.appletParams = wr.getAppletParams();
    }

    public Winrune() {
        loading = ("<html><body bgcolor=#3e5266 text=white><br><br><br><br><br><center><table bgcolor=black><tr><td><table bgcolor=#aaaaaa><tr><td bgcolor=black width=400><font face=Arial><center><h1>RuneScape</h1>By Jagex Software<h3>Checking for latest updates<br>Please wait a moment...</h3></center><font></td></tr></table></td></tr></table></center></body></html>");
        downloading = ("<html><body bgcolor=#3e5266 text=white><br><br><br><br><center><table bgcolor=black><tr><td><table bgcolor=#aaaaaa><tr><td bgcolor=black width=400><font face=Arial><center><h1>RuneScape</h1>By Jagex Software<h3>RuneScape has been updated!<br>Please wait - Fetching new files...<br></h3>This may take a few minutes, but only<br>needs to be done when the game is updated.<br><h3>Downloading: %s (%dk)</h3></center></font></td></tr></table></td></tr></table></center></body></html>");
        launching = ("<html><head></head><body bgcolor=#3e5266 text=white><br><br><br><br><br><center><table bgcolor=black><tr><td><table bgcolor=#aaaaaa><tr><td bgcolor=black width=400><font face=Arial><center><h1>RuneScape</h1>By Jagex Software<h3><br>Launching game...</h3></center></font></td></tr></table></td></tr></table></center></body></html>");

        frame = new JFrame("RuneScape - by Jagex Software");

        editorPane = new JEditorPane();
        HTMLEditorKit kit = new HTMLEditorKit();
        editorPane.setEditorKit(kit);
        Document doc = kit.createDefaultDocument();
        editorPane.setEditable(false);
        editorPane.setText(loading);
        editorPane.setDoubleBuffered(true);

        final Winrune wr = this;
        editorPane.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                new Thread(wr).start();
            }
        });

        frame.getContentPane().add(editorPane, BorderLayout.CENTER);
        editorPane.setBackground(new Color(62, 85, 102));

        frame.setSize(540, 545);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private String findcachedir() {
        String[] as = {
                "c:/", "~/", ""
        };
        for (int i = 0; i < as.length; i++) {
            try {
                String s = as[i];
                if (s.length() > 0) {
                    File file = new File(s);
                    if (!file.exists()) {
                        continue;
                    }
                }
                File file = new File(s + cacheid);
                if (file.exists() || file.mkdir()) {
                    return s + cacheid + "/";
                }
            } catch (Exception ex) {
            }
        }
        return null;
    }

    @Override
    public void run() {
        String cachedir = findcachedir();
        if (cachedir == null) {
            return;
        }
        for (int i = 0; i < internetname.length; i++) {
            String filename = internetname[i] + crc[i];
            byte[] bytes = loadfile(cachedir + filename);
            if (bytes != null) {
                continue;
            }
            try {
                URL url = new URL(getCodeBase(), filename);
                DataInputStream input = new DataInputStream(url.openStream());
                int size = this.size[i];
                bytes = new byte[size];
                for (int j = 0; j < size; j += 1000) {
                    int k = size - j;
                    if (k > 1000) {
                        k = 1000;
                    }
                    input.readFully(bytes, j, k);
                    editorPane.setText(String.format(downloading, nicename[i], j / 1000));
                    repaint();
                }
                input.close();
                FileOutputStream output = new FileOutputStream(cachedir + filename);
                output.write(bytes);
                output.close();
            } catch (Exception ex) {
            }
        }
        editorPane.setText(launching);
        try {
            load();
            launch();
        } catch (Exception e) {
        }
    }

    private byte[] loadfile(String filename) {
        try {
            return Files.readAllBytes(Paths.get(filename));
        } catch (Exception ex) {
            return null;
        }
    }

    private Map<String, String> getAppletParams() throws IOException, URISyntaxException {
        Map<String, String> params = new HashMap<String, String>();
        String uri = "http://" + server + ".runescape.com/j0,a1";// unsigned applet url cus no cabbase
        BufferedReader in = new BufferedReader(new InputStreamReader(new URL(uri).openStream()));
        String line = null;
        while ((line = in.readLine()) != null) {
            if (line.startsWith("<param name=")) {
                line = line.substring("<param name=".length());
                line = line.substring(0, line.length() - 1);
                if (line.contains("\"")) {
                    line = line.replaceAll("\"", "");
                }
                String name = line.substring(0, line.indexOf(' '));
                String value = line.substring(line.indexOf("value=") + "value=".length());
                System.out.println("PARAM PUT \"" + name + "\" = \"" + value + "\"");
                params.put(name, value);
            }
        }
        return params;
    }

    private void load() throws ClassNotFoundException, IllegalAccessException, InstantiationException, NoSuchMethodException, InvocationTargetException {
        Class clazz = Class.forName(client);
        gameApplet = (Applet) clazz.newInstance();
        Method method = clazz.getMethod("provideLoaderApplet",
                new Class[]{
                        Class.forName("java.applet.Applet")
                }
        );
        method.invoke((Object) null, new Object[]{this});
    }

    private void launch() {
        System.out.println("SERVER " + server);
        System.out.println("CLIENT " + client);

        frame.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                System.out.println("CLOSING");
                gameApplet.stop();
                gameApplet.destroy();
                try {
                    Thread.sleep(1000L);
                } catch (Exception ex) {
                }
                System.exit(0);
            }
        });
        gameApplet.setBounds(0, 0, 540, 545);
        Container container = frame.getContentPane();
        container.removeAll();
        container.add(this);
        frame.setContentPane(container);

        gameApplet.init();
        gameApplet.start();
    }

    public String getParameter(String name) {
        System.out.println("PARAM GET " + name);
        if (appletParams.containsKey(name)) {
            return appletParams.get(name);
        }
        return "";
    }

    public URL getDocumentBase() {
        try {
            return new URL("http://" + server + ".runescape.com/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public URL getCodeBase() {
        try {
            return new URL("http://" + server + ".runescape.com/");
        } catch (MalformedURLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void destroy() {
        if (gameApplet != null) {
            gameApplet.destroy();
        }
    }

    public void paint(Graphics g) {
        if (gameApplet != null) {
            gameApplet.paint(g);
        }
        if (editorPane != null) {
            editorPane.paint(g);
        }
    }

    public void update(Graphics g) {
        if (gameApplet != null) {
            gameApplet.paint(g);
        }
        if (editorPane != null) {
            editorPane.paint(g);
        }
    }
}
