/*    */ package jagex;
/*    */ 
/*    */ class Bzip2Context
/*    */ {
/* 28 */   final int anInt395 = 4096;
/* 29 */   final int anInt396 = 16;
/* 30 */   final int anInt397 = 258;
/* 31 */   final int anInt398 = 23;
/* 32 */   final int anInt399 = 0;
/* 33 */   final int anInt400 = 1;
/* 34 */   final int anInt401 = 6;
/* 35 */   final int anInt402 = 50;
/* 36 */   final int anInt403 = 4;
/* 37 */   final int anInt404 = 18002;
/*    */   byte[] aByteArray405;
/*    */   int anInt406;
/*    */   int anInt407;
/*    */   int anInt408;
/*    */   int anInt409;
/*    */   byte[] aByteArray410;
/*    */   int anInt411;
/*    */   int anInt412;
/*    */   int anInt413;
/*    */   int anInt414;
/*    */   byte aByte415;
/*    */   int anInt416;
/*    */   boolean aBoolean417;
/*    */   int anInt418;
/*    */   int anInt419;
/*    */   int anInt420;
/*    */   int anInt421;
/*    */   int anInt422;
/*    */   int anInt423;
/*    */   int anInt424;
/*    */   int[] anIntArray425;
/*    */   int anInt426;
/*    */   int[] anIntArray427;
/*    */   int[] anIntArray428;
/*    */   public static int[] anIntArray429;
/*    */   int anInt430;
/*    */   boolean[] aBooleanArray431;
/*    */   boolean[] aBooleanArray432;
/*    */   byte[] aByteArray433;
/*    */   byte[] aByteArray434;
/*    */   int[] anIntArray435;
/*    */   byte[] aByteArray436;
/*    */   byte[] aByteArray437;
/*    */   byte[][] aByteArrayArray438;
/*    */   int[][] anIntArrayArray439;
/*    */   int[][] anIntArrayArray440;
/*    */   int[][] anIntArrayArray441;
/*    */   int[] anIntArray442;
/*    */   int anInt443;
/*    */ 
/*    */   Bzip2Context()
/*    */   {
/* 11 */     this.anIntArray425 = new int[256];
/* 12 */     this.anIntArray427 = new int[257];
/* 13 */     this.anIntArray428 = new int[257];
/* 14 */     this.aBooleanArray431 = new boolean[256];
/* 15 */     this.aBooleanArray432 = new boolean[16];
/* 16 */     this.aByteArray433 = new byte[256];
/* 17 */     this.aByteArray434 = new byte[4096];
/* 18 */     this.anIntArray435 = new int[16];
/* 19 */     this.aByteArray436 = new byte[18002];
/* 20 */     this.aByteArray437 = new byte[18002];
/* 21 */     this.aByteArrayArray438 = new byte[6][258];
/* 22 */     this.anIntArrayArray439 = new int[6][258];
/* 23 */     this.anIntArrayArray440 = new int[6][258];
/* 24 */     this.anIntArrayArray441 = new int[6][258];
/* 25 */     this.anIntArray442 = new int[6];
/*    */   }
/*    */ }

/* Location:           /Users/zack/Desktop/SkyCubeDemo/CubeDemo.jar
 * Qualified Name:     jagex.Bzip2Context
 * JD-Core Version:    0.6.2
 */