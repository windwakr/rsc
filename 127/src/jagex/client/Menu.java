package jagex.client;

public class Menu {
	protected Surface surface;
	int element_cnt;
	int anInt247;
	public boolean[] element_enabled;
	public boolean[] aBooleanArray249;
	public boolean[] aBooleanArray250;
	public boolean[] element_hit;
	public int[] anIntArray252;
	public int[] anIntArray253;
	public int[] element_toggle;
	public int[] anIntArray255;
	boolean[] element_is_background;
	int[] element_x;
	int[] element_y;
	int[] element_type;
	int[] element_w;
	int[] element_h;
	int[] element_cap;
	int[] element_font;
	String[] element_text;
	String[][] element_entries;
	int anInt266;
	int anInt267;
	int anInt268;
	int anInt269;
	int focus_ptr;
	int anInt271;
	int anInt272;
	int anInt273;
	int anInt274;
	int anInt275;
	int anInt276;
	int anInt277;
	int anInt278;
	int anInt279;
	int anInt280;
	int anInt281;
	int anInt282;
	int anInt283;
	public boolean aBoolean284;
	public static boolean aBoolean285 = true;
	public static int anInt286;
	public static int r_mod = 114;
	public static int g_mod = 114;
	public static int b_mod = 176;
	public static int anInt290;

	public Menu(Surface surface, int i) {
		this.focus_ptr = -1;
		this.aBoolean284 = true;
		this.surface = surface;
		this.anInt247 = i;
		this.element_enabled = new boolean[i];
		this.aBooleanArray249 = new boolean[i];
		this.aBooleanArray250 = new boolean[i];
		this.element_hit = new boolean[i];
		this.element_is_background = new boolean[i];
		this.anIntArray252 = new int[i];
		this.anIntArray253 = new int[i];
		this.element_toggle = new int[i];
		this.anIntArray255 = new int[i];
		this.element_x = new int[i];
		this.element_y = new int[i];
		this.element_type = new int[i];
		this.element_w = new int[i];
		this.element_h = new int[i];
		this.element_cap = new int[i];
		this.element_font = new int[i];
		this.element_text = new String[i];
		this.element_entries = new String[i][];
		this.anInt272 = mod_rgb(114, 114, 176);
		this.anInt273 = mod_rgb(14, 14, 62);
		this.anInt274 = mod_rgb(200, 208, 232);
		this.anInt275 = mod_rgb(96, 129, 184);
		this.anInt276 = mod_rgb(53, 95, 115);
		this.anInt277 = mod_rgb(117, 142, 171);
		this.anInt278 = mod_rgb(98, 122, 158);
		this.anInt279 = mod_rgb(86, 100, 136);
		this.anInt280 = mod_rgb(135, 146, 179);
		this.anInt281 = mod_rgb(97, 112, 151);
		this.anInt282 = mod_rgb(88, 102, 136);
		this.anInt283 = mod_rgb(84, 93, 120);
	}

	public int mod_rgb(int i, int j, int k) {
		return Surface.rgb(r_mod * i / 114, g_mod * j / 114, b_mod * k / 176);
	}

	public void method293() {
		this.anInt268 = 0;
	}

	public void consume(int i, int j, int k, int l) {
		this.anInt266 = i;
		this.anInt267 = j;
		this.anInt269 = l;
		if (k != 0)
			this.anInt268 = k;
		if (k == 1) {
			for (int i1 = 0; i1 < this.element_cnt; i1++) {
				if ((this.element_enabled[i1])
						&& (this.element_type[i1] == 10)
						&& (this.anInt266 >= this.element_x[i1])
						&& (this.anInt267 >= this.element_y[i1])
						&& (this.anInt266 <= this.element_x[i1]
								+ this.element_w[i1])
						&& (this.anInt267 <= this.element_y[i1]
								+ this.element_h[i1]))
					this.element_hit[i1] = true;
				if ((this.element_enabled[i1])
						&& (this.element_type[i1] == 14)
						&& (this.anInt266 >= this.element_x[i1])
						&& (this.anInt267 >= this.element_y[i1])
						&& (this.anInt266 <= this.element_x[i1]
								+ this.element_w[i1])
						&& (this.anInt267 <= this.element_y[i1]
								+ this.element_h[i1])) {
					this.element_toggle[i1] = (1 - this.element_toggle[i1]);
				}
			}
		}
		if (l == 1)
			this.anInt271 += 1;
		else
			this.anInt271 = 0;
		if ((k == 1) || (this.anInt271 > 20)) {
			for (int j1 = 0; j1 < this.element_cnt; j1++) {
				if ((this.element_enabled[j1])
						&& (this.element_type[j1] == 15)
						&& (this.anInt266 >= this.element_x[j1])
						&& (this.anInt267 >= this.element_y[j1])
						&& (this.anInt266 <= this.element_x[j1]
								+ this.element_w[j1])
						&& (this.anInt267 <= this.element_y[j1]
								+ this.element_h[j1]))
					this.element_hit[j1] = true;
			}
			this.anInt271 -= 5;
		}
	}

	public boolean selected(int i) {
		if ((this.element_enabled[i]) && (this.element_hit[i])) {
			this.element_hit[i] = false;
			return true;
		}
		return false;
	}

	public void on_key(int i) {
		if (i == 0)
			return;
		if ((this.focus_ptr != -1)
				&& (this.element_text[this.focus_ptr] != null)
				&& (this.element_enabled[this.focus_ptr])) {
			int j = this.element_text[this.focus_ptr].length();
			if ((i == 8) && (j > 0))
				this.element_text[this.focus_ptr] = this.element_text[this.focus_ptr]
						.substring(0, j - 1);
			if (((i == 10) || (i == 13)) && (j > 0))
				this.element_hit[this.focus_ptr] = true;
			String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"£$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
			if (j < this.element_cap[this.focus_ptr]) {
				for (int k = 0; k < s.length(); k++)
					if (i == s.charAt(k)) {
						int tmp164_161 = this.focus_ptr;
						String[] tmp164_157 = this.element_text;
						tmp164_157[tmp164_161] = (tmp164_157[tmp164_161] + (char) i);
					}
			}
			if (i == 9) {
				do
					this.focus_ptr = ((this.focus_ptr + 1) % this.element_cnt);
				while ((this.element_type[this.focus_ptr] != 5)
						&& (this.element_type[this.focus_ptr] != 6));
				return;
			}
		}
	}

	public void display() {
		for (int i = 0; i < this.element_cnt; i++) {
			if (this.element_enabled[i])
				if (this.element_type[i] == 0)
					method299(i, this.element_x[i], this.element_y[i],
							this.element_text[i], this.element_font[i]);
				else if (this.element_type[i] == 1)
					method299(
							i,
							this.element_x[i]
									- Surface.text_width(
											this.element_text[i],
											this.element_font[i]) / 2,
							this.element_y[i], this.element_text[i],
							this.element_font[i]);
				else if (this.element_type[i] == 2)
					method302(this.element_x[i], this.element_y[i],
							this.element_w[i], this.element_h[i]);
				else if (this.element_type[i] == 3)
					method305(this.element_x[i], this.element_y[i],
							this.element_w[i]);
				else if (this.element_type[i] == 4)
					method306(i, this.element_x[i], this.element_y[i],
							this.element_w[i], this.element_h[i],
							this.element_font[i], this.element_entries[i],
							this.anIntArray253[i], this.anIntArray252[i]);
				else if ((this.element_type[i] == 5)
						|| (this.element_type[i] == 6))
					method301(i, this.element_x[i], this.element_y[i],
							this.element_w[i], this.element_h[i],
							this.element_text[i], this.element_font[i]);
				else if (this.element_type[i] == 7)
					method308(i, this.element_x[i], this.element_y[i],
							this.element_font[i], this.element_entries[i]);
				else if (this.element_type[i] == 8)
					method309(i, this.element_x[i], this.element_y[i],
							this.element_font[i], this.element_entries[i]);
				else if (this.element_type[i] == 9)
					method310(i, this.element_x[i], this.element_y[i],
							this.element_w[i], this.element_h[i],
							this.element_font[i], this.element_entries[i],
							this.anIntArray253[i], this.anIntArray252[i]);
				else if (this.element_type[i] == 11)
					method303(this.element_x[i], this.element_y[i],
							this.element_w[i], this.element_h[i]);
				else if (this.element_type[i] == 12)
					method304(this.element_x[i], this.element_y[i],
							this.element_font[i]);
				else if (this.element_type[i] == 14)
					method298(i, this.element_x[i], this.element_y[i],
							this.element_w[i], this.element_h[i]);
		}
		this.anInt268 = 0;
	}

	protected void method298(int i, int j, int k, int l, int i1) {
		this.surface.rect_fill(j, k, l, i1, 16777215);
		this.surface.line_horiz(j, k, l, this.anInt280);
		this.surface.line_vert(j, k, i1, this.anInt280);
		this.surface.line_horiz(j, k + i1 - 1, l, this.anInt283);
		this.surface.line_vert(j + l - 1, k, i1, this.anInt283);
		if (this.element_toggle[i] == 1)
			for (int j1 = 0; j1 < i1; j1++) {
				this.surface.line_horiz(j + j1, k + j1, 1, 0);
				this.surface.line_horiz(j + l - 1 - j1, k + j1, 1, 0);
			}
	}

	protected void method299(int i, int j, int k, String s, int l) {
		int i1 = k + Surface.height(l) / 3;
		method300(i, j, i1, s, l);
	}

	protected void method300(int i, int j, int k, String s, int l) {
		int i1;
		if (this.element_is_background[i])
			i1 = 16777215;
		else
			i1 = 0;
		this.surface.text_draw(s, j, k, l, i1);
	}

	protected void method301(int i, int j, int k, int l, int i1, String s,
			int j1) {
		if (this.aBooleanArray250[i]) {
			int k1 = s.length();
			s = "";
			for (int i2 = 0; i2 < k1; i2++) {
				s = s + "X";
			}
		}
		if (this.element_type[i] == 5) {
			if ((this.anInt268 == 1) && (this.anInt266 >= j)
					&& (this.anInt267 >= k - i1 / 2)
					&& (this.anInt266 <= j + l)
					&& (this.anInt267 <= k + i1 / 2))
				this.focus_ptr = i;
		} else if (this.element_type[i] == 6) {
			if ((this.anInt268 == 1) && (this.anInt266 >= j - l / 2)
					&& (this.anInt267 >= k - i1 / 2)
					&& (this.anInt266 <= j + l / 2)
					&& (this.anInt267 <= k + i1 / 2))
				this.focus_ptr = i;
			j -= Surface.text_width(s, j1) / 2;
		}
		if (this.focus_ptr == i)
			s = s + "*";
		int l1 = k + Surface.height(j1) / 3;
		method300(i, j, l1, s, j1);
	}

	public void method302(int i, int j, int k, int l) {
		this.surface.set_rect(i, j, i + k, j + l);
		this.surface.grad_rect_fill(i, j, k, l, this.anInt283, this.anInt280);
		if (aBoolean285) {
			for (int i1 = i - (j & 0x3F); i1 < i + k; i1 += 128) {
				for (int j1 = j - (j & 0x1F); j1 < j + l; j1 += 128)
					this.surface.trans_sprite_plot(i1, j1, 6 + anInt286, 128);
			}
		}
		this.surface.line_horiz(i, j, k, this.anInt280);
		this.surface.line_horiz(i + 1, j + 1, k - 2, this.anInt280);
		this.surface.line_horiz(i + 2, j + 2, k - 4, this.anInt281);
		this.surface.line_vert(i, j, l, this.anInt280);
		this.surface.line_vert(i + 1, j + 1, l - 2, this.anInt280);
		this.surface.line_vert(i + 2, j + 2, l - 4, this.anInt281);
		this.surface.line_horiz(i, j + l - 1, k, this.anInt283);
		this.surface.line_horiz(i + 1, j + l - 2, k - 2, this.anInt283);
		this.surface.line_horiz(i + 2, j + l - 3, k - 4, this.anInt282);
		this.surface.line_vert(i + k - 1, j, l, this.anInt283);
		this.surface.line_vert(i + k - 2, j + 1, l - 2, this.anInt283);
		this.surface.line_vert(i + k - 3, j + 2, l - 4, this.anInt282);
		this.surface.reset();
	}

	public void method303(int i, int j, int k, int l) {
		this.surface.rect_fill(i, j, k, l, 0);
		this.surface.rect_draw(i, j, k, l, this.anInt277);
		this.surface.rect_draw(i + 1, j + 1, k - 2, l - 2, this.anInt278);
		this.surface.rect_draw(i + 2, j + 2, k - 4, l - 4, this.anInt279);
		this.surface.sprite_plot(i, j, 2 + anInt286);
		this.surface.sprite_plot(i + k - 7, j, 3 + anInt286);
		this.surface.sprite_plot(i, j + l - 7, 4 + anInt286);
		this.surface.sprite_plot(i + k - 7, j + l - 7, 5 + anInt286);
	}

	protected void method304(int i, int j, int k) {
		this.surface.sprite_plot(i, j, k);
	}

	protected void method305(int i, int j, int k) {
		this.surface.line_horiz(i, j, k, 0);
	}

	protected void method306(int i, int j, int k, int l, int i1, int j1,
			String[] as, int k1, int l1) {
		int i2 = i1 / Surface.height(j1);
		if (l1 > k1 - i2)
			l1 = k1 - i2;
		if (l1 < 0)
			l1 = 0;
		this.anIntArray252[i] = l1;
		if (i2 < k1) {
			int j2 = j + l - 12;
			int l2 = (i1 - 27) * i2 / k1;
			if (l2 < 6)
				l2 = 6;
			int j3 = (i1 - 27 - l2) * l1 / (k1 - i2);
			if ((this.anInt269 == 1) && (this.anInt266 >= j2)
					&& (this.anInt266 <= j2 + 12)) {
				if ((this.anInt267 > k) && (this.anInt267 < k + 12) && (l1 > 0))
					l1--;
				if ((this.anInt267 > k + i1 - 12) && (this.anInt267 < k + i1)
						&& (l1 < k1 - i2))
					l1++;
				this.anIntArray252[i] = l1;
			}
			if ((this.anInt269 == 1)
					&& (((this.anInt266 >= j2) && (this.anInt266 <= j2 + 12)) || ((this.anInt266 >= j2 - 12)
							&& (this.anInt266 <= j2 + 24) && (this.aBooleanArray249[i])))) {
				if ((this.anInt267 > k + 12) && (this.anInt267 < k + i1 - 12)) {
					this.aBooleanArray249[i] = true;
					int l3 = this.anInt267 - k - 12 - l2 / 2;
					l1 = l3 * k1 / (i1 - 24);
					if (l1 > k1 - i2)
						l1 = k1 - i2;
					if (l1 < 0)
						l1 = 0;
					this.anIntArray252[i] = l1;
				}
			} else
				this.aBooleanArray249[i] = false;

			j3 = (i1 - 27 - l2) * l1 / (k1 - i2);
			method307(j, k, l, i1, j3, l2);
		}
		int k2 = i1 - i2 * Surface.height(j1);
		int i3 = k + Surface.height(j1) * 5 / 6 + k2 / 2;
		for (int k3 = l1; k3 < k1; k3++) {
			method300(i, j + 2, i3, as[k3], j1);
			i3 += Surface.height(j1) - anInt290;
			if (i3 >= k + i1)
				return;
		}
	}

	protected void method307(int i, int j, int k, int l, int i1, int j1) {
		int k1 = i + k - 12;
		this.surface.rect_draw(k1, j, 12, l, 0);
		this.surface.sprite_plot(k1 + 1, j + 1, anInt286);
		this.surface.sprite_plot(k1 + 1, j + l - 12, 1 + anInt286);
		this.surface.line_horiz(k1, j + 13, 12, 0);
		this.surface.line_horiz(k1, j + l - 13, 12, 0);
		this.surface.grad_rect_fill(k1 + 1, j + 14, 11, l - 27, this.anInt272,
				this.anInt273);
		this.surface.rect_fill(k1 + 3, i1 + j + 14, 7, j1, this.anInt275);
		this.surface.line_vert(k1 + 2, i1 + j + 14, j1, this.anInt274);
		this.surface.line_vert(k1 + 2 + 8, i1 + j + 14, j1, this.anInt276);
	}

	protected void method308(int i, int j, int k, int l, String[] as) {
		int i1 = 0;
		int j1 = as.length;
		for (int k1 = 0; k1 < j1; k1++) {
			i1 += Surface.text_width(as[k1], l);
			if (k1 < j1 - 1) {
				i1 += Surface.text_width("  ", l);
			}
		}
		int l1 = j - i1 / 2;
		int i2 = k + Surface.height(l) / 3;
		for (int j2 = 0; j2 < j1; j2++) {
			int k2;
			if (this.element_is_background[i])
				k2 = 16777215;
			else
				k2 = 0;
			if ((this.anInt266 >= l1)
					&& (this.anInt266 <= l1
							+ Surface.text_width(as[j2], l))
					&& (this.anInt267 <= i2)
					&& (this.anInt267 > i2 - Surface.height(l))) {
				if (this.element_is_background[i])
					k2 = 8421504;
				else
					k2 = 16777215;
				if (this.anInt268 == 1) {
					this.element_toggle[i] = j2;
					this.element_hit[i] = true;
				}
			}
			if (this.element_toggle[i] == j2)
				if (this.element_is_background[i])
					k2 = 16711680;
				else
					k2 = 12582912;
			this.surface.text_draw(as[j2], l1, i2, l, k2);
			l1 += Surface.text_width(as[j2] + "  ", l);
		}
	}

	protected void method309(int i, int j, int k, int l, String[] as) {
		int i1 = as.length;
		int j1 = k - Surface.height(l) * (i1 - 1) / 2;
		for (int k1 = 0; k1 < i1; k1++) {
			int l1;
			if (this.element_is_background[i])
				l1 = 16777215;
			else
				l1 = 0;
			int i2 = Surface.text_width(as[k1], l);
			if ((this.anInt266 >= j - i2 / 2) && (this.anInt266 <= j + i2 / 2)
					&& (this.anInt267 - 2 <= j1)
					&& (this.anInt267 - 2 > j1 - Surface.height(l))) {
				if (this.element_is_background[i])
					l1 = 8421504;
				else
					l1 = 16777215;
				if (this.anInt268 == 1) {
					this.element_toggle[i] = k1;
					this.element_hit[i] = true;
				}
			}
			if (this.element_toggle[i] == k1)
				if (this.element_is_background[i])
					l1 = 16711680;
				else
					l1 = 12582912;
			this.surface.text_draw(as[k1], j - i2 / 2, j1, l, l1);
			j1 += Surface.height(l);
		}
	}

	protected void method310(int i, int j, int k, int l, int i1, int j1,
			String[] as, int k1, int l1) {
		int i2 = i1 / Surface.height(j1);
		if (i2 < k1) {
			int j2 = j + l - 12;
			int l2 = (i1 - 27) * i2 / k1;
			if (l2 < 6)
				l2 = 6;
			int j3 = (i1 - 27 - l2) * l1 / (k1 - i2);
			if ((this.anInt269 == 1) && (this.anInt266 >= j2)
					&& (this.anInt266 <= j2 + 12)) {
				if ((this.anInt267 > k) && (this.anInt267 < k + 12) && (l1 > 0))
					l1--;
				if ((this.anInt267 > k + i1 - 12) && (this.anInt267 < k + i1)
						&& (l1 < k1 - i2))
					l1++;
				this.anIntArray252[i] = l1;
			}
			if ((this.anInt269 == 1)
					&& (((this.anInt266 >= j2) && (this.anInt266 <= j2 + 12)) || ((this.anInt266 >= j2 - 12)
							&& (this.anInt266 <= j2 + 24) && (this.aBooleanArray249[i])))) {
				if ((this.anInt267 > k + 12) && (this.anInt267 < k + i1 - 12)) {
					this.aBooleanArray249[i] = true;
					int l3 = this.anInt267 - k - 12 - l2 / 2;
					l1 = l3 * k1 / (i1 - 24);
					if (l1 < 0)
						l1 = 0;
					if (l1 > k1 - i2)
						l1 = k1 - i2;
					this.anIntArray252[i] = l1;
				}
			} else
				this.aBooleanArray249[i] = false;

			j3 = (i1 - 27 - l2) * l1 / (k1 - i2);
			method307(j, k, l, i1, j3, l2);
		} else {
			l1 = 0;
			this.anIntArray252[i] = 0;
		}
		this.anIntArray255[i] = -1;
		int k2 = i1 - i2 * Surface.height(j1);
		int i3 = k + Surface.height(j1) * 5 / 6 + k2 / 2;
		for (int k3 = l1; k3 < k1; k3++) {
			int i4;
			if (this.element_is_background[i])
				i4 = 16777215;
			else
				i4 = 0;
			if ((this.anInt266 >= j + 2)
					&& (this.anInt266 <= j + 2
							+ Surface.text_width(as[k3], j1))
					&& (this.anInt267 - 2 <= i3)
					&& (this.anInt267 - 2 > i3 - Surface.height(j1))) {
				if (this.element_is_background[i])
					i4 = 8421504;
				else
					i4 = 16777215;
				this.anIntArray255[i] = k3;
				if (this.anInt268 == 1) {
					this.element_toggle[i] = k3;
					this.element_hit[i] = true;
				}
			}
			if ((this.element_toggle[i] == k3) && (this.aBoolean284))
				i4 = 16711680;
			this.surface.text_draw(as[k3], j + 2, i3, j1, i4);
			i3 += Surface.height(j1);
			if (i3 >= k + i1)
				return;
		}
	}

	public int label_create(int i, int j, String s, int k, boolean flag) {
		this.element_type[this.element_cnt] = 0;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_font[this.element_cnt] = k;
		this.element_is_background[this.element_cnt] = flag;
		this.element_x[this.element_cnt] = i;
		this.element_y[this.element_cnt] = j;
		this.element_text[this.element_cnt] = s;
		return this.element_cnt++;
	}

	public int centered_label_create(int i, int j, String s, int k, boolean flag) {
		this.element_type[this.element_cnt] = 1;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_font[this.element_cnt] = k;
		this.element_is_background[this.element_cnt] = flag;
		this.element_x[this.element_cnt] = i;
		this.element_y[this.element_cnt] = j;
		this.element_text[this.element_cnt] = s;
		return this.element_cnt++;
	}

	public int rect_create(int i, int j, int k, int l) {
		this.element_type[this.element_cnt] = 2;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_x[this.element_cnt] = (i - k / 2);
		this.element_y[this.element_cnt] = (j - l / 2);
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = l;
		return this.element_cnt++;
	}

	public int method314(int i, int j, int k, int l) {
		this.element_type[this.element_cnt] = 11;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_x[this.element_cnt] = (i - k / 2);
		this.element_y[this.element_cnt] = (j - l / 2);
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = l;
		return this.element_cnt++;
	}

	public int sprite_create(int i, int j, int k) {
		int l = this.surface.sprite_width[k];
		int i1 = this.surface.sprite_height[k];
		this.element_type[this.element_cnt] = 12;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_x[this.element_cnt] = (i - l / 2);
		this.element_y[this.element_cnt] = (j - i1 / 2);
		this.element_w[this.element_cnt] = l;
		this.element_h[this.element_cnt] = i1;
		this.element_font[this.element_cnt] = k;
		return this.element_cnt++;
	}

	public int method316(int i, int j, int k, int l, int i1, int j1,
			boolean flag) {
		this.element_type[this.element_cnt] = 4;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_x[this.element_cnt] = i;
		this.element_y[this.element_cnt] = j;
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = l;
		this.element_is_background[this.element_cnt] = flag;
		this.element_font[this.element_cnt] = i1;
		this.element_cap[this.element_cnt] = j1;
		this.anIntArray253[this.element_cnt] = 0;
		this.anIntArray252[this.element_cnt] = 0;
		this.element_entries[this.element_cnt] = new String[j1];
		return this.element_cnt++;
	}

	public int method317(int i, int j, int k, int l, int i1, int j1,
			boolean flag, boolean flag1) {
		this.element_type[this.element_cnt] = 5;
		this.element_enabled[this.element_cnt] = true;
		this.aBooleanArray250[this.element_cnt] = flag;
		this.element_hit[this.element_cnt] = false;
		this.element_font[this.element_cnt] = i1;
		this.element_is_background[this.element_cnt] = flag1;
		this.element_x[this.element_cnt] = i;
		this.element_y[this.element_cnt] = j;
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = l;
		this.element_cap[this.element_cnt] = j1;
		this.element_text[this.element_cnt] = "";
		return this.element_cnt++;
	}

	public int text_field_create(int i, int j, int k, int l, int i1, int j1,
			boolean flag, boolean flag1) {
		this.element_type[this.element_cnt] = 6;
		this.element_enabled[this.element_cnt] = true;
		this.aBooleanArray250[this.element_cnt] = flag;
		this.element_hit[this.element_cnt] = false;
		this.element_font[this.element_cnt] = i1;
		this.element_is_background[this.element_cnt] = flag1;
		this.element_x[this.element_cnt] = i;
		this.element_y[this.element_cnt] = j;
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = l;
		this.element_cap[this.element_cnt] = j1;
		this.element_text[this.element_cnt] = "";
		return this.element_cnt++;
	}

	public int list_create(int i, int j, String[] as, int k, boolean flag) {
		this.element_type[this.element_cnt] = 8;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_font[this.element_cnt] = k;
		this.element_is_background[this.element_cnt] = flag;
		this.element_x[this.element_cnt] = i;
		this.element_y[this.element_cnt] = j;
		this.element_entries[this.element_cnt] = as;
		this.element_toggle[this.element_cnt] = 0;
		return this.element_cnt++;
	}

	public int method320(int i, int j, int k, int l, int i1, int j1,
			boolean flag) {
		this.element_type[this.element_cnt] = 9;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_font[this.element_cnt] = i1;
		this.element_is_background[this.element_cnt] = flag;
		this.element_x[this.element_cnt] = i;
		this.element_y[this.element_cnt] = j;
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = l;
		this.element_cap[this.element_cnt] = j1;
		this.element_entries[this.element_cnt] = new String[j1];
		this.anIntArray253[this.element_cnt] = 0;
		this.anIntArray252[this.element_cnt] = 0;
		this.element_toggle[this.element_cnt] = -1;
		this.anIntArray255[this.element_cnt] = -1;
		return this.element_cnt++;
	}

	public int button_create(int i, int j, int k, int l) {
		this.element_type[this.element_cnt] = 10;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_x[this.element_cnt] = (i - k / 2);
		this.element_y[this.element_cnt] = (j - l / 2);
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = l;
		return this.element_cnt++;
	}

	public int check_box_create(int i, int j, int k) {
		this.element_type[this.element_cnt] = 14;
		this.element_enabled[this.element_cnt] = true;
		this.element_hit[this.element_cnt] = false;
		this.element_x[this.element_cnt] = (i - k / 2);
		this.element_y[this.element_cnt] = (j - k / 2);
		this.element_w[this.element_cnt] = k;
		this.element_h[this.element_cnt] = k;
		return this.element_cnt++;
	}

	public void method323(int i) {
		this.anIntArray253[i] = 0;
	}

	public void method324(int i) {
		this.anIntArray252[i] = 0;
		this.anIntArray255[i] = -1;
	}

	public void method325(int i, int j, String s) {
		this.element_entries[i][j] = s;
		if (j + 1 > this.anIntArray253[i])
			this.anIntArray253[i] = (j + 1);
	}

	public void method326(int i, String s, boolean flag) {
		int tmp5_4 = i;
		int[] tmp5_1 = this.anIntArray253;
		int tmp7_6 = tmp5_1[tmp5_4];
		tmp5_1[tmp5_4] = (tmp7_6 + 1);
		int j = tmp7_6;
		if (j >= this.element_cap[i]) {
			j--;
			this.anIntArray253[i] -= 1;
			for (int k = 0; k < j; k++) {
				this.element_entries[i][k] = this.element_entries[i][(k + 1)];
			}
		}
		this.element_entries[i][j] = s;
		if (flag)
			this.anIntArray252[i] = 999999;
	}

	public void set_text(int i, String s) {
		this.element_text[i] = s;
	}

	public String text(int i) {
		if (this.element_text[i] == null) {
			return "null";
		}
		return this.element_text[i];
	}

	public void enable(int i) {
		this.element_enabled[i] = true;
	}

	public void disable(int i) {
		this.element_enabled[i] = false;
	}

	public void select(int i) {
		this.focus_ptr = i;
	}

	public int method332(int i) {
		return this.element_toggle[i];
	}

	public int method333(int i) {
		int j = this.anIntArray255[i];
		return j;
	}

	public void set_toggle(int i, int j) {
		this.element_toggle[i] = j;
	}
}