package jagex.client;

import java.awt.Event;
import java.awt.Frame;
import java.awt.Graphics;

@SuppressWarnings("serial")
public final class Window extends Frame {
	int w;
	int h;
	int anInt45;
	int y_off;
	Game game;
	Graphics graphics;

	public Window(Game game, int w, int h, String title, boolean resize, boolean use_y_off) {
		y_off = 28;
		this.w = w;
		this.h = h;
		this.game = game;
		if (use_y_off)
			y_off = 48;
		else
			y_off = 28;
		setTitle(title);
		setResizable(resize);
		setVisible(true);
		toFront();
		resize(w, h);
		graphics = getGraphics();
	}

	public Graphics getGraphics() {
		Graphics g = super.getGraphics();
		if (anInt45 == 0)
			g.translate(0, 24);
		else
			g.translate(-5, 0);
		return g;
	}

	@SuppressWarnings("deprecation")
	public void resize(int i, int j) {
		super.resize(i, j + y_off);
	}

	@SuppressWarnings("deprecation")
	public boolean handleEvent(Event event) {
		if (event.id == 401)
			game.keyDown(event, event.key);
		else if (event.id == 402)
			game.keyUp(event, event.key);
		else if (event.id == 501)
			game.mouseDown(event, event.x, event.y - 24);
		else if (event.id == 506)
			game.mouseDrag(event, event.x, event.y - 24);
		else if (event.id == 502)
			game.mouseUp(event, event.x, event.y - 24);
		else if (event.id == 503)
			game.mouseMove(event, event.x, event.y - 24);
		else if (event.id == 201)
			game.destroy();
		else if (event.id == 1001)
			game.action(event, event.target);
		else if (event.id == 403)
			game.keyDown(event, event.key);
		else if (event.id == 404)
			game.keyUp(event, event.key);
		return true;
	}

	public final void paint(Graphics g) {
		game.paint(g);
	}
}