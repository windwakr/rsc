package jagex;

import java.io.PrintStream;

public class Bzip2
{
	static final int anInt471 = 1;
	static final int anInt472 = 2;
	static final int anInt473 = 10;
	static final int anInt474 = 14;
	static final int anInt475 = 0;
	static final int anInt476 = 4;
	static final int anInt477 = 4096;
	static final int anInt478 = 16;
	static final int anInt479 = 258;
	static final int anInt480 = 23;
	static final int anInt481 = 0;
	static final int anInt482 = 1;
	static final int anInt483 = 6;
	static final int anInt484 = 50;
	static final int anInt485 = 4;
	static final int anInt486 = 18002;

	public static int decompress(byte[] abyte0, int i, byte[] abyte1, int j,
			int k)
	{
		Bzip2Context class9 = new Bzip2Context();
		class9.aByteArray405 = abyte1;
		class9.anInt406 = k;
		class9.aByteArray410 = abyte0;
		class9.anInt411 = 0;
		class9.anInt407 = j;
		class9.anInt412 = i;
		class9.anInt419 = 0;
		class9.anInt418 = 0;
		class9.anInt408 = 0;
		class9.anInt409 = 0;
		class9.anInt413 = 0;
		class9.anInt414 = 0;
		class9.anInt421 = 0;
		method398(class9);
		i -= class9.anInt412;
		return i;
	}

	private static void method397(Bzip2Context class9)
	{
		byte byte4 = class9.aByte415;
		int i = class9.anInt416;
		int j = class9.anInt426;
		int k = class9.anInt424;
		int[] ai = Bzip2Context.anIntArray429;
		int l = class9.anInt423;
		byte[] abyte0 = class9.aByteArray410;
		int i1 = class9.anInt411;
		int j1 = class9.anInt412;
		int k1 = j1;
		int l1 = class9.anInt443 + 1;
		while (true)
		{
			if (i > 0)
			{
				while (true)
				{
					if (j1 == 0)
						break;
					if (i == 1)
						break;
					abyte0[i1] = byte4;
					i--;
					i1++;
					j1--;
				}
				if (j1 == 0)
				{
					i = 1;
					break;
				}
				abyte0[i1] = byte4;
				i1++;
				j1--;
			}
			boolean flag = true;
			while (flag)
			{
				flag = false;
				if (j == l1)
				{
					i = 0;
					break;
				}
				byte4 = (byte) k;
				l = ai[l];
				byte byte0 = (byte) (l & 0xFF);
				l >>= 8;
				j++;
				if (byte0 != k)
				{
					k = byte0;
					if (j1 == 0)
					{
						i = 1;
						break;
					}
					abyte0[i1] = byte4;
					i1++;
					j1--;
					flag = true;
				}
				else if (j == l1)
				{
					if (j1 == 0)
					{
						i = 1;
						break;
					}
					abyte0[i1] = byte4;
					i1++;
					j1--;
					flag = true;
				}
			}
			i = 2;
			l = ai[l];
			byte byte1 = (byte) (l & 0xFF);
			l >>= 8;
			j++;
			if (j != l1)
				if (byte1 != k)
				{
					k = byte1;
				}
				else
				{
					i = 3;
					l = ai[l];
					byte byte2 = (byte) (l & 0xFF);
					l >>= 8;
					j++;
					if (j != l1)
						if (byte2 != k)
						{
							k = byte2;
						}
						else
						{
							l = ai[l];
							byte byte3 = (byte) (l & 0xFF);
							l >>= 8;
							j++;
							i = (byte3 & 0xFF) + 4;
							l = ai[l];
							k = (byte) (l & 0xFF);
							l >>= 8;
							j++;
						}
				}
		}
		int i2 = class9.anInt413;
		class9.anInt413 += k1 - j1;
		if (class9.anInt413 < i2)
			class9.anInt414 += 1;
		class9.aByte415 = byte4;
		class9.anInt416 = i;
		class9.anInt426 = j;
		class9.anInt424 = k;
		Bzip2Context.anIntArray429 = ai;
		class9.anInt423 = l;
		class9.aByteArray410 = abyte0;
		class9.anInt411 = i1;
		class9.anInt412 = j1;
	}

	private static void method398(Bzip2Context class9)
	{
		boolean flag = false;
		boolean flag1 = false;
		boolean flag2 = false;
		boolean flag3 = false;
		boolean flag4 = false;
		boolean flag5 = false;
		boolean flag6 = false;
		boolean flag7 = false;
		boolean flag8 = false;
		boolean flag9 = false;
		boolean flag10 = false;
		boolean flag11 = false;
		boolean flag12 = false;
		boolean flag13 = false;
		boolean flag14 = false;
		boolean flag15 = false;
		boolean flag16 = false;
		boolean flag17 = false;
		boolean flag18 = false;
		int k8 = 0;
		int[] ai = null;
		int[] ai1 = null;
		int[] ai2 = null;
		class9.anInt420 = 1;
		if (Bzip2Context.anIntArray429 == null)
			Bzip2Context.anIntArray429 = new int[class9.anInt420 * 100000];
		boolean flag19 = true;
		while (flag19)
		{
			byte byte0 = method399(class9);
			if (byte0 == 23)
				return;
			byte0 = method399(class9);
			byte0 = method399(class9);
			byte0 = method399(class9);
			byte0 = method399(class9);
			byte0 = method399(class9);
			class9.anInt421 += 1;
			byte0 = method399(class9);
			byte0 = method399(class9);
			byte0 = method399(class9);
			byte0 = method399(class9);
			byte0 = method400(class9);
			if (byte0 != 0)
				class9.aBoolean417 = true;
			else
				class9.aBoolean417 = false;
			if (class9.aBoolean417)
				System.out.println("PANIC! RANDOMISED BLOCK!");
			class9.anInt422 = 0;
			byte0 = method399(class9);
			class9.anInt422 = (class9.anInt422 << 8 | byte0 & 0xFF);
			byte0 = method399(class9);
			class9.anInt422 = (class9.anInt422 << 8 | byte0 & 0xFF);
			byte0 = method399(class9);
			class9.anInt422 = (class9.anInt422 << 8 | byte0 & 0xFF);
			for (int j = 0; j < 16; j++)
			{
				byte byte1 = method400(class9);
				if (byte1 == 1)
					class9.aBooleanArray432[j] = true;
				else
				{
					class9.aBooleanArray432[j] = false;
				}
			}
			for (int k = 0; k < 256; k++)
			{
				class9.aBooleanArray431[k] = false;
			}
			for (int l = 0; l < 16; l++)
			{
				if (class9.aBooleanArray432[l])
				{
					for (int i3 = 0; i3 < 16; i3++)
					{
						byte byte2 = method400(class9);
						if (byte2 == 1)
						{
							class9.aBooleanArray431[(l * 16 + i3)] = true;
						}
					}
				}
			}
			method403(class9);
			int i4 = class9.anInt430 + 2;
			int j4 = method401(3, class9);
			int k4 = method401(15, class9);
			for (int i1 = 0; i1 < k4; i1++)
			{
				int j3 = 0;
				while (true)
				{
					byte byte3 = method400(class9);
					if (byte3 == 0)
						break;
					j3++;
				}
				class9.aByteArray437[i1] = ((byte) j3);
			}

			byte[] abyte0 = new byte[6];
			for (byte byte16 = 0; byte16 < j4; byte16 = (byte) (byte16 + 1))
			{
				abyte0[byte16] = byte16;
			}
			for (int j1 = 0; j1 < k4; j1++)
			{
				byte byte17 = class9.aByteArray437[j1];
				byte byte15 = abyte0[byte17];
				for (; byte17 > 0; byte17 = (byte) (byte17 - 1))
				{
					abyte0[byte17] = abyte0[(byte17 - 1)];
				}
				abyte0[0] = byte15;
				class9.aByteArray436[j1] = byte15;
			}

			for (int k3 = 0; k3 < j4; k3++)
			{
				int l6 = method401(5, class9);
				for (int k1 = 0; k1 < i4; k1++)
				{
					while (true)
					{
						byte byte4 = method400(class9);
						if (byte4 == 0)
							break;
						byte4 = method400(class9);
						if (byte4 == 0)
							l6++;
						else
							l6--;
					}
					class9.aByteArrayArray438[k3][k1] = ((byte) l6);
				}

			}

			for (int l3 = 0; l3 < j4; l3++)
			{
				byte byte8 = 32;
				int i = 0;
				for (int l1 = 0; l1 < i4; l1++)
				{
					if (class9.aByteArrayArray438[l3][l1] > i)
						i = class9.aByteArrayArray438[l3][l1];
					if (class9.aByteArrayArray438[l3][l1] < byte8)
					{
						byte8 = class9.aByteArrayArray438[l3][l1];
					}
				}
				method404(class9.anIntArrayArray439[l3],
						class9.anIntArrayArray440[l3],
						class9.anIntArrayArray441[l3],
						class9.aByteArrayArray438[l3], byte8, i, i4);
				class9.anIntArray442[l3] = byte8;
			}

			int l4 = class9.anInt430 + 1;
			int l5 = 100000 * class9.anInt420;
			int i5 = -1;
			int j5 = 0;
			for (int i2 = 0; i2 <= 255; i2++)
			{
				class9.anIntArray425[i2] = 0;
			}
			int j9 = 4095;
			for (int l8 = 15; l8 >= 0; l8--)
			{
				for (int i9 = 15; i9 >= 0; i9--)
				{
					class9.aByteArray434[j9] = ((byte) (l8 * 16 + i9));
					j9--;
				}

				class9.anIntArray435[l8] = (j9 + 1);
			}

			int i6 = 0;
			if (j5 == 0)
			{
				i5++;
				j5 = 50;
				byte byte12 = class9.aByteArray436[i5];
				k8 = class9.anIntArray442[byte12];
				ai = class9.anIntArrayArray439[byte12];
				ai2 = class9.anIntArrayArray441[byte12];
				ai1 = class9.anIntArrayArray440[byte12];
			}
			j5--;
			int i7 = k8;
			byte byte9;
			int l7;
			for (l7 = method401(i7, class9); l7 > ai[i7]; l7 = l7 << 1
					| byte9)
			{
				i7++;
				byte9 = method400(class9);
			}

			for (int k5 = ai2[(l7 - ai1[i7])]; k5 != l4;)
			{
				if ((k5 == 0) || (k5 == 1))
				{
					int j6 = -1;
					int k6 = 1;
					do
					{
						if (k5 == 0)
							j6 += k6;
						else if (k5 == 1)
							j6 += 2 * k6;
						k6 *= 2;
						if (j5 == 0)
						{
							i5++;
							j5 = 50;
							byte byte13 = class9.aByteArray436[i5];
							k8 = class9.anIntArray442[byte13];
							ai = class9.anIntArrayArray439[byte13];
							ai2 = class9.anIntArrayArray441[byte13];
							ai1 = class9.anIntArrayArray440[byte13];
						}
						j5--;
						int j7 = k8;
						byte byte10;
						int i8;
						for (i8 = method401(j7, class9); i8 > ai[j7]; i8 = i8 << 1
								| byte10)
						{
							j7++;
							byte10 = method400(class9);
						}

						k5 = ai2[(i8 - ai1[j7])];
					}
					while ((k5 == 0) || (k5 == 1));
					j6++;
					byte byte5 = class9.aByteArray433[(class9.aByteArray434[class9.anIntArray435[0]] & 0xFF)];
					class9.anIntArray425[(byte5 & 0xFF)] += j6;
					for (; j6 > 0; j6--)
					{
						Bzip2Context.anIntArray429[i6] = (byte5 & 0xFF);
						i6++;
					}
				}
				else
				{
					int j11 = k5 - 1;
					byte byte6;
					if (j11 < 16)
					{
						int j10 = class9.anIntArray435[0];
						byte6 = class9.aByteArray434[(j10 + j11)];
						for (; j11 > 3; j11 -= 4)
						{
							int k11 = j10 + j11;
							class9.aByteArray434[k11] = class9.aByteArray434[(k11 - 1)];
							class9.aByteArray434[(k11 - 1)] = class9.aByteArray434[(k11 - 2)];
							class9.aByteArray434[(k11 - 2)] = class9.aByteArray434[(k11 - 3)];
							class9.aByteArray434[(k11 - 3)] = class9.aByteArray434[(k11 - 4)];
						}

						for (; j11 > 0; j11--)
						{
							class9.aByteArray434[(j10 + j11)] = class9.aByteArray434[(j10
									+ j11 - 1)];
						}
						class9.aByteArray434[j10] = byte6;
					}
					else
					{
						int l10 = j11 / 16;
						int i11 = j11 % 16;
						int k10 = class9.anIntArray435[l10] + i11;
						byte6 = class9.aByteArray434[k10];
						for (; k10 > class9.anIntArray435[l10]; k10--)
						{
							class9.aByteArray434[k10] = class9.aByteArray434[(k10 - 1)];
						}
						class9.anIntArray435[l10] += 1;
						for (; l10 > 0; l10--)
						{
							class9.anIntArray435[l10] -= 1;
							class9.aByteArray434[class9.anIntArray435[l10]] = class9.aByteArray434[(class9.anIntArray435[(l10 - 1)] + 16 - 1)];
						}

						class9.anIntArray435[0] -= 1;
						class9.aByteArray434[class9.anIntArray435[0]] = byte6;
						if (class9.anIntArray435[0] == 0)
						{
							int i10 = 4095;
							for (int k9 = 15; k9 >= 0; k9--)
							{
								for (int l9 = 15; l9 >= 0; l9--)
								{
									class9.aByteArray434[i10] = class9.aByteArray434[(class9.anIntArray435[k9] + l9)];
									i10--;
								}

								class9.anIntArray435[k9] = (i10 + 1);
							}
						}
					}

					class9.anIntArray425[(class9.aByteArray433[(byte6 & 0xFF)] & 0xFF)] += 1;
					Bzip2Context.anIntArray429[i6] = (class9.aByteArray433[(byte6 & 0xFF)] & 0xFF);
					i6++;
					if (j5 == 0)
					{
						i5++;
						j5 = 50;
						byte byte14 = class9.aByteArray436[i5];
						k8 = class9.anIntArray442[byte14];
						ai = class9.anIntArrayArray439[byte14];
						ai2 = class9.anIntArrayArray441[byte14];
						ai1 = class9.anIntArrayArray440[byte14];
					}
					j5--;
					int k7 = k8;
					byte byte11;
					int j8;
					for (j8 = method401(k7, class9); j8 > ai[k7]; j8 = j8 << 1
							| byte11)
					{
						k7++;
						byte11 = method400(class9);
					}

					k5 = ai2[(j8 - ai1[k7])];
				}
			}
			class9.anInt416 = 0;
			class9.aByte415 = 0;
			class9.anIntArray427[0] = 0;
			for (int j2 = 1; j2 <= 256; j2++)
			{
				class9.anIntArray427[j2] = class9.anIntArray425[(j2 - 1)];
			}
			for (int k2 = 1; k2 <= 256; k2++)
			{
				class9.anIntArray427[k2] += class9.anIntArray427[(k2 - 1)];
			}
			for (int l2 = 0; l2 < i6; l2++)
			{
				byte byte7 = (byte) (Bzip2Context.anIntArray429[l2] & 0xFF);
				Bzip2Context.anIntArray429[class9.anIntArray427[(byte7 & 0xFF)]] |= l2 << 8;
				class9.anIntArray427[(byte7 & 0xFF)] += 1;
			}

			class9.anInt423 = (Bzip2Context.anIntArray429[class9.anInt422] >> 8);
			class9.anInt426 = 0;
			class9.anInt423 = Bzip2Context.anIntArray429[class9.anInt423];
			class9.anInt424 = ((byte) (class9.anInt423 & 0xFF));
			class9.anInt423 >>= 8;
			class9.anInt426 += 1;
			class9.anInt443 = i6;
			method397(class9);
			if ((class9.anInt426 == class9.anInt443 + 1)
					&& (class9.anInt416 == 0))
				flag19 = true;
			else
				flag19 = false;
		}
	}

	private static byte method399(Bzip2Context class9)
	{
		return (byte) method401(8, class9);
	}

	private static byte method400(Bzip2Context class9)
	{
		return (byte) method401(1, class9);
	}

	private static int method401(int i, Bzip2Context class9)
	{
		int j;

		while (true)
		{
			if (class9.anInt419 >= i)
			{
				int k = class9.anInt418 >> class9.anInt419 - i & (1 << i) - 1;
				class9.anInt419 -= i;
				j = k;
				break;
			}
			class9.anInt418 = (class9.anInt418 << 8 | class9.aByteArray405[class9.anInt406] & 0xFF);
			class9.anInt419 += 8;
			class9.anInt406 += 1;
			class9.anInt407 -= 1;
			class9.anInt408 += 1;
			if (class9.anInt408 == 0)
				class9.anInt409 += 1;
		}
		return j;
	}

	private static int method402(int i, int[] ai)
	{
		int j = 0;
		int k = 256;
		do
		{
			int l = j + k >> 1;
			if (i >= ai[l])
				j = l;
			else
				k = l;
		}
		while (k - j != 1);
		return j;
	}

	private static void method403(Bzip2Context class9)
	{
		class9.anInt430 = 0;
		for (int i = 0; i < 256; i++)
			if (class9.aBooleanArray431[i])
			{
				class9.aByteArray433[class9.anInt430] = ((byte) i);
				class9.anInt430 += 1;
			}
	}

	private static void method404(int[] ai, int[] ai1, int[] ai2,
			byte[] abyte0, int i, int j, int k)
	{
		int l = 0;
		for (int i1 = i; i1 <= j; i1++)
		{
			for (int l2 = 0; l2 < k; l2++)
			{
				if (abyte0[l2] == i1)
				{
					ai2[l] = l2;
					l++;
				}
			}
		}

		for (int j1 = 0; j1 < 23; j1++)
		{
			ai1[j1] = 0;
		}
		for (int k1 = 0; k1 < k; k1++)
		{
			ai1[(abyte0[k1] + 1)] += 1;
		}
		for (int l1 = 1; l1 < 23; l1++)
		{
			ai1[l1] += ai1[(l1 - 1)];
		}
		for (int i2 = 0; i2 < 23; i2++)
		{
			ai[i2] = 0;
		}
		int i3 = 0;
		for (int j2 = i; j2 <= j; j2++)
		{
			i3 += ai1[(j2 + 1)] - ai1[j2];
			ai[j2] = (i3 - 1);
			i3 <<= 1;
		}

		for (int k2 = i + 1; k2 <= j; k2++)
			ai1[k2] = ((ai[(k2 - 1)] + 1 << 1) - ai1[k2]);
	}
}