import jagex.DataUtil;

public class Config {
	public final int anInt496 = 12345678;
	public static int item_cnt;
	public static int item_max_sprite;
	public static String[] item_name;
	public static String[] item_desc;
	public static String[] item_actions;
	public static int[] item_sprite;
	public static int[] item_val;
	public static int[] item_stackable;
	public static int[] anIntArray505;
	public static int[] item_equipable;
	public static int[] item_tint;
	public static int[] item_tradable;
	public static int[] item_members;
	public static int npc_cnt;
	public static String[] npc_name;
	public static String[] npc_actions;
	public static int[] npc_atk_lvl;
	public static int[] npc_def_lvl;
	public static int[] npc_str_lvl;
	public static int[] npc_hp_lvl;
	public static int[] npc_attackable;
	public static int[][] anIntArrayArray518;
	public static int[] anIntArray519;
	public static int[] anIntArray520;
	public static int[] anIntArray521;
	public static int[] anIntArray522;
	public static int[] anIntArray523;
	public static int[] anIntArray524;
	public static int[] anIntArray525;
	public static int[] anIntArray526;
	public static int[] anIntArray527;
	public static int anInt528;
	public static String[] aStringArray529;
	public static String[] aStringArray530;
	public static int anInt531;
	public static String[] entity_name;
	public static int[] anIntArray533;
	public static int[] anIntArray534;
	public static int[] anIntArray535;
	public static int[] anIntArray536;
	public static int[] entity_sprite_off;
	public static int anInt538;
	public static String[] aStringArray539;
	public static String[] aStringArray540;
	public static String[] aStringArray541;
	public static String[] aStringArray542;
	public static int[] model_id;
	public static int[] anIntArray544;
	public static int[] anIntArray545;
	public static int[] anIntArray546;
	public static int[] anIntArray547;
	public static int anInt548;
	public static String[] aStringArray549;
	public static String[] aStringArray550;
	public static String[] aStringArray551;
	public static String[] aStringArray552;
	public static int[] anIntArray553;
	public static int[] anIntArray554;
	public static int[] anIntArray555;
	public static int[] anIntArray556;
	public static int[] anIntArray557;
	public static int anInt558;
	public static int[] anIntArray559;
	public static int[] anIntArray560;
	public static int anInt561;
	public static int[] anIntArray562;
	public static int[] anIntArray563;
	public static int[] anIntArray564;
	public static int proj_sprite_cnt;
	public static int anInt566;
	public static String[] aStringArray567;
	public static String[] aStringArray568;
	public static int[] anIntArray569;
	public static int[] anIntArray570;
	public static int[] anIntArray571;
	public static int[][] anIntArrayArray572;
	public static int[][] anIntArrayArray573;
	public static int anInt574;
	public static String[] aStringArray575;
	public static String[] aStringArray576;
	public static int[] anIntArray577;
	public static int[] anIntArray578;
	public static int anInt579;
	public static String[] aStringArray580 = new String[5000];
	public static int anInt581;
	public static String[] aStringArray582 = new String[5000];
	public static int model_cnt;
	public static String[] model_names = new String['È'];
	static byte[] str_buf;
	static byte[] int_buf;
	static int str_ptr;
	static int int_ptr;

	public static int lookup_model(String s) {
		if (s.equalsIgnoreCase("na"))
			return 0;
		for (int i = 0; i < model_cnt; i++) {
			if (model_names[i].equalsIgnoreCase(s))
				return i;
		}
		model_names[(model_cnt++)] = s;
		return model_cnt - 1;
	}

	public static int byte_get() {
		int i = int_buf[int_ptr] & 0xFF;
		int_ptr += 1;
		return i;
	}

	public static int short_get() {
		int i = DataUtil.short_get(int_buf, int_ptr);
		int_ptr += 2;
		return i;
	}

	public static int int_get() {
		int i = DataUtil.int_get(int_buf, int_ptr);
		int_ptr += 4;
		if (i > 99999999)
			i = 99999999 - i;
		return i;
	}

	public static String str_get() {
		String s = "";
		for (; str_buf[str_ptr] != 0; s = s + (char) str_buf[(str_ptr++)])
			;
		str_ptr += 1;
		return s;
	}

	public static void decode(byte[] abyte0, boolean is_members) {
		str_buf = DataUtil.entry_extract("string.dat", 0, abyte0);
		str_ptr = 0;
		int_buf = DataUtil.entry_extract("integer.dat", 0, abyte0);
		int_ptr = 0;
		item_cnt = short_get();
		item_name = new String[item_cnt];
		item_desc = new String[item_cnt];
		item_actions = new String[item_cnt];
		item_sprite = new int[item_cnt];
		item_val = new int[item_cnt];
		item_stackable = new int[item_cnt];
		anIntArray505 = new int[item_cnt];
		item_equipable = new int[item_cnt];
		item_tint = new int[item_cnt];
		item_tradable = new int[item_cnt];
		item_members = new int[item_cnt];
		for (int i = 0; i < item_cnt; i++) {
			item_name[i] = str_get();
		}
		for (int j = 0; j < item_cnt; j++) {
			item_desc[j] = str_get();
		}
		for (int k = 0; k < item_cnt; k++) {
			item_actions[k] = str_get();
		}
		for (int l = 0; l < item_cnt; l++) {
			item_sprite[l] = short_get();
			if (item_sprite[l] + 1 > item_max_sprite) {
				item_max_sprite = item_sprite[l] + 1;
			}
		}
		for (int i1 = 0; i1 < item_cnt; i1++) {
			item_val[i1] = int_get();
		}
		for (int j1 = 0; j1 < item_cnt; j1++) {
			item_stackable[j1] = byte_get();
		}
		for (int k1 = 0; k1 < item_cnt; k1++) {
			anIntArray505[k1] = byte_get();
		}
		for (int l1 = 0; l1 < item_cnt; l1++) {
			item_equipable[l1] = short_get();
		}
		for (int i2 = 0; i2 < item_cnt; i2++) {
			item_tint[i2] = int_get();
		}
		for (int j2 = 0; j2 < item_cnt; j2++) {
			item_tradable[j2] = byte_get();
		}
		for (int k2 = 0; k2 < item_cnt; k2++) {
			item_members[k2] = byte_get();
		}
		for (int l2 = 0; l2 < item_cnt; l2++) {
			if ((!is_members) && (item_members[l2] == 1)) {
				item_name[l2] = "Members object";
				item_desc[l2] = "You need to be a member to use this object";
				item_val[l2] = 0;
				item_actions[l2] = "";
				anIntArray505[0] = 0;
				item_equipable[l2] = 0;
				item_tradable[l2] = 1;
			}
		}
		npc_cnt = short_get();
		npc_name = new String[npc_cnt];
		npc_actions = new String[npc_cnt];
		npc_atk_lvl = new int[npc_cnt];
		npc_def_lvl = new int[npc_cnt];
		npc_str_lvl = new int[npc_cnt];
		npc_hp_lvl = new int[npc_cnt];
		npc_attackable = new int[npc_cnt];
		anIntArrayArray518 = new int[npc_cnt][12];
		anIntArray519 = new int[npc_cnt];
		anIntArray520 = new int[npc_cnt];
		anIntArray521 = new int[npc_cnt];
		anIntArray522 = new int[npc_cnt];
		anIntArray523 = new int[npc_cnt];
		anIntArray524 = new int[npc_cnt];
		anIntArray525 = new int[npc_cnt];
		anIntArray526 = new int[npc_cnt];
		anIntArray527 = new int[npc_cnt];
		for (int i3 = 0; i3 < npc_cnt; i3++) {
			npc_name[i3] = str_get();
		}
		for (int j3 = 0; j3 < npc_cnt; j3++) {
			npc_actions[j3] = str_get();
		}
		for (int k3 = 0; k3 < npc_cnt; k3++) {
			npc_atk_lvl[k3] = byte_get();
		}
		for (int l3 = 0; l3 < npc_cnt; l3++) {
			npc_def_lvl[l3] = byte_get();
		}
		for (int i4 = 0; i4 < npc_cnt; i4++) {
			npc_str_lvl[i4] = byte_get();
		}
		for (int j4 = 0; j4 < npc_cnt; j4++) {
			npc_hp_lvl[j4] = byte_get();
		}
		for (int k4 = 0; k4 < npc_cnt; k4++) {
			npc_attackable[k4] = byte_get();
		}
		for (int l4 = 0; l4 < npc_cnt; l4++) {
			for (int i5 = 0; i5 < 12; i5++) {
				anIntArrayArray518[l4][i5] = byte_get();
				if (anIntArrayArray518[l4][i5] == 255) {
					anIntArrayArray518[l4][i5] = -1;
				}
			}
		}

		for (int j5 = 0; j5 < npc_cnt; j5++) {
			anIntArray519[j5] = int_get();
		}
		for (int k5 = 0; k5 < npc_cnt; k5++) {
			anIntArray520[k5] = int_get();
		}
		for (int l5 = 0; l5 < npc_cnt; l5++) {
			anIntArray521[l5] = int_get();
		}
		for (int i6 = 0; i6 < npc_cnt; i6++) {
			anIntArray522[i6] = int_get();
		}
		for (int j6 = 0; j6 < npc_cnt; j6++) {
			anIntArray523[j6] = short_get();
		}
		for (int k6 = 0; k6 < npc_cnt; k6++) {
			anIntArray524[k6] = short_get();
		}
		for (int l6 = 0; l6 < npc_cnt; l6++) {
			anIntArray525[l6] = byte_get();
		}
		for (int i7 = 0; i7 < npc_cnt; i7++) {
			anIntArray526[i7] = byte_get();
		}
		for (int j7 = 0; j7 < npc_cnt; j7++) {
			anIntArray527[j7] = byte_get();
		}
		anInt528 = short_get();
		aStringArray529 = new String[anInt528];
		aStringArray530 = new String[anInt528];
		for (int k7 = 0; k7 < anInt528; k7++) {
			aStringArray529[k7] = str_get();
		}
		for (int l7 = 0; l7 < anInt528; l7++) {
			aStringArray530[l7] = str_get();
		}
		anInt531 = short_get();
		entity_name = new String[anInt531];
		anIntArray533 = new int[anInt531];
		anIntArray534 = new int[anInt531];
		anIntArray535 = new int[anInt531];
		anIntArray536 = new int[anInt531];
		entity_sprite_off = new int[anInt531];
		for (int i8 = 0; i8 < anInt531; i8++) {
			entity_name[i8] = str_get();
		}
		for (int j8 = 0; j8 < anInt531; j8++) {
			anIntArray533[j8] = int_get();
		}
		for (int k8 = 0; k8 < anInt531; k8++) {
			anIntArray534[k8] = byte_get();
		}
		for (int l8 = 0; l8 < anInt531; l8++) {
			anIntArray535[l8] = byte_get();
		}
		for (int i9 = 0; i9 < anInt531; i9++) {
			anIntArray536[i9] = byte_get();
		}
		for (int j9 = 0; j9 < anInt531; j9++) {
			entity_sprite_off[j9] = byte_get();
		}
		anInt538 = short_get();
		aStringArray539 = new String[anInt538];
		aStringArray540 = new String[anInt538];
		aStringArray541 = new String[anInt538];
		aStringArray542 = new String[anInt538];
		model_id = new int[anInt538];
		anIntArray544 = new int[anInt538];
		anIntArray545 = new int[anInt538];
		anIntArray546 = new int[anInt538];
		anIntArray547 = new int[anInt538];
		for (int k9 = 0; k9 < anInt538; k9++) {
			aStringArray539[k9] = str_get();
		}
		for (int l9 = 0; l9 < anInt538; l9++) {
			aStringArray540[l9] = str_get();
		}
		for (int i10 = 0; i10 < anInt538; i10++) {
			aStringArray541[i10] = str_get();
		}
		for (int j10 = 0; j10 < anInt538; j10++) {
			aStringArray542[j10] = str_get();
		}
		for (int k10 = 0; k10 < anInt538; k10++) {
			model_id[k10] = lookup_model(str_get());
		}
		for (int l10 = 0; l10 < anInt538; l10++) {
			anIntArray544[l10] = byte_get();
		}
		for (int i11 = 0; i11 < anInt538; i11++) {
			anIntArray545[i11] = byte_get();
		}
		for (int j11 = 0; j11 < anInt538; j11++) {
			anIntArray546[j11] = byte_get();
		}
		for (int k11 = 0; k11 < anInt538; k11++) {
			anIntArray547[k11] = byte_get();
		}
		anInt548 = short_get();
		aStringArray549 = new String[anInt548];
		aStringArray550 = new String[anInt548];
		aStringArray551 = new String[anInt548];
		aStringArray552 = new String[anInt548];
		anIntArray553 = new int[anInt548];
		anIntArray554 = new int[anInt548];
		anIntArray555 = new int[anInt548];
		anIntArray556 = new int[anInt548];
		anIntArray557 = new int[anInt548];
		for (int l11 = 0; l11 < anInt548; l11++) {
			aStringArray549[l11] = str_get();
		}
		for (int i12 = 0; i12 < anInt548; i12++) {
			aStringArray550[i12] = str_get();
		}
		for (int j12 = 0; j12 < anInt548; j12++) {
			aStringArray551[j12] = str_get();
		}
		for (int k12 = 0; k12 < anInt548; k12++) {
			aStringArray552[k12] = str_get();
		}
		for (int l12 = 0; l12 < anInt548; l12++) {
			anIntArray553[l12] = short_get();
		}
		for (int i13 = 0; i13 < anInt548; i13++) {
			anIntArray554[i13] = int_get();
		}
		for (int j13 = 0; j13 < anInt548; j13++) {
			anIntArray555[j13] = int_get();
		}
		for (int k13 = 0; k13 < anInt548; k13++) {
			anIntArray556[k13] = byte_get();
		}
		for (int l13 = 0; l13 < anInt548; l13++) {
			anIntArray557[l13] = byte_get();
		}
		anInt558 = short_get();
		anIntArray559 = new int[anInt558];
		anIntArray560 = new int[anInt558];
		for (int i14 = 0; i14 < anInt558; i14++) {
			anIntArray559[i14] = byte_get();
		}
		for (int j14 = 0; j14 < anInt558; j14++) {
			anIntArray560[j14] = byte_get();
		}
		anInt561 = short_get();
		anIntArray562 = new int[anInt561];
		anIntArray563 = new int[anInt561];
		anIntArray564 = new int[anInt561];
		for (int k14 = 0; k14 < anInt561; k14++) {
			anIntArray562[k14] = int_get();
		}
		for (int l14 = 0; l14 < anInt561; l14++) {
			anIntArray563[l14] = byte_get();
		}
		for (int i15 = 0; i15 < anInt561; i15++) {
			anIntArray564[i15] = byte_get();
		}
		proj_sprite_cnt = short_get();
		anInt566 = short_get();
		aStringArray567 = new String[anInt566];
		aStringArray568 = new String[anInt566];
		anIntArray569 = new int[anInt566];
		anIntArray570 = new int[anInt566];
		anIntArray571 = new int[anInt566];
		anIntArrayArray572 = new int[anInt566][];
		anIntArrayArray573 = new int[anInt566][];
		for (int j15 = 0; j15 < anInt566; j15++) {
			aStringArray567[j15] = str_get();
		}
		for (int k15 = 0; k15 < anInt566; k15++) {
			aStringArray568[k15] = str_get();
		}
		for (int l15 = 0; l15 < anInt566; l15++) {
			anIntArray569[l15] = byte_get();
		}
		for (int i16 = 0; i16 < anInt566; i16++) {
			anIntArray570[i16] = byte_get();
		}
		for (int j16 = 0; j16 < anInt566; j16++) {
			anIntArray571[j16] = byte_get();
		}
		for (int k16 = 0; k16 < anInt566; k16++) {
			int l16 = byte_get();
			anIntArrayArray572[k16] = new int[l16];
			for (int j17 = 0; j17 < l16; j17++) {
				anIntArrayArray572[k16][j17] = short_get();
			}
		}

		for (int i17 = 0; i17 < anInt566; i17++) {
			int k17 = byte_get();
			anIntArrayArray573[i17] = new int[k17];
			for (int i18 = 0; i18 < k17; i18++) {
				anIntArrayArray573[i17][i18] = byte_get();
			}
		}

		anInt574 = short_get();
		aStringArray575 = new String[anInt574];
		aStringArray576 = new String[anInt574];
		anIntArray577 = new int[anInt574];
		anIntArray578 = new int[anInt574];
		for (int l17 = 0; l17 < anInt574; l17++) {
			aStringArray575[l17] = str_get();
		}
		for (int j18 = 0; j18 < anInt574; j18++) {
			aStringArray576[j18] = str_get();
		}
		for (int k18 = 0; k18 < anInt574; k18++) {
			anIntArray577[k18] = byte_get();
		}
		for (int l18 = 0; l18 < anInt574; l18++) {
			anIntArray578[l18] = byte_get();
		}
		byte[] word = DataUtil.entry_extract("words.txt", 0, abyte0);
		read_words(word, 0);
		byte[] badwords = DataUtil.entry_extract("badwords.txt", 0, abyte0);
		read_badwords(badwords, 0);
		str_buf = null;
		int_buf = null;
	}

	public static void read_words(byte[] abyte0, int i) {
		try {
			while (true) {
				String s = "";
				for (; abyte0[i] != 13; s = s + (char) abyte0[(i++)])
					;
				i++;
				if (abyte0[i] == 10)
					i++;
				if ((!s.equals("-EOF-")) && (s != null) && (s.length() <= 0))
					break;
				aStringArray580[(anInt579++)] = s;
			}

		} catch (Exception _ex) {
			DataUtil.replacement_term_cnt = anInt579;
			DataUtil.replacements = aStringArray580;
		}
	}

	public static void read_badwords(byte[] abyte0, int i) {
		try {
			while (true) {
				String s = "";
				for (; abyte0[i] != 13; s = s + (char) abyte0[(i++)])
					;
				i++;
				if (abyte0[i] == 10)
					i++;
				if ((!s.equals("-EOF-")) && (s != null) && (s.length() <= 0))
					break;
				aStringArray582[(anInt581++)] = s;
			}

		} catch (Exception e) {
			String[] vowels = { "a", "e", "i", "o", "u" };

			int j = anInt581;
			for (int k = 0; k < j; k++) {
				String s1 = aStringArray582[k];
				if (s1.length() >= 5) {
					for (int l = 1; l < s1.length() - 1; l++) {
						char c = s1.charAt(l);
						if ((c == 'a') || (c == 'e') || (c == 'i')
								|| (c == 'o') || (c == 'u') || (c == 'y')) {
							for (int i1 = 0; i1 < 5; i1++) {
								String s2 = vowels[i1];
								if (s2.charAt(0) != c) {
									String s5 = s1.substring(0, l) + s2
											+ s1.substring(l + 1);
									aStringArray582[(anInt581++)] = s5;
									s5 = s1.substring(0, l) + s2 + c
											+ s1.substring(l + 1);
									aStringArray582[(anInt581++)] = s5;
								}
							}

							String s3 = s1.substring(0, l)
									+ s1.substring(l + 1);
							aStringArray582[(anInt581++)] = s3;
						}
						char c1 = s1.charAt(l + 1);
						String s4 = s1.substring(0, l) + c1 + c
								+ s1.substring(l + 2);
						aStringArray582[(anInt581++)] = s4;
					}
				}

			}

			DataUtil.search_term_cnt = anInt581;
			DataUtil.search_terms = aStringArray582;
		}
	}
}