package sysdevs;

import jagex.DataUtil;
import jagex.client.Game;
import jagex.client.Model;
import jagex.client.Scene;
import jagex.client.Surface;

import java.awt.Graphics;
import java.io.IOException;

@SuppressWarnings("serial")
public class CubeTest extends Game {

	private Surface surface;
	private Scene scene;
	private Graphics graphics;
	private final int w, h;
	private final Model[] models = new Model[3];

	int ticks;
	int last_x, last_y;
		
	int cam_x = 0, cam_y = 8, cam_z = -50;
	int cam_yaw = 0, cam_pitch = 0, cam_dist = 10;
	
	CubeTest(int w, int h) {
		this.w = w;
		this.h = h;
	}
	
	void startDemo() {
		start(w, h, "tedward", false);
	}

	Model create_cube() {
		int front = Scene.rgb(255, 0, 0);
		int back = Scene.rgb(255, 0, 0);
		
		Model cube = new Model(8, 12);
		cube.vert_add(-64, -64, -64);
		cube.vert_add(-64, -64, 64);
		cube.vert_add(-64, 64, -64);
		cube.vert_add(-64, 64, 64);
		cube.vert_add(64, -64, -64);
		cube.vert_add(64, -64, 64);
		cube.vert_add(64, 64, -64);
		cube.vert_add(64, 64, 64);
		
		cube.face_add(3, new int[] { 0, 6, 4 }, front, back);
		cube.face_add(3, new int[] { 0, 2, 6 }, front, back);
		cube.face_add(3, new int[] { 0, 3, 2 }, front, back);
		cube.face_add(3, new int[] { 0, 1, 3 }, front, back);
		
		cube.face_add(3, new int[] { 2, 7, 6 }, front, back);
		cube.face_add(3, new int[] { 2, 3, 7 }, front, back);
		cube.face_add(3, new int[] { 4, 6, 7 }, front, back);
		cube.face_add(3, new int[] { 4, 7, 5 }, front, back);
		
		cube.face_add(3, new int[] { 0, 4, 5 }, front, back);
		cube.face_add(3, new int[] { 0, 5, 1 }, front, back);
		cube.face_add(3, new int[] { 1, 5, 7 }, front, back);
		cube.face_add(3, new int[] { 1, 7, 3 }, front, back);

		cube.scale(20, 20, 20);
		return cube;
	}
	
	Model create_plane() {
		int front = Scene.rgb(255, 0, 0);
		int back = Scene.rgb(0, 255, 0);
		
		Model plane = new Model(6, 2);
		
		plane.vert_add(-64, -64, 0);
		plane.vert_add(-64, 64, 0);
		plane.vert_add(64, 64, 0);
		
		plane.vert_add(-64, -64, 0);
		plane.vert_add(64, -64, 0);
		plane.vert_add(64, 64, 0);
		
		plane.face_add(3, new int[] { 0, 1, 2 }, front, back);
		plane.face_add(3, new int[] { 3, 4, 5 }, back, front);
		
		plane.scale(20, 20, 20);
		return plane;
	}

	public void load() {
		graphics = getGraphics();
		surface = new Surface(w, h, 1, this);

		byte[] buf = new byte[196626];
		try {
			DataUtil.read("Sky.tga", buf, 196626);
			surface.sprite_define(buf, 0, 0, false, 1, 1, true);
		} catch (IOException ioex) {
		}

		scene = new Scene(surface, models.length + 1, 100, 0);
		scene.clip_far_3d = 1500;
		scene.clip_far_2d = 150;
		scene.fog_falloff = 1;
		scene.fog_dist = 1200;
        scene.set_bounds(w / 2, h / 2, w / 2, h / 2, w, 9);
		scene.set_camera(cam_x, cam_y, cam_z, cam_yaw, cam_pitch, 0, cam_dist);
		scene.textures_allocate(1, 1, 1);
		scene.texture_define(0, surface.sprite_raster[0], surface.sprite_palette[0], 1);

		int start_x = -20;
		for (int i = 0; i < models.length; i++) {
			models[i] = create_cube();
			scene.add(models[i]);
			models[i].position(start_x, 10, 0);
			start_x += 20;
			
			models[i].set_light(i, 48, 48, -50, -10, -30);
		}
		set_rate(60);
	}

	public synchronized void update() {
		switch (pressed_key) {
		case 32: // space
			int yaw = 0;
			int pitch = 0;
			if (mouse_x != last_x) {
				pitch = mouse_x - last_x;
				last_x = mouse_x;
			}
			if (mouse_y != last_y) {
				yaw = mouse_y - last_y;
				last_y = mouse_y;
			}
			yaw %= 256;
			pitch %= 256;
			for (int i = 0; i < models.length; i++) {
				models[i].rotate(yaw, -pitch, 0);
			}
			break;
		case 99: // c
			scene.set_camera(cam_x, cam_y, cam_z, cam_yaw, -cam_pitch, 0, --cam_dist);
			break;
		case 100: // d
			scene.set_camera(cam_x, cam_y, cam_z, cam_yaw, -cam_pitch, 0, ++cam_dist);
			break;
		case 1004: // up
			scene.set_camera(cam_x, cam_y, ++cam_z, cam_yaw, -cam_pitch, 0, cam_dist);
			break;
		case 1005: // down
			scene.set_camera(cam_x, cam_y, --cam_z, cam_yaw, -cam_pitch, 0, cam_dist);
			break;
		case 1006: // left
			scene.set_camera(--cam_x, cam_y, cam_z, cam_yaw, -cam_pitch, 0, cam_dist);
			break;
		case 1007: // right
			scene.set_camera(++cam_x, cam_y, cam_z, cam_yaw, -cam_pitch, 0, cam_dist);
			break;
		}
		
		if (pressed_key != 32 && mouse_state != 0) { // space
			if (mouse_x != last_x) {
				cam_pitch += (mouse_x - last_x) / 3;
				last_x = mouse_x;
			}
			if (mouse_y != last_y) {
				//cam_yaw += mouse_y - last_y;
				last_y = mouse_y;
			}
			cam_yaw %= 0xFF;
			cam_pitch %= 0xFF;
			scene.set_camera(cam_x, cam_y, cam_z, cam_yaw, -cam_pitch, 0, cam_dist);
		}
	}

	public synchronized void display() {
		surface.rect_fill(0, 0, w, h, Surface.DARK_GRAY);
		scene.render();
		surface.text_draw("polygon count: " + scene.last_poly_cnt, 10, 20, Surface.FONT_BOLD, Surface.YELLOW);
		surface.text_draw("fps: " + fps, 10, 35, Surface.FONT_BOLD, Surface.YELLOW);
		surface.copy(graphics, 0, 0);
		surface.clear();
	}

	public static void main(String[] argv) {
		CubeTest demo = new CubeTest(600, 250);
		demo.startDemo();
		demo.min_delta = 15;
	}

	@Override
	public void cleanup() {
		
	}

	@Override
	public void preload_draw() {
		
	}
}