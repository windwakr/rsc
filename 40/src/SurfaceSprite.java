// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

import jagex.client.Surface;

import java.awt.*;

public class SurfaceSprite extends Surface {

    public mudclient mudclientref;

    public SurfaceSprite(int j, int k, int l, Component component) {
        super(j, k, l, component);
        boolean flag = false;
        if (flag)
            new Surface(j, k, l, component);
    }

    public void spriteClipping(int j, int k, int l, int i1, int j1, int k1, int l1) {
        if (j1 >= 40000) {
            mudclientref.drawItem(j, k, l, i1, j1 - 40000, k1, l1);
            return;
        }
        if (j1 >= 20000) {
            mudclientref.drawNpc(j, k, l, i1, j1 - 20000, k1, l1);
            return;
        }
        if (j1 >= 5000) {
            mudclientref.drawPlayer(j, k, l, i1, j1 - 5000, k1, l1);
            return;
        } else {
            super.spriteClipping(j, k, l, i1, j1);
            return;
        }
    }
}
