// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

package jagex.client;


// Referenced classes of package jagex.client:
//            i

public class Panel {

    public static boolean cg;
    public static boolean drawBackgroundArrow = true;
    public static int baseSpriteStart;
    public static int fg = 114;
    public static int gg = 114;
    public static int hg = 176;
    public static int textListEntryHeightMod;
    public int controlFlashText[];
    public int ve[];
    public int we[];
    public int xe[];
    public boolean bg;
    protected Surface me;
    int ne;
    int oe;
    boolean pe[];
    boolean qe[];
    boolean re[];
    boolean se[];
    boolean te[];
    boolean ye[];
    int ze[];
    int af[];
    int bf[];
    int cf[];
    int df[];
    int ef[];
    int ff[];
    String gf[];
    String hf[][];
    int _fldif;
    int jf;
    int kf;
    int lf;
    int mf;
    int nf;
    int of;
    int pf;
    int qf;
    int rf;
    int sf;
    int tf;
    int uf;
    int vf;
    int wf;
    int xf;
    int yf;
    int zf;
    int ag;
    public Panel(Surface j, int k) {
        mf = -1;
        bg = true;
        me = j;
        oe = k;
        pe = new boolean[k];
        qe = new boolean[k];
        re = new boolean[k];
        se = new boolean[k];
        ye = new boolean[k];
        te = new boolean[k];
        controlFlashText = new int[k];
        ve = new int[k];
        we = new int[k];
        xe = new int[k];
        ze = new int[k];
        af = new int[k];
        bf = new int[k];
        cf = new int[k];
        df = new int[k];
        ef = new int[k];
        ff = new int[k];
        gf = new String[k];
        hf = new String[k][];
        pf = uc(114, 114, 176);
        qf = uc(14, 14, 62);
        rf = uc(200, 208, 232);
        sf = uc(96, 129, 184);
        tf = uc(53, 95, 115);
        uf = uc(117, 142, 171);
        vf = uc(98, 122, 158);
        wf = uc(86, 100, 136);
        xf = uc(135, 146, 179);
        yf = uc(97, 112, 151);
        zf = uc(88, 102, 136);
        ag = uc(84, 93, 120);
    }

    public int uc(int j, int k, int l) {
        return Surface.rgb2long((fg * j) / 114, (gg * k) / 114, (hg * l) / 176);
    }

    public void bd(int j) {
        pe[j] = true;
    }

    public void nd(int j) {
        pe[j] = false;
    }

    public void kd() {
        kf = 0;
    }

    public void handleMouse(int j, int k, int l, int i1) {
        _fldif = j - nf;
        jf = k - of;
        lf = i1;
        if (l != 0)
            kf = l;
        if (l == 1) {
            for (int j1 = 0; j1 < ne; j1++)
                if (pe[j1] && bf[j1] == 10 && _fldif >= ze[j1] && jf >= af[j1] && _fldif <= ze[j1] + cf[j1] && jf <= af[j1] + df[j1])
                    se[j1] = true;

        }
    }

    public boolean isClicked(int j) {
        if (pe[j] && se[j]) {
            se[j] = false;
            return true;
        } else {
            return false;
        }
    }

    public void keyPress(int j) {
        if (j == 0)
            return;
        if (mf != -1 && gf[mf] != null && pe[mf]) {
            int k = gf[mf].length();
            if (j == 8 && k > 0)
                gf[mf] = gf[mf].substring(0, k - 1);
            if ((j == 10 || j == 13) && k > 0)
                se[mf] = true;
            String s = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789!\"\243$%^&*()-_=+[{]};:'@#~,<.>/?\\| ";
            if (k < ef[mf]) {
                for (int l = 0; l < s.length(); l++)
                    if (j == s.charAt(l))
                        gf[mf] += (char) j;

            }
            te[mf] = true;
            if (j == 9) {
                do
                    mf = (mf + 1) % ne;
                while (bf[mf] != 5 && bf[mf] != 6);
                te[mf] = true;
            }
        }
    }

    public void setFocus(int j) {
        mf = j;
    }

    public void tc(int j, int k, int l, int i1) {
        if (cg) {
            me.setBounds(j, k, l, i1);
            for (int j1 = 0; j1 < ne; j1++)
                if (pe[j1])
                    if (bf[j1] == 0)
                        dd(j1, ze[j1], af[j1], gf[j1], ff[j1]);
                    else if (bf[j1] == 1)
                        dd(j1, ze[j1] - me.textWidth(gf[j1], ff[j1]) / 2, af[j1], gf[j1], ff[j1]);
                    else if (bf[j1] == 2)
                        ed(ze[j1], af[j1], cf[j1], df[j1]);
                    else if (bf[j1] == 3)
                        sc(ze[j1], af[j1], cf[j1]);
                    else if (bf[j1] == 11)
                        cd(ze[j1], af[j1], cf[j1], df[j1]);
                    else if (bf[j1] == 12)
                        wc(ze[j1], af[j1], ff[j1]);

            me.mf();
        }
    }

    public void drawPanel() {
        for (int j = 0; j < ne; j++)
            if (pe[j]) {
                if (!cg)
                    if (bf[j] == 0)
                        dd(j, ze[j], af[j], gf[j], ff[j]);
                    else if (bf[j] == 1)
                        dd(j, ze[j] - me.textWidth(gf[j], ff[j]) / 2, af[j], gf[j], ff[j]);
                    else if (bf[j] == 2)
                        ed(ze[j], af[j], cf[j], df[j]);
                    else if (bf[j] == 3)
                        sc(ze[j], af[j], cf[j]);
                    else if (bf[j] == 11)
                        cd(ze[j], af[j], cf[j], df[j]);
                    else if (bf[j] == 12)
                        wc(ze[j], af[j], ff[j]);
                if (bf[j] == 4)
                    zc(j, ze[j], af[j], cf[j], df[j], ff[j], hf[j], ve[j], controlFlashText[j]);
                else if (bf[j] == 5 || bf[j] == 6)
                    pc(j, ze[j], af[j], cf[j], df[j], gf[j], ff[j]);
                else if (bf[j] == 7)
                    mc(j, ze[j], af[j], ff[j], hf[j]);
                else if (bf[j] == 8)
                    zb(j, ze[j], af[j], ff[j], hf[j]);
                else if (bf[j] == 9)
                    dc(j, ze[j], af[j], cf[j], df[j], ff[j], hf[j], ve[j], controlFlashText[j]);
            }

        kf = 0;
    }

    public int rc(int j) {
        return we[j];
    }

    public int getListEntryIndex(int j) {
        int k = xe[j];
        return k;
    }

    protected void dd(int j, int k, int l, String s, int i1) {
        int j1 = l + me.textHeight(i1) / 3;
        fd(j, k, j1, s, i1);
    }

    protected void fd(int j, int k, int l, String s, int i1) {
        int j1;
        if (ye[j])
            j1 = 0xffffff;
        else
            j1 = 0;
        me.drawstring(s, k, l, i1, j1);
    }

    protected void pc(int j, int k, int l, int i1, int j1, String s, int k1) {
        if (re[j]) {
            int l1 = s.length();
            s = "";
            for (int j2 = 0; j2 < l1; j2++)
                s = s + "X";

        }
        if (bf[j] == 5) {
            if (kf == 1 && _fldif >= k && jf >= l - j1 / 2 && _fldif <= k + i1 && jf <= l + j1 / 2) {
                te[mf] = true;
                te[j] = true;
                mf = j;
            }
            if (te[j]) {
                tc(k, l - j1 / 2, k + i1, l + j1 / 2);
                te[j] = false;
            }
        } else if (bf[j] == 6) {
            if (kf == 1 && _fldif >= k - i1 / 2 && jf >= l - j1 / 2 && _fldif <= k + i1 / 2 && jf <= l + j1 / 2) {
                te[mf] = true;
                te[j] = true;
                mf = j;
            }
            if (te[j]) {
                tc(k - i1 / 2, l - j1 / 2, k + i1 / 2, l + j1 / 2);
                te[j] = false;
            }
            k -= me.textWidth(s, k1) / 2;
        }
        if (mf == j)
            s = s + "*";
        int i2 = l + me.textHeight(k1) / 3;
        fd(j, k, i2, s, k1);
    }

    public void ed(int j, int k, int l, int i1) {
        me.setBounds(j, k, j + l, k + i1);
        me.ag(j, k, l, i1, ag, xf);
        if (drawBackgroundArrow) {
            for (int j1 = j - (k & 0x3f); j1 < j + l; j1 += 128) {
                for (int k1 = k - (k & 0x1f); k1 < k + i1; k1 += 128)
                    me.vf(j1, k1, 6 + baseSpriteStart);

            }

        }
        me.drawLineHoriz(j, k, l, xf);
        me.drawLineHoriz(j + 1, k + 1, l - 2, xf);
        me.drawLineHoriz(j + 2, k + 2, l - 4, yf);
        me.drawLineVert(j, k, i1, xf);
        me.drawLineVert(j + 1, k + 1, i1 - 2, xf);
        me.drawLineVert(j + 2, k + 2, i1 - 4, yf);
        me.drawLineHoriz(j, (k + i1) - 1, l, ag);
        me.drawLineHoriz(j + 1, (k + i1) - 2, l - 2, ag);
        me.drawLineHoriz(j + 2, (k + i1) - 3, l - 4, zf);
        me.drawLineVert((j + l) - 1, k, i1, ag);
        me.drawLineVert((j + l) - 2, k + 1, i1 - 2, ag);
        me.drawLineVert((j + l) - 3, k + 2, i1 - 4, zf);
        me.mf();
    }

    public void cd(int j, int k, int l, int i1) {
        me.drawBox(j, k, l, i1, 0);
        me.drawBoxEdge(j, k, l, i1, uf);
        me.drawBoxEdge(j + 1, k + 1, l - 2, i1 - 2, vf);
        me.drawBoxEdge(j + 2, k + 2, l - 4, i1 - 4, wf);
        me.drawSprite(j, k, 2 + baseSpriteStart);
        me.drawSprite((j + l) - 7, k, 3 + baseSpriteStart);
        me.drawSprite(j, (k + i1) - 7, 4 + baseSpriteStart);
        me.drawSprite((j + l) - 7, (k + i1) - 7, 5 + baseSpriteStart);
    }

    protected void wc(int j, int k, int l) {
        me.drawSprite(j, k, l);
    }

    protected void sc(int j, int k, int l) {
        me.drawLineHoriz(j, k, l, 0);
    }

    protected void zc(int j, int k, int l, int i1, int j1, int k1, String as[],
                      int l1, int i2) {
        int j2 = j1 / me.textHeight(k1);
        if (i2 > l1 - j2)
            i2 = l1 - j2;
        if (i2 < 0)
            i2 = 0;
        controlFlashText[j] = i2;
        if (j2 < l1) {
            int k2 = (k + i1) - 12;
            int i3 = ((j1 - 27) * j2) / l1;
            if (i3 < 6)
                i3 = 6;
            int k3 = ((j1 - 27 - i3) * i2) / (l1 - j2);
            if (lf == 1 && _fldif >= k2 && _fldif <= k2 + 12) {
                if (jf > l && jf < l + 12 && i2 > 0)
                    i2--;
                if (jf > (l + j1) - 12 && jf < l + j1 && i2 < l1 - j2)
                    i2++;
                controlFlashText[j] = i2;
                te[j] = true;
            }
            if (lf == 1 && (_fldif >= k2 && _fldif <= k2 + 12 || _fldif >= k2 - 12 && _fldif <= k2 + 24 && qe[j])) {
                if (jf > l + 12 && jf < (l + j1) - 12) {
                    qe[j] = true;
                    int i4 = jf - l - 12 - i3 / 2;
                    i2 = (i4 * l1) / (j1 - 24);
                    if (i2 > l1 - j2)
                        i2 = l1 - j2;
                    if (i2 < 0)
                        i2 = 0;
                    controlFlashText[j] = i2;
                    te[j] = true;
                }
            } else {
                qe[j] = false;
            }
            if (te[j]) {
                tc(k, l, k + i1, l + j1);
                te[j] = false;
            }
            k3 = ((j1 - 27 - i3) * i2) / (l1 - j2);
            jd(k, l, i1, j1, k3, i3);
        }
        if (te[j]) {
            tc(k, l, k + i1, l + j1);
            te[j] = false;
        }
        int l2 = j1 - j2 * me.textHeight(k1);
        int j3 = l + (me.textHeight(k1) * 5) / 6 + l2 / 2;
        for (int l3 = i2; l3 < l1; l3++) {
            fd(j, k + 2, j3, as[l3], k1);
            j3 += me.textHeight(k1) - textListEntryHeightMod;
            if (j3 >= l + j1)
                return;
        }

    }

    protected void jd(int j, int k, int l, int i1, int j1, int k1) {
        int l1 = (j + l) - 12;
        me.drawBoxEdge(l1, k, 12, i1, 0);
        me.drawSprite(l1 + 1, k + 1, baseSpriteStart);
        me.drawSprite(l1 + 1, (k + i1) - 12, 1 + baseSpriteStart);
        me.drawLineHoriz(l1, k + 13, 12, 0);
        me.drawLineHoriz(l1, (k + i1) - 13, 12, 0);
        me.ag(l1 + 1, k + 14, 11, i1 - 27, pf, qf);
        me.drawBox(l1 + 3, j1 + k + 14, 7, k1, sf);
        me.drawLineVert(l1 + 2, j1 + k + 14, k1, rf);
        me.drawLineVert(l1 + 2 + 8, j1 + k + 14, k1, tf);
    }

    protected void mc(int j, int k, int l, int i1, String as[]) {
        int j1 = 0;
        int k1 = as.length;
        for (int l1 = 0; l1 < k1; l1++) {
            j1 += me.textWidth(as[l1], i1);
            if (l1 < k1 - 1)
                j1 += me.textWidth("  ", i1);
        }

        int i2 = k - j1 / 2;
        int j2 = l + me.textHeight(i1) / 3;
        for (int k2 = 0; k2 < k1; k2++) {
            int l2;
            if (ye[j])
                l2 = 0xffffff;
            else
                l2 = 0;
            if (_fldif >= i2 && _fldif <= i2 + me.textWidth(as[k2], i1) && jf <= j2 && jf > j2 - me.textHeight(i1)) {
                if (ye[j])
                    l2 = 0x808080;
                else
                    l2 = 0xffffff;
                if (kf == 1) {
                    we[j] = k2;
                    se[j] = true;
                }
            }
            if (we[j] == k2)
                if (ye[j])
                    l2 = 0xff0000;
                else
                    l2 = 0xc00000;
            me.drawstring(as[k2], i2, j2, i1, l2);
            i2 += me.textWidth(as[k2] + "  ", i1);
        }

    }

    protected void zb(int j, int k, int l, int i1, String as[]) {
        int j1 = as.length;
        int k1 = l - (me.textHeight(i1) * (j1 - 1)) / 2;
        for (int l1 = 0; l1 < j1; l1++) {
            int i2;
            if (ye[j])
                i2 = 0xffffff;
            else
                i2 = 0;
            int j2 = me.textWidth(as[l1], i1);
            if (_fldif >= k - j2 / 2 && _fldif <= k + j2 / 2 && jf - 2 <= k1 && jf - 2 > k1 - me.textHeight(i1)) {
                if (ye[j])
                    i2 = 0x808080;
                else
                    i2 = 0xffffff;
                if (kf == 1) {
                    we[j] = l1;
                    se[j] = true;
                }
            }
            if (we[j] == l1)
                if (ye[j])
                    i2 = 0xff0000;
                else
                    i2 = 0xc00000;
            me.drawstring(as[l1], k - j2 / 2, k1, i1, i2);
            k1 += me.textHeight(i1);
        }

    }

    protected void dc(int j, int k, int l, int i1, int j1, int k1, String as[],
                      int l1, int i2) {
        int j2 = j1 / me.textHeight(k1);
        if (j2 < l1) {
            int k2 = (k + i1) - 12;
            int i3 = ((j1 - 27) * j2) / l1;
            if (i3 < 6)
                i3 = 6;
            int k3 = ((j1 - 27 - i3) * i2) / (l1 - j2);
            if (lf == 1 && _fldif >= k2 && _fldif <= k2 + 12) {
                if (jf > l && jf < l + 12 && i2 > 0)
                    i2--;
                if (jf > (l + j1) - 12 && jf < l + j1 && i2 < l1 - j2)
                    i2++;
                controlFlashText[j] = i2;
                te[j] = true;
            }
            if (lf == 1 && (_fldif >= k2 && _fldif <= k2 + 12 || _fldif >= k2 - 12 && _fldif <= k2 + 24 && qe[j])) {
                if (jf > l + 12 && jf < (l + j1) - 12) {
                    qe[j] = true;
                    int i4 = jf - l - 12 - i3 / 2;
                    i2 = (i4 * l1) / (j1 - 24);
                    if (i2 < 0)
                        i2 = 0;
                    if (i2 > l1 - j2)
                        i2 = l1 - j2;
                    controlFlashText[j] = i2;
                    te[j] = true;
                }
            } else {
                qe[j] = false;
            }
            if (te[j]) {
                tc(k, l, k + i1, l + j1);
                te[j] = false;
            }
            k3 = ((j1 - 27 - i3) * i2) / (l1 - j2);
            jd(k, l, i1, j1, k3, i3);
        } else {
            i2 = 0;
            controlFlashText[j] = 0;
        }
        if (te[j]) {
            tc(k, l, k + i1, l + j1);
            te[j] = false;
        }
        xe[j] = -1;
        int l2 = j1 - j2 * me.textHeight(k1);
        int j3 = l + (me.textHeight(k1) * 5) / 6 + l2 / 2;
        for (int l3 = i2; l3 < l1; l3++) {
            int j4;
            if (ye[j])
                j4 = 0xffffff;
            else
                j4 = 0;
            if (_fldif >= k + 2 && _fldif <= k + 2 + me.textWidth(as[l3], k1) && jf - 2 <= j3 && jf - 2 > j3 - me.textHeight(k1)) {
                if (ye[j])
                    j4 = 0x808080;
                else
                    j4 = 0xffffff;
                xe[j] = l3;
                if (kf == 1) {
                    we[j] = l3;
                    se[j] = true;
                }
            }
            if (we[j] == l3 && bg)
                j4 = 0xff0000;
            me.drawstring(as[l3], k + 2, j3, k1, j4);
            j3 += me.textHeight(k1);
            if (j3 >= l + j1)
                return;
        }

    }

    public int addText(int j, int k, String s, int l, boolean flag) {
        bf[ne] = 1;
        pe[ne] = true;
        se[ne] = false;
        ff[ne] = l;
        ye[ne] = flag;
        ze[ne] = j;
        af[ne] = k;
        gf[ne] = s;
        return ne++;
    }

    public void updateText(int j, String s) {
        gf[j] = s;
        te[j] = true;
    }

    public String oc(int j) {
        if (gf[j] == null)
            return "null";
        else
            return gf[j];
    }

    public int addButtonBackground(int j, int k, int l, int i1) {
        bf[ne] = 2;
        pe[ne] = true;
        se[ne] = false;
        ze[ne] = j - l / 2;
        af[ne] = k - i1 / 2;
        cf[ne] = l;
        df[ne] = i1;
        return ne++;
    }

    public int gc(int j, int k, int l, int i1) {
        bf[ne] = 11;
        pe[ne] = true;
        se[ne] = false;
        ze[ne] = j;
        af[ne] = k;
        cf[ne] = l;
        df[ne] = i1;
        return ne++;
    }

    public int addBoxRounded(int j, int k, int l, int i1) {
        bf[ne] = 11;
        pe[ne] = true;
        se[ne] = false;
        ze[ne] = j - l / 2;
        af[ne] = k - i1 / 2;
        cf[ne] = l;
        df[ne] = i1;
        return ne++;
    }

    public int addSprite(int j, int k, int l) {
        int i1 = me.fk[l];
        int j1 = me.gk[l];
        bf[ne] = 12;
        pe[ne] = true;
        se[ne] = false;
        ze[ne] = j - i1 / 2;
        af[ne] = k - j1 / 2;
        cf[ne] = i1;
        df[ne] = j1;
        ff[ne] = l;
        return ne++;
    }

    public int addTextList(int j, int k, int l, int i1, int j1, int k1, boolean flag) {
        bf[ne] = 4;
        pe[ne] = true;
        se[ne] = false;
        ze[ne] = j;
        af[ne] = k;
        cf[ne] = l;
        df[ne] = i1;
        ye[ne] = flag;
        ff[ne] = j1;
        ef[ne] = k1;
        ve[ne] = 0;
        controlFlashText[ne] = 0;
        hf[ne] = new String[k1];
        return ne++;
    }

    public void removeListEntry(int j, String s, boolean flag) {
        int k = ve[j]++;
        if (k >= ef[j]) {
            k--;
            ve[j]--;
            for (int l = 0; l < k; l++)
                hf[j][l] = hf[j][l + 1];

        }
        hf[j][k] = s;
        if (flag)
            controlFlashText[j] = 0xf423f;
        te[j] = true;
    }

    public int addTextListInput(int j, int k, int l, int i1, int j1, int k1, boolean flag,
                                boolean flag1) {
        bf[ne] = 5;
        pe[ne] = true;
        re[ne] = flag;
        se[ne] = false;
        ff[ne] = j1;
        ye[ne] = flag1;
        ze[ne] = j;
        af[ne] = k;
        cf[ne] = l;
        df[ne] = i1;
        ef[ne] = k1;
        gf[ne] = "";
        return ne++;
    }

    public int vc(int j, int k, int l, int i1, int j1, int k1, boolean flag,
                  boolean flag1) {
        bf[ne] = 6;
        pe[ne] = true;
        re[ne] = flag;
        se[ne] = false;
        ff[ne] = j1;
        ye[ne] = flag1;
        ze[ne] = j;
        af[ne] = k;
        cf[ne] = l;
        df[ne] = i1;
        ef[ne] = k1;
        gf[ne] = "";
        return ne++;
    }

    public int qc(int j, int k, String as[], int l, boolean flag) {
        bf[ne] = 7;
        pe[ne] = true;
        se[ne] = false;
        ff[ne] = l;
        ye[ne] = flag;
        ze[ne] = j;
        af[ne] = k;
        hf[ne] = as;
        we[ne] = 0;
        return ne++;
    }

    public int ac(int j, int k, String as[], int l, boolean flag) {
        bf[ne] = 8;
        pe[ne] = true;
        se[ne] = false;
        ff[ne] = l;
        ye[ne] = flag;
        ze[ne] = j;
        af[ne] = k;
        hf[ne] = as;
        we[ne] = 0;
        return ne++;
    }

    public int addTextListenerInteractive(int j, int k, int l, int i1, int j1, int k1, boolean flag) {
        bf[ne] = 9;
        pe[ne] = true;
        se[ne] = false;
        ff[ne] = j1;
        ye[ne] = flag;
        ze[ne] = j;
        af[ne] = k;
        cf[ne] = l;
        df[ne] = i1;
        ef[ne] = k1;
        hf[ne] = new String[k1];
        ve[ne] = 0;
        controlFlashText[ne] = 0;
        we[ne] = -1;
        xe[ne] = -1;
        return ne++;
    }

    public void clearList(int j) {
        ve[j] = 0;
        te[j] = true;
    }

    public void resetListProps(int j) {
        controlFlashText[j] = 0;
        xe[j] = -1;
    }

    public void addListEntry(int j, int k, String s) {
        hf[j][k] = s;
        if (k + 1 > ve[j])
            ve[j] = k + 1;
        te[j] = true;
    }

    public int addButton(int j, int k, int l, int i1) {
        bf[ne] = 10;
        pe[ne] = true;
        se[ne] = false;
        ze[ne] = j - l / 2;
        af[ne] = k - i1 / 2;
        cf[ne] = l;
        df[ne] = i1;
        return ne++;
    }

}
