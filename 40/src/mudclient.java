// Decompiled by Jad v1.5.8g. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.kpdus.com/jad.html
// Decompiler options: packimports(3) 

import jagex.Utility;
import jagex.client.*;
import jagex.client.Panel;

import java.awt.*;
import java.io.IOException;

public class mudclient extends GameConnection {
    public boolean appletMode;
    public int cameraRotation;
    public int bu;
    public int characterTopBottomColours[] = {
            0xff0000, 0xff8000, 0xffe000, 0xa0e000, 57344, 32768, 41088, 45311, 33023, 12528,
            0xe000e0, 0x303030, 0x604000, 0x805000, 0xffffff
    };
    public int characterHairColours[] = {
            0xffc030, 0xffa040, 0x805030, 0x604020, 0x303030, 0xff6020, 0xff4000, 0xffffff, 65280, 65535
    };
    public int characterSkinColours[] = {
            0xecded0, 0xccb366, 0xb38c40, 0x997326, 0x906020
    };
    boolean zs;
    int at;
    boolean cameraAutoAngleDebug;
    boolean optionCameraModeAuto;
    boolean optionPlayerKiller;
    int et;
    int ft;
    int gt;
    int showDialogSocialInput;
    String it;
    int jt;
    boolean kt;
    int combatTimeout;
    int mt;
    boolean errorLoadingCodebase;
    boolean errorLoadingMemory;
    Graphics graphics;
    Scene scene;
    SurfaceSprite surface;
    Image imageHbar;
    int tt;
    int magicLoc;
    int loggedIn;
    int npcWalkModel[] = {
            0, 1, 2, 1
    };
    int npcCombatModelArray1[] = {
            0, 1, 2, 1, 0, 0, 0, 0
    };
    int npcCombatModelArray2[] = {
            0, 0, 0, 0, 0, 1, 2, 1
    };
    int gameWidth;
    int gameHeight;
    int eu;
    int loginTimer;
    int deathScreenTimeout;
    int hu;
    boolean iu;
    int ju;
    int ku;
    int lu;
    int cameraZoom;
    boolean fogOfWar;
    int cameraAutoRotatePlayerX;
    int cameraAutoRotatePlayerY;
    int cameraAngle;
    int ru;
    int planeWidth;
    int planeHeight;
    int uu;
    int lastPlaneIndex;
    int areaX;
    int areaY;
    int planeIndex;
    int localLowerX;
    int localLowerY;
    int localUpperX;
    int localUpperY;
    World world;
    boolean showRightClickMenu;
    int menuX;
    int menuY;
    int menuWidth;
    int menuHeight;
    int menuItemsCount;
    int kv;
    boolean optionMouseButtonOne;
    String menuItemText2[];
    String menuItemText1[];
    int menuItemId[];
    int menuItemX[];
    int menuItemY[];
    int menuSourceType[];
    int menuSourceIndex[];
    int menuTargetIndex[];
    int menuIndices[];
    int maxPlayersServerCount;
    int maxPlayersCount;
    int playerCount;
    int spriteCount;
    Character playersServer[];
    Character players[];
    Character localPlayer;
    int regionX;
    int regionY;
    int maxNpcsServerCount;
    int maxNpcsCount;
    int npcCount;
    Character npcsServer[];
    Character npcs[];
    int jw;
    int kw;
    int npcAnimationArray[][] = {
            {
                    11, 2, 9, 7, 1, 6, 10, 0, 5, 8,
                    3, 4
            }, {
            11, 2, 9, 7, 1, 6, 10, 0, 5, 8,
            3, 4
    }, {
            11, 3, 2, 9, 7, 1, 6, 10, 0, 5,
            8, 4
    }, {
            3, 4, 2, 9, 7, 1, 6, 10, 8, 11,
            0, 5
    }, {
            3, 4, 2, 9, 7, 1, 6, 10, 8, 11,
            0, 5
    }, {
            4, 3, 2, 9, 7, 1, 6, 10, 8, 11,
            0, 5
    }, {
            11, 4, 2, 9, 7, 1, 6, 10, 0, 5,
            8, 3
    }, {
            11, 2, 9, 7, 1, 6, 10, 0, 5, 8,
            4, 3
    }
    };
    int mw;
    int groundItemCount;
    int spriteItem;
    int groundItemX[];
    int groundItemY[];
    int groundItemId[];
    int groundItemZ[];
    int tw;
    int objectCount;
    GameModel objectModel[];
    int objectX[];
    int objectY[];
    int objectId[];
    int objectDirection[];
    GameModel gameModels[];
    boolean objectAlreadyInMenu[];
    int cx;
    int wallObjectCount;
    GameModel wallObjectModel[];
    int wallObjectX[];
    int wallObjectY[];
    int wallObjectDirection[];
    int wallObjectId[];
    boolean wallObjectAlreadyInMenu[];
    int kx;
    int walkPathX[];
    int walkPathY[];
    int mouseButtonClick;
    int showUiTab;
    int inventoryItemsCount;
    int inventoryItemId[];
    int inventoryItemStackCount[];
    int inventoryEquipped[];
    int selectedItemInventoryIndex;
    String selectedItemName;
    boolean showDialogTrade;
    String wx;
    int tradeItemsCount;
    int tradeItems[];
    int tradeItemCount[];
    int ay;
    int tradeRecipientItems[];
    int cy[];
    boolean tradeRecipientAccepted;
    boolean tradeAccepted;
    int mouseButtonDownTime;
    int mouseButtonItemCountIncrement;
    boolean hy;
    int shopSellPriceMod;
    int shopBuyPriceMod;
    int shopItem[];
    int ly[];
    int shopItemPrice[];
    int shopSelectedItemIndex;
    int shopSelectedIemType;
    int playerStatCurrent[];
    int playerStatBase[];
    int playerStatEquipment[];
    int playerQuestPoints;
    String skillNameShort[] = {
            "Attack", "Defense", "Strength", "Hits", "Ranged", "Prayer", "Magic", "Cooking", "Woodcutting", "Fletching",
            "Fishing", "Firemaking", "Crafting", "Smithing", "Mining", "Herblaw"
    };
    String equipmentStatNames[] = {
            "Armour", "WeaponAim", "WeaponPower", "Magic", "Prayer"
    };
    int vy;
    int wy[];
    boolean showOptionMenu;
    int optionMenuCount;
    String optionMenuEntry[];
    int spriteUtil;
    int spriteMedia;
    int loginScreen;
    String loginUser;
    String loginPass;
    String loginUserDesc;
    String loginUserDisp;
    int messageTabFlashAll;
    int messageTabFlashHistory;
    int messageTabFlashQuest;
    int messageTabFlashPrivate;
    Panel panelMessageTabs;
    int controlTextListChat;
    int controlTextListAll;
    int controlTextListQuest;
    int controlTextListPrivate;
    int messageTabsSelected;
    int maxMessagesCount;
    String messageHistory[];
    int messageHistoryTimeout[];
    int combatStyle;
    int vz;
    int sprite_unknown;
    Panel panelLoginWelcome;
    Panel panelLoginNewUser;
    Panel panelLoginExistingUser;
    Panel panelAppearance;
    Panel bab;
    int controlWelcomeNewUser;
    int controlWelcomeExistingUser;
    int eab;
    int fab;
    int gab;
    int hab;
    int iab;
    int jab;
    int kab;
    int lab;
    int mab;
    int nab;
    int oab;
    int pab;
    int qab;
    int rab;
    int controlLoginUser;
    int controlLoginPass;
    int controlLoginOk;
    int controlLoginCancel;
    int wab;
    int xab;
    int yab;
    int controlButtonAppearanceHead1;
    int controlButtonAppearanceHead2;
    int controlButtonAppearanceHair1;
    int controlButtonAppearanceHair2;
    int controlButtonAppearanceGender1;
    int controlButtonAppearanceGender2;
    int controlButtonAppearanceTop1;
    int controlButtonAppearanceTop2;
    int controlButtonAppearanceSkin1;
    int controlButtonAppearanceSkin2;
    int controlButtonAppearanceBottom1;
    int controlButtonAppearanceBottom2;
    int controlButtonAppearanceAccept;
    int mbb;
    int nbb;
    int obb;
    int pbb;
    int qbb;
    int rbb;
    int sbb;
    boolean showAppearanceChange;
    int appearanceHeadType;
    int appearanceBodyGender;
    int wbb;
    int appearanceHairColour;
    int appearanceTopColour;
    int appearanceBottomColour;
    int appearanceSkinColour;
    int appearanceHeadGender;
    Panel panelMagic;
    int controlMagicPanel;
    int tabMagicPrayer;
    Panel panelSocial;
    int controlSocialPanel;
    int tabSocial;
    long privateMessageTarget;
    int selectedSpell;
    String textCast;
    int objectAnimationCount;
    int objectAnimationNumberTorch;
    int objectAnimationNumberFireLightningSpell;
    int lastObjectAnimationNumberTorch;
    int lastObjectAnimationNumberFireLightningSpell;
    int mouseClickXStep;
    int mouseClickXX;
    int mouseClickXY;
    int receivedMessagesCount;
    String receivedMessages[];
    int receivedMessageX[];
    int receivedMessageY[];
    int receivedMessageMidPoint[];
    int receivedMessageHeight[];
    int itemsAboveHeadCount;
    int actionBubbleX[];
    int actionBubbleY[];
    int actionBubbleScale[];
    int actionBubbleItem[];
    int healthBarCount;
    int healthBarX[];
    int healthBarY[];
    int healthBarMissing[];
    public mudclient() {
        zs = false;
        cameraAutoAngleDebug = false;
        optionCameraModeAuto = true;
        optionPlayerKiller = false;
        it = "";
        kt = false;
        errorLoadingCodebase = false;
        errorLoadingMemory = false;
        magicLoc = 128;
        appletMode = true;
        cameraRotation = 128;
        gameWidth = 512;
        gameHeight = 334;
        eu = 9;
        hu = 0xbc614e;
        iu = false;
        cameraZoom = 550;
        fogOfWar = false;
        cameraAngle = 1;
        lastPlaneIndex = -1;
        planeIndex = -1;
        showRightClickMenu = false;
        kv = 200;
        optionMouseButtonOne = false;
        menuItemText2 = new String[kv];
        menuItemText1 = new String[kv];
        menuItemId = new int[kv];
        menuItemX = new int[kv];
        menuItemY = new int[kv];
        menuSourceType = new int[kv];
        menuSourceIndex = new int[kv];
        menuTargetIndex = new int[kv];
        menuIndices = new int[kv];
        maxPlayersServerCount = 4000;
        maxPlayersCount = 500;
        playersServer = new Character[maxPlayersServerCount];
        players = new Character[maxPlayersCount];
        localPlayer = new Character();
        maxNpcsServerCount = 1000;
        maxNpcsCount = 500;
        npcsServer = new Character[maxNpcsServerCount];
        npcs = new Character[maxNpcsCount];
        mw = 500;
        groundItemX = new int[mw];
        groundItemY = new int[mw];
        groundItemId = new int[mw];
        groundItemZ = new int[mw];
        tw = 1500;
        objectModel = new GameModel[tw];
        objectX = new int[tw];
        objectY = new int[tw];
        objectId = new int[tw];
        objectDirection = new int[tw];
        gameModels = new GameModel[200];
        objectAlreadyInMenu = new boolean[tw];
        cx = 500;
        wallObjectModel = new GameModel[cx];
        wallObjectX = new int[cx];
        wallObjectY = new int[cx];
        wallObjectDirection = new int[cx];
        wallObjectId = new int[cx];
        wallObjectAlreadyInMenu = new boolean[cx];
        kx = 8000;
        walkPathX = new int[kx];
        walkPathY = new int[kx];
        inventoryItemId = new int[30];
        inventoryItemStackCount = new int[30];
        inventoryEquipped = new int[30];
        selectedItemInventoryIndex = -1;
        selectedItemName = "";
        showDialogTrade = false;
        wx = "";
        tradeItems = new int[14];
        tradeItemCount = new int[14];
        tradeRecipientItems = new int[14];
        cy = new int[14];
        tradeRecipientAccepted = false;
        tradeAccepted = false;
        hy = false;
        shopItem = new int[256];
        ly = new int[256];
        shopItemPrice = new int[256];
        shopSelectedItemIndex = -1;
        shopSelectedIemType = -2;
        playerStatCurrent = new int[16];
        playerStatBase = new int[16];
        playerStatEquipment = new int[5];
        wy = new int[500];
        showOptionMenu = false;
        optionMenuEntry = new String[5];
        loginUser = "";
        loginPass = "";
        loginUserDesc = "";
        loginUserDisp = "";
        maxMessagesCount = 5;
        messageHistory = new String[maxMessagesCount];
        messageHistoryTimeout = new int[maxMessagesCount];
        vz = 40;
        showAppearanceChange = false;
        appearanceBodyGender = 1;
        wbb = 2;
        appearanceHairColour = 2;
        appearanceTopColour = 8;
        appearanceBottomColour = 14;
        appearanceHeadGender = 1;
        selectedSpell = -1;
        lastObjectAnimationNumberTorch = -1;
        lastObjectAnimationNumberFireLightningSpell = -1;
        receivedMessages = new String[50];
        receivedMessageX = new int[50];
        receivedMessageY = new int[50];
        receivedMessageMidPoint = new int[50];
        receivedMessageHeight = new int[50];
        actionBubbleX = new int[50];
        actionBubbleY = new int[50];
        actionBubbleScale = new int[50];
        actionBubbleItem = new int[50];
        healthBarX = new int[50];
        healthBarY = new int[50];
        healthBarMissing = new int[50];
    }

    public static void main(String as[]) throws IOException {
        mudclient mudclient1 = new mudclient();
        mudclient1.appletMode = false;
        if (as.length == 1)
            mudclient1.serverAddress2 = "server2.runescape.com";
        mudclient1.startApplication(mudclient1.gameWidth, mudclient1.gameHeight + 22, "Runescape by Andrew Gower", false);
        mudclient1.threadSleep = 10;
    }

    public void startGame() {
        if (appletMode) {
            String s = getDocumentBase().getHost().toLowerCase();
            if (!s.endsWith("jagex.com") && !s.endsWith("jagex.co.uk") && !s.endsWith("jagex.superb.net") && !s.endsWith("207.228.231.226") && !s.endsWith("runescape.com") && !s.endsWith("runescape.co.uk") && !s.endsWith("64.23.60.47") && !s.endsWith("penguin.local") && !s.endsWith("jagex.dnsalias.com")) {
                errorLoadingCodebase = true;
                return;
            }
        }
        super.port = 43594;
        super.yOffset = -11;// todo
        GameConnection.vc = 500;
        GameConnection.uc = false;
        GameConnection.xc = 6;
        loadGameConfig();
        spriteMedia = 2000;
        spriteUtil = spriteMedia + 100;
        spriteItem = spriteUtil + 50;
        sprite_unknown = spriteItem + 300;
        graphics = getGraphics();
        setTargetFps(50);
        surface = new SurfaceSprite(gameWidth, gameHeight + 12, 2600, this);
        surface.mudclientref = this;
        surface.setBounds(0, 0, gameWidth, gameHeight + 12);
        Panel.drawBackgroundArrow = false;
        Panel.baseSpriteStart = spriteUtil;
        panelMagic = new Panel(surface, 5);
        int x = ((Surface) (surface)).width2 - 199;
        byte y = 36;
        controlMagicPanel = panelMagic.addTextListenerInteractive(x, y + 24, 196, 90, 1, 500, true);
        panelSocial = new Panel(surface, 5);
        controlSocialPanel = panelSocial.addTextListenerInteractive(x, y + 40, 196, 126, 1, 500, true);
        loadMedia();
        loadEntities(true);
        scene = new Scene(surface, 15000, 15000, 1000);
        scene.setMidpoints(gameWidth / 2, gameHeight / 2, gameWidth / 2, gameHeight / 2, gameWidth, eu);
        scene.clipFar3d = 2400;
        scene.clipFar2d = 2400;
        scene.fogZFalloff = 1;
        scene.fogZDistance = 2300;
        scene.xh(-50, -10, -50);
        world = new World(scene, surface);
        world.baseMediaSprite = spriteMedia;
        loadTextures();
        loadModels();
        loadMaps();
        showLoadingProgress(100, "Starting game...");
        createMessageTabPanel();
        createLoginPanels();
        createAppearancePanel();
        resetLoginScreenVariables();
        drawHbar();
    }

    public void loadGameConfig() {
        if (inAppletMode()) {
            byte abyte0[] = null;
            try {
                abyte0 = readDataFile("config" + Version.CONFIG + ".jag", "Configuration", 10);
            } catch (IOException ioexception) {
                System.out.println("Load error:" + ioexception);
            }
            for (String filename : new String[]{"projectile", "entity", "objects", "npc", "location", "boundary", "roof", "floor", "spells", "shop"}) {
                write("config", filename + ".txt", abyte0);
            }

            GameData.loadData(abyte0);
            return;
        } else {
            drawLoadingScreen(10, "Loading configuration");
            GameData.loadData();
            return;
        }
    }

    public void loadMedia() {
        if (inAppletMode()) {
            byte abyte0[] = null;
            try {
                abyte0 = readDataFile("media" + Version.MEDIA + ".jag", "2d graphics", 20);
            } catch (IOException ioexception) {
                System.out.println("Load error:" + ioexception);
            }
            surface.loadSprite(abyte0, Utility.getDataFileOffset("inv1.tga", abyte0), spriteMedia, true, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("inv2.tga", abyte0), spriteMedia + 1, true, 1, 6, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("bubble.tga", abyte0), spriteMedia + 9, true, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("runescape.tga", abyte0), spriteMedia + 10, true, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("splat.tga", abyte0), spriteMedia + 11, true, 3, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("icon.tga", abyte0), spriteMedia + 14, true, 4, 2, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("hbar.tga", abyte0), spriteMedia + 22, false, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("hbar2.tga", abyte0), spriteMedia + 23, true, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("compass.tga", abyte0), spriteMedia + 24, true, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("scrollbar.tga", abyte0), spriteUtil, true, 2, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("corners.tga", abyte0), spriteUtil + 2, true, 4, false);
            surface.loadSprite(abyte0, Utility.getDataFileOffset("arrows.tga", abyte0), spriteUtil + 6, true, 2, false);
            imageHbar = createImage(Utility.unpackData("hbar.tga", 0, abyte0));
            for (String filename : new String[]{"inv1.tga", "inv2.tga", "bubble.tga", "runescape.tga", "splat.tga",
                    "icon.tga", "hbar.tga", "hbar2.tga", "compass.tga", "scrollbar.tga", "corners.tga", "arrows.tga"}) {
                write("media", filename, abyte0);
            }
            int i1 = GameData.itemSpriteCount;
            for (int k1 = 1; i1 > 0; k1++) {
                int i2 = i1;
                i1 -= 30;
                if (i2 > 30)
                    i2 = 30;
                surface.loadSprite(abyte0, Utility.getDataFileOffset("objects" + k1 + ".tga", abyte0), spriteItem + (k1 - 1) * 30, true, 10, (i2 + 9) / 10, false);
                write("media", "objects" + k1 + ".tga", abyte0);
            }

            surface.loadSprite(abyte0, Utility.getDataFileOffset("projectile.tga", abyte0), sprite_unknown, true, GameData.ijb, false);
            write("media", "projectile.tga", abyte0);
            return;
        }
        byte abyte1[] = new byte[0x186a0];
        showLoadingProgress(20, "Loading 2d graphics");
        try {
            Utility.loadData("../gamedata/media/inv1.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia, true, false);
            Utility.loadData("../gamedata/media/inv2.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 1, true, 1, 6, false);
            Utility.loadData("../gamedata/media/bubble.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 9, true, false);
            Utility.loadData("../gamedata/media/runescape.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 10, true, false);
            Utility.loadData("../gamedata/media/splat.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 11, true, 3, false);
            Utility.loadData("../gamedata/media/icon.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 14, true, 4, 2, false);
            Utility.loadData("../gamedata/media/hbar.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 22, false, false);
            imageHbar = createImage(abyte1);
            Utility.loadData("../gamedata/media/hbar2.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 23, true, false);
            Utility.loadData("../gamedata/media/compass.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteMedia + 24, true, false);
            Utility.loadData("../gamedata/media/scrollbar.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteUtil, true, 2, false);
            Utility.loadData("../gamedata/media/corners.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteUtil + 2, true, 4, false);
            Utility.loadData("../gamedata/media/arrows.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, spriteUtil + 6, true, 2, false);
            int j1 = GameData.itemSpriteCount;
            for (int l1 = 1; j1 > 0; l1++) {
                int j2 = j1;
                j1 -= 30;
                if (j2 > 30)
                    j2 = 30;
                Utility.loadData("../gamedata/media/objects" + l1 + ".tga", abyte1, 0x186a0);
                surface.loadSprite(abyte1, 0, spriteItem + (l1 - 1) * 30, true, 10, (j2 + 9) / 10, false);
            }

            Utility.loadData("../gamedata/media/projectile.tga", abyte1, 0x186a0);
            surface.loadSprite(abyte1, 0, sprite_unknown, true, GameData.ijb, false);
            return;
        } catch (IOException _ex) {
            System.out.println("ERROR: in raw media loader");
        }
    }

    public void loadEntities(boolean flag) {
        jw = 0;
        kw = jw;
        byte abyte0[] = null;
        if (inAppletMode() && flag) {
            String s = "entity" + Version.ENTITY + ".jag";
            try {
                abyte0 = readDataFile(s, "people and monsters", 30);
            } catch (IOException ioexception) {
                System.out.println("Load error:" + ioexception);
            }
        } else {
            showLoadingProgress(30, "Loading people and monsters");
        }
        int i1 = 0;
        label0:
        for (int j1 = 0; j1 < GameData.entityCount; j1++) {
            String s1 = GameData.entityVar2[j1];
            for (int k1 = 0; k1 < j1; k1++) {
                if (!GameData.entityVar2[k1].equalsIgnoreCase(s1))
                    continue;
                GameData.entityVar7[j1] = GameData.entityVar7[k1];
                continue label0;
            }

            if (flag)
                if (inAppletMode()) {
                    boolean flag1 = true;
                    if (GameData.entityVar4[j1] != 0)
                        flag1 = false;
                    surface.loadSprite(Utility.unpackData(s1 + ".tga", 0, abyte0), 0, kw, true, 15, flag1);
                    write("entity", s1 + ".tga", abyte0);
                    i1 += 15;
                    if (GameData.entityVar5[j1] == 1) {
                        surface.loadSprite(Utility.unpackData(s1 + "a.tga", 0, abyte0), 0, kw + 15, true, 3, true);
                        write("entity", s1 + "a.tga", abyte0);
                        i1 += 3;
                    }
                    if (GameData.entityVar6[j1] == 1) {
                        surface.loadSprite(Utility.unpackData(s1 + "f.tga", 0, abyte0), 0, kw + 18, true, 9, true);
                        write("entity", s1 + "f.tga", abyte0);
                        i1 += 9;
                    }
                } else {
                    try {
                        byte abyte1[] = new byte[0x493e0];
                        Utility.loadData("../gamedata/entity/" + s1 + ".tga", abyte1, 0x493e0);
                        i1 += 15;
                        boolean flag2 = true;
                        if (GameData.entityVar4[j1] != 0)
                            flag2 = false;
                        surface.loadSprite(abyte1, 0, kw, true, 15, flag2);
                        if (GameData.entityVar5[j1] == 1) {
                            Utility.loadData("../gamedata/entity/" + s1 + "a.tga", abyte1, 0x493e0);
                            i1 += 3;
                            surface.loadSprite(abyte1, 0, kw + 15, true, 3, true);
                        }
                        if (GameData.entityVar6[j1] == 1) {
                            Utility.loadData("../gamedata/entity/" + s1 + "f.tga", abyte1, 0x493e0);
                            i1 += 9;
                            surface.loadSprite(abyte1, 0, kw + 18, true, 9, true);
                        }
                    } catch (IOException _ex) {
                        System.out.println("ERROR: in raw entity loader - no:" + j1 + " " + s1);
                    }
                }
            GameData.entityVar7[j1] = kw;
            kw += 27;
        }

        System.out.println("Loaded: " + i1 + " frames of animation");
    }

    public void loadTextures() {
        if (inAppletMode()) {
            scene.loadTextures("textures" + Version.TEXTURES + ".jag", 7, 11, 50, this);
            return;
        } else {
            showLoadingProgress(50, "Loading textures");
            scene.loadTextures("../gamedata/textures");
            return;
        }
    }

    public void loadModels() {
        GameData.getModelId("torcha2");
        GameData.getModelId("torcha3");
        GameData.getModelId("torcha4");
        GameData.getModelId("skulltorcha2");
        GameData.getModelId("skulltorcha3");
        GameData.getModelId("skulltorcha4");
        GameData.getModelId("firea2");
        GameData.getModelId("firea3");
        if (inAppletMode()) {
            byte abyte0[] = null;
            try {
                abyte0 = readDataFile("models" + Version.MODELS + ".jag", "3d models", 70);
            } catch (IOException ioexception) {
                System.out.println("Load error:" + ioexception);
            }
            for (int j1 = 0; j1 < GameData.modelCount; j1++) {
                int k1 = Utility.getDataFileOffset(GameData.modelName[j1] + ".ob2", abyte0);
                write("models", GameData.modelName[j1] + ".ob2", abyte0);
                if (k1 != 0)
                    gameModels[j1] = new GameModel(abyte0, k1);
                else
                    gameModels[j1] = new GameModel(1, 1);
            }

            return;
        }
        showLoadingProgress(70, "Loading 3d models");
        for (int i1 = 0; i1 < GameData.modelCount; i1++)
            gameModels[i1] = new GameModel("../gamedata/models/" + GameData.modelName[i1] + ".ob2");

    }

    public void loadMaps() {
        if (inAppletMode()) {
            world.mapPack = null;
            try {
                world.mapPack = readDataFile("maps" + Version.MAPS + ".jag", "map", 90);
                /*for(int plane = 0; plane < 10; plane++) {
                    long l = System.currentTimeMillis();
                    for(int x = 0; x < 500; x++) {
                        for(int y = 0; y < 500; y++) {
                            String s = "m" + plane + x / 10 + x % 10 + y / 10 + y % 10;
                            if(Utility.getDataFileOffset(s + ".jm", world.mapPack) != 0) {
                                write("maps", s + ".jm", world.mapPack);
                            }
                        }
                    }
                    System.out.println("plane " + plane + " - " + (System.currentTimeMillis() - l));
                }*/
                return;
            } catch (IOException ioexception) {
                System.out.println("Load error:" + ioexception);
            }
            return;
        } else {
            world.vdb = false;
            return;
        }
    }

    public void createMessageTabPanel() {
        panelMessageTabs = new Panel(surface, 10);
        controlTextListChat = panelMessageTabs.addTextList(5, 269, 502, 56, 1, 20, true);
        controlTextListAll = panelMessageTabs.addTextListInput(7, 324, 498, 14, 1, 80, false, true);
        controlTextListQuest = panelMessageTabs.addTextList(5, 269, 502, 56, 1, 20, true);
        controlTextListPrivate = panelMessageTabs.addTextList(5, 269, 502, 56, 1, 20, true);
        panelMessageTabs.setFocus(controlTextListAll);
    }

    public void handleInputs() {
        if (errorLoadingCodebase)
            return;
        if (errorLoadingMemory)
            return;
        try {
            loginTimer++;
            if (loggedIn == 0) {
                super.lastMouseAction = 0;
                handleLoginScreenInput();
            }
            if (loggedIn == 1) {
                jt++;
                super.lastMouseAction++;
                handleGameInput();
            }
            if (at > 0)
                at--;
            if (zs && at == 0) {
                zs = false;
                super.clientStream.flush();
                at = 24;
            }
            super.lastMouseButtonDown = 0;
            super.lastKeyCode2 = 0;
            if (messageTabFlashAll > 0)
                messageTabFlashAll--;
            if (messageTabFlashHistory > 0)
                messageTabFlashHistory--;
            if (messageTabFlashQuest > 0)
                messageTabFlashQuest--;
            if (messageTabFlashPrivate > 0) {
                messageTabFlashPrivate--;
                return;
            }
        } catch (OutOfMemoryError _ex) {
            disposeAndCollect();
            errorLoadingMemory = true;
        }
    }

    public void draw() {
        if (errorLoadingCodebase) {
            Graphics g1 = getGraphics();
            g1.setColor(Color.black);
            g1.fillRect(0, 0, 512, 356);
            g1.setFont(new Font("Helvetica", 1, 20));
            g1.setColor(Color.white);
            g1.drawString("Error - unable to load game!", 50, 50);
            g1.drawString("To play RuneScape make sure you play from", 50, 100);
            g1.drawString("http://www.runescape.com", 50, 150);
            setTargetFps(1);
            return;
        }
        if (errorLoadingMemory) {
            Graphics g2 = getGraphics();
            g2.setColor(Color.black);
            g2.fillRect(0, 0, 512, 356);
            g2.setFont(new Font("Helvetica", 1, 20));
            g2.setColor(Color.white);
            g2.drawString("Error - out of memory!", 50, 50);
            g2.drawString("Close ALL unnecessary programs", 50, 100);
            g2.drawString("and windows before loading the game", 50, 150);
            g2.drawString("RuneScape needs about 48meg of spare RAM", 50, 200);
            setTargetFps(1);
            return;
        }
        try {
            if (loggedIn == 0) {
                surface.loggedIn = false;
                drawLoginScreens();
            }
            if (loggedIn == 1) {
                surface.loggedIn = true;
                drawGame();
                return;
            }
        } catch (OutOfMemoryError _ex) {
            disposeAndCollect();
            errorLoadingMemory = true;
        }
    }

    public void onClosing() {
        closeConnection();
        disposeAndCollect();
    }

    public void disposeAndCollect() {
        try {
            if (surface != null) {
                surface.clear();
                surface.pixels = null;
                surface = null;
            }
            if (scene != null) {
                scene.clear();
                scene = null;
            }
            gameModels = null;
            objectModel = null;
            wallObjectModel = null;
            playersServer = null;
            players = null;
            npcsServer = null;
            npcs = null;
            localPlayer = null;
            if (world != null) {
                world.yeb = null;
                world.zeb = null;
                world.afb = null;
                world.parentModel = null;
                world = null;
            }
            System.gc();
            return;
        } catch (Exception _ex) {
            return;
        }
    }

    public void drawHbar() {
        graphics.drawImage(imageHbar, 0, 0, this);
    }

    public void handleKeyPress(int i1) {
        if (loggedIn == 0) {
            if (loginScreen == 0)
                panelLoginWelcome.keyPress(i1);
            if (loginScreen == 1)
                panelLoginNewUser.keyPress(i1);
            if (loginScreen == 2)
                panelLoginExistingUser.keyPress(i1);
        }
        if (showAppearanceChange)
            panelAppearance.keyPress(i1);
        if (loggedIn == 1) {
            if (gt == 0 && showDialogSocialInput == 0)
                panelMessageTabs.keyPress(i1);
            if (gt == 3 || gt == 4)
                gt = 0;
        }
    }

    public void finalisePacket() {
        super.clientStream.sendPacket();
        zs = true;
        pong();
    }

    public void resetLoginScreenVariables() {
        loggedIn = 0;
        loginScreen = 0;
        loginUser = "";
        loginPass = "";
        loginUserDesc = "Please enter a username:";
        loginUserDisp = "*" + loginUser + "*";
        playerCount = 0;
        npcCount = 0;
    }

    public void resetPMText() {
        super.inputPmCurrent = "";
        super.inputPmFinal = "";
    }

    public boolean trylogout() {
        if (loggedIn == 0)
            return true;
        if (combatTimeout > 450) {
            showMessage("@cya@You can't logout during combat!", 3);
            return false;
        }
        if (combatTimeout > 0) {
            showMessage("@cya@You can't logout for 10 seconds after combat", 3);
            return false;
        } else {
            closeConnection();
            return true;
        }
    }

    public void createAppearancePanel() {
        panelAppearance = new Panel(surface, 100);
        panelAppearance.addText(256, 10, "Design Your Character", 4, true);
        char c1 = '\214';
        int i1 = 34;
        panelAppearance.addButtonBackground(c1, i1, 200, 25);
        panelAppearance.addText(c1, i1, "Appearance", 4, false);
        i1 += 15;
        panelAppearance.addText(c1 - 55, i1 + 110, "Front", 3, true);
        panelAppearance.addText(c1, i1 + 110, "Side", 3, true);
        panelAppearance.addText(c1 + 55, i1 + 110, "Back", 3, true);
        byte byte0 = 54;
        i1 += 145;
        panelAppearance.addBoxRounded(c1 - byte0, i1, 53, 41);
        panelAppearance.addText(c1 - byte0, i1 - 8, "Head", 1, true);
        panelAppearance.addText(c1 - byte0, i1 + 8, "Type", 1, true);
        panelAppearance.addSprite(c1 - byte0 - 40, i1, Panel.baseSpriteStart + 7);
        controlButtonAppearanceHead1 = panelAppearance.addButton(c1 - byte0 - 40, i1, 20, 20);
        panelAppearance.addSprite((c1 - byte0) + 40, i1, Panel.baseSpriteStart + 6);
        controlButtonAppearanceHead2 = panelAppearance.addButton((c1 - byte0) + 40, i1, 20, 20);
        panelAppearance.addBoxRounded(c1 + byte0, i1, 53, 41);
        panelAppearance.addText(c1 + byte0, i1 - 8, "Hair", 1, true);
        panelAppearance.addText(c1 + byte0, i1 + 8, "Color", 1, true);
        panelAppearance.addSprite((c1 + byte0) - 40, i1, Panel.baseSpriteStart + 7);
        controlButtonAppearanceHair1 = panelAppearance.addButton((c1 + byte0) - 40, i1, 20, 20);
        panelAppearance.addSprite(c1 + byte0 + 40, i1, Panel.baseSpriteStart + 6);
        controlButtonAppearanceHair2 = panelAppearance.addButton(c1 + byte0 + 40, i1, 20, 20);
        i1 += 50;
        panelAppearance.addBoxRounded(c1 - byte0, i1, 53, 41);
        panelAppearance.addText(c1 - byte0, i1, "Gender", 1, true);
        panelAppearance.addSprite(c1 - byte0 - 40, i1, Panel.baseSpriteStart + 7);
        controlButtonAppearanceGender1 = panelAppearance.addButton(c1 - byte0 - 40, i1, 20, 20);
        panelAppearance.addSprite((c1 - byte0) + 40, i1, Panel.baseSpriteStart + 6);
        controlButtonAppearanceGender2 = panelAppearance.addButton((c1 - byte0) + 40, i1, 20, 20);
        panelAppearance.addBoxRounded(c1 + byte0, i1, 53, 41);
        panelAppearance.addText(c1 + byte0, i1 - 8, "Top", 1, true);
        panelAppearance.addText(c1 + byte0, i1 + 8, "Color", 1, true);
        panelAppearance.addSprite((c1 + byte0) - 40, i1, Panel.baseSpriteStart + 7);
        controlButtonAppearanceTop1 = panelAppearance.addButton((c1 + byte0) - 40, i1, 20, 20);
        panelAppearance.addSprite(c1 + byte0 + 40, i1, Panel.baseSpriteStart + 6);
        controlButtonAppearanceTop2 = panelAppearance.addButton(c1 + byte0 + 40, i1, 20, 20);
        i1 += 50;
        panelAppearance.addBoxRounded(c1 - byte0, i1, 53, 41);
        panelAppearance.addText(c1 - byte0, i1 - 8, "Skin", 1, true);
        panelAppearance.addText(c1 - byte0, i1 + 8, "Color", 1, true);
        panelAppearance.addSprite(c1 - byte0 - 40, i1, Panel.baseSpriteStart + 7);
        controlButtonAppearanceSkin1 = panelAppearance.addButton(c1 - byte0 - 40, i1, 20, 20);
        panelAppearance.addSprite((c1 - byte0) + 40, i1, Panel.baseSpriteStart + 6);
        controlButtonAppearanceSkin2 = panelAppearance.addButton((c1 - byte0) + 40, i1, 20, 20);
        panelAppearance.addBoxRounded(c1 + byte0, i1, 53, 41);
        panelAppearance.addText(c1 + byte0, i1 - 8, "Bottom", 1, true);
        panelAppearance.addText(c1 + byte0, i1 + 8, "Color", 1, true);
        panelAppearance.addSprite((c1 + byte0) - 40, i1, Panel.baseSpriteStart + 7);
        controlButtonAppearanceBottom1 = panelAppearance.addButton((c1 + byte0) - 40, i1, 20, 20);
        panelAppearance.addSprite(c1 + byte0 + 40, i1, Panel.baseSpriteStart + 6);
        controlButtonAppearanceBottom2 = panelAppearance.addButton(c1 + byte0 + 40, i1, 20, 20);
        c1 = '\u0174';
        i1 = 35;
        panelAppearance.addButtonBackground(c1, i1, 200, 25);
        panelAppearance.addText(c1, i1, "Character Type", 4, false);
        i1 += 22;
        panelAppearance.addText(c1, i1, "Each character type has different starting", 0, true);
        i1 += 13;
        panelAppearance.addText(c1, i1, "bonuses. But the choice you make here", 0, true);
        i1 += 13;
        panelAppearance.addText(c1, i1, "isn't permanent, and will change depending", 0, true);
        i1 += 13;
        panelAppearance.addText(c1, i1, "on how you play the game.", 0, true);
        i1 += 73;
        panelAppearance.addBoxRounded(c1, i1, 215, 125);
        String as[] = {
                "Adventurer", "Warrior", "Wizard", "Ranger", "Miner"
        };
        mbb = panelAppearance.ac(c1, i1 + 2, as, 3, true);
        i1 += 75;
        panelAppearance.addBoxRounded(c1, i1 + 21, 215, 60);
        panelAppearance.addText(c1, i1, "Do you wish to be able to fight with other", 0, true);
        i1 += 13;
        panelAppearance.addText(c1, i1, "players? Warning! If you choose 'yes' then", 0, true);
        i1 += 13;
        panelAppearance.addText(c1, i1, "other players will be able to attack you too!", 0, true);
        i1 += 13;
        String as1[] = {
                "No thanks", "Yes I'll fight"
        };
        nbb = panelAppearance.qc(c1, i1, as1, 1, true);
        i1 += 32;
        panelAppearance.addButtonBackground(c1, i1, 200, 30);
        panelAppearance.addText(c1, i1, "Start Game", 4, false);
        controlButtonAppearanceAccept = panelAppearance.addButton(c1, i1, 200, 30);
    }

    public void drawAppearancePanelCharacterSprites() {
        surface.interlace = false;
        surface.blackScreen();
        panelAppearance.tc(0, 0, gameWidth, gameHeight);
        panelAppearance.drawPanel();
        char c1 = '\214';
        byte byte0 = 50;
        surface.xf(c1 - 32 - 55, byte0, 64, 102, GameData.entityVar7[wbb], characterTopBottomColours[appearanceBottomColour]);
        surface.spriteClipping(c1 - 32 - 55, byte0, 64, 102, GameData.entityVar7[appearanceBodyGender], characterTopBottomColours[appearanceTopColour], characterSkinColours[appearanceSkinColour], 0, false);
        surface.spriteClipping(c1 - 32 - 55, byte0, 64, 102, GameData.entityVar7[appearanceHeadType], characterHairColours[appearanceHairColour], characterSkinColours[appearanceSkinColour], 0, false);
        surface.xf(c1 - 32, byte0, 64, 102, GameData.entityVar7[wbb] + 6, characterTopBottomColours[appearanceBottomColour]);
        surface.spriteClipping(c1 - 32, byte0, 64, 102, GameData.entityVar7[appearanceBodyGender] + 6, characterTopBottomColours[appearanceTopColour], characterSkinColours[appearanceSkinColour], 0, false);
        surface.spriteClipping(c1 - 32, byte0, 64, 102, GameData.entityVar7[appearanceHeadType] + 6, characterHairColours[appearanceHairColour], characterSkinColours[appearanceSkinColour], 0, false);
        surface.xf((c1 - 32) + 55, byte0, 64, 102, GameData.entityVar7[wbb] + 12, characterTopBottomColours[appearanceBottomColour]);
        surface.spriteClipping((c1 - 32) + 55, byte0, 64, 102, GameData.entityVar7[appearanceBodyGender] + 12, characterTopBottomColours[appearanceTopColour], characterSkinColours[appearanceSkinColour], 0, false);
        surface.spriteClipping((c1 - 32) + 55, byte0, 64, 102, GameData.entityVar7[appearanceHeadType] + 12, characterHairColours[appearanceHairColour], characterSkinColours[appearanceSkinColour], 0, false);
        surface.drawSprite(0, gameHeight, spriteMedia + 22);
        surface.draw(graphics, 0, 11);
    }

    public void handleAppearancePanelControls() {
        panelAppearance.handleMouse(super.mouseX, super.mouseY, super.lastMouseButtonDown, super.mouseButtonDown);
        if (panelAppearance.isClicked(controlButtonAppearanceHead1))
            do
                appearanceHeadType = ((appearanceHeadType - 1) + GameData.entityCount) % GameData.entityCount;
            while ((GameData.entityVar4[appearanceHeadType] & 3) != 1 || (GameData.entityVar4[appearanceHeadType] & 4 * appearanceHeadGender) == 0);
        if (panelAppearance.isClicked(controlButtonAppearanceHead2))
            do
                appearanceHeadType = (appearanceHeadType + 1) % GameData.entityCount;
            while ((GameData.entityVar4[appearanceHeadType] & 3) != 1 || (GameData.entityVar4[appearanceHeadType] & 4 * appearanceHeadGender) == 0);
        if (panelAppearance.isClicked(controlButtonAppearanceHair1))
            appearanceHairColour = ((appearanceHairColour - 1) + characterHairColours.length) % characterHairColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceHair2))
            appearanceHairColour = (appearanceHairColour + 1) % characterHairColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceGender1) || panelAppearance.isClicked(controlButtonAppearanceGender2)) {
            for (appearanceHeadGender = 3 - appearanceHeadGender; (GameData.entityVar4[appearanceHeadType] & 3) != 1 || (GameData.entityVar4[appearanceHeadType] & 4 * appearanceHeadGender) == 0; appearanceHeadType = (appearanceHeadType + 1) % GameData.entityCount)
                ;
            for (; (GameData.entityVar4[appearanceBodyGender] & 3) != 2 || (GameData.entityVar4[appearanceBodyGender] & 4 * appearanceHeadGender) == 0; appearanceBodyGender = (appearanceBodyGender + 1) % GameData.entityCount)
                ;
        }
        if (panelAppearance.isClicked(controlButtonAppearanceTop1))
            appearanceTopColour = ((appearanceTopColour - 1) + characterTopBottomColours.length) % characterTopBottomColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceTop2))
            appearanceTopColour = (appearanceTopColour + 1) % characterTopBottomColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceSkin1))
            appearanceSkinColour = ((appearanceSkinColour - 1) + characterSkinColours.length) % characterSkinColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceSkin2))
            appearanceSkinColour = (appearanceSkinColour + 1) % characterSkinColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceBottom1))
            appearanceBottomColour = ((appearanceBottomColour - 1) + characterTopBottomColours.length) % characterTopBottomColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceBottom2))
            appearanceBottomColour = (appearanceBottomColour + 1) % characterTopBottomColours.length;
        if (panelAppearance.isClicked(controlButtonAppearanceAccept)) {
            super.clientStream.newPacket(236);
            super.clientStream.writeByte(appearanceHeadGender);
            super.clientStream.writeByte(appearanceHeadType);
            super.clientStream.writeByte(appearanceBodyGender);
            super.clientStream.writeByte(wbb);
            super.clientStream.writeByte(appearanceHairColour);
            super.clientStream.writeByte(appearanceTopColour);
            super.clientStream.writeByte(appearanceBottomColour);
            super.clientStream.writeByte(appearanceSkinColour);
            super.clientStream.writeByte(panelAppearance.rc(mbb));
            super.clientStream.writeByte(panelAppearance.rc(nbb));
            super.clientStream.flushPacket();
            showAppearanceChange = false;
        }
    }

    public void createLoginPanels() {
        panelLoginWelcome = new Panel(surface, 50);
        int i1 = 35;
        panelLoginWelcome.addText(250, 200 + i1, "Click on an option", 5, true);
        panelLoginWelcome.addButtonBackground(150, 240 + i1, 120, 35);
        panelLoginWelcome.addButtonBackground(350, 240 + i1, 120, 35);
        panelLoginWelcome.addText(150, 240 + i1, "New User", 5, false);
        panelLoginWelcome.addText(350, 240 + i1, "Existing User", 5, false);
        controlWelcomeNewUser = panelLoginWelcome.addButton(150, 240 + i1, 120, 35);
        controlWelcomeExistingUser = panelLoginWelcome.addButton(350, 240 + i1, 120, 35);
        i1 = 60;
        byte byte0 = 110;
        boolean flag = false;
        panelLoginNewUser = new Panel(surface, 50);
        panelLoginNewUser.addButtonBackground(250, i1 + 17, 420, 34);
        panelLoginNewUser.addText(250, i1 + 8, "Choose a Username (This is the name other users will see)", 4, flag);
        nab = panelLoginNewUser.vc(250, i1 + 25, 200, 40, 4, 12, false, flag);
        i1 += 40;
        panelLoginNewUser.addButtonBackground(250, i1 + 17, 420, 34);
        panelLoginNewUser.addText(250, i1 + 8, "Choose a Password (You will require this to login)", 4, flag);
        pab = panelLoginNewUser.vc(250, i1 + 25, 200, 40, 4, 12, false, flag);
        i1 += 40;
        panelLoginNewUser.addButtonBackground(250, i1 + 17, 420, 34);
        panelLoginNewUser.addText(250, i1 + 8, "E-mail address", 4, flag);
        oab = panelLoginNewUser.vc(250, i1 + 25, 200, 40, 4, 40, false, flag);
        i1 += 40;
        panelLoginNewUser.addButtonBackground(250, i1 + 22, 420, 44);
        panelLoginNewUser.addText(250, i1 + 7, "Do you want to receive our free weekly newsletter? Get news of the latest", 1, flag);
        panelLoginNewUser.addText(250, i1 + 21, "improvements, new-quests!, hints+tips, hiscores, special-events! etc...", 1, flag);
        String as[] = {
                "Yes sounds great!", "No-thanks"
        };
        jab = panelLoginNewUser.qc(250, i1 + 35, as, 1, flag);
        i1 += 50;
        panelLoginNewUser.addButtonBackground(((250 - byte0) + 50) - 15, i1 + 17, 270, 34);
        eab = panelLoginNewUser.addText(((250 - byte0) + 50) - 15, i1 + 8, "To create an account please enter", 4, true);
        fab = panelLoginNewUser.addText(((250 - byte0) + 50) - 15, i1 + 25, "all the requested details", 4, true);
        panelLoginNewUser.addButtonBackground(350, i1 + 17, 70, 34);
        panelLoginNewUser.addText(350, i1 + 17, "Submit", 5, flag);
        mab = panelLoginNewUser.addButton(335, i1 + 17, 100, 34);
        panelLoginNewUser.addButtonBackground(425, i1 + 17, 70, 34);
        panelLoginNewUser.addText(425, i1 + 17, "Cancel", 5, flag);
        lab = panelLoginNewUser.addButton(425, i1 + 17, 100, 34);
        panelLoginNewUser.setFocus(nab);
        panelLoginExistingUser = new Panel(surface, 50);
        i1 = 83;
        panelLoginExistingUser.addButtonBackground(250, i1, 300, 40);
        qab = panelLoginExistingUser.addText(250, i1 - 10, "Please enter your", 5, true);
        rab = panelLoginExistingUser.addText(250, i1 + 10, "username and password", 5, true);
        i1 += 60;
        panelLoginExistingUser.addButtonBackground(250, i1, 200, 40);
        panelLoginExistingUser.addText(250, i1 - 10, "Username:", 4, flag);
        controlLoginUser = panelLoginExistingUser.vc(250, i1 + 10, 200, 40, 4, 12, false, flag);
        i1 += 60;
        panelLoginExistingUser.addButtonBackground(250, i1, 200, 40);
        panelLoginExistingUser.addText(250, i1 - 10, "Password:", 4, flag);
        controlLoginPass = panelLoginExistingUser.vc(250, i1 + 10, 200, 40, 4, 20, true, flag);
        i1 += 60;
        byte0 = 70;
        panelLoginExistingUser.addButtonBackground(250 - byte0, i1, 110, 40);
        panelLoginExistingUser.addText(250 - byte0, i1, "Ok", 4, flag);
        controlLoginOk = panelLoginExistingUser.addButton(250 - byte0, i1, 110, 40);
        panelLoginExistingUser.addButtonBackground(250 + byte0, i1, 110, 40);
        panelLoginExistingUser.addText(250 + byte0, i1, "Cancel", 4, flag);
        controlLoginCancel = panelLoginExistingUser.addButton(250 + byte0, i1, 110, 40);
        panelLoginExistingUser.setFocus(controlLoginUser);
        bab = new Panel(surface, 50);
        i1 = 20;
        bab.addText(250, i1, "Runescape-Rules / Terms+Conditions", 5, true);
        i1 += 30;
        bab.gc(40, i1 - 10, 420, 220);
        qbb = bab.addTextList(50, i1, 400, 200, 1, 1000, true);
        em(bab, qbb);
        i1 += 240;
        bab.addButtonBackground(120, i1, 170, 50);
        bab.addText(120, i1 - 10, "I have read the terms", 1, false);
        bab.addText(120, i1, "and conditions above", 1, false);
        bab.addText(120, i1 + 10, "And I Agree", 1, false);
        obb = bab.addButton(120, i1, 170, 50);
        bab.addButtonBackground(380, i1, 170, 50);
        bab.addText(380, i1, "I do not agree", 1, false);
        pbb = bab.addButton(380, i1, 170, 50);
    }

    public void drawLoginScreens() {
        surface.interlace = false;
        surface.blackScreen();
        if (loginScreen == 0)
            surface._mthif(256, 95, spriteMedia + 10);
        if (loginScreen == 0) {
            panelLoginWelcome.tc(0, 0, gameWidth, gameHeight);
            panelLoginWelcome.drawPanel();
        }
        if (loginScreen == 1) {
            panelLoginNewUser.tc(0, 0, gameWidth, gameHeight);
            panelLoginNewUser.drawPanel();
        }
        if (loginScreen == 2) {
            panelLoginExistingUser.tc(0, 0, gameWidth, gameHeight);
            panelLoginExistingUser.drawPanel();
        }
        if (loginScreen == 3) {
            bab.tc(0, 0, gameWidth, gameHeight);
            bab.drawPanel();
        }
        surface.drawSprite(0, gameHeight, spriteMedia + 22);
        surface.draw(graphics, 0, 11);
    }

    public void handleLoginScreenInput() {
        if (loginScreen == 0) {
            panelLoginWelcome.handleMouse(super.mouseX, super.mouseY, super.lastMouseButtonDown, super.mouseButtonDown);
            if (panelLoginWelcome.isClicked(controlWelcomeNewUser))
                loginScreen = 3;
            if (panelLoginWelcome.isClicked(controlWelcomeExistingUser)) {
                loginScreen = 2;
                panelLoginExistingUser.updateText(qab, "Please enter your");
                panelLoginExistingUser.updateText(rab, "username and password");
                panelLoginExistingUser.updateText(controlLoginUser, "");
                panelLoginExistingUser.updateText(controlLoginPass, "");
                panelLoginExistingUser.setFocus(controlLoginUser);
                return;
            }
        } else if (loginScreen == 1) {
            panelLoginNewUser.handleMouse(super.mouseX, super.mouseY, super.lastMouseButtonDown, super.mouseButtonDown);
            if (panelLoginNewUser.isClicked(nab))
                panelLoginNewUser.setFocus(pab);
            if (panelLoginNewUser.isClicked(pab))
                panelLoginNewUser.setFocus(oab);
            if (panelLoginNewUser.isClicked(oab))
                panelLoginNewUser.setFocus(nab);
            if (panelLoginNewUser.isClicked(lab))
                loginScreen = 0;
            if (panelLoginNewUser.isClicked(mab)) {
                if (panelLoginNewUser.oc(nab) != null && panelLoginNewUser.oc(nab).length() == 0 || panelLoginNewUser.oc(oab) != null && panelLoginNewUser.oc(oab).length() == 0 || panelLoginNewUser.oc(pab) != null && panelLoginNewUser.oc(pab).length() == 0) {
                    panelLoginNewUser.updateText(eab, "Please fill in ALL requested");
                    panelLoginNewUser.updateText(fab, "information to continue!");
                    return;
                }
                panelLoginNewUser.updateText(eab, "Please wait...");
                panelLoginNewUser.updateText(fab, "Creating new account");
                drawLoginScreens();
                resetTimings();
                panelLoginNewUser.oc(gab);
                panelLoginNewUser.oc(hab);
                String s = panelLoginNewUser.oc(nab);
                String s1 = panelLoginNewUser.oc(pab);
                String s2 = panelLoginNewUser.oc(oab);
                int i1 = panelLoginNewUser.rc(kab);
                int j1 = panelLoginNewUser.rc(jab);
                int k1 = 0;
                String s3 = panelLoginNewUser.oc(iab);
                try {
                    k1 = Integer.parseInt(s3);
                } catch (Exception _ex) {
                }
                newPlayer(s, s1, s2, i1, k1, j1);
                return;
            }
        } else {
            if (loginScreen == 2) {
                panelLoginExistingUser.handleMouse(super.mouseX, super.mouseY, super.lastMouseButtonDown, super.mouseButtonDown);
                if (panelLoginExistingUser.isClicked(controlLoginCancel))
                    loginScreen = 0;
                if (panelLoginExistingUser.isClicked(controlLoginUser))
                    panelLoginExistingUser.setFocus(controlLoginPass);
                if (panelLoginExistingUser.isClicked(controlLoginPass) || panelLoginExistingUser.isClicked(controlLoginOk)) {
                    loginUser = panelLoginExistingUser.oc(controlLoginUser);
                    loginPass = panelLoginExistingUser.oc(controlLoginPass);
                    login(loginUser, loginPass);
                }
                return;
            }
            if (loginScreen == 3) {
                bab.handleMouse(super.mouseX, super.mouseY, super.lastMouseButtonDown, super.mouseButtonDown);
                if (bab.isClicked(obb)) {
                    loginScreen = 1;
                    panelLoginNewUser.updateText(eab, "To create an account please enter");
                    panelLoginNewUser.updateText(fab, "all the requested details");
                    panelLoginNewUser.updateText(nab, "");
                    panelLoginNewUser.updateText(oab, "");
                    panelLoginNewUser.updateText(pab, "");
                    panelLoginNewUser.setFocus(nab);
                }
                if (bab.isClicked(pbb))
                    loginScreen = 0;
            }
        }
    }

    public void showLoginScreenStatus(String s, String s1) {
        if (loginScreen == 1) {
            panelLoginNewUser.updateText(eab, s);
            panelLoginNewUser.updateText(fab, s1);
        }
        if (loginScreen == 2) {
            panelLoginExistingUser.updateText(qab, s);
            panelLoginExistingUser.updateText(rab, s1);
        }
        loginUserDisp = s1;
        drawLoginScreens();
        resetTimings();
    }

    public void resetLoginVars() {
        loginScreen = 0;
        loggedIn = 0;
    }

    public void q() {
        resetGame();
    }

    public void resetGame() {
        combatStyle = 0;
        loginScreen = 0;
        loggedIn = 1;
        resetPMText();
        surface.blackScreen();
        surface.draw(graphics, 0, 11);
        for (int i1 = 0; i1 < objectCount; i1++) {
            scene.freeModel(objectModel[i1]);
            world.removeObject(objectX[i1], objectY[i1], objectId[i1]);
        }

        for (int j1 = 0; j1 < wallObjectCount; j1++) {
            scene.freeModel(wallObjectModel[j1]);
            world.removeWallObject(wallObjectX[j1], wallObjectY[j1], wallObjectDirection[j1], wallObjectId[j1]);
        }

        objectCount = 0;
        wallObjectCount = 0;
        groundItemCount = 0;
        playerCount = 0;
        for (int k1 = 0; k1 < maxPlayersServerCount; k1++)
            playersServer[k1] = null;

        for (int l1 = 0; l1 < maxPlayersCount; l1++)
            players[l1] = null;

        npcCount = 0;
        for (int i2 = 0; i2 < maxNpcsServerCount; i2++)
            npcsServer[i2] = null;

        for (int j2 = 0; j2 < maxNpcsCount; j2++)
            npcs[j2] = null;

        mouseButtonClick = 0;
        super.lastMouseButtonDown = 0;
        super.mouseButtonDown = 0;
    }

    public void hb() {
        String s = panelLoginNewUser.oc(nab);
        String s1 = panelLoginNewUser.oc(pab);
        loginScreen = 2;
        panelLoginExistingUser.updateText(qab, "Please enter your");
        panelLoginExistingUser.updateText(rab, "username and password");
        panelLoginExistingUser.updateText(controlLoginUser, s);
        panelLoginExistingUser.updateText(controlLoginPass, s1);
        drawLoginScreens();
        resetTimings();
        login(s, s1);
    }

    public void em(Panel g1, int i1) {
        g1.removeListEntry(i1, "Runescape rules of use", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "In order to keep runescape enjoyable for everyone there are a few", false);
        g1.removeListEntry(i1, "rules you must observe. You must agree to these rules to play", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "When using the built in chat facility you must not use any language", false);
        g1.removeListEntry(i1, "which may be considered by others to be offensive, racist or", false);
        g1.removeListEntry(i1, "obscene. You must not use the chat facility to harass, threaten or", false);
        g1.removeListEntry(i1, "deceive other players.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "You must not exploit any cheats or errors which you find in the", false);
        g1.removeListEntry(i1, "game, to give yourself an unfair advantage. Any exploits which you", false);
        g1.removeListEntry(i1, "find must be immediately reported to Jagex Software.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "You must not attempt to use other programs in conjunction with", false);
        g1.removeListEntry(i1, "RuneScape to give yourself an unfair advantage at the game. You", false);
        g1.removeListEntry(i1, "may not use any bots or macros to control your character for you.", false);
        g1.removeListEntry(i1, "When you are not playing the game you must log-out. You may not", false);
        g1.removeListEntry(i1, "circumvent any of our mechanisms designed to log out inactive", false);
        g1.removeListEntry(i1, "players automatically.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "You must not create multiple characters and use them to help each", false);
        g1.removeListEntry(i1, "other. You may create more than one character, but if you do, you", false);
        g1.removeListEntry(i1, "may not log in more than one at any time, and they must not interact", false);
        g1.removeListEntry(i1, "with each other in any way. If you wish to form an adventuring", false);
        g1.removeListEntry(i1, "party you should do so by cooperating with other players in the game", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "Terms and conditions", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "You agree that your character and account in runescape, is the", false);
        g1.removeListEntry(i1, "property of, and remains the property of Jagex Software. You may", false);
        g1.removeListEntry(i1, "not sell, transfer, or lend your character to anyone else. We may", false);
        g1.removeListEntry(i1, "delete or modify your character at any time for any reason.", false);
        g1.removeListEntry(i1, "For instance failing to follow the rules above may be cause for", false);
        g1.removeListEntry(i1, "IMMEDIATE DELETION of all your characters.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "You agree that for purposes such as preventing offensive language", false);
        g1.removeListEntry(i1, "we may automatically or manually censor the chat as we see fit,", false);
        g1.removeListEntry(i1, "and that we may record the chat to help us identify offenders.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "No Warranty is supplied with this Software. All implied warranties", false);
        g1.removeListEntry(i1, "conditions or terms are excluded to the fullest extent permitted by", false);
        g1.removeListEntry(i1, "law. We do not warrant that the operation of the Software will be", false);
        g1.removeListEntry(i1, "uninterrupted or error free. We accept no responsibility for any", false);
        g1.removeListEntry(i1, "consequential or indirect loss or damages. You use this software at", false);
        g1.removeListEntry(i1, "your own risk, and assume full responsibility for any and all real,", false);
        g1.removeListEntry(i1, "claimed, or supposed damages that may occur as a result of running", false);
        g1.removeListEntry(i1, "this software.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "We reserve all rights related to the runescape name, logo, web site,", false);
        g1.removeListEntry(i1, "and game. All materials associated with runescape are protected", false);
        g1.removeListEntry(i1, "by UK copyright laws and all other applicable national laws, and", false);
        g1.removeListEntry(i1, "may not be copied, reproduced, republished, uploaded, posted,", false);
        g1.removeListEntry(i1, "transmitted, or distributed in any way without our prior written", false);
        g1.removeListEntry(i1, "consent. We reserve the right to modify or remove this game at any", false);
        g1.removeListEntry(i1, "time. You agree that we may change this service, and these terms", false);
        g1.removeListEntry(i1, "and conditions, as and when we deem necessary.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "We accept no responsibility for the actions of other users of our", false);
        g1.removeListEntry(i1, "website. You acknowledge that it is inpractical for us to control", false);
        g1.removeListEntry(i1, "and monitor everything that users do in our game or post on our", false);
        g1.removeListEntry(i1, "message boards, and that we therefore cannot be held responsible", false);
        g1.removeListEntry(i1, "for any abusive or inappropriate content which appears on our site", false);
        g1.removeListEntry(i1, "as a result.", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "Occasionally we may accept ideas and game additions from the", false);
        g1.removeListEntry(i1, "players. You agree that by submitting material for inclusion in", false);
        g1.removeListEntry(i1, "runescape you are giving us a non-exclusive, perpetual, worldwide,", false);
        g1.removeListEntry(i1, "royalty-free license to use or modify the submission as we see", false);
        g1.removeListEntry(i1, "fit. You agree that you will not withdraw the submission or attempt", false);
        g1.removeListEntry(i1, "to make a charge for its use. Furthermore you warrant that you", false);
        g1.removeListEntry(i1, "are the exclusive copyright holder of the submission, and that the", false);
        g1.removeListEntry(i1, "submission in no way violates any other person or entity's rights", false);
        g1.removeListEntry(i1, "", false);
        g1.removeListEntry(i1, "These Terms shall be governed by the laws of England, and the", false);
        g1.removeListEntry(i1, "courts of England shall have exclusive jurisdiction in all matters", false);
        g1.removeListEntry(i1, "arising.", false);
    }

    public void handleGameInput() {
        checkConnection();
        if (super.lastMouseAction > 4500 && combatTimeout == 0) {
            closeConnection();
            return;
        }
        if (localPlayer.animationCurrent == 8 || localPlayer.animationCurrent == 9)
            combatTimeout = 500;
        if (combatTimeout > 0)
            combatTimeout--;
        for (int i1 = 0; i1 < playerCount; i1++) {
            Character character = players[i1];
            int k1 = (character.waypointCurrent + 1) % 10;
            if (character.movingStep != k1) {
                int j2 = -1;
                int k4 = character.movingStep;
                int k5;
                if (k4 < k1)
                    k5 = k1 - k4;
                else
                    k5 = (10 + k1) - k4;
                int j6 = 4;
                if (k5 > 2)
                    j6 = (k5 - 1) * 4;
                if (character.waypointsX[k4] - character.currentX > magicLoc * 3 || character.waypointsY[k4] - character.currentY > magicLoc * 3 || character.waypointsX[k4] - character.currentX < -magicLoc * 3 || character.waypointsY[k4] - character.currentY < -magicLoc * 3 || k5 > 8) {
                    character.currentX = character.waypointsX[k4];
                    character.currentY = character.waypointsY[k4];
                } else {
                    if (character.currentX < character.waypointsX[k4]) {
                        character.currentX += j6;
                        character.stepCount++;
                        j2 = 2;
                    } else if (character.currentX > character.waypointsX[k4]) {
                        character.currentX -= j6;
                        character.stepCount++;
                        j2 = 6;
                    }
                    if (character.currentX - character.waypointsX[k4] < j6 && character.currentX - character.waypointsX[k4] > -j6)
                        character.currentX = character.waypointsX[k4];
                    if (character.currentY < character.waypointsY[k4]) {
                        character.currentY += j6;
                        character.stepCount++;
                        if (j2 == -1)
                            j2 = 4;
                        else if (j2 == 2)
                            j2 = 3;
                        else
                            j2 = 5;
                    } else if (character.currentY > character.waypointsY[k4]) {
                        character.currentY -= j6;
                        character.stepCount++;
                        if (j2 == -1)
                            j2 = 0;
                        else if (j2 == 2)
                            j2 = 1;
                        else
                            j2 = 7;
                    }
                    if (character.currentY - character.waypointsY[k4] < j6 && character.currentY - character.waypointsY[k4] > -j6)
                        character.currentY = character.waypointsY[k4];
                }
                if (j2 != -1)
                    character.animationCurrent = j2;
                if (character.currentX == character.waypointsX[k4] && character.currentY == character.waypointsY[k4])
                    character.movingStep = (k4 + 1) % 10;
            } else {
                character.animationCurrent = character.animationNext;
            }
            if (character.messageTimeout > 0)
                character.messageTimeout--;
            if (character.bubbleTimeout > 0)
                character.bubbleTimeout--;
            if (character.combatTimer > 0)
                character.combatTimer--;
            if (deathScreenTimeout > 0) {
                deathScreenTimeout--;
                if (deathScreenTimeout == 0)
                    showMessage("You have been granted another life. Be more careful this time!", 3);
                if (deathScreenTimeout == 0)
                    showMessage("You retain your skills. Unless you attacked another player recently,", 3);
                if (deathScreenTimeout == 0)
                    showMessage("you also keep your best 3 items. Everything else lands where you died.", 3);
            }
        }

        for (int j1 = 0; j1 < npcCount; j1++) {
            Character npc = npcs[j1];
            int k2 = (npc.waypointCurrent + 1) % 10;
            if (npc.movingStep != k2) {
                int l4 = -1;
                int l5 = npc.movingStep;
                int k6;
                if (l5 < k2)
                    k6 = k2 - l5;
                else
                    k6 = (10 + k2) - l5;
                int l6 = 4;
                if (k6 > 2)
                    l6 = (k6 - 1) * 4;
                if (npc.waypointsX[l5] - npc.currentX > magicLoc * 3 || npc.waypointsY[l5] - npc.currentY > magicLoc * 3 || npc.waypointsX[l5] - npc.currentX < -magicLoc * 3 || npc.waypointsY[l5] - npc.currentY < -magicLoc * 3 || k6 > 8) {
                    npc.currentX = npc.waypointsX[l5];
                    npc.currentY = npc.waypointsY[l5];
                } else {
                    if (npc.currentX < npc.waypointsX[l5]) {
                        npc.currentX += l6;
                        npc.stepCount++;
                        l4 = 2;
                    } else if (npc.currentX > npc.waypointsX[l5]) {
                        npc.currentX -= l6;
                        npc.stepCount++;
                        l4 = 6;
                    }
                    if (npc.currentX - npc.waypointsX[l5] < l6 && npc.currentX - npc.waypointsX[l5] > -l6)
                        npc.currentX = npc.waypointsX[l5];
                    if (npc.currentY < npc.waypointsY[l5]) {
                        npc.currentY += l6;
                        npc.stepCount++;
                        if (l4 == -1)
                            l4 = 4;
                        else if (l4 == 2)
                            l4 = 3;
                        else
                            l4 = 5;
                    } else if (npc.currentY > npc.waypointsY[l5]) {
                        npc.currentY -= l6;
                        npc.stepCount++;
                        if (l4 == -1)
                            l4 = 0;
                        else if (l4 == 2)
                            l4 = 1;
                        else
                            l4 = 7;
                    }
                    if (npc.currentY - npc.waypointsY[l5] < l6 && npc.currentY - npc.waypointsY[l5] > -l6)
                        npc.currentY = npc.waypointsY[l5];
                }
                if (l4 != -1)
                    npc.animationCurrent = l4;
                if (npc.currentX == npc.waypointsX[l5] && npc.currentY == npc.waypointsY[l5])
                    npc.movingStep = (l5 + 1) % 10;
            } else {
                npc.animationCurrent = npc.animationNext;
            }
            if (npc.messageTimeout > 0)
                npc.messageTimeout--;
            if (npc.bubbleTimeout > 0)
                npc.bubbleTimeout--;
            if (npc.combatTimer > 0)
                npc.combatTimer--;
        }

        for (int i2 = 0; i2 < playerCount; i2++) {
            Character l3 = players[i2];
            if (l3.projectileRange > 0)
                l3.projectileRange--;
        }

        if (cameraAutoAngleDebug) {
            if (cameraAutoRotatePlayerX - localPlayer.currentX < -500 || cameraAutoRotatePlayerX - localPlayer.currentX > 500 || cameraAutoRotatePlayerY - localPlayer.currentY < -500 || cameraAutoRotatePlayerY - localPlayer.currentY > 500) {
                cameraAutoRotatePlayerX = localPlayer.currentX;
                cameraAutoRotatePlayerY = localPlayer.currentY;
            }
        } else {
            if (cameraAutoRotatePlayerX - localPlayer.currentX < -500 || cameraAutoRotatePlayerX - localPlayer.currentX > 500 || cameraAutoRotatePlayerY - localPlayer.currentY < -500 || cameraAutoRotatePlayerY - localPlayer.currentY > 500) {
                cameraAutoRotatePlayerX = localPlayer.currentX;
                cameraAutoRotatePlayerY = localPlayer.currentY;
            }
            if (cameraAutoRotatePlayerX != localPlayer.currentX)
                cameraAutoRotatePlayerX += (localPlayer.currentX - cameraAutoRotatePlayerX) / (16 + (cameraZoom - 500) / 15);
            if (cameraAutoRotatePlayerY != localPlayer.currentY)
                cameraAutoRotatePlayerY += (localPlayer.currentY - cameraAutoRotatePlayerY) / (16 + (cameraZoom - 500) / 15);
            if (optionCameraModeAuto) {
                int i3 = cameraAngle * 32;
                int i5 = i3 - cameraRotation;
                byte byte0 = 1;
                if (i5 != 0) {
                    ru++;
                    if (i5 > 128) {
                        byte0 = -1;
                        i5 = 256 - i5;
                    } else if (i5 > 0)
                        byte0 = 1;
                    else if (i5 < -128) {
                        byte0 = 1;
                        i5 = 256 + i5;
                    } else if (i5 < 0) {
                        byte0 = -1;
                        i5 = -i5;
                    }
                    cameraRotation += ((ru * i5 + 255) / 256) * byte0;
                    cameraRotation &= 0xff;
                } else {
                    ru = 0;
                }
            }
        }
        if (showAppearanceChange) {
            handleAppearancePanelControls();
            return;
        }
        if (super.mouseY > gameHeight - 4) {
            if (super.mouseX > 15 && super.mouseX < 96 && super.lastMouseButtonDown == 1)
                messageTabsSelected = 0;
            if (super.mouseX > 110 && super.mouseX < 194 && super.lastMouseButtonDown == 1) {
                messageTabsSelected = 1;
                panelMessageTabs.controlFlashText[controlTextListChat] = 0xf423f;
            }
            if (super.mouseX > 215 && super.mouseX < 295 && super.lastMouseButtonDown == 1) {
                messageTabsSelected = 2;
                panelMessageTabs.controlFlashText[controlTextListQuest] = 0xf423f;
            }
            if (super.mouseX > 315 && super.mouseX < 395 && super.lastMouseButtonDown == 1) {
                messageTabsSelected = 3;
                panelMessageTabs.controlFlashText[controlTextListPrivate] = 0xf423f;
            }
            super.lastMouseButtonDown = 0;
            super.mouseButtonDown = 0;
        }
        panelMessageTabs.handleMouse(super.mouseX, super.mouseY, super.lastMouseButtonDown, super.mouseButtonDown);
        if (messageTabsSelected > 0 && super.mouseX >= 494 && super.mouseY >= gameHeight - 66)
            super.lastMouseButtonDown = 0;
        if (panelMessageTabs.isClicked(controlTextListAll)) {
            String s = panelMessageTabs.oc(controlTextListAll);
            panelMessageTabs.updateText(controlTextListAll, "");
            if (s.equalsIgnoreCase("simlostcon99"))
                super.clientStream.close();
            else if (!sendCommandString(s)) {
                localPlayer.messageTimeout = 150;
                localPlayer.message = s;
                showMessage(localPlayer.name + ": " + s, 2);
            }
        }
        if (messageTabsSelected == 0) {
            for (int j3 = 0; j3 < maxMessagesCount; j3++)
                if (messageHistoryTimeout[j3] > 0)
                    messageHistoryTimeout[j3]--;

        }
        if (deathScreenTimeout != 0)
            super.lastMouseButtonDown = 0;
        if (showDialogTrade) {
            if (super.mouseButtonDown != 0)
                mouseButtonDownTime++;
            else
                mouseButtonDownTime = 0;
            if (mouseButtonDownTime > 300)
                mouseButtonItemCountIncrement += 50;
            else if (mouseButtonDownTime > 150)
                mouseButtonItemCountIncrement += 5;
            else if (mouseButtonDownTime > 50)
                mouseButtonItemCountIncrement++;
            else if (mouseButtonDownTime > 20 && (mouseButtonDownTime & 5) == 0)
                mouseButtonItemCountIncrement++;
        } else {
            mouseButtonDownTime = 0;
            mouseButtonItemCountIncrement = 0;
        }
        if (super.lastMouseButtonDown == 1)
            mouseButtonClick = 1;
        else if (super.lastMouseButtonDown == 2)
            mouseButtonClick = 2;
        scene.ph(super.mouseX, super.mouseY);
        super.lastMouseButtonDown = 0;
        if (optionCameraModeAuto) {
            if (ru == 0 || cameraAutoAngleDebug) {
                if (super.keyLeft) {
                    cameraAngle = cameraAngle + 1 & 7;
                    super.keyLeft = false;
                    if (!fogOfWar) {
                        if ((cameraAngle & 1) == 0)
                            cameraAngle = cameraAngle + 1 & 7;
                        for (int k3 = 0; k3 < 8; k3++) {
                            if (isValidCameraAngle(cameraAngle))
                                break;
                            cameraAngle = cameraAngle + 1 & 7;
                        }

                    }
                }
                if (super.keyRight) {
                    cameraAngle = cameraAngle + 7 & 7;
                    super.keyRight = false;
                    if (!fogOfWar) {
                        if ((cameraAngle & 1) == 0)
                            cameraAngle = cameraAngle + 7 & 7;
                        for (int i4 = 0; i4 < 8; i4++) {
                            if (isValidCameraAngle(cameraAngle))
                                break;
                            cameraAngle = cameraAngle + 7 & 7;
                        }

                    }
                }
            }
        } else if (super.keyLeft)
            cameraRotation = cameraRotation + 2 & 0xff;
        else if (super.keyRight)
            cameraRotation = cameraRotation - 2 & 0xff;
        if (fogOfWar && cameraZoom > 550)
            cameraZoom -= 4;
        else if (!fogOfWar && cameraZoom < 750)
            cameraZoom += 4;
        if (mouseClickXStep > 0)
            mouseClickXStep--;
        else if (mouseClickXStep < 0)
            mouseClickXStep++;
        scene.ai(17);
        objectAnimationCount++;
        if (objectAnimationCount > 5) {
            objectAnimationCount = 0;
            objectAnimationNumberTorch = objectAnimationNumberTorch + 1 & 3;
            objectAnimationNumberFireLightningSpell = (objectAnimationNumberFireLightningSpell + 1) % 3;
        }
        for (int j4 = 0; j4 < objectCount; j4++) {
            int j5 = objectX[j4];
            int i6 = objectY[j4];
            if (j5 >= 0 && i6 >= 0 && j5 < 96 && i6 < 96 && objectId[j4] == 74)
                objectModel[j4].pe(1, 0, 0);
        }

    }

    public void showMessage(String s, int i1) {
        if (i1 == 2 || i1 == 4 || i1 == 6) {
            for (; s.length() > 5 && s.charAt(0) == '@' && s.charAt(4) == '@'; s = s.substring(5)) ;
            int j1 = s.indexOf(":");
            if (j1 != -1) {
                String s1 = s.substring(0, j1);
                long l1 = Utility.username2hash(s1);
                for (int i2 = 0; i2 < super.ignoreListCount; i2++)
                    if (super.ignoreList[i2] == l1)
                        return;

            }
        }
        if (i1 == 2)
            s = "@yel@" + s;
        if (i1 == 3 || i1 == 4)
            s = "@whi@" + s;
        if (i1 == 6)
            s = "@cya@" + s;
        if (messageTabsSelected != 0) {
            if (i1 == 4 || i1 == 3)
                messageTabFlashAll = 200;
            if (i1 == 2 && messageTabsSelected != 1)
                messageTabFlashHistory = 200;
            if (i1 == 5 && messageTabsSelected != 2)
                messageTabFlashQuest = 200;
            if (i1 == 6 && messageTabsSelected != 3)
                messageTabFlashPrivate = 200;
            if (i1 == 3 && messageTabsSelected != 0)
                messageTabsSelected = 0;
            if (i1 == 6 && messageTabsSelected != 3 && messageTabsSelected != 0)
                messageTabsSelected = 3;
        }
        for (int k1 = maxMessagesCount - 1; k1 > 0; k1--) {
            messageHistory[k1] = messageHistory[k1 - 1];
            messageHistoryTimeout[k1] = messageHistoryTimeout[k1 - 1];
        }

        messageHistory[0] = s;
        messageHistoryTimeout[0] = 300;
        if (i1 == 2)
            if (panelMessageTabs.controlFlashText[controlTextListChat] == panelMessageTabs.ve[controlTextListChat] - 4)
                panelMessageTabs.removeListEntry(controlTextListChat, s, true);
            else
                panelMessageTabs.removeListEntry(controlTextListChat, s, false);
        if (i1 == 5)
            if (panelMessageTabs.controlFlashText[controlTextListQuest] == panelMessageTabs.ve[controlTextListQuest] - 4)
                panelMessageTabs.removeListEntry(controlTextListQuest, s, true);
            else
                panelMessageTabs.removeListEntry(controlTextListQuest, s, false);
        if (i1 == 6) {
            if (panelMessageTabs.controlFlashText[controlTextListPrivate] == panelMessageTabs.ve[controlTextListPrivate] - 4) {
                panelMessageTabs.removeListEntry(controlTextListPrivate, s, true);
                return;
            }
            panelMessageTabs.removeListEntry(controlTextListPrivate, s, false);
        }
    }

    public void showServerMessage(String s) {
        if (s.equals("@sys@k")) {
            closeConnection();
            return;
        }
        if (s.startsWith("@cha@")) {
            showMessage(s, 2);
            return;
        }
        if (s.startsWith("@bor@")) {
            showMessage(s, 4);
            return;
        }
        if (s.startsWith("@que@")) {
            showMessage("@whi@" + s, 5);
            return;
        }
        if (s.startsWith("@pri@")) {
            showMessage(s, 6);
            return;
        } else {
            showMessage(s, 3);
            return;
        }
    }

    public void handleIncomingPacket(int opcode, int psize, byte pdata[]) {
        try {
            if (opcode == 230) {
                int verConfig = pdata[1] & 0xff;
                int verMaps = pdata[2] & 0xff;
                int verMedia = pdata[3] & 0xff;
                int verModels = pdata[4] & 0xff;
                int verTextures = pdata[5] & 0xff;
                int verEntity = pdata[6] & 0xff;
                if (verConfig > Version.CONFIG || verMaps > Version.MAPS || verMedia > Version.MEDIA || verModels > Version.MODELS || verTextures > Version.TEXTURES || verEntity > Version.ENTITY) {
                    closeConnection();
                    super.up = "Runescape has been updated. Getting latest files";
                    super.loadingStep = 2;
                    showLoadingProgress(0, "Loading");
                    if (verConfig > Version.CONFIG) {
                        Version.CONFIG = verConfig;
                        loadGameConfig();
                    }
                    if (verMedia > Version.MEDIA) {
                        Version.MEDIA = verMedia;
                        loadMedia();
                    }
                    if (verEntity > Version.ENTITY) {
                        Version.ENTITY = verEntity;
                        loadEntities(true);
                    } else {
                        loadEntities(false);
                    }
                    if (verTextures > Version.TEXTURES) {
                        Version.TEXTURES = verTextures;
                        loadTextures();
                    }
                    if (verModels > Version.MODELS) {
                        Version.MODELS = verModels;
                        loadModels();
                    }
                    if (verMaps > Version.MAPS) {
                        Version.MAPS = verMaps;
                        loadMaps();
                    }
                    world.playerAlive = false;
                    lastPlaneIndex = -1;
                    super.loadingStep = 0;
                    login(loginUser, loginPass);
                    return;
                }
            }
            if (opcode == 232) {
                int size = (psize - 1) / 2;
                for (int i = 0; i < size; i++) {
                    int serverIndex = Utility.getUnsignedShort(pdata, 1 + i * 2);
                    for (int j = 0; j < playerCount; j++) {
                        Character player = players[j];
                        if (player == null || player.serverIndex == serverIndex) {
                            playerCount--;
                            for (int k = j; k < playerCount; k++)
                                players[k] = players[k + 1];

                            j--;
                        }
                    }

                }

            }
            if (opcode == 231) {
                int i2 = (psize - 1) / 2;
                for (int l6 = 0; l6 < i2; l6++) {
                    int k11 = Utility.getUnsignedShort(pdata, 1 + l6 * 2);
                    for (int i16 = 0; i16 < npcCount; i16++) {
                        Character l21 = npcs[i16];
                        if (l21 == null || l21.serverIndex == k11) {
                            npcCount--;
                            for (int i24 = i16; i24 < npcCount; i24++)
                                npcs[i24] = npcs[i24 + 1];

                            i16--;
                        }
                    }

                }

            }
            if (opcode == 255) {
                int j2 = 1;
                int i7 = 0;
                int l31 = 0;
                iu = true;
                while (j2 < psize) {
                    int l11;
                    int j16;
                    int i20;
                    int j24;
                    boolean flag;
                    if (i7 == 0) {
                        l11 = Utility.getUnsignedShort(pdata, 1);
                        regionX = Utility.getUnsignedShort(pdata, 3);
                        regionY = Utility.getUnsignedShort(pdata, 5);
                        j24 = Utility.getUnsignedByte(pdata[7]);
                        j2 += 7;
                        loadNextRegion(regionX, regionY);
                        regionX -= areaX;
                        regionY -= areaY;
                        j16 = regionX * magicLoc + 64;
                        i20 = regionY * magicLoc + 64;
                        if (j24 >= 128) {
                            j24 -= 128;
                            flag = true;
                        } else {
                            flag = false;
                        }
                    } else {
                        int k38 = Utility.getUnsignedShort(pdata, j2);
                        int k34;
                        int i37;
                        if ((k38 & 0xfc00) == 64512) {
                            int k39 = Utility.getUnsignedShort(pdata, j2 + 2);
                            j2 += 4;
                            k34 = k38 >> 5 & 0x1f;
                            if (k34 > 15)
                                k34 -= 32;
                            i37 = k38 & 0x1f;
                            if (i37 > 15)
                                i37 -= 32;
                            l11 = k39 >> 4 & 0xfff;
                            j24 = k39 & 0xf;
                            if (j24 == 15) {
                                j24 = 0;
                                flag = true;
                            } else {
                                flag = false;
                            }
                        } else {
                            int l39 = Utility.getUnsignedByte(pdata[j2 + 2]);
                            j2 += 3;
                            l11 = k38 >> 6 & 0x3ff;
                            k34 = k38 >> 1 & 0x1f;
                            if (k34 > 15)
                                k34 -= 32;
                            i37 = (k38 << 4 & 0x10) + (l39 >> 4 & 0xf);
                            if (i37 > 15)
                                i37 -= 32;
                            j24 = l39 & 0xf;
                            if (j24 == 15) {
                                j24 = 0;
                                flag = true;
                            } else {
                                flag = false;
                            }
                        }
                        j16 = (regionX + k34) * magicLoc + 64;
                        i20 = (regionY + i37) * magicLoc + 64;
                    }
                    if (playersServer[l11] == null) {
                        playersServer[l11] = new Character();
                        playersServer[l11].serverIndex = l11;
                        playersServer[l11].br = 0;
                    }
                    Character l34 = playersServer[l11];
                    if (i7 == 0)
                        localPlayer = l34;
                    boolean flag3 = false;
                    for (int l38 = 0; l38 < playerCount; l38++) {
                        if (players[l38] != l34)
                            continue;
                        flag3 = true;
                        break;
                    }

                    if (!flag3)
                        players[playerCount++] = playersServer[l11];
                    if (flag3 && iu) {
                        l34.animationNext = j24;
                        int i40 = l34.waypointCurrent;
                        if (j16 != l34.waypointsX[i40] || i20 != l34.waypointsY[i40]) {
                            l34.waypointCurrent = i40 = (i40 + 1) % 10;
                            l34.waypointsX[i40] = j16;
                            l34.waypointsY[i40] = i20;
                        }
                    } else {
                        l34.serverIndex = l11;
                        l34.movingStep = 0;
                        l34.waypointCurrent = 0;
                        l34.waypointsX[0] = l34.currentX = j16;
                        l34.waypointsY[0] = l34.currentY = i20;
                        l34.animationNext = l34.animationCurrent = j24;
                        l34.stepCount = 0;
                        if (!flag && !flag3)
                            wy[l31++] = l11;
                    }
                    i7++;
                }
                if (l31 > 0) {
                    super.clientStream.newPacket(254);
                    super.clientStream.writeShort(l31);
                    for (int i35 = 0; i35 < l31; i35++) {
                        Character l37 = playersServer[wy[i35]];
                        super.clientStream.writeShort(l37.serverIndex);
                        super.clientStream.writeShort(l37.br);
                    }

                    super.clientStream.flushPacket();
                    return;
                }
            } else {
                if (opcode == 254) {
                    for (int k2 = 1; k2 < psize; )
                        if (Utility.getUnsignedByte(pdata[k2]) == 255) {
                            int j7 = 0;
                            int i12 = regionX + pdata[k2 + 1] >> 3;
                            int k16 = regionY + pdata[k2 + 2] >> 3;
                            k2 += 3;
                            for (int j20 = 0; j20 < groundItemCount; j20++) {
                                int k24 = (groundItemX[j20] >> 3) - i12;
                                int l27 = (groundItemY[j20] >> 3) - k16;
                                if (k24 != 0 || l27 != 0) {
                                    if (j20 != j7) {
                                        groundItemX[j7] = groundItemX[j20];
                                        groundItemY[j7] = groundItemY[j20];
                                        groundItemId[j7] = groundItemId[j20];
                                        groundItemZ[j7] = groundItemZ[j20];
                                    }
                                    j7++;
                                }
                            }

                            groundItemCount = j7;
                        } else {
                            int k7 = Utility.getUnsignedShort(pdata, k2);
                            k2 += 2;
                            int j12 = regionX + pdata[k2++];
                            int l16 = regionY + pdata[k2++];
                            if ((k7 & 0x8000) == 0) {
                                groundItemX[groundItemCount] = j12;
                                groundItemY[groundItemCount] = l16;
                                groundItemId[groundItemCount] = k7;
                                groundItemZ[groundItemCount] = 0;
                                for (int k20 = 0; k20 < objectCount; k20++) {
                                    if (objectX[k20] != j12 || objectY[k20] != l16)
                                        continue;
                                    groundItemZ[groundItemCount] = GameData.locationVar10[objectId[k20]];
                                    break;
                                }

                                groundItemCount++;
                            } else {
                                k7 &= 0x7fff;
                                int i21 = 0;
                                for (int i25 = 0; i25 < groundItemCount; i25++)
                                    if (groundItemX[i25] != j12 || groundItemY[i25] != l16 || groundItemId[i25] != k7) {
                                        if (i25 != i21) {
                                            groundItemX[i21] = groundItemX[i25];
                                            groundItemY[i21] = groundItemY[i25];
                                            groundItemId[i21] = groundItemId[i25];
                                            groundItemZ[i21] = groundItemZ[i25];
                                        }
                                        i21++;
                                    } else {
                                        k7 = -123;
                                    }

                                groundItemCount = i21;
                            }
                        }

                    return;
                }
                if (opcode == 253) {
                    for (int l2 = 1; l2 < psize; )
                        if (Utility.getUnsignedByte(pdata[l2]) == 255) {
                            int l7 = 0;
                            int k12 = regionX + pdata[l2 + 1] >> 3;
                            int i17 = regionY + pdata[l2 + 2] >> 3;
                            l2 += 3;
                            for (int j21 = 0; j21 < objectCount; j21++) {
                                int j25 = (objectX[j21] >> 3) - k12;
                                int i28 = (objectY[j21] >> 3) - i17;
                                if (j25 != 0 || i28 != 0) {
                                    if (j21 != l7) {
                                        objectModel[l7] = objectModel[j21];
                                        objectModel[l7].key = l7;
                                        objectX[l7] = objectX[j21];
                                        objectY[l7] = objectY[j21];
                                        objectId[l7] = objectId[j21];
                                        objectDirection[l7] = objectDirection[j21];
                                    }
                                    l7++;
                                } else {
                                    scene.freeModel(objectModel[j21]);
                                    world.removeObject(objectX[j21], objectY[j21], objectId[j21]);
                                }
                            }

                            objectCount = l7;
                        } else {
                            int i8 = Utility.getUnsignedShort(pdata, l2);
                            l2 += 2;
                            int l12 = regionX + pdata[l2++];
                            int j17 = regionY + pdata[l2++];
                            int k21 = 0;
                            for (int k25 = 0; k25 < objectCount; k25++)
                                if (objectX[k25] != l12 || objectY[k25] != j17) {
                                    if (k25 != k21) {
                                        objectModel[k21] = objectModel[k25];
                                        objectModel[k21].key = k21;
                                        objectX[k21] = objectX[k25];
                                        objectY[k21] = objectY[k25];
                                        objectId[k21] = objectId[k25];
                                        objectDirection[k21] = objectDirection[k25];
                                    }
                                    k21++;
                                } else {
                                    scene.freeModel(objectModel[k25]);
                                    world.removeObject(objectX[k25], objectY[k25], objectId[k25]);
                                }

                            objectCount = k21;
                            if (i8 != 60000) {
                                int j28 = world.en(l12, j17);
                                int i32;
                                int j35;
                                if (j28 == 0 || j28 == 4) {
                                    i32 = GameData.locationVar4[i8];
                                    j35 = GameData.locationVar5[i8];
                                } else {
                                    j35 = GameData.locationVar4[i8];
                                    i32 = GameData.locationVar5[i8];
                                }
                                int j37 = ((l12 + l12 + i32) * magicLoc) / 2;
                                int i39 = ((j17 + j17 + j35) * magicLoc) / 2;
                                int j40 = GameData.locationVar3[i8];
                                GameModel h2 = gameModels[j40].copy();
                                scene.addModel(h2);
                                h2.key = objectCount;
                                h2.pe(0, j28 * 32, 0);
                                h2.translate(j37, -world.getElevation(j37, i39), i39);
                                h2.setLight(true, 48, 48, -50, -10, -50);
                                world.qn(l12, j17, i8);
                                if (i8 == 74)
                                    h2.translate(0, -480, 0);
                                objectX[objectCount] = l12;
                                objectY[objectCount] = j17;
                                objectId[objectCount] = i8;
                                objectDirection[objectCount] = j28;
                                objectModel[objectCount++] = h2;
                            }
                        }

                    return;
                }
                if (opcode == 252) {
                    inventoryItemsCount = (psize - 1) / 4;
                    for (int i3 = 0; i3 < inventoryItemsCount; i3++) {
                        inventoryItemId[i3] = Utility.getUnsignedShort(pdata, i3 * 4 + 1);
                        if (inventoryItemId[i3] >= 32768) {
                            inventoryItemId[i3] -= 32768;
                            inventoryEquipped[i3] = 1;
                        } else {
                            inventoryEquipped[i3] = 0;
                        }
                        inventoryItemStackCount[i3] = Utility.getUnsignedShort(pdata, i3 * 4 + 3);
                    }

                    return;
                }
                if (opcode == 251) {
                    int j3 = Utility.getUnsignedShort(pdata, 1);
                    int j8 = 3;
                    for (int i13 = 0; i13 < j3; i13++) {
                        int k17 = Utility.getUnsignedShort(pdata, j8);
                        j8 += 2;
                        Character l22 = playersServer[k17];
                        if (l22 != null) {
                            l22.br = Utility.getUnsignedShort(pdata, j8);
                            j8 += 2;
                            l22.hash = Utility.getUnsignedLong(pdata, j8);
                            j8 += 8;
                            l22.name = Utility.hash2username(l22.hash);
                            int l25 = Utility.getUnsignedByte(pdata[j8]);
                            j8++;
                            for (int k28 = 0; k28 < l25; k28++) {
                                l22.mr[k28] = Utility.getUnsignedByte(pdata[j8]);
                                j8++;
                            }

                            for (int j32 = l25; j32 < 12; j32++)
                                l22.mr[j32] = 0;

                            l22.xr = pdata[j8++] & 0xff;
                            l22.yr = pdata[j8++] & 0xff;
                            l22.colourBottom = pdata[j8++] & 0xff;
                            l22.as = pdata[j8++] & 0xff;
                            l22.attackable = pdata[j8++] & 0xff;
                            l22.level = pdata[j8++] & 0xff;
                            l22.hs = pdata[j8++] & 0xff;
                        } else {
                            j8 += 14;
                            int i26 = Utility.getUnsignedByte(pdata[j8]);
                            j8 += i26 + 1;
                        }
                    }

                    return;
                }
                if (opcode == 250) {
                    int k3 = Utility.getUnsignedShort(pdata, 1);
                    int k8 = 3;
                    for (int j13 = 0; j13 < k3; j13++) {
                        int l17 = Utility.getUnsignedShort(pdata, k8);
                        k8 += 2;
                        Character l23 = playersServer[l17];
                        byte byte4 = pdata[k8];
                        k8++;
                        if (byte4 == 0) {
                            int l28 = Utility.getUnsignedShort(pdata, k8);
                            k8 += 2;
                            if (l23 != null) {
                                l23.bubbleTimeout = 150;
                                l23.pr = l28;
                            }
                        } else if (byte4 == 1) {
                            byte byte5 = pdata[k8];
                            k8++;
                            if (l23 != null) {
                                String s1 = new String(pdata, k8, byte5);
                                if (s1.startsWith("@que@")) {
                                    l23.messageTimeout = 150;
                                    l23.message = s1;
                                    if (l23 == localPlayer)
                                        showMessage("@yel@" + l23.name + ": " + l23.message, 5);
                                } else if (l23 != localPlayer) {
                                    boolean flag2 = false;
                                    for (int k37 = 0; k37 < super.ignoreListCount; k37++)
                                        if (super.ignoreList[k37] == l23.hash)
                                            flag2 = true;

                                    if (!flag2) {
                                        s1 = Utility.filterString(s1, true);
                                        l23.messageTimeout = 150;
                                        l23.message = s1;
                                        showMessage(l23.name + ": " + l23.message, 2);
                                    }
                                }
                            }
                            k8 += byte5;
                        } else if (byte4 == 2) {
                            int i29 = Utility.getUnsignedByte(pdata[k8]);
                            k8++;
                            int k32 = Utility.getUnsignedByte(pdata[k8]);
                            k8++;
                            int k35 = Utility.getUnsignedByte(pdata[k8]);
                            k8++;
                            if (l23 != null) {
                                l23.rr = i29;
                                l23.healthCurrent = k32;
                                l23.healthMax = k35;
                                l23.combatTimer = 200;
                                if (l23 == localPlayer) {
                                    playerStatCurrent[3] = k32;
                                    playerStatBase[3] = k35;
                                }
                            }
                        } else if (byte4 == 3) {
                            int j29 = Utility.getUnsignedShort(pdata, k8);
                            k8 += 2;
                            int l32 = Utility.getUnsignedShort(pdata, k8);
                            k8 += 2;
                            if (l23 != null) {
                                l23.incomingProjectileSprite = j29;
                                l23.attackingNpcServerIndex = l32;
                                l23.attackingPlayerServerIndex = -1;
                                l23.projectileRange = vz;
                            }
                        } else if (byte4 == 4) {
                            int k29 = Utility.getUnsignedShort(pdata, k8);
                            k8 += 2;
                            int i33 = Utility.getUnsignedShort(pdata, k8);
                            k8 += 2;
                            if (l23 != null) {
                                l23.incomingProjectileSprite = k29;
                                l23.attackingPlayerServerIndex = i33;
                                l23.attackingNpcServerIndex = -1;
                                l23.projectileRange = vz;
                            }
                        }
                    }

                    return;
                }
                if (opcode == 249) {
                    for (int l3 = 1; l3 < psize; )
                        if (Utility.getUnsignedByte(pdata[l3]) == 255) {
                            int l8 = 0;
                            int k13 = regionX + pdata[l3 + 1] >> 3;
                            int i18 = regionY + pdata[l3 + 2] >> 3;
                            l3 += 3;
                            for (int i22 = 0; i22 < wallObjectCount; i22++) {
                                int j26 = (wallObjectX[i22] >> 3) - k13;
                                int l29 = (wallObjectY[i22] >> 3) - i18;
                                if (j26 != 0 || l29 != 0) {
                                    if (i22 != l8) {
                                        wallObjectModel[l8] = wallObjectModel[i22];
                                        wallObjectModel[l8].key = l8 + 10000;
                                        wallObjectX[l8] = wallObjectX[i22];
                                        wallObjectY[l8] = wallObjectY[i22];
                                        wallObjectDirection[l8] = wallObjectDirection[i22];
                                        wallObjectId[l8] = wallObjectId[i22];
                                    }
                                    l8++;
                                } else {
                                    scene.freeModel(wallObjectModel[i22]);
                                    world.removeWallObject(wallObjectX[i22], wallObjectY[i22], wallObjectDirection[i22], wallObjectId[i22]);
                                }
                            }

                            wallObjectCount = l8;
                        } else {
                            int i9 = Utility.getUnsignedShort(pdata, l3);
                            l3 += 2;
                            int l13 = regionX + pdata[l3++];
                            int j18 = regionY + pdata[l3++];
                            byte byte3 = pdata[l3++];
                            int k26 = 0;
                            for (int i30 = 0; i30 < wallObjectCount; i30++)
                                if (wallObjectX[i30] != l13 || wallObjectY[i30] != j18 || wallObjectDirection[i30] != byte3) {
                                    if (i30 != k26) {
                                        wallObjectModel[k26] = wallObjectModel[i30];
                                        wallObjectModel[k26].key = k26 + 10000;
                                        wallObjectX[k26] = wallObjectX[i30];
                                        wallObjectY[k26] = wallObjectY[i30];
                                        wallObjectDirection[k26] = wallObjectDirection[i30];
                                        wallObjectId[k26] = wallObjectId[i30];
                                    }
                                    k26++;
                                } else {
                                    scene.freeModel(wallObjectModel[i30]);
                                    world.removeWallObject(wallObjectX[i30], wallObjectY[i30], wallObjectDirection[i30], wallObjectId[i30]);
                                }

                            wallObjectCount = k26;
                            if (i9 != 65535) {
                                world.co(l13, j18, byte3, i9);
                                GameModel h1 = createModel(l13, j18, byte3, i9, wallObjectCount);
                                wallObjectModel[wallObjectCount] = h1;
                                wallObjectX[wallObjectCount] = l13;
                                wallObjectY[wallObjectCount] = j18;
                                wallObjectId[wallObjectCount] = i9;
                                wallObjectDirection[wallObjectCount++] = byte3;
                            }
                        }

                    return;
                }
                if (opcode == 248) {
                    int i4 = (psize - 1) / 4;
                    int j9 = 1;
                    for (int i14 = 0; i14 < i4; i14++) {
                        int k18 = Utility.getUnsignedShort(pdata, j9);
                        int j22 = Utility.getUnsignedByte(pdata[j9 + 2]);
                        int l26 = k18 >> 6 & 0x3ff;
                        int j30 = k18 >> 1 & 0x1f;
                        if (j30 > 15)
                            j30 -= 32;
                        int j33 = (k18 << 4 & 0x10) + (j22 >> 4 & 0xf);
                        if (j33 > 15)
                            j33 -= 32;
                        int l35 = j22 & 0xf;
                        int i38 = (regionX + j30) * magicLoc + 64;
                        int j39 = (regionY + j33) * magicLoc + 64;
                        int k40 = Utility.getUnsignedByte(pdata[j9 + 3]);
                        j9 += 4;
                        if (k40 >= GameData.npcCount)
                            k40 = 24;
                        if (npcsServer[l26] == null) {
                            npcsServer[l26] = new Character();
                            npcsServer[l26].serverIndex = l26;
                        }
                        Character l40 = npcsServer[l26];
                        boolean flag4 = false;
                        for (int i41 = 0; i41 < npcCount; i41++) {
                            if (npcs[i41] != l40)
                                continue;
                            flag4 = true;
                            break;
                        }

                        if (!flag4)
                            npcs[npcCount++] = npcsServer[l26];
                        if (flag4) {
                            l40.npcId = k40;
                            l40.animationNext = l35;
                            int j41 = l40.waypointCurrent;
                            if (i38 != l40.waypointsX[j41] || j39 != l40.waypointsY[j41]) {
                                l40.waypointCurrent = j41 = (j41 + 1) % 10;
                                l40.waypointsX[j41] = i38;
                                l40.waypointsY[j41] = j39;
                            }
                        } else {
                            l40.serverIndex = l26;
                            l40.movingStep = 0;
                            l40.waypointCurrent = 0;
                            l40.waypointsX[0] = l40.currentX = i38;
                            l40.waypointsY[0] = l40.currentY = j39;
                            l40.npcId = k40;
                            l40.animationNext = l40.animationCurrent = l35;
                            l40.stepCount = 0;
                        }
                    }

                    return;
                }
                if (opcode == 247) {
                    int j4 = Utility.getUnsignedShort(pdata, 1);
                    int k9 = 3;
                    for (int j14 = 0; j14 < j4; j14++) {
                        int l18 = Utility.getUnsignedShort(pdata, k9);
                        k9 += 2;
                        Character l24 = npcsServer[l18];
                        int i27 = Utility.getUnsignedByte(pdata[k9]);
                        k9++;
                        if (i27 == 1) {
                            int k30 = Utility.getUnsignedShort(pdata, k9);
                            k9 += 2;
                            byte byte6 = pdata[k9];
                            k9++;
                            if (l24 != null) {
                                String s2 = new String(pdata, k9, byte6);
                                l24.messageTimeout = 150;
                                l24.message = s2;
                                if (k30 == localPlayer.serverIndex)
                                    showMessage("@yel@" + GameData.npcVar1[l24.npcId][0] + ": " + l24.message, 5);
                            }
                            k9 += byte6;
                        } else if (i27 == 2) {
                            int l30 = Utility.getUnsignedByte(pdata[k9]);
                            k9++;
                            int k33 = Utility.getUnsignedByte(pdata[k9]);
                            k9++;
                            int i36 = Utility.getUnsignedByte(pdata[k9]);
                            k9++;
                            if (l24 != null) {
                                l24.rr = l30;
                                l24.healthCurrent = k33;
                                l24.healthMax = i36;
                                l24.combatTimer = 200;
                            }
                        }
                    }

                    return;
                }
                if (opcode == 246) {
                    showOptionMenu = true;
                    int k4 = Utility.getUnsignedByte(pdata[1]);
                    optionMenuCount = k4;
                    int l9 = 2;
                    for (int k14 = 0; k14 < k4; k14++) {
                        int i19 = Utility.getUnsignedByte(pdata[l9]);
                        l9++;
                        optionMenuEntry[k14] = new String(pdata, l9, i19);
                        l9 += i19;
                    }

                    return;
                }
                if (opcode == 245) {
                    showOptionMenu = false;
                    return;
                }
                if (opcode == 244) {
                    planeWidth = Utility.getUnsignedShort(pdata, 1);
                    planeHeight = Utility.getUnsignedShort(pdata, 3);
                    planeIndex = Utility.getUnsignedShort(pdata, 5);
                    uu = Utility.getUnsignedShort(pdata, 7);
                    planeHeight -= planeIndex * uu;
                    return;
                }
                if (opcode == 243) {
                    int l4 = 1;
                    for (int i10 = 0; i10 < 16; i10++)
                        playerStatCurrent[i10] = Utility.getUnsignedByte(pdata[l4++]);

                    for (int l14 = 0; l14 < 16; l14++)
                        playerStatBase[l14] = Utility.getUnsignedByte(pdata[l4++]);

                    playerQuestPoints = Utility.getUnsignedByte(pdata[l4++]);
                    return;
                }
                if (opcode == 242) {
                    for (int i5 = 0; i5 < 5; i5++)
                        playerStatEquipment[i5] = Utility.getUnsignedByte(pdata[1 + i5]);

                    return;
                }
                if (opcode == 241) {
                    deathScreenTimeout = 250;
                    vy += 10;
                    return;
                }
                if (opcode == 240) {
                    int j5 = (psize - 1) / 4;
                    for (int j10 = 0; j10 < j5; j10++) {
                        int i15 = regionX + Utility.getSignedShort(pdata, 1 + j10 * 4) >> 3;
                        int j19 = regionY + Utility.getSignedShort(pdata, 3 + j10 * 4) >> 3;
                        int k22 = 0;
                        for (int j27 = 0; j27 < groundItemCount; j27++) {
                            int i31 = (groundItemX[j27] >> 3) - i15;
                            int l33 = (groundItemY[j27] >> 3) - j19;
                            if (i31 != 0 || l33 != 0) {
                                if (j27 != k22) {
                                    groundItemX[k22] = groundItemX[j27];
                                    groundItemY[k22] = groundItemY[j27];
                                    groundItemId[k22] = groundItemId[j27];
                                    groundItemZ[k22] = groundItemZ[j27];
                                }
                                k22++;
                            }
                        }

                        groundItemCount = k22;
                        k22 = 0;
                        for (int j31 = 0; j31 < objectCount; j31++) {
                            int i34 = (objectX[j31] >> 3) - i15;
                            int j36 = (objectY[j31] >> 3) - j19;
                            if (i34 != 0 || j36 != 0) {
                                if (j31 != k22) {
                                    objectModel[k22] = objectModel[j31];
                                    objectModel[k22].key = k22;
                                    objectX[k22] = objectX[j31];
                                    objectY[k22] = objectY[j31];
                                    objectId[k22] = objectId[j31];
                                    objectDirection[k22] = objectDirection[j31];
                                }
                                k22++;
                            } else {
                                scene.freeModel(objectModel[j31]);
                                world.removeObject(objectX[j31], objectY[j31], objectId[j31]);
                            }
                        }

                        objectCount = k22;
                        k22 = 0;
                        for (int j34 = 0; j34 < wallObjectCount; j34++) {
                            int k36 = (wallObjectX[j34] >> 3) - i15;
                            int j38 = (wallObjectY[j34] >> 3) - j19;
                            if (k36 != 0 || j38 != 0) {
                                if (j34 != k22) {
                                    wallObjectModel[k22] = wallObjectModel[j34];
                                    wallObjectModel[k22].key = k22 + 10000;
                                    wallObjectX[k22] = wallObjectX[j34];
                                    wallObjectY[k22] = wallObjectY[j34];
                                    wallObjectDirection[k22] = wallObjectDirection[j34];
                                    wallObjectId[k22] = wallObjectId[j34];
                                }
                                k22++;
                            } else {
                                scene.freeModel(wallObjectModel[j34]);
                                world.removeWallObject(wallObjectX[j34], wallObjectY[j34], wallObjectDirection[j34], wallObjectId[j34]);
                            }
                        }

                        wallObjectCount = k22;
                    }

                    return;
                }
                if (opcode == 239) {
                    showAppearanceChange = true;
                    return;
                }
                if (opcode == 238) {
                    int k5 = Utility.getUnsignedShort(pdata, 1);
                    if (playersServer[k5] != null)
                        wx = playersServer[k5].name;
                    showDialogTrade = true;
                    tradeRecipientAccepted = false;
                    tradeAccepted = false;
                    tradeItemsCount = 0;
                    ay = 0;
                    return;
                }
                if (opcode == 237) {
                    showDialogTrade = false;
                    return;
                }
                if (opcode == 236) {
                    ay = pdata[1] & 0xff;
                    int l5 = 2;
                    for (int k10 = 0; k10 < ay; k10++) {
                        tradeRecipientItems[k10] = Utility.getUnsignedShort(pdata, l5);
                        l5 += 2;
                        cy[k10] = Utility.getUnsignedShort(pdata, l5);
                        l5 += 2;
                    }

                    tradeRecipientAccepted = false;
                    tradeAccepted = false;
                    return;
                }
                if (opcode == 235) {
                    byte byte0 = pdata[1];
                    if (byte0 == 1) {
                        tradeRecipientAccepted = true;
                        return;
                    } else {
                        tradeRecipientAccepted = false;
                        return;
                    }
                }
                if (opcode == 234) {
                    hy = true;
                    int i6 = 1;
                    int l10 = pdata[i6++] & 0xff;
                    byte byte2 = pdata[i6++];
                    shopSellPriceMod = pdata[i6++] & 0xff;
                    shopBuyPriceMod = pdata[i6++] & 0xff;
                    for (int k19 = 0; k19 < 40; k19++)
                        shopItem[k19] = -1;

                    for (int i23 = 0; i23 < l10; i23++) {
                        shopItem[i23] = Utility.getUnsignedShort(pdata, i6);
                        i6 += 2;
                        ly[i23] = Utility.getUnsignedShort(pdata, i6);
                        i6 += 2;
                        shopItemPrice[i23] = pdata[i6++];
                    }

                    if (byte2 == 1) {
                        int k27 = 39;
                        for (int k31 = 0; k31 < inventoryItemsCount; k31++) {
                            if (k27 < l10)
                                break;
                            boolean flag1 = false;
                            for (int l36 = 0; l36 < 40; l36++) {
                                if (shopItem[l36] != inventoryItemId[k31])
                                    continue;
                                flag1 = true;
                                break;
                            }

                            if (inventoryItemId[k31] == 10)
                                flag1 = true;
                            if (!flag1) {
                                shopItem[k27] = inventoryItemId[k31] & 0x7fff;
                                ly[k27] = 0;
                                shopItemPrice[k27] = 0;
                                k27--;
                            }
                        }

                    }
                    if (shopSelectedItemIndex >= 0 && shopSelectedItemIndex < 40 && shopItem[shopSelectedItemIndex] != shopSelectedIemType) {
                        shopSelectedItemIndex = -1;
                        shopSelectedIemType = -2;
                        return;
                    }
                } else {
                    if (opcode == 233) {
                        hy = false;
                        return;
                    }
                    if (opcode == 229) {
                        byte byte1 = pdata[1];
                        if (byte1 == 1) {
                            tradeAccepted = true;
                            return;
                        } else {
                            tradeAccepted = false;
                            return;
                        }
                    }
                    if (opcode == 228) {
                        System.out.println("Got config");
                        optionPlayerKiller = Utility.getUnsignedByte(pdata[1]) == 1;
                        optionCameraModeAuto = Utility.getUnsignedByte(pdata[2]) == 1;
                        et = Utility.getUnsignedByte(pdata[3]);
                        optionMouseButtonOne = Utility.getUnsignedByte(pdata[4]) == 1;
                    }
                }
            }
            return;
        } catch (RuntimeException runtimeexception) {
            if (mt < 3) {
                super.clientStream.newPacket(17);
                super.clientStream.writeString(runtimeexception.toString());
                finalisePacket();
                super.clientStream.newPacket(17);
                super.clientStream.writeString("ptype:" + opcode + " psize:" + psize);
                finalisePacket();
                super.clientStream.newPacket(17);
                super.clientStream.writeString("rx:" + regionX + " ry:" + regionY + " num3l:" + objectCount);
                finalisePacket();
                String s = "";
                for (int j15 = 0; j15 < 80 && j15 < psize; j15++)
                    s = s + pdata[j15] + " ";

                super.clientStream.newPacket(17);
                super.clientStream.writeString(s);
                finalisePacket();
                mt++;
            }
        }
    }

    public boolean isValidCameraAngle(int i1) {
        int j1 = localPlayer.currentX / 128;
        int k1 = localPlayer.currentY / 128;
        for (int l1 = 2; l1 >= 1; l1--) {
            if (i1 == 1 && ((world.objectAdjacency[j1][k1 - l1] & 0x80) == 128 || (world.objectAdjacency[j1 - l1][k1] & 0x80) == 128 || (world.objectAdjacency[j1 - l1][k1 - l1] & 0x80) == 128))
                return false;
            if (i1 == 3 && ((world.objectAdjacency[j1][k1 + l1] & 0x80) == 128 || (world.objectAdjacency[j1 - l1][k1] & 0x80) == 128 || (world.objectAdjacency[j1 - l1][k1 + l1] & 0x80) == 128))
                return false;
            if (i1 == 5 && ((world.objectAdjacency[j1][k1 + l1] & 0x80) == 128 || (world.objectAdjacency[j1 + l1][k1] & 0x80) == 128 || (world.objectAdjacency[j1 + l1][k1 + l1] & 0x80) == 128))
                return false;
            if (i1 == 7 && ((world.objectAdjacency[j1][k1 - l1] & 0x80) == 128 || (world.objectAdjacency[j1 + l1][k1] & 0x80) == 128 || (world.objectAdjacency[j1 + l1][k1 - l1] & 0x80) == 128))
                return false;
            if (i1 == 0 && (world.objectAdjacency[j1][k1 - l1] & 0x80) == 128)
                return false;
            if (i1 == 2 && (world.objectAdjacency[j1 - l1][k1] & 0x80) == 128)
                return false;
            if (i1 == 4 && (world.objectAdjacency[j1][k1 + l1] & 0x80) == 128)
                return false;
            if (i1 == 6 && (world.objectAdjacency[j1 + l1][k1] & 0x80) == 128)
                return false;
        }

        return true;
    }

    public void autorotateCamera() {
        if ((cameraAngle & 1) == 1 && isValidCameraAngle(cameraAngle))
            return;
        if ((cameraAngle & 1) == 0 && isValidCameraAngle(cameraAngle)) {
            if (isValidCameraAngle(cameraAngle + 1 & 7)) {
                cameraAngle = cameraAngle + 1 & 7;
                return;
            }
            if (isValidCameraAngle(cameraAngle + 7 & 7))
                cameraAngle = cameraAngle + 7 & 7;
            return;
        }
        int ai[] = {
                1, -1, 2, -2, 3, -3, 4
        };
        for (int i1 = 0; i1 < 7; i1++) {
            if (!isValidCameraAngle(cameraAngle + ai[i1] + 8 & 7))
                continue;
            cameraAngle = cameraAngle + ai[i1] + 8 & 7;
            break;
        }

        if ((cameraAngle & 1) == 0 && isValidCameraAngle(cameraAngle)) {
            if (isValidCameraAngle(cameraAngle + 1 & 7)) {
                cameraAngle = cameraAngle + 1 & 7;
                return;
            }
            if (isValidCameraAngle(cameraAngle + 7 & 7))
                cameraAngle = cameraAngle + 7 & 7;
            return;
        } else {
            return;
        }
    }

    public void drawGame() {
        if (deathScreenTimeout != 0) {
            surface.fade2black();
            surface.drawStringCenter("Oh dear! You are dead...", gameWidth / 2, gameHeight / 2, 7, 0xff0000);
            drawChatMessageTabs();
            surface.draw(graphics, 0, 11);
            return;
        }
        if (!world.playerAlive)
            return;
        if (showAppearanceChange) {
            drawAppearancePanelCharacterSprites();
            return;
        }
        for (int i1 = 0; i1 < 64; i1++) {
            scene.freeModel(world.afb[lastPlaneIndex][i1]);
            if (lastPlaneIndex == 0) {
                scene.freeModel(world.zeb[1][i1]);
                scene.freeModel(world.afb[1][i1]);
                scene.freeModel(world.zeb[2][i1]);
                scene.freeModel(world.afb[2][i1]);
            }
            fogOfWar = true;
            if (lastPlaneIndex == 0 && (world.objectAdjacency[localPlayer.currentX / 128][localPlayer.currentY / 128] & 0x80) == 0) {
                scene.addModel(world.afb[lastPlaneIndex][i1]);
                if (lastPlaneIndex == 0) {
                    scene.addModel(world.zeb[1][i1]);
                    scene.addModel(world.afb[1][i1]);
                    scene.addModel(world.zeb[2][i1]);
                    scene.addModel(world.afb[2][i1]);
                }
                fogOfWar = false;
            }
        }

        if (objectAnimationNumberTorch != lastObjectAnimationNumberTorch) {
            lastObjectAnimationNumberTorch = objectAnimationNumberTorch;
            for (int j1 = 0; j1 < objectCount; j1++) {
                if (objectId[j1] == 51) {
                    int i2 = objectX[j1];
                    int j3 = objectY[j1];
                    int k4 = i2 - localPlayer.currentX / 128;
                    int j6 = j3 - localPlayer.currentY / 128;
                    byte byte0 = 7;
                    if (i2 >= 0 && j3 >= 0 && i2 < 96 && j3 < 96 && k4 > -byte0 && k4 < byte0 && j6 > -byte0 && j6 < byte0) {
                        scene.freeModel(objectModel[j1]);
                        String s1 = "torcha" + (objectAnimationNumberTorch + 1);
                        int l10 = GameData.getModelId(s1);
                        GameModel h1 = gameModels[l10].copy();
                        scene.addModel(h1);
                        h1.setLight(true, 48, 48, -50, -10, -50);
                        h1.copyPosition(objectModel[j1]);
                        h1.key = j1;
                        objectModel[j1] = h1;
                    }
                }
                if (objectId[j1] == 143) {
                    int j2 = objectX[j1];
                    int k3 = objectY[j1];
                    int i5 = j2 - localPlayer.currentX / 128;
                    int k6 = k3 - localPlayer.currentY / 128;
                    byte byte1 = 7;
                    if (j2 >= 0 && k3 >= 0 && j2 < 96 && k3 < 96 && i5 > -byte1 && i5 < byte1 && k6 > -byte1 && k6 < byte1) {
                        scene.freeModel(objectModel[j1]);
                        String s2 = "skulltorcha" + (objectAnimationNumberTorch + 1);
                        int i11 = GameData.getModelId(s2);
                        GameModel h2 = gameModels[i11].copy();
                        scene.addModel(h2);
                        h2.setLight(true, 48, 48, -50, -10, -50);
                        h2.copyPosition(objectModel[j1]);
                        h2.key = j1;
                        objectModel[j1] = h2;
                    }
                }
            }

        }
        if (objectAnimationNumberFireLightningSpell != lastObjectAnimationNumberFireLightningSpell) {
            lastObjectAnimationNumberFireLightningSpell = objectAnimationNumberFireLightningSpell;
            for (int k1 = 0; k1 < objectCount; k1++)
                if (objectId[k1] == 97) {
                    int k2 = objectX[k1];
                    int l3 = objectY[k1];
                    int j5 = k2 - localPlayer.currentX / 128;
                    int i7 = l3 - localPlayer.currentY / 128;
                    byte byte2 = 9;
                    if (k2 >= 0 && l3 >= 0 && k2 < 96 && l3 < 96 && j5 > -byte2 && j5 < byte2 && i7 > -byte2 && i7 < byte2) {
                        scene.freeModel(objectModel[k1]);
                        String s3 = "firea" + (objectAnimationNumberFireLightningSpell + 1);
                        int j11 = GameData.getModelId(s3);
                        GameModel model = gameModels[j11].copy();
                        scene.addModel(model);
                        model.setLight(true, 48, 48, -50, -10, -50);
                        model.copyPosition(objectModel[k1]);
                        model.key = k1;
                        objectModel[k1] = model;
                    }
                }

        }
        scene.reduceSprites(spriteCount);
        spriteCount = 0;
        for (int l1 = 0; l1 < playerCount; l1++) {
            Character l2 = players[l1];
            if (l2.colourBottom != 255) {
                int i4 = l2.currentX;
                int k5 = l2.currentY;
                int j7 = -world.getElevation(i4, k5);
                int i9 = scene.drawSprite(5000 + l1, i4, j7, k5, 145, 220, l1 + 10000);
                spriteCount++;
                if (l2 == localPlayer)
                    scene.setFaceSpriteLocalPlayer(i9);
                if (l2.animationCurrent == 8)
                    scene.setCombatXOffset(i9, -30);
                if (l2.animationCurrent == 9)
                    scene.setCombatXOffset(i9, 30);
            }
        }

        for (int i3 = 0; i3 < playerCount; i3++) {
            Character l4 = players[i3];
            if (l4.projectileRange > 0) {
                Character l5 = null;
                if (l4.attackingNpcServerIndex != -1)
                    l5 = npcsServer[l4.attackingNpcServerIndex];
                else if (l4.attackingPlayerServerIndex != -1)
                    l5 = playersServer[l4.attackingPlayerServerIndex];
                if (l5 != null) {
                    int k7 = l4.currentX;
                    int j9 = l4.currentY;
                    int j10 = -world.getElevation(k7, j9) - 110;
                    int k11 = l5.currentX;
                    int i12 = l5.currentY;
                    int j12 = -world.getElevation(k11, i12) - GameData.npcVar24[l5.npcId] / 2;
                    int k12 = (k7 * l4.projectileRange + k11 * (vz - l4.projectileRange)) / vz;
                    int l12 = (j10 * l4.projectileRange + j12 * (vz - l4.projectileRange)) / vz;
                    int i13 = (j9 * l4.projectileRange + i12 * (vz - l4.projectileRange)) / vz;
                    scene.drawSprite(sprite_unknown + l4.incomingProjectileSprite, k12, l12, i13, 32, 32, 0);
                    spriteCount++;
                }
            }
        }

        for (int j4 = 0; j4 < npcCount; j4++) {
            Character l6 = npcs[j4];
            int l7 = l6.currentX;
            int k9 = l6.currentY;
            int k10 = -world.getElevation(l7, k9);
            int l11 = scene.drawSprite(20000 + j4, l7, k10, k9, GameData.npcVar23[l6.npcId], GameData.npcVar24[l6.npcId], j4 + 30000);
            spriteCount++;
            if (l6.animationCurrent == 8)
                scene.setCombatXOffset(l11, -30);
            if (l6.animationCurrent == 9)
                scene.setCombatXOffset(l11, 30);
        }

        for (int i6 = 0; i6 < groundItemCount; i6++) {
            int i8 = groundItemX[i6] * magicLoc + 64;
            int l9 = groundItemY[i6] * magicLoc + 64;
            scene.drawSprite(40000 + groundItemId[i6], i8, -world.getElevation(i8, l9) - groundItemZ[i6], l9, 96, 64, i6 + 20000);
            spriteCount++;
        }

        surface.interlace = false;
        surface.blackScreen();
        surface.interlace = super.interlace;
        if (lastPlaneIndex == 3) {
            int j8 = 40 + (int) (Math.random() * 3D);
            int i10 = 40 + (int) (Math.random() * 7D);
            scene.th(true, j8, i10, -50, -10, -50);
        }
        itemsAboveHeadCount = 0;
        receivedMessagesCount = 0;
        healthBarCount = 0;
        if (cameraAutoAngleDebug) {
            if (optionCameraModeAuto && !fogOfWar) {
                int k8 = cameraAngle;
                autorotateCamera();
                if (cameraAngle != k8) {
                    cameraAutoRotatePlayerX = localPlayer.currentX;
                    cameraAutoRotatePlayerY = localPlayer.currentY;
                }
            }
            scene.clipFar3d = 3000;
            scene.clipFar2d = 3000;
            scene.fogZFalloff = 1;
            scene.fogZDistance = 2800;
            cameraRotation = cameraAngle * 32;
            scene.setCamera(cameraAutoRotatePlayerX, -world.getElevation(cameraAutoRotatePlayerX, cameraAutoRotatePlayerY), cameraAutoRotatePlayerY, 912, cameraRotation * 4, 0, 2000);
        } else {
            if (optionCameraModeAuto && !fogOfWar)
                autorotateCamera();
            if (!super.interlace) {
                scene.clipFar3d = 2400;
                scene.clipFar2d = 2400;
                scene.fogZFalloff = 1;
                scene.fogZDistance = 2300;
            } else {
                scene.clipFar3d = 2200;
                scene.clipFar2d = 2200;
                scene.fogZFalloff = 1;
                scene.fogZDistance = 2100;
            }
            scene.setCamera(cameraAutoRotatePlayerX, -world.getElevation(cameraAutoRotatePlayerX, cameraAutoRotatePlayerY), cameraAutoRotatePlayerY, 912, cameraRotation * 4, 0, cameraZoom * 2);
        }
        scene.endScene();
        drawAboveHeadStuff();
        if (mouseClickXStep > 0)
            surface.drawSprite(mouseClickXX - 8, mouseClickXY - 8, spriteMedia + 14 + (24 - mouseClickXStep) / 6);
        if (mouseClickXStep < 0)
            surface.drawSprite(mouseClickXX - 8, mouseClickXY - 8, spriteMedia + 18 + (24 + mouseClickXStep) / 6);
        surface.drawstring("Fps: " + super.fps, 450, gameHeight - 10, 1, 0xffff00);
        if (messageTabsSelected == 0) {
            for (int l8 = 0; l8 < maxMessagesCount; l8++)
                if (messageHistoryTimeout[l8] > 0) {
                    String s = messageHistory[l8];
                    surface.drawstring(s, 7, gameHeight - 18 - l8 * 12, 1, 0xffff00);
                }

        }
        panelMessageTabs.nd(controlTextListChat);
        panelMessageTabs.nd(controlTextListQuest);
        panelMessageTabs.nd(controlTextListPrivate);
        if (messageTabsSelected == 1)
            panelMessageTabs.bd(controlTextListChat);
        else if (messageTabsSelected == 2)
            panelMessageTabs.bd(controlTextListQuest);
        else if (messageTabsSelected == 3)
            panelMessageTabs.bd(controlTextListPrivate);
        Panel.textListEntryHeightMod = 2;
        panelMessageTabs.drawPanel();
        Panel.textListEntryHeightMod = 0;
        surface.vf(((Surface) (surface)).width2 - 3 - 197, 3, spriteMedia);
        drawUi();
        surface.loggedIn = false;
        drawChatMessageTabs();
        surface.draw(graphics, 0, 11);
    }

    public void drawChatMessageTabs() {
        surface.drawSprite(0, gameHeight - 4, spriteMedia + 23);
        int i1 = Surface.rgb2long(200, 200, 255);
        if (messageTabsSelected == 0)
            i1 = Surface.rgb2long(255, 200, 50);
        if (messageTabFlashAll % 30 > 15)
            i1 = Surface.rgb2long(255, 50, 50);
        surface.drawStringCenter("All messages", 54, gameHeight + 6, 0, i1);
        i1 = Surface.rgb2long(200, 200, 255);
        if (messageTabsSelected == 1)
            i1 = Surface.rgb2long(255, 200, 50);
        if (messageTabFlashHistory % 30 > 15)
            i1 = Surface.rgb2long(255, 50, 50);
        surface.drawStringCenter("Chat history", 155, gameHeight + 6, 0, i1);
        i1 = Surface.rgb2long(200, 200, 255);
        if (messageTabsSelected == 2)
            i1 = Surface.rgb2long(255, 200, 50);
        if (messageTabFlashQuest % 30 > 15)
            i1 = Surface.rgb2long(255, 50, 50);
        surface.drawStringCenter("Quest history", 255, gameHeight + 6, 0, i1);
        i1 = Surface.rgb2long(200, 200, 255);
        if (messageTabsSelected == 3)
            i1 = Surface.rgb2long(255, 200, 50);
        if (messageTabFlashPrivate % 30 > 15)
            i1 = Surface.rgb2long(255, 50, 50);
        surface.drawStringCenter("Private history", 355, gameHeight + 6, 0, i1);
    }

    public void drawItem(int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
        int l2 = GameData.objectVar3[i2] + spriteItem;
        int i3 = GameData.objectVar16[i2];
        surface.spriteClipping(i1, j1, k1, l1, l2, i3, 0, 0, false);
    }

    public void drawNpc(int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
        Character npc = npcs[i2];
        int i3 = npc.animationCurrent + (cameraRotation + 16) / 32 & 7;
        boolean flag = false;
        int j3 = i3;
        if (j3 == 5) {
            j3 = 3;
            flag = true;
        } else if (j3 == 6) {
            j3 = 2;
            flag = true;
        } else if (j3 == 7) {
            j3 = 1;
            flag = true;
        }
        int k3 = j3 * 3 + npcWalkModel[(npc.stepCount / GameData.npcVar25[npc.npcId]) % 4];
        if (npc.animationCurrent == 8) {
            j3 = 5;
            byte byte0 = 2;
            flag = false;
            i1 -= (GameData.npcVar27[npc.npcId] * k2) / 100;
            k3 = j3 * 3 + npcCombatModelArray1[(loginTimer / (GameData.npcVar26[npc.npcId] - 1)) % 8];
        } else if (npc.animationCurrent == 9) {
            j3 = 5;
            byte byte1 = 2;
            flag = true;
            i1 += (GameData.npcVar27[npc.npcId] * k2) / 100;
            k3 = j3 * 3 + npcCombatModelArray2[(loginTimer / GameData.npcVar26[npc.npcId]) % 8];
        }
        for (int l3 = 0; l3 < 12; l3++) {
            int i4 = npcAnimationArray[j3][l3];
            int l4 = GameData.npcVar18[npc.npcId][i4];
            if (l4 >= 0) {
                int j5 = 0;
                int k5 = 0;
                int l5 = k3;
                if (flag && j3 >= 1 && j3 <= 3 && GameData.entityVar6[l4] == 1)
                    l5 += 15;
                if (j3 != 5 || GameData.entityVar5[l4] == 1) {
                    int i6 = l5 + GameData.entityVar7[l4];
                    j5 = (j5 * k1) / ((Surface) (surface)).spriteWidthFull[i6];
                    k5 = (k5 * l1) / ((Surface) (surface)).spriteHeightFull[i6];
                    int j6 = (k1 * ((Surface) (surface)).spriteWidthFull[i6]) / ((Surface) (surface)).spriteWidthFull[GameData.entityVar7[l4]];
                    j5 -= (j6 - k1) / 2;
                    int k6 = GameData.entityVar3[l4];
                    int l6 = 0;
                    if (k6 == 1) {
                        k6 = GameData.npcVar19[npc.npcId];
                        l6 = GameData.npcVar22[npc.npcId];
                    } else if (k6 == 2) {
                        k6 = GameData.npcVar20[npc.npcId];
                        l6 = GameData.npcVar22[npc.npcId];
                    } else if (k6 == 3) {
                        k6 = GameData.npcVar21[npc.npcId];
                        l6 = GameData.npcVar22[npc.npcId];
                    }
                    surface.spriteClipping(i1 + j5, j1 + k5, j6, l1, i6, k6, l6, j2, flag);
                }
            }
        }

        if (npc.messageTimeout > 0) {
            receivedMessageMidPoint[receivedMessagesCount] = surface.textWidth(npc.message, 1) / 2;
            receivedMessageHeight[receivedMessagesCount] = surface.textHeight(1);
            if (receivedMessageMidPoint[receivedMessagesCount] > 300) {
                receivedMessageMidPoint[receivedMessagesCount] = 300;
                receivedMessageHeight[receivedMessagesCount] *= 2;
            }
            receivedMessageX[receivedMessagesCount] = i1 + k1 / 2;
            receivedMessageY[receivedMessagesCount] = j1;
            receivedMessages[receivedMessagesCount++] = npc.message;
        }
        if (npc.animationCurrent == 8 || npc.animationCurrent == 9 || npc.combatTimer != 0) {
            if (npc.combatTimer > 0) {
                int j4 = i1;
                if (npc.animationCurrent == 8)
                    j4 -= (20 * k2) / 100;
                else if (npc.animationCurrent == 9)
                    j4 += (20 * k2) / 100;
                int i5 = (npc.healthCurrent * 30) / npc.healthMax;
                healthBarX[healthBarCount] = j4 + k1 / 2;
                healthBarY[healthBarCount] = j1;
                healthBarMissing[healthBarCount++] = i5;
            }
            if (npc.combatTimer > 150) {
                int k4 = i1;
                if (npc.animationCurrent == 8)
                    k4 -= (10 * k2) / 100;
                else if (npc.animationCurrent == 9)
                    k4 += (10 * k2) / 100;
                surface._mthif(k4 + k1 / 2, j1 + l1 / 2, spriteMedia + 12);
                surface.drawStringCenter(String.valueOf(npc.rr), (k4 + k1 / 2) - 1, j1 + l1 / 2 + 5, 3, 0xffffff);
            }
        }
    }

    public void drawPlayer(int i1, int j1, int k1, int l1, int i2, int j2, int k2) {
        Character l2 = players[i2];
        if (l2.colourBottom == 255)
            return;
        int i3 = l2.animationCurrent + (cameraRotation + 16) / 32 & 7;
        boolean flag = false;
        int j3 = i3;
        if (j3 == 5) {
            j3 = 3;
            flag = true;
        } else if (j3 == 6) {
            j3 = 2;
            flag = true;
        } else if (j3 == 7) {
            j3 = 1;
            flag = true;
        }
        int k3 = j3 * 3 + npcWalkModel[(l2.stepCount / 6) % 4];
        if (l2.animationCurrent == 8) {
            j3 = 5;
            i3 = 2;
            flag = false;
            i1 -= (5 * k2) / 100;
            k3 = j3 * 3 + npcCombatModelArray1[(loginTimer / 5) % 8];
        } else if (l2.animationCurrent == 9) {
            j3 = 5;
            i3 = 2;
            flag = true;
            i1 += (5 * k2) / 100;
            k3 = j3 * 3 + npcCombatModelArray2[(loginTimer / 6) % 8];
        }
        for (int l3 = 0; l3 < 12; l3++) {
            int i4 = npcAnimationArray[i3][l3];
            int i5 = l2.mr[i4] - 1;
            if (i5 >= 0) {
                int l5 = 0;
                int j6 = 0;
                int k6 = k3;
                if (flag && j3 >= 1 && j3 <= 3)
                    if (GameData.entityVar6[i5] == 1)
                        k6 += 15;
                    else if (i4 == 4 && j3 == 1) {
                        l5 = -22;
                        j6 = -3;
                        k6 = j3 * 3 + npcWalkModel[(2 + l2.stepCount / 6) % 4];
                    } else if (i4 == 4 && j3 == 2) {
                        l5 = 0;
                        j6 = -8;
                        k6 = j3 * 3 + npcWalkModel[(2 + l2.stepCount / 6) % 4];
                    } else if (i4 == 4 && j3 == 3) {
                        l5 = 26;
                        j6 = -5;
                        k6 = j3 * 3 + npcWalkModel[(2 + l2.stepCount / 6) % 4];
                    } else if (i4 == 3 && j3 == 1) {
                        l5 = 22;
                        j6 = 3;
                        k6 = j3 * 3 + npcWalkModel[(2 + l2.stepCount / 6) % 4];
                    } else if (i4 == 3 && j3 == 2) {
                        l5 = 0;
                        j6 = 8;
                        k6 = j3 * 3 + npcWalkModel[(2 + l2.stepCount / 6) % 4];
                    } else if (i4 == 3 && j3 == 3) {
                        l5 = -26;
                        j6 = 5;
                        k6 = j3 * 3 + npcWalkModel[(2 + l2.stepCount / 6) % 4];
                    }
                if (j3 != 5 || GameData.entityVar5[i5] == 1) {
                    int l6 = k6 + GameData.entityVar7[i5];
                    l5 = (l5 * k1) / ((Surface) (surface)).spriteWidthFull[l6];
                    j6 = (j6 * l1) / ((Surface) (surface)).spriteHeightFull[l6];
                    int i7 = (k1 * ((Surface) (surface)).spriteWidthFull[l6]) / ((Surface) (surface)).spriteWidthFull[GameData.entityVar7[i5]];
                    l5 -= (i7 - k1) / 2;
                    int j7 = GameData.entityVar3[i5];
                    int k7 = 0;
                    if (j7 == 1) {
                        j7 = characterHairColours[l2.xr];
                        k7 = characterSkinColours[l2.as];
                    } else if (j7 == 2) {
                        j7 = characterTopBottomColours[l2.yr];
                        k7 = characterSkinColours[l2.as];
                    } else if (j7 == 3) {
                        j7 = characterTopBottomColours[l2.colourBottom];
                        k7 = characterSkinColours[l2.as];
                    }
                    surface.spriteClipping(i1 + l5, j1 + j6, i7, l1, l6, j7, k7, j2, flag);
                }
            }
        }

        if (l2.messageTimeout > 0) {
            receivedMessageMidPoint[receivedMessagesCount] = surface.textWidth(l2.message, 1) / 2;
            receivedMessageHeight[receivedMessagesCount] = surface.textHeight(1);
            if (receivedMessageMidPoint[receivedMessagesCount] > 300) {
                receivedMessageMidPoint[receivedMessagesCount] = 300;
                receivedMessageHeight[receivedMessagesCount] *= 2;
            }
            receivedMessageX[receivedMessagesCount] = i1 + k1 / 2;
            receivedMessageY[receivedMessagesCount] = j1;
            receivedMessages[receivedMessagesCount++] = l2.message;
        }
        if (l2.bubbleTimeout > 0) {
            actionBubbleX[itemsAboveHeadCount] = i1 + k1 / 2;
            actionBubbleY[itemsAboveHeadCount] = j1;
            actionBubbleScale[itemsAboveHeadCount] = k2;
            actionBubbleItem[itemsAboveHeadCount++] = l2.pr;
        }
        if (l2.animationCurrent == 8 || l2.animationCurrent == 9 || l2.combatTimer != 0) {
            if (l2.combatTimer > 0) {
                int j4 = i1;
                if (l2.animationCurrent == 8)
                    j4 -= (20 * k2) / 100;
                else if (l2.animationCurrent == 9)
                    j4 += (20 * k2) / 100;
                int j5 = (l2.healthCurrent * 30) / l2.healthMax;
                healthBarX[healthBarCount] = j4 + k1 / 2;
                healthBarY[healthBarCount] = j1;
                healthBarMissing[healthBarCount++] = j5;
            }
            if (l2.combatTimer > 150) {
                int k4 = i1;
                if (l2.animationCurrent == 8)
                    k4 -= (10 * k2) / 100;
                else if (l2.animationCurrent == 9)
                    k4 += (10 * k2) / 100;
                surface._mthif(k4 + k1 / 2, j1 + l1 / 2, spriteMedia + 11);
                surface.drawStringCenter(String.valueOf(l2.rr), (k4 + k1 / 2) - 1, j1 + l1 / 2 + 5, 3, 0xffffff);
            }
        }
        if (l2.hs == 1 && l2.bubbleTimeout == 0) {
            int l4 = j2 + i1 + k1 / 2;
            if (l2.animationCurrent == 8)
                l4 -= (20 * k2) / 100;
            else if (l2.animationCurrent == 9)
                l4 += (20 * k2) / 100;
            int k5 = (16 * k2) / 100;
            int i6 = (16 * k2) / 100;
            surface.spriteClipping(l4 - k5 / 2, j1 - i6 / 2 - (10 * k2) / 100, k5, i6, spriteMedia + 13);
        }
    }

    public void drawAboveHeadStuff() {
        for (int i1 = 0; i1 < receivedMessagesCount; i1++) {
            int j1 = receivedMessageX[i1];
            int l1 = receivedMessageY[i1];
            int k2 = receivedMessageMidPoint[i1];
            int j3 = receivedMessageHeight[i1];
            boolean flag = true;
            while (flag) {
                flag = false;
                for (int k4 = 0; k4 < i1; k4++)
                    if (l1 > receivedMessageY[k4] - receivedMessageHeight[k4] && l1 - j3 < receivedMessageY[k4] && j1 - k2 < receivedMessageX[k4] + receivedMessageMidPoint[k4] && j1 + k2 > receivedMessageX[k4] - receivedMessageMidPoint[k4]) {
                        l1 = receivedMessageY[k4] - j3;
                        flag = true;
                    }

            }
            receivedMessageY[i1] = l1;
            surface.centrepara(receivedMessages[i1], j1, l1, 1, 0xffff00, 300);
        }

        for (int k1 = 0; k1 < itemsAboveHeadCount; k1++) {
            int i2 = actionBubbleX[k1];
            int l2 = actionBubbleY[k1];
            int k3 = actionBubbleScale[k1];
            int i4 = actionBubbleItem[k1];
            int l4 = (39 * k3) / 100;
            int i5 = (27 * k3) / 100;
            int j5 = l2 - i5;
            surface.drawActionBubble(i2 - l4 / 2, j5, l4, i5, spriteMedia + 9, 85);
            int k5 = (36 * k3) / 100;
            int l5 = (24 * k3) / 100;
            surface.spriteClipping(i2 - k5 / 2, (j5 + i5 / 2) - l5 / 2, k5, l5, GameData.objectVar3[i4] + spriteItem, GameData.objectVar16[i4], 0, 0, false);
        }

        for (int j2 = 0; j2 < healthBarCount; j2++) {
            int i3 = healthBarX[j2];
            int l3 = healthBarY[j2];
            int j4 = healthBarMissing[j2];
            surface.drawBoxAlpha(i3 - 15, l3 - 3, j4, 5, 65280, 192);
            surface.drawBoxAlpha((i3 - 15) + j4, l3 - 3, 30 - j4, 5, 0xff0000, 192);
        }

    }

    public int getInventoryCount(int i1) {
        int j1 = 0;
        for (int k1 = 0; k1 < inventoryItemsCount; k1++)
            if (inventoryItemId[k1] == i1)
                if (GameData.objectVar5[i1] == 1)
                    j1++;
                else
                    j1 += inventoryItemStackCount[k1];

        return j1;
    }

    public boolean hasStaffOrRunes(int i1, int j1) {
        if (i1 == 31 && isItemEquipped(197))
            return true;
        if (i1 == 32 && isItemEquipped(102))
            return true;
        if (i1 == 33 && isItemEquipped(101))
            return true;
        if (i1 == 34 && isItemEquipped(103))
            return true;
        return getInventoryCount(i1) >= j1;
    }

    public boolean isItemEquipped(int i1) {
        for (int j1 = 0; j1 < inventoryItemsCount; j1++)
            if (inventoryItemId[j1] == i1 && inventoryEquipped[j1] == 1)
                return true;

        return false;
    }

    public void drawMinimapEntity(int i1, int j1, int k1) {
        surface.gg(i1, j1, k1);
        surface.gg(i1 - 1, j1, k1);
        surface.gg(i1 + 1, j1, k1);
        surface.gg(i1, j1 - 1, k1);
        surface.gg(i1, j1 + 1, k1);
    }

    public void walkToActionSource(int i1, int j1, int k1, int l1, boolean flag) {
        walkToActionSource(i1, j1, k1, l1, k1, l1, false, flag);
    }

    public void walkToGroundItem(int i1, int j1, int k1, int l1, boolean flag) {
        if (walkToActionSource(i1, j1, k1, l1, k1, l1, false, flag)) {
            return;
        } else {
            walkToActionSource(i1, j1, k1, l1, k1, l1, true, flag);
            return;
        }
    }

    public void walkToObject(int i1, int j1, int k1, int l1) {
        int i2;
        int j2;
        if (k1 == 0 || k1 == 4) {
            i2 = GameData.locationVar4[l1];
            j2 = GameData.locationVar5[l1];
        } else {
            j2 = GameData.locationVar4[l1];
            i2 = GameData.locationVar5[l1];
        }
        if (GameData.locationVar6[l1] == 2 || GameData.locationVar6[l1] == 3) {
            if (k1 == 0) {
                i1--;
                i2++;
            }
            if (k1 == 2)
                j2++;
            if (k1 == 4)
                i2++;
            if (k1 == 6) {
                j1--;
                j2++;
            }
            walkToActionSource(regionX, regionY, i1, j1, (i1 + i2) - 1, (j1 + j2) - 1, false, true);
            return;
        } else {
            walkToActionSource(regionX, regionY, i1, j1, (i1 + i2) - 1, (j1 + j2) - 1, true, true);
            return;
        }
    }

    public void walkToWallObject(int i1, int j1, int k1) {
        if (k1 == 0) {
            walkToActionSource(regionX, regionY, i1, j1 - 1, i1, j1, false, true);
            return;
        }
        if (k1 == 1) {
            walkToActionSource(regionX, regionY, i1 - 1, j1, i1, j1, false, true);
            return;
        } else {
            walkToActionSource(regionX, regionY, i1, j1, i1, j1, true, true);
            return;
        }
    }

    public boolean walkToActionSource(int i1, int j1, int k1, int l1, int i2, int j2, boolean flag,
                                      boolean flag1) {
        int k2 = world.route(i1, j1, k1, l1, i2, j2, walkPathX, walkPathY, flag);
        if (k2 == -1)
            return false;
        k2--;
        i1 = walkPathX[k2];
        j1 = walkPathY[k2];
        k2--;
        if (flag1)
            super.clientStream.newPacket(215);
        else
            super.clientStream.newPacket(255);
        super.clientStream.writeShort(i1 + areaX);
        super.clientStream.writeShort(j1 + areaY);
        for (int l2 = k2; l2 >= 0 && l2 > k2 - 25; l2--) {
            super.clientStream.writeByte(walkPathX[l2] - i1);
            super.clientStream.writeByte(walkPathY[l2] - j1);
        }

        finalisePacket();
        mouseClickXStep = -24;
        mouseClickXX = super.mouseX;
        mouseClickXY = super.mouseY;
        return true;
    }

    public void loadNextRegion(int i1, int j1) {
        if (deathScreenTimeout != 0) {
            world.playerAlive = false;
            return;
        }
        i1 += planeWidth;
        j1 += planeHeight;
        if (lastPlaneIndex == planeIndex && i1 > localLowerX && i1 < localUpperX && j1 > localLowerY && j1 < localUpperY) {
            world.playerAlive = true;
            return;
        }
        surface.drawStringCenter("Loading... Please wait", 256, 192, 1, 0xffffff);
        drawChatMessageTabs();
        surface.draw(graphics, 0, 11);
        int k1 = areaX;
        int l1 = areaY;
        int i2 = (i1 + 24) / 48;
        int j2 = (j1 + 24) / 48;
        lastPlaneIndex = planeIndex;
        areaX = i2 * 48 - 48;
        areaY = j2 * 48 - 48;
        localLowerX = i2 * 48 - 32;
        localLowerY = j2 * 48 - 32;
        localUpperX = i2 * 48 + 32;
        localUpperY = j2 * 48 + 32;
        world.loadSection(i1, j1, lastPlaneIndex);
        areaX -= planeWidth;
        areaY -= planeHeight;
        int k2 = areaX - k1;
        int l2 = areaY - l1;
        for (int i3 = 0; i3 < objectCount; i3++) {
            objectX[i3] -= k2;
            objectY[i3] -= l2;
            int j3 = objectX[i3];
            int l3 = objectY[i3];
            int k4 = objectId[i3];
            GameModel h1 = objectModel[i3];
            try {
                int i6 = objectDirection[i3];
                int i7;
                int k7;
                if (i6 == 0 || i6 == 4) {
                    i7 = GameData.locationVar4[k4];
                    k7 = GameData.locationVar5[k4];
                } else {
                    k7 = GameData.locationVar4[k4];
                    i7 = GameData.locationVar5[k4];
                }
                int l7 = ((j3 + j3 + i7) * magicLoc) / 2;
                int i8 = ((l3 + l3 + k7) * magicLoc) / 2;
                if (j3 >= 0 && l3 >= 0 && j3 < 96 && l3 < 96) {
                    scene.addModel(h1);
                    h1.ce(l7, -world.getElevation(l7, i8), i8);
                    world.qn(j3, l3, k4);
                    if (k4 == 74)
                        h1.translate(0, -480, 0);
                }
            } catch (RuntimeException runtimeexception) {
                System.out.println("Loc Error: " + runtimeexception.getMessage());
                System.out.println("i:" + i3 + " obj:" + h1);
                runtimeexception.printStackTrace();
            }
        }

        for (int k3 = 0; k3 < wallObjectCount; k3++) {
            wallObjectX[k3] -= k2;
            wallObjectY[k3] -= l2;
            int i4 = wallObjectX[k3];
            int l4 = wallObjectY[k3];
            int j5 = wallObjectId[k3];
            int j6 = wallObjectDirection[k3];
            try {
                world.co(i4, l4, j6, j5);
                GameModel h2 = createModel(i4, l4, j6, j5, k3);
                wallObjectModel[k3] = h2;
            } catch (RuntimeException runtimeexception1) {
                System.out.println("Bound Error: " + runtimeexception1.getMessage());
                runtimeexception1.printStackTrace();
            }
        }

        for (int j4 = 0; j4 < groundItemCount; j4++) {
            groundItemX[j4] -= k2;
            groundItemY[j4] -= l2;
        }

        for (int i5 = 0; i5 < playerCount; i5++) {
            Character l5 = players[i5];
            l5.currentX -= k2 * magicLoc;
            l5.currentY -= l2 * magicLoc;
            for (int k6 = 0; k6 < l5.waypointCurrent; k6++) {
                l5.waypointsX[k6] -= k2 * magicLoc;
                l5.waypointsY[k6] -= l2 * magicLoc;
            }

        }

        for (int k5 = 0; k5 < npcCount; k5++) {
            Character l6 = npcs[k5];
            l6.currentX -= k2 * magicLoc;
            l6.currentY -= l2 * magicLoc;
            for (int j7 = 0; j7 < l6.waypointCurrent; j7++) {
                l6.waypointsX[j7] -= k2 * magicLoc;
                l6.waypointsY[j7] -= l2 * magicLoc;
            }

        }

        world.playerAlive = true;
        iu = false;
    }

    public GameModel createModel(int i1, int j1, int k1, int l1, int i2) {
        int j2 = i1;
        int k2 = j1;
        int l2 = i1;
        int i3 = j1;
        int j3 = GameData.boundaryVar4[l1];
        int k3 = GameData.boundaryVar5[l1];
        int l3 = GameData.boundaryVar3[l1];
        GameModel h1 = new GameModel(4, 1);
        if (k1 == 0)
            l2 = i1 + 1;
        if (k1 == 1)
            i3 = j1 + 1;
        if (k1 == 2) {
            j2 = i1 + 1;
            i3 = j1 + 1;
        }
        if (k1 == 3) {
            l2 = i1 + 1;
            i3 = j1 + 1;
        }
        j2 *= magicLoc;
        k2 *= magicLoc;
        l2 *= magicLoc;
        i3 *= magicLoc;
        int i4 = h1.je(j2, -world.getElevation(j2, k2), k2);
        int j4 = h1.je(j2, -world.getElevation(j2, k2) - l3, k2);
        int k4 = h1.je(l2, -world.getElevation(l2, i3) - l3, i3);
        int l4 = h1.je(l2, -world.getElevation(l2, i3), i3);
        int ai[] = {
                i4, j4, k4, l4
        };
        h1.createFace(4, ai, j3, k3);
        h1.setLight(false, 60, 24, -50, -10, -50);
        if (i1 >= 0 && j1 >= 0 && i1 < 96 && j1 < 96)
            scene.addModel(h1);
        h1.key = i2 + 10000;
        return h1;
    }

    public void drawUi() {
        if (hy && combatTimeout == 0)
            drawDialogShop();
        else if (showDialogTrade)
            drawDialogTrade();
        else if (gt != 0)
            kk();
        else if (showDialogSocialInput != 0)
            drawDialogSocialInput();
        else if (ft == 1)
            gm();
        else if (!kt && jt > 0x2bf20 && combatTimeout == 0) {
            el();
        } else {
            if (showOptionMenu)
                drawOptionMenu();
            if (localPlayer.combatTimer > 0)
                drawDialogCombatStyle();
            setActiveUiTab();
            boolean nomenus = !showOptionMenu && !showRightClickMenu;
            if (nomenus)
                menuItemsCount = 0;
            if (showUiTab == 0 && nomenus)
                createRightClickMenu();
            if (showUiTab == 1)
                drawUiTabInventory(nomenus);
            if (showUiTab == 2)
                drawUiTabMinimap(nomenus);
            if (showUiTab == 3)
                drawUiTabPlayerInfo(nomenus);
            if (showUiTab == 4)
                drawUiTabMagic(nomenus);
            if (showUiTab == 5)
                drawUiTabSocial(nomenus);
            if (showUiTab == 6)
                drawUiTabOptions(nomenus);
            if (!showRightClickMenu && !showOptionMenu)
                createTopMouseMenu();
            if (showRightClickMenu && !showOptionMenu)
                drawRightClickMenu();
        }
        mouseButtonClick = 0;
    }

    public void drawOptionMenu() {
        if (mouseButtonClick != 0) {
            for (int i1 = 0; i1 < optionMenuCount; i1++) {
                if (super.mouseX >= surface.textWidth(optionMenuEntry[i1], 1) || super.mouseY <= i1 * 12 || super.mouseY >= 12 + i1 * 12)
                    continue;
                super.clientStream.newPacket(237);
                super.clientStream.writeByte(i1);
                finalisePacket();
                break;
            }

            mouseButtonClick = 0;
            showOptionMenu = false;
            return;
        }
        for (int j1 = 0; j1 < optionMenuCount; j1++) {
            int k1 = 65535;
            if (super.mouseX < surface.textWidth(optionMenuEntry[j1], 1) && super.mouseY > j1 * 12 && super.mouseY < 12 + j1 * 12)
                k1 = 0xff0000;
            surface.drawstring(optionMenuEntry[j1], 6, 12 + j1 * 12, 1, k1);
        }

    }

    public void drawDialogCombatStyle() {
        byte byte0 = 7;
        byte byte1 = 15;
        char c1 = '\257';
        if (mouseButtonClick != 0) {
            for (int i1 = 0; i1 < 5; i1++) {
                if (i1 <= 0 || super.mouseX <= byte0 || super.mouseX >= byte0 + c1 || super.mouseY <= byte1 + i1 * 20 || super.mouseY >= byte1 + i1 * 20 + 20)
                    continue;
                combatStyle = i1 - 1;
                mouseButtonClick = 0;
                super.clientStream.newPacket(231);
                super.clientStream.writeByte(combatStyle);
                finalisePacket();
                break;
            }

        }
        for (int j1 = 0; j1 < 5; j1++) {
            if (j1 == combatStyle + 1)
                surface.drawBoxAlpha(byte0, byte1 + j1 * 20, c1, 20, Surface.rgb2long(255, 0, 0), 128);
            else
                surface.drawBoxAlpha(byte0, byte1 + j1 * 20, c1, 20, Surface.rgb2long(190, 190, 190), 128);
            surface.drawLineHoriz(byte0, byte1 + j1 * 20, c1, 0);
            surface.drawLineHoriz(byte0, byte1 + j1 * 20 + 20, c1, 0);
        }

        surface.drawStringCenter("Select combat style", byte0 + c1 / 2, byte1 + 16, 3, 0xffffff);
        surface.drawStringCenter("Controlled (+1 of each)", byte0 + c1 / 2, byte1 + 36, 3, 0);
        surface.drawStringCenter("Aggressive (+3 strength)", byte0 + c1 / 2, byte1 + 56, 3, 0);
        surface.drawStringCenter("Accurate   (+3 attack)", byte0 + c1 / 2, byte1 + 76, 3, 0);
        surface.drawStringCenter("Defensive  (+3 defense)", byte0 + c1 / 2, byte1 + 96, 3, 0);
    }

    public void el() {
        if (mouseButtonClick != 0) {
            mouseButtonClick = 0;
            if (super.mouseX > 200 && super.mouseX < 300 && super.mouseY > 230 && super.mouseY < 253) {
                kt = true;
                return;
            }
        }
        int i1 = 90;
        surface.drawBox(106, 70, 300, 190, 0);
        surface.drawBoxEdge(106, 70, 300, 190, 0xffffff);
        surface.drawStringCenter("You have been playing for", 256, i1, 4, 0xffffff);
        i1 += 20;
        surface.drawStringCenter("over 1 hour. Please consider", 256, i1, 4, 0xffffff);
        i1 += 20;
        surface.drawStringCenter("visiting our advertiser if you", 256, i1, 4, 0xffffff);
        i1 += 20;
        surface.drawStringCenter("see an advert which interests you.", 256, i1, 4, 0xffffff);
        i1 += 40;
        surface.drawStringCenter("Doing so will help ensure", 256, i1, 4, 0xffffff);
        i1 += 20;
        surface.drawStringCenter("Runescape remains free.", 256, i1, 4, 0xffffff);
        i1 += 40;
        int j1 = 0xffffff;
        if (super.mouseX > 200 && super.mouseX < 300 && super.mouseY > i1 - 20 && super.mouseY < i1 + 3)
            j1 = 0xffff00;
        surface.drawStringCenter("Close", 256, i1, 4, j1);
    }

    public void gm() {
        if (mouseButtonClick != 0) {
            mouseButtonClick = 0;
            char c1 = '\372';
            if (super.mouseX < 56 || super.mouseY < 70 || super.mouseX > 456 || super.mouseY > 260) {
                ft = 0;
                return;
            }
            if (super.mouseX > 250 && super.mouseX < 350 && super.mouseY > c1 - 20 && super.mouseY < c1 + 3) {
                ft = 0;
                return;
            }
            if (super.mouseX > 150 && super.mouseX < 250 && super.mouseY > c1 - 20 && super.mouseY < c1 + 3) {
                optionPlayerKiller = !optionPlayerKiller;
                super.clientStream.newPacket(213);
                super.clientStream.writeByte(1);
                super.clientStream.writeByte(optionPlayerKiller ? 1 : 0);
                super.clientStream.flushPacket();
                ft = 0;
                return;
            }
        }
        surface.drawBox(56, 70, 400, 190, 0);
        surface.drawBoxEdge(56, 70, 400, 190, 0xffffff);
        int i1 = 90;
        if (!optionPlayerKiller) {
            surface.drawStringCenter("Are you sure you want to change", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("to being able to fight other players?", 256, i1, 4, 0xffffff);
            i1 += 40;
            surface.drawStringCenter("If you do other players will be able to", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("attack you, and you will probably die", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("much more often.", 256, i1, 4, 0xffffff);
            i1 += 40;
        }
        if (optionPlayerKiller) {
            surface.drawStringCenter("Are you sure you want to change", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("to not fighting other players?", 256, i1, 4, 0xffffff);
            i1 += 40;
            surface.drawStringCenter("This will make you safe from attack,", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("but will also preventing you from attacking", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("others (except in the arena - coming soon)", 256, i1, 4, 0xffffff);
            i1 += 40;
        }
        if (et == 2) {
            surface.drawStringCenter("You can only change a total of 2 times", 256, i1, 4, 0xffffff);
            i1 += 20;
        }
        if (et == 1) {
            surface.drawStringCenter("You will not be allowed to change back again", 256, i1, 4, 0xffffff);
            i1 += 20;
        }
        i1 = 250;
        int j1 = 0xffffff;
        if (super.mouseX > 150 && super.mouseX < 250 && super.mouseY > i1 - 20 && super.mouseY < i1 + 3)
            j1 = 0xffff00;
        surface.drawStringCenter("Yes I'm sure", 200, i1, 4, j1);
        j1 = 0xffffff;
        if (super.mouseX > 250 && super.mouseX < 350 && super.mouseY > i1 - 20 && super.mouseY < i1 + 3)
            j1 = 0xffff00;
        surface.drawStringCenter("No thanks", 300, i1, 4, j1);
    }

    public void kk() {
        if (mouseButtonClick != 0) {
            mouseButtonClick = 0;
            if (super.mouseX < 106 || super.mouseY < 150 || super.mouseX > 406 || super.mouseY > 210) {
                gt = 0;
                return;
            }
        }
        int i1 = 150;
        surface.drawBox(106, i1, 300, 60, 0);
        surface.drawBoxEdge(106, i1, 300, 60, 0xffffff);
        i1 += 22;
        if (gt == 1) {
            surface.drawStringCenter("Please enter your new password", 256, i1, 4, 0xffffff);
            i1 += 25;
            String s = "*";
            for (int j1 = 0; j1 < super.inputTextCurrent.length(); j1++)
                s = "X" + s;

            surface.drawStringCenter(s, 256, i1, 4, 0xffffff);
            if (super.inputTextFinal.length() > 0) {
                it = super.inputTextFinal;
                super.inputTextCurrent = "";
                super.inputTextFinal = "";
                gt = 2;
                return;
            }
        } else if (gt == 2) {
            surface.drawStringCenter("Enter password again to confirm", 256, i1, 4, 0xffffff);
            i1 += 25;
            String s1 = "*";
            for (int k1 = 0; k1 < super.inputTextCurrent.length(); k1++)
                s1 = "X" + s1;

            surface.drawStringCenter(s1, 256, i1, 4, 0xffffff);
            if (super.inputTextFinal.length() > 0)
                if (super.inputTextFinal.equalsIgnoreCase(it)) {
                    gt = 4;
                    ab(it);
                    return;
                } else {
                    gt = 3;
                    return;
                }
        } else {
            if (gt == 3) {
                surface.drawStringCenter("Passwords do not match!", 256, i1, 4, 0xffffff);
                i1 += 25;
                surface.drawStringCenter("Press any key to close", 256, i1, 4, 0xffffff);
                return;
            }
            if (gt == 4) {
                surface.drawStringCenter("Ok, your request has been sent", 256, i1, 4, 0xffffff);
                i1 += 25;
                surface.drawStringCenter("Press any key to close", 256, i1, 4, 0xffffff);
            }
        }
    }

    public void drawDialogSocialInput() {
        if (mouseButtonClick != 0) {
            mouseButtonClick = 0;
            if (showDialogSocialInput == 1 && (super.mouseX < 106 || super.mouseY < 145 || super.mouseX > 406 || super.mouseY > 215)) {
                showDialogSocialInput = 0;
                return;
            }
            if (showDialogSocialInput == 2 && (super.mouseX < 6 || super.mouseY < 145 || super.mouseX > 506 || super.mouseY > 215)) {
                showDialogSocialInput = 0;
                return;
            }
            if (showDialogSocialInput == 3 && (super.mouseX < 106 || super.mouseY < 145 || super.mouseX > 406 || super.mouseY > 215)) {
                showDialogSocialInput = 0;
                return;
            }
            if (super.mouseX > 236 && super.mouseX < 276 && super.mouseY > 193 && super.mouseY < 213) {
                showDialogSocialInput = 0;
                return;
            }
        }
        int i1 = 145;
        if (showDialogSocialInput == 1) {
            surface.drawBox(106, i1, 300, 70, 0);
            surface.drawBoxEdge(106, i1, 300, 70, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("Enter name to add to friends list", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter(super.inputTextCurrent + "*", 256, i1, 4, 0xffffff);
            if (super.inputTextFinal.length() > 0) {
                String s = super.inputTextFinal.trim();
                super.inputTextCurrent = "";
                super.inputTextFinal = "";
                showDialogSocialInput = 0;
                if (s.length() > 0 && Utility.username2hash(s) != localPlayer.hash)
                    friendAdd(s);
            }
        }
        if (showDialogSocialInput == 2) {
            surface.drawBox(6, i1, 500, 70, 0);
            surface.drawBoxEdge(6, i1, 500, 70, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("Enter message to send to " + Utility.hash2username(privateMessageTarget), 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter(super.inputPmCurrent + "*", 256, i1, 4, 0xffffff);
            if (super.inputPmFinal.length() > 0) {
                String s1 = super.inputPmFinal;
                super.inputPmCurrent = "";
                super.inputPmFinal = "";
                showDialogSocialInput = 0;
                sendPrivateMessage(privateMessageTarget, s1);
            }
        }
        if (showDialogSocialInput == 3) {
            surface.drawBox(106, i1, 300, 70, 0);
            surface.drawBoxEdge(106, i1, 300, 70, 0xffffff);
            i1 += 20;
            surface.drawStringCenter("Enter name to add to ignore list", 256, i1, 4, 0xffffff);
            i1 += 20;
            surface.drawStringCenter(super.inputTextCurrent + "*", 256, i1, 4, 0xffffff);
            if (super.inputTextFinal.length() > 0) {
                String s2 = super.inputTextFinal.trim();
                super.inputTextCurrent = "";
                super.inputTextFinal = "";
                showDialogSocialInput = 0;
                if (s2.length() > 0 && Utility.username2hash(s2) != localPlayer.hash)
                    ignoreAdd(s2);
            }
        }
        int j1 = 0xffffff;
        if (super.mouseX > 236 && super.mouseX < 276 && super.mouseY > 193 && super.mouseY < 213)
            j1 = 0xffff00;
        surface.drawStringCenter("Cancel", 256, 208, 1, j1);
    }

    public void drawDialogShop() {
        if (mouseButtonClick != 0) {
            mouseButtonClick = 0;
            int i1 = super.mouseX - 52;
            int j1 = super.mouseY - 44;
            if (i1 >= 0 && j1 >= 12 && i1 < 408 && j1 < 246) {
                int k1 = 0;
                for (int i2 = 0; i2 < 5; i2++) {
                    for (int i3 = 0; i3 < 8; i3++) {
                        int l3 = 7 + i3 * 49;
                        int l4 = 28 + i2 * 34;
                        if (i1 > l3 && i1 < l3 + 49 && j1 > l4 && j1 < l4 + 34 && shopItem[k1] != -1) {
                            shopSelectedItemIndex = k1;
                            shopSelectedIemType = shopItem[k1];
                        }
                        k1++;
                    }

                }

                if (shopSelectedItemIndex >= 0) {
                    int j3 = shopItem[shopSelectedItemIndex];
                    if (j3 != -1) {
                        if (ly[shopSelectedItemIndex] > 0 && i1 > 298 && j1 >= 204 && i1 < 408 && j1 <= 215) {
                            int i4 = shopBuyPriceMod + shopItemPrice[shopSelectedItemIndex];
                            if (i4 < 10)
                                i4 = 10;
                            int i5 = (i4 * GameData.objectVar4[j3]) / 100;
                            super.clientStream.newPacket(217);
                            super.clientStream.writeShort(shopItem[shopSelectedItemIndex]);
                            super.clientStream.writeShort(i5);
                            finalisePacket();
                        }
                        if (getInventoryCount(j3) > 0 && i1 > 2 && j1 >= 229 && i1 < 112 && j1 <= 240) {
                            int j4 = shopSellPriceMod + shopItemPrice[shopSelectedItemIndex];
                            if (j4 < 10)
                                j4 = 10;
                            int j5 = (j4 * GameData.objectVar4[j3]) / 100;
                            super.clientStream.newPacket(216);
                            super.clientStream.writeShort(shopItem[shopSelectedItemIndex]);
                            super.clientStream.writeShort(j5);
                            finalisePacket();
                        }
                    }
                }
            } else {
                super.clientStream.newPacket(218);
                finalisePacket();
                hy = false;
                return;
            }
        }
        byte byte0 = 52;
        byte byte1 = 44;
        surface.drawBox(byte0, byte1, 408, 12, 192);
        int l1 = 0x989898;
        surface.drawBoxAlpha(byte0, byte1 + 12, 408, 17, l1, 160);
        surface.drawBoxAlpha(byte0, byte1 + 29, 8, 170, l1, 160);
        surface.drawBoxAlpha(byte0 + 399, byte1 + 29, 9, 170, l1, 160);
        surface.drawBoxAlpha(byte0, byte1 + 199, 408, 47, l1, 160);
        surface.drawstring("Buying and selling items", byte0 + 1, byte1 + 10, 1, 0xffffff);
        int j2 = 0xffffff;
        if (super.mouseX > byte0 + 320 && super.mouseY >= byte1 && super.mouseX < byte0 + 408 && super.mouseY < byte1 + 12)
            j2 = 0xff0000;
        surface.drawstringRight("Close window", byte0 + 406, byte1 + 10, 1, j2);
        surface.drawstring("Shops stock in green", byte0 + 2, byte1 + 24, 1, 65280);
        surface.drawstring("Number you own in blue", byte0 + 135, byte1 + 24, 1, 65535);
        surface.drawstring("Your money: " + getInventoryCount(10) + "gp", byte0 + 280, byte1 + 24, 1, 0xffff00);
        int k3 = 0xd0d0d0;
        int k4 = 0;
        for (int k5 = 0; k5 < 5; k5++) {
            for (int l5 = 0; l5 < 8; l5++) {
                int j6 = byte0 + 7 + l5 * 49;
                int i7 = byte1 + 28 + k5 * 34;
                if (shopSelectedItemIndex == k4)
                    surface.drawBoxAlpha(j6, i7, 49, 34, 0xff0000, 160);
                else
                    surface.drawBoxAlpha(j6, i7, 49, 34, k3, 160);
                surface.drawBoxEdge(j6, i7, 50, 35, 0);
                if (shopItem[k4] != -1) {
                    surface.spriteClipping(j6, i7, 48, 32, spriteItem + GameData.objectVar3[shopItem[k4]], GameData.objectVar16[shopItem[k4]], 0, 0, false);
                    surface.drawstring(String.valueOf(ly[k4]), j6 + 1, i7 + 10, 1, 65280);
                    surface.drawstringRight(String.valueOf(getInventoryCount(shopItem[k4])), j6 + 47, i7 + 10, 1, 65535);
                }
                k4++;
            }

        }

        surface.drawLineHoriz(byte0 + 5, byte1 + 222, 398, 0);
        if (shopSelectedItemIndex == -1) {
            surface.drawStringCenter("Select an object to buy or sell", byte0 + 204, byte1 + 214, 3, 0xffff00);
            return;
        }
        int i6 = shopItem[shopSelectedItemIndex];
        if (i6 != -1) {
            if (ly[shopSelectedItemIndex] > 0) {
                int k6 = shopBuyPriceMod + shopItemPrice[shopSelectedItemIndex];
                if (k6 < 10)
                    k6 = 10;
                int j7 = (k6 * GameData.objectVar4[i6]) / 100;
                surface.drawstring("Buy a new " + GameData.objectVar1[i6][0] + " for " + j7 + "gp", byte0 + 2, byte1 + 214, 1, 0xffff00);
                int k2 = 0xffffff;
                if (super.mouseX > byte0 + 298 && super.mouseY >= byte1 + 204 && super.mouseX < byte0 + 408 && super.mouseY <= byte1 + 215)
                    k2 = 0xff0000;
                surface.drawstringRight("Click here to buy", byte0 + 405, byte1 + 214, 3, k2);
            } else {
                surface.drawStringCenter("This item is not currently available to buy", byte0 + 204, byte1 + 214, 3, 0xffff00);
            }
            if (getInventoryCount(i6) > 0) {
                int l6 = shopSellPriceMod + shopItemPrice[shopSelectedItemIndex];
                if (l6 < 10)
                    l6 = 10;
                int k7 = (l6 * GameData.objectVar4[i6]) / 100;
                surface.drawstringRight("Sell your " + GameData.objectVar1[i6][0] + " for " + k7 + "gp", byte0 + 405, byte1 + 239, 1, 0xffff00);
                int l2 = 0xffffff;
                if (super.mouseX > byte0 + 2 && super.mouseY >= byte1 + 229 && super.mouseX < byte0 + 112 && super.mouseY <= byte1 + 240)
                    l2 = 0xff0000;
                surface.drawstring("Click here to sell", byte0 + 2, byte1 + 239, 3, l2);
                return;
            }
            surface.drawStringCenter("You do not have any of this item to sell", byte0 + 204, byte1 + 239, 3, 0xffff00);
        }
    }

    public void drawDialogTrade() {
        if (mouseButtonClick != 0 && mouseButtonItemCountIncrement == 0)
            mouseButtonItemCountIncrement = 1;
        if (mouseButtonItemCountIncrement > 0) {
            int i1 = super.mouseX - 22;
            int j1 = super.mouseY - 36;
            if (i1 >= 0 && j1 >= 0 && i1 < 468 && j1 < 262) {
                if (i1 > 216 && j1 > 30 && i1 < 462 && j1 < 235) {
                    int k1 = (i1 - 217) / 49 + ((j1 - 31) / 34) * 5;
                    if (k1 >= 0 && k1 < inventoryItemsCount) {
                        boolean flag = false;
                        int l2 = 0;
                        int k3 = inventoryItemId[k1];
                        for (int k4 = 0; k4 < tradeItemsCount; k4++)
                            if (tradeItems[k4] == k3)
                                if (GameData.objectVar5[k3] == 0) {
                                    for (int i5 = 0; i5 < mouseButtonItemCountIncrement; i5++) {
                                        if (tradeItemCount[k4] < inventoryItemStackCount[k1])
                                            tradeItemCount[k4]++;
                                        flag = true;
                                    }

                                } else {
                                    l2++;
                                }

                        if (getInventoryCount(k3) <= l2)
                            flag = true;
                        if (!flag && tradeItemsCount < 12) {
                            tradeItems[tradeItemsCount] = k3;
                            tradeItemCount[tradeItemsCount] = 1;
                            tradeItemsCount++;
                            flag = true;
                        }
                        if (flag) {
                            super.clientStream.newPacket(234);
                            super.clientStream.writeByte(tradeItemsCount);
                            for (int j5 = 0; j5 < tradeItemsCount; j5++) {
                                super.clientStream.writeShort(tradeItems[j5]);
                                super.clientStream.writeShort(tradeItemCount[j5]);
                            }

                            finalisePacket();
                            tradeRecipientAccepted = false;
                            tradeAccepted = false;
                        }
                    }
                }
                if (i1 > 8 && j1 > 30 && i1 < 205 && j1 < 133) {
                    int l1 = (i1 - 9) / 49 + ((j1 - 31) / 34) * 4;
                    if (l1 >= 0 && l1 < tradeItemsCount) {
                        int j2 = tradeItems[l1];
                        for (int i3 = 0; i3 < mouseButtonItemCountIncrement; i3++) {
                            if (GameData.objectVar5[j2] == 0 && tradeItemCount[l1] > 1) {
                                tradeItemCount[l1]--;
                                continue;
                            }
                            tradeItemsCount--;
                            mouseButtonDownTime = 0;
                            for (int l3 = l1; l3 < tradeItemsCount; l3++) {
                                tradeItems[l3] = tradeItems[l3 + 1];
                                tradeItemCount[l3] = tradeItemCount[l3 + 1];
                            }

                            break;
                        }

                        super.clientStream.newPacket(234);
                        super.clientStream.writeByte(tradeItemsCount);
                        for (int i4 = 0; i4 < tradeItemsCount; i4++) {
                            super.clientStream.writeShort(tradeItems[i4]);
                            super.clientStream.writeShort(tradeItemCount[i4]);
                        }

                        finalisePacket();
                        tradeRecipientAccepted = false;
                        tradeAccepted = false;
                    }
                }
                if (i1 > 143 && j1 > 141 && i1 < 154 && j1 < 152) {
                    tradeAccepted = !tradeAccepted;
                    super.clientStream.newPacket(232);
                    super.clientStream.writeByte(tradeAccepted ? 1 : 0);
                    finalisePacket();
                }
                if (i1 > 413 && j1 > 237 && i1 < 462 && j1 < 258) {
                    showDialogTrade = false;
                    super.clientStream.newPacket(233);
                    finalisePacket();
                }
            } else if (mouseButtonClick != 0) {
                showDialogTrade = false;
                super.clientStream.newPacket(233);
                finalisePacket();
            }
            mouseButtonClick = 0;
            mouseButtonItemCountIncrement = 0;
        }
        if (!showDialogTrade)
            return;
        byte byte0 = 22;
        byte byte1 = 36;
        surface.drawBox(byte0, byte1, 468, 12, 192);
        int i2 = 0x989898;
        surface.drawBoxAlpha(byte0, byte1 + 12, 468, 18, i2, 160);
        surface.drawBoxAlpha(byte0, byte1 + 30, 8, 248, i2, 160);
        surface.drawBoxAlpha(byte0 + 205, byte1 + 30, 11, 248, i2, 160);
        surface.drawBoxAlpha(byte0 + 462, byte1 + 30, 6, 248, i2, 160);
        surface.drawBoxAlpha(byte0 + 8, byte1 + 133, 197, 22, i2, 160);
        surface.drawBoxAlpha(byte0 + 8, byte1 + 258, 197, 20, i2, 160);
        surface.drawBoxAlpha(byte0 + 216, byte1 + 235, 246, 43, i2, 160);
        int k2 = 0xd0d0d0;
        surface.drawBoxAlpha(byte0 + 8, byte1 + 30, 197, 103, k2, 160);
        surface.drawBoxAlpha(byte0 + 8, byte1 + 155, 197, 103, k2, 160);
        surface.drawBoxAlpha(byte0 + 216, byte1 + 30, 246, 205, k2, 160);
        for (int j3 = 0; j3 < 4; j3++)
            surface.drawLineHoriz(byte0 + 8, byte1 + 30 + j3 * 34, 197, 0);

        for (int j4 = 0; j4 < 4; j4++)
            surface.drawLineHoriz(byte0 + 8, byte1 + 155 + j4 * 34, 197, 0);

        for (int l4 = 0; l4 < 7; l4++)
            surface.drawLineHoriz(byte0 + 216, byte1 + 30 + l4 * 34, 246, 0);

        for (int k5 = 0; k5 < 6; k5++) {
            if (k5 < 5)
                surface.drawLineVert(byte0 + 8 + k5 * 49, byte1 + 30, 103, 0);
            if (k5 < 5)
                surface.drawLineVert(byte0 + 8 + k5 * 49, byte1 + 155, 103, 0);
            surface.drawLineVert(byte0 + 216 + k5 * 49, byte1 + 30, 205, 0);
        }

        surface.drawstring("Trading with: " + wx, byte0 + 1, byte1 + 10, 1, 0xffffff);
        surface.drawstring("Your Offer", byte0 + 9, byte1 + 27, 4, 0xffffff);
        surface.drawstring("Opponent's Offer", byte0 + 9, byte1 + 152, 4, 0xffffff);
        surface.drawstring("Your Inventory", byte0 + 216, byte1 + 27, 4, 0xffffff);
        surface.drawstringRight("Accepted", byte0 + 204, byte1 + 27, 4, 65280);
        surface.drawBoxEdge(byte0 + 125, byte1 + 16, 11, 11, 65280);
        if (tradeRecipientAccepted)
            surface.drawBox(byte0 + 127, byte1 + 18, 7, 7, 65280);
        surface.drawstringRight("Accept", byte0 + 204, byte1 + 152, 4, 65280);
        surface.drawBoxEdge(byte0 + 143, byte1 + 141, 11, 11, 65280);
        if (tradeAccepted)
            surface.drawBox(byte0 + 145, byte1 + 143, 7, 7, 65280);
        surface.drawstringRight("Close", byte0 + 408 + 49, byte1 + 251, 4, 0xc00000);
        surface.drawBoxEdge(byte0 + 364 + 49, byte1 + 237, 49, 21, 0xc00000);
        for (int l5 = 0; l5 < inventoryItemsCount; l5++) {
            int i6 = 217 + byte0 + (l5 % 5) * 49;
            int k6 = 31 + byte1 + (l5 / 5) * 34;
            surface.spriteClipping(i6, k6, 48, 32, spriteItem + GameData.objectVar3[inventoryItemId[l5]], GameData.objectVar16[inventoryItemId[l5]], 0, 0, false);
            if (GameData.objectVar5[inventoryItemId[l5]] == 0)
                surface.drawstring(String.valueOf(inventoryItemStackCount[l5]), i6 + 1, k6 + 10, 1, 0xffff00);
        }

        for (int j6 = 0; j6 < tradeItemsCount; j6++) {
            int l6 = 9 + byte0 + (j6 % 4) * 49;
            int j7 = 31 + byte1 + (j6 / 4) * 34;
            surface.spriteClipping(l6, j7, 48, 32, spriteItem + GameData.objectVar3[tradeItems[j6]], GameData.objectVar16[tradeItems[j6]], 0, 0, false);
            if (GameData.objectVar5[tradeItems[j6]] == 0)
                surface.drawstring(String.valueOf(tradeItemCount[j6]), l6 + 1, j7 + 10, 1, 0xffff00);
            if (super.mouseX > l6 && super.mouseX < l6 + 48 && super.mouseY > j7 && super.mouseY < j7 + 32)
                surface.drawstring(GameData.objectVar1[tradeItems[j6]][0] + ": @whi@" + GameData.objectVar2[tradeItems[j6]], byte0 + 8, byte1 + 273, 1, 0xffff00);
        }

        for (int i7 = 0; i7 < ay; i7++) {
            int k7 = 9 + byte0 + (i7 % 4) * 49;
            int l7 = 156 + byte1 + (i7 / 4) * 34;
            surface.spriteClipping(k7, l7, 48, 32, spriteItem + GameData.objectVar3[tradeRecipientItems[i7]], GameData.objectVar16[tradeRecipientItems[i7]], 0, 0, false);
            if (GameData.objectVar5[tradeRecipientItems[i7]] == 0)
                surface.drawstring(String.valueOf(cy[i7]), k7 + 1, l7 + 10, 1, 0xffff00);
            if (super.mouseX > k7 && super.mouseX < k7 + 48 && super.mouseY > l7 && super.mouseY < l7 + 32)
                surface.drawstring(GameData.objectVar1[tradeRecipientItems[i7]][0] + ": @whi@" + GameData.objectVar2[tradeRecipientItems[i7]], byte0 + 8, byte1 + 273, 1, 0xffff00);
        }

    }

    public void setActiveUiTab() {
        if (showUiTab == 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 && super.mouseY < 35)
            showUiTab = 1;
        if (showUiTab == 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 33 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 33 && super.mouseY < 35)
            showUiTab = 2;
        if (showUiTab == 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 66 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 66 && super.mouseY < 35)
            showUiTab = 3;
        if (showUiTab == 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 99 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 99 && super.mouseY < 35)
            showUiTab = 4;
        if (showUiTab == 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 132 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 132 && super.mouseY < 35)
            showUiTab = 5;
        if (showUiTab == 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 165 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 165 && super.mouseY < 35)
            showUiTab = 6;
        if (showUiTab != 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 && super.mouseY < 26)
            showUiTab = 1;
        if (showUiTab != 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 33 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 33 && super.mouseY < 26)
            showUiTab = 2;
        if (showUiTab != 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 66 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 66 && super.mouseY < 26)
            showUiTab = 3;
        if (showUiTab != 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 99 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 99 && super.mouseY < 26)
            showUiTab = 4;
        if (showUiTab != 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 132 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 132 && super.mouseY < 26)
            showUiTab = 5;
        if (showUiTab != 0 && super.mouseX >= ((Surface) (surface)).width2 - 35 - 165 && super.mouseY >= 3 && super.mouseX < ((Surface) (surface)).width2 - 3 - 165 && super.mouseY < 26)
            showUiTab = 6;
        if (showUiTab == 1 && (super.mouseX < ((Surface) (surface)).width2 - 248 || super.mouseY > 240))
            showUiTab = 0;
        if (showUiTab >= 2 && showUiTab <= 5 && (super.mouseX < ((Surface) (surface)).width2 - 199 || super.mouseY > 240))
            showUiTab = 0;
        if (showUiTab == 6 && (super.mouseX < ((Surface) (surface)).width2 - 199 || super.mouseY > 267))
            showUiTab = 0;
    }

    public void drawUiTabInventory(boolean flag) {
        int i1 = ((Surface) (surface)).width2 - 248;
        surface.drawSprite(i1, 3, spriteMedia + 1);
        for (int j1 = 0; j1 < 30; j1++) {
            int k1 = i1 + (j1 % 5) * 49;
            int i2 = 36 + (j1 / 5) * 34;
            if (j1 < inventoryItemsCount && inventoryEquipped[j1] == 1)
                surface.drawBoxAlpha(k1, i2, 49, 34, 0xff0000, 128);
            else
                surface.drawBoxAlpha(k1, i2, 49, 34, Surface.rgb2long(181, 181, 181), 128);
            if (j1 < inventoryItemsCount) {
                surface.spriteClipping(k1, i2, 48, 32, spriteItem + GameData.objectVar3[inventoryItemId[j1]], GameData.objectVar16[inventoryItemId[j1]], 0, 0, false);
                if (GameData.objectVar5[inventoryItemId[j1]] == 0)
                    surface.drawstring(String.valueOf(inventoryItemStackCount[j1]), k1 + 1, i2 + 10, 1, 0xffff00);
            }
        }

        for (int l1 = 1; l1 <= 4; l1++)
            surface.drawLineVert(i1 + l1 * 49, 36, 204, 0);

        for (int j2 = 1; j2 <= 5; j2++)
            surface.drawLineHoriz(i1, 36 + j2 * 34, 245, 0);

        if (!flag)
            return;
        i1 = super.mouseX - (((Surface) (surface)).width2 - 248);
        int k2 = super.mouseY - 36;
        if (i1 >= 0 && k2 >= 0 && i1 < 248 && k2 < 204) {
            int l2 = i1 / 49 + (k2 / 34) * 5;
            if (l2 < inventoryItemsCount) {
                int i3 = inventoryItemId[l2];
                if (selectedSpell >= 0) {
                    if (GameData.spellVar4[selectedSpell] == 3) {
                        menuItemText1[menuItemsCount] = textCast + GameData.spellVar1[selectedSpell] + " on";
                        menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                        menuItemId[menuItemsCount] = 600;
                        menuSourceType[menuItemsCount] = l2;
                        menuSourceIndex[menuItemsCount] = selectedSpell;
                        menuItemsCount++;
                        return;
                    }
                } else {
                    if (selectedItemInventoryIndex >= 0) {
                        menuItemText1[menuItemsCount] = "Use " + selectedItemName + " with";
                        menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                        menuItemId[menuItemsCount] = 610;
                        menuSourceType[menuItemsCount] = l2;
                        menuSourceIndex[menuItemsCount] = selectedItemInventoryIndex;
                        menuItemsCount++;
                        return;
                    }
                    if (inventoryEquipped[l2] == 1) {
                        menuItemText1[menuItemsCount] = "Remove";
                        menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                        menuItemId[menuItemsCount] = 620;
                        menuSourceType[menuItemsCount] = l2;
                        menuItemsCount++;
                    } else if (GameData.objectVar14[i3] != 0) {
                        if ((GameData.objectVar14[i3] & 0x18) != 0)
                            menuItemText1[menuItemsCount] = "Wield";
                        else
                            menuItemText1[menuItemsCount] = "Wear";
                        menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                        menuItemId[menuItemsCount] = 630;
                        menuSourceType[menuItemsCount] = l2;
                        menuItemsCount++;
                    }
                    if (!GameData.objectVar6[i3].equals("_")) {
                        menuItemText1[menuItemsCount] = GameData.objectVar6[i3];
                        menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                        menuItemId[menuItemsCount] = 640;
                        menuSourceType[menuItemsCount] = l2;
                        menuItemsCount++;
                    }
                    menuItemText1[menuItemsCount] = "Use";
                    menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                    menuItemId[menuItemsCount] = 650;
                    menuSourceType[menuItemsCount] = l2;
                    menuItemsCount++;
                    menuItemText1[menuItemsCount] = "Drop";
                    menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                    menuItemId[menuItemsCount] = 660;
                    menuSourceType[menuItemsCount] = l2;
                    menuItemsCount++;
                    menuItemText1[menuItemsCount] = "Examine";
                    menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[i3][0];
                    menuItemId[menuItemsCount] = 3600;
                    menuSourceType[menuItemsCount] = i3;
                    menuItemsCount++;
                }
            }
        }
    }

    public void drawUiTabMinimap(boolean flag) {
        int i1 = ((Surface) (surface)).width2 - 199;
        char c1 = '\234';
        char c3 = '\230';
        surface.drawSprite(i1 - 49, 3, spriteMedia + 2);
        i1 += 40;
        surface.drawBox(i1, 36, c1, c3, 0);
        surface.setBounds(i1, 36, i1 + c1, 36 + c3);
        char c5 = '\300';
        int k1 = ((localPlayer.currentX - 6040) * 3 * c5) / 2048;
        int i3 = ((localPlayer.currentY - 6040) * 3 * c5) / 2048;
        int k4 = Scene.hm[1024 - cameraRotation * 4 & 0x3ff];
        int i5 = Scene.hm[(1024 - cameraRotation * 4 & 0x3ff) + 1024];
        int k5 = i3 * k4 + k1 * i5 >> 18;
        i3 = i3 * i5 - k1 * k4 >> 18;
        k1 = k5;
        surface.drawMinimapSprite((i1 + c1 / 2) - k1, 36 + c3 / 2 + i3, spriteMedia - 1, cameraRotation + 64 & 0xff, c5);
        for (int i7 = 0; i7 < objectCount; i7++) {
            int l1 = (((objectX[i7] * magicLoc + 64) - localPlayer.currentX) * 3 * c5) / 2048;
            int j3 = (((objectY[i7] * magicLoc + 64) - localPlayer.currentY) * 3 * c5) / 2048;
            int l5 = j3 * k4 + l1 * i5 >> 18;
            j3 = j3 * i5 - l1 * k4 >> 18;
            l1 = l5;
            drawMinimapEntity(i1 + c1 / 2 + l1, (36 + c3 / 2) - j3, 65535);
        }

        for (int j7 = 0; j7 < groundItemCount; j7++) {
            int i2 = (((groundItemX[j7] * magicLoc + 64) - localPlayer.currentX) * 3 * c5) / 2048;
            int k3 = (((groundItemY[j7] * magicLoc + 64) - localPlayer.currentY) * 3 * c5) / 2048;
            int i6 = k3 * k4 + i2 * i5 >> 18;
            k3 = k3 * i5 - i2 * k4 >> 18;
            i2 = i6;
            drawMinimapEntity(i1 + c1 / 2 + i2, (36 + c3 / 2) - k3, 0xff0000);
        }

        for (int k7 = 0; k7 < npcCount; k7++) {
            Character l7 = npcs[k7];
            int j2 = ((l7.currentX - localPlayer.currentX) * 3 * c5) / 2048;
            int l3 = ((l7.currentY - localPlayer.currentY) * 3 * c5) / 2048;
            int j6 = l3 * k4 + j2 * i5 >> 18;
            l3 = l3 * i5 - j2 * k4 >> 18;
            j2 = j6;
            drawMinimapEntity(i1 + c1 / 2 + j2, (36 + c3 / 2) - l3, 0xffff00);
        }

        for (int i8 = 0; i8 < playerCount; i8++) {
            Character l8 = players[i8];
            int k2 = ((l8.currentX - localPlayer.currentX) * 3 * c5) / 2048;
            int i4 = ((l8.currentY - localPlayer.currentY) * 3 * c5) / 2048;
            int k6 = i4 * k4 + k2 * i5 >> 18;
            i4 = i4 * i5 - k2 * k4 >> 18;
            k2 = k6;
            drawMinimapEntity(i1 + c1 / 2 + k2, (36 + c3 / 2) - i4, 0xffffff);
        }

        surface.drawCircle(i1 + c1 / 2, 36 + c3 / 2, 2, 0xffffff, 255);
        surface.drawMinimapSprite(i1 + 19, 55, spriteMedia + 24, cameraRotation + 128 & 0xff, 128);
        surface.setBounds(0, 0, gameWidth, gameHeight + 12);
        if (!flag)
            return;
        i1 = super.mouseX - (((Surface) (surface)).width2 - 199);
        int j8 = super.mouseY - 36;
        if (i1 >= 40 && j8 >= 0 && i1 < 196 && j8 < 152) {
            char c2 = '\234';
            char c4 = '\230';
            char c6 = '\300';
            int j1 = ((Surface) (surface)).width2 - 199;
            j1 += 40;
            int l2 = ((super.mouseX - (j1 + c2 / 2)) * 16384) / (3 * c6);
            int j4 = ((super.mouseY - (36 + c4 / 2)) * 16384) / (3 * c6);
            int l4 = Scene.hm[1024 - cameraRotation * 4 & 0x3ff];
            int j5 = Scene.hm[(1024 - cameraRotation * 4 & 0x3ff) + 1024];
            int l6 = j4 * l4 + l2 * j5 >> 15;
            j4 = j4 * j5 - l2 * l4 >> 15;
            l2 = l6;
            l2 += localPlayer.currentX;
            j4 = localPlayer.currentY - j4;
            if (mouseButtonClick == 1)
                walkToActionSource(regionX, regionY, l2 / 128, j4 / 128, false);
            mouseButtonClick = 0;
        }
    }

    public void drawUiTabPlayerInfo(boolean flag) {
        int i1 = ((Surface) (surface)).width2 - 199;
        surface.drawSprite(i1 - 49, 3, spriteMedia + 3);
        char c1 = '\304';
        char c2 = '\266';
        surface.drawBoxAlpha(i1, 36, c1, c2, Surface.rgb2long(181, 181, 181), 160);
        int j1 = 48;
        surface.drawstring("Skills", i1 + 5, j1, 3, 0xffff00);
        j1 += 13;
        for (int k1 = 0; k1 < 8; k1++) {
            surface.drawstring(skillNameShort[k1] + ":@yel@" + playerStatCurrent[k1] + "/" + playerStatBase[k1], i1 + 5, j1, 1, 0xffffff);
            surface.drawstring(skillNameShort[k1 + 8] + ":@yel@" + playerStatCurrent[k1 + 8] + "/" + playerStatBase[k1 + 8], (i1 + c1 / 2) - 8, j1 - 13, 1, 0xffffff);
            j1 += 13;
        }

        surface.drawstring("Quest Points:@yel@" + playerQuestPoints, (i1 + c1 / 2) - 8, j1 - 13, 1, 0xffffff);
        j1 += 8;
        surface.drawstring("Equipment Status", i1 + 5, j1, 3, 0xffff00);
        j1 += 12;
        for (int l1 = 0; l1 < 3; l1++) {
            surface.drawstring(equipmentStatNames[l1] + ":@yel@" + playerStatEquipment[l1], i1 + 5, j1, 1, 0xffffff);
            if (l1 < 2)
                surface.drawstring(equipmentStatNames[l1 + 3] + ":@yel@" + playerStatEquipment[l1 + 3], i1 + c1 / 2 + 25, j1, 1, 0xffffff);
            j1 += 13;
        }

    }

    public void drawUiTabMagic(boolean flag) {
        int i1 = ((Surface) (surface)).width2 - 199;
        int j1 = 36;
        surface.drawSprite(i1 - 49, 3, spriteMedia + 4);
        char c1 = '\304';
        char c2 = '\266';
        int l1;
        int k1 = l1 = Surface.rgb2long(160, 160, 160);
        if (tabMagicPrayer == 0)
            k1 = Surface.rgb2long(220, 220, 220);
        else
            l1 = Surface.rgb2long(220, 220, 220);
        surface.drawBoxAlpha(i1, j1, c1 / 2, 24, k1, 128);
        surface.drawBoxAlpha(i1 + c1 / 2, j1, c1 / 2, 24, l1, 128);
        surface.drawBoxAlpha(i1, j1 + 24, c1, 90, Surface.rgb2long(220, 220, 220), 128);
        surface.drawBoxAlpha(i1, j1 + 24 + 90, c1, c2 - 90 - 24, Surface.rgb2long(160, 160, 160), 128);
        surface.drawLineHoriz(i1, j1 + 24, c1, 0);
        surface.drawLineVert(i1 + c1 / 2, j1, 24, 0);
        surface.drawLineHoriz(i1, j1 + 113, c1, 0);
        surface.drawStringCenter("Magic", i1 + c1 / 4, j1 + 16, 4, 0);
        surface.drawStringCenter("Prayers", i1 + c1 / 4 + c1 / 2, j1 + 16, 4, 0);
        if (tabMagicPrayer == 0) {
            panelMagic.clearList(controlMagicPanel);
            int i2 = 0;
            for (int k2 = 0; k2 < GameData.spellCount; k2++) {
                String s = "@yel@";
                for (int k3 = 0; k3 < GameData.spellVar6[k2]; k3++) {
                    int j4 = GameData.spellVar7[k2][k3];
                    if (hasStaffOrRunes(j4, GameData.spellVar8[k2][k3]))
                        continue;
                    s = "@whi@";
                    break;
                }

                int k4 = playerStatCurrent[6];
                if (GameData.spellVar2[k2] > k4)
                    s = "@bla@";
                panelMagic.addListEntry(controlMagicPanel, i2++, s + "Level " + GameData.spellVar2[k2] + ": " + GameData.spellVar1[k2]);
            }

            panelMagic.drawPanel();
            int i3 = panelMagic.getListEntryIndex(controlMagicPanel);
            if (i3 != -1) {
                surface.drawstring("Level " + GameData.spellVar2[i3] + ": " + GameData.spellVar1[i3], i1 + 2, j1 + 124, 1, 0);
                surface.drawstring(GameData.spellVar3[i3], i1 + 2, j1 + 136, 0, 0);
                for (int l3 = 0; l3 < GameData.spellVar6[i3]; l3++) {
                    int l4 = GameData.spellVar7[i3][l3];
                    surface.drawSprite(i1 + 2 + l3 * 44, j1 + 150, spriteItem + GameData.objectVar3[l4]);
                    int i5 = getInventoryCount(l4);
                    int j5 = GameData.spellVar8[i3][l3];
                    String s1 = "@red@";
                    if (hasStaffOrRunes(l4, j5))
                        s1 = "@gre@";
                    surface.drawstring(s1 + i5 + "/" + j5, i1 + 2 + l3 * 44, j1 + 150, 1, 0xffffff);
                }

            } else {
                surface.drawstring("Point at a spell for a description", i1 + 2, j1 + 124, 1, 0);
            }
        }
        if (!flag)
            return;
        i1 = super.mouseX - (((Surface) (surface)).width2 - 199);
        j1 = super.mouseY - 36;
        if (i1 >= 0 && j1 >= 0 && i1 < 196 && j1 < 182) {
            panelMagic.handleMouse(i1 + (((Surface) (surface)).width2 - 199), j1 + 36, super.lastMouseButtonDown, super.mouseButtonDown);
            if (j1 <= 24 && mouseButtonClick == 1)
                if (i1 < 98 && tabMagicPrayer == 1) {
                    tabMagicPrayer = 0;
                    panelMagic.resetListProps(controlMagicPanel);
                } else if (i1 > 98 && tabMagicPrayer == 0) {
                    tabMagicPrayer = 1;
                    panelMagic.resetListProps(controlMagicPanel);
                }
            if (mouseButtonClick == 1 && tabMagicPrayer == 0) {
                int j2 = panelMagic.getListEntryIndex(controlMagicPanel);
                if (j2 != -1) {
                    int l2 = playerStatCurrent[6];
                    if (GameData.spellVar2[j2] > l2) {
                        showMessage("Your magic ability is not high enough for this spell", 3);
                    } else {
                        int j3;
                        for (j3 = 0; j3 < GameData.spellVar6[j2]; j3++) {
                            int i4 = GameData.spellVar7[j2][j3];
                            if (hasStaffOrRunes(i4, GameData.spellVar8[j2][j3]))
                                continue;
                            showMessage("You don't have all the reagents you need for this spell", 3);
                            j3 = -1;
                            break;
                        }

                        if (j3 == GameData.spellVar6[j2]) {
                            selectedSpell = j2;
                            selectedItemInventoryIndex = -1;
                            textCast = "Cast ";
                        }
                    }
                }
            }
            mouseButtonClick = 0;
        }
    }

    public void drawUiTabSocial(boolean flag) {
        int i1 = ((Surface) (surface)).width2 - 199;
        int j1 = 36;
        surface.drawSprite(i1 - 49, 3, spriteMedia + 5);
        char c1 = '\304';
        char c2 = '\266';
        int l1;
        int k1 = l1 = Surface.rgb2long(160, 160, 160);
        if (tabSocial == 0)
            k1 = Surface.rgb2long(220, 220, 220);
        else
            l1 = Surface.rgb2long(220, 220, 220);
        surface.drawBoxAlpha(i1, j1, c1 / 2, 24, k1, 128);
        surface.drawBoxAlpha(i1 + c1 / 2, j1, c1 / 2, 24, l1, 128);
        surface.drawBoxAlpha(i1, j1 + 24, c1, c2 - 24, Surface.rgb2long(220, 220, 220), 128);
        surface.drawLineHoriz(i1, j1 + 24, c1, 0);
        surface.drawLineVert(i1 + c1 / 2, j1, 24, 0);
        surface.drawLineHoriz(i1, (j1 + c2) - 16, c1, 0);
        surface.drawStringCenter("Friends", i1 + c1 / 4, j1 + 16, 4, 0);
        surface.drawStringCenter("Ignore", i1 + c1 / 4 + c1 / 2, j1 + 16, 4, 0);
        panelSocial.clearList(controlSocialPanel);
        if (tabSocial == 0) {
            for (int i2 = 0; i2 < super.friendListCount; i2++) {
                String s;
                if (super.friendListOnline[i2] == 2)
                    s = "@gre@";
                else if (super.friendListOnline[i2] == 1)
                    s = "@yel@";
                else
                    s = "@red@";
                panelSocial.addListEntry(controlSocialPanel, i2, s + Utility.hash2username(super.friendListHashes[i2]) + "~439~@whi@Remove         WWWWWWWWWW");
            }

        }
        if (tabSocial == 1) {
            for (int j2 = 0; j2 < super.ignoreListCount; j2++)
                panelSocial.addListEntry(controlSocialPanel, j2, "@yel@" + Utility.hash2username(super.ignoreList[j2]) + "~439~@whi@Remove         WWWWWWWWWW");

        }
        panelSocial.drawPanel();
        if (tabSocial == 0) {
            int k2 = panelSocial.getListEntryIndex(controlSocialPanel);
            if (k2 >= 0 && super.mouseX < 489) {
                if (super.mouseX > 429)
                    surface.drawStringCenter("Click to remove " + Utility.hash2username(super.friendListHashes[k2]), i1 + c1 / 2, j1 + 35, 1, 0xffffff);
                else if (super.friendListOnline[k2] == 2)
                    surface.drawStringCenter("Click to message " + Utility.hash2username(super.friendListHashes[k2]), i1 + c1 / 2, j1 + 35, 1, 0xffffff);
                else if (super.friendListOnline[k2] == 1)
                    surface.drawStringCenter(Utility.hash2username(super.friendListHashes[k2]) + " is on a different server", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
                else
                    surface.drawStringCenter(Utility.hash2username(super.friendListHashes[k2]) + " is offline", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
            } else {
                surface.drawStringCenter("Click a name to send a message", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
            }
            int k3;
            if (super.mouseX > i1 && super.mouseX < i1 + c1 && super.mouseY > (j1 + c2) - 16 && super.mouseY < j1 + c2)
                k3 = 0xffff00;
            else
                k3 = 0xffffff;
            surface.drawStringCenter("Click here to add a friend", i1 + c1 / 2, (j1 + c2) - 3, 1, k3);
        }
        if (tabSocial == 1) {
            int l2 = panelSocial.getListEntryIndex(controlSocialPanel);
            if (l2 >= 0 && super.mouseX < 489 && super.mouseX > 429) {
                if (super.mouseX > 429)
                    surface.drawStringCenter("Click to remove " + Utility.hash2username(super.ignoreList[l2]), i1 + c1 / 2, j1 + 35, 1, 0xffffff);
            } else {
                surface.drawStringCenter("Blocking messages from:", i1 + c1 / 2, j1 + 35, 1, 0xffffff);
            }
            int l3;
            if (super.mouseX > i1 && super.mouseX < i1 + c1 && super.mouseY > (j1 + c2) - 16 && super.mouseY < j1 + c2)
                l3 = 0xffff00;
            else
                l3 = 0xffffff;
            surface.drawStringCenter("Click here to add a name", i1 + c1 / 2, (j1 + c2) - 3, 1, l3);
        }
        if (!flag)
            return;
        i1 = super.mouseX - (((Surface) (surface)).width2 - 199);
        j1 = super.mouseY - 36;
        if (i1 >= 0 && j1 >= 0 && i1 < 196 && j1 < 182) {
            panelSocial.handleMouse(i1 + (((Surface) (surface)).width2 - 199), j1 + 36, super.lastMouseButtonDown, super.mouseButtonDown);
            if (j1 <= 24 && mouseButtonClick == 1)
                if (i1 < 98 && tabSocial == 1) {
                    tabSocial = 0;
                    panelSocial.resetListProps(controlSocialPanel);
                } else if (i1 > 98 && tabSocial == 0) {
                    tabSocial = 1;
                    panelSocial.resetListProps(controlSocialPanel);
                }
            if (mouseButtonClick == 1 && tabSocial == 0) {
                int i3 = panelSocial.getListEntryIndex(controlSocialPanel);
                if (i3 >= 0 && super.mouseX < 489)
                    if (super.mouseX > 429)
                        y(super.friendListHashes[i3]);
                    else if (super.friendListOnline[i3] != 0) {
                        showDialogSocialInput = 2;
                        privateMessageTarget = super.friendListHashes[i3];
                        super.inputPmCurrent = "";
                        super.inputPmFinal = "";
                    }
            }
            if (mouseButtonClick == 1 && tabSocial == 1) {
                int j3 = panelSocial.getListEntryIndex(controlSocialPanel);
                if (j3 >= 0 && super.mouseX < 489 && super.mouseX > 429)
                    cb(super.ignoreList[j3]);
            }
            if (j1 > 166 && mouseButtonClick == 1 && tabSocial == 0) {
                showDialogSocialInput = 1;
                super.inputTextCurrent = "";
                super.inputTextFinal = "";
            }
            if (j1 > 166 && mouseButtonClick == 1 && tabSocial == 1) {
                showDialogSocialInput = 3;
                super.inputTextCurrent = "";
                super.inputTextFinal = "";
            }
            mouseButtonClick = 0;
        }
    }

    public void drawUiTabOptions(boolean flag) {
        int i1 = ((Surface) (surface)).width2 - 199;
        int j1 = 36;
        surface.drawSprite(i1 - 49, 3, spriteMedia + 6);
        char c1 = '\304';
        surface.drawBoxAlpha(i1, 36, c1, 90, Surface.rgb2long(181, 181, 181), 160);
        surface.drawBoxAlpha(i1, 126, c1, 105, Surface.rgb2long(201, 2011, 201), 160);
        surface.drawBoxAlpha(i1, 231, c1, 30, Surface.rgb2long(181, 181, 181), 160);
        int k1 = i1 + 3;
        int i2 = j1 + 15;
        surface.drawstring("Game options - click to toggle", k1, i2, 1, 0);
        i2 += 15;
        if (optionCameraModeAuto)
            surface.drawstring("Camera angle mode - @gre@Auto", k1, i2, 1, 0xffffff);
        else
            surface.drawstring("Camera angle mode - @red@Manual", k1, i2, 1, 0xffffff);
        i2 += 15;
        if (optionMouseButtonOne)
            surface.drawstring("Mouse buttons - @red@One", k1, i2, 1, 0xffffff);
        else
            surface.drawstring("Mouse buttons - @gre@Two", k1, i2, 1, 0xffffff);
        i2 += 15;
        if (optionPlayerKiller)
            surface.drawstring("Player type: @red@Player-Killer", k1, i2, 1, 0xffffff);
        else
            surface.drawstring("Player type: @gre@Non Player-Killer", k1, i2, 1, 0xffffff);
        i2 += 15;
        int k2 = 0xffffff;
        if (super.mouseX > k1 && super.mouseX < k1 + c1 && super.mouseY > i2 - 12 && super.mouseY < i2 + 4)
            k2 = 0xffff00;
        surface.drawstring("Change password", k1, i2, 1, k2);
        i2 += 15;
        i2 += 15;
        surface.drawstring("Privacy settings. Will be applied to", i1 + 3, i2, 1, 0);
        i2 += 15;
        surface.drawstring("all people not on your friends list", i1 + 3, i2, 1, 0);
        i2 += 15;
        if (super.settingsHideStatus == 0)
            surface.drawstring("Hide online-status: @red@<off>", i1 + 3, i2, 1, 0xffffff);
        else
            surface.drawstring("Hide online-status: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
        i2 += 15;
        if (super.settingsBlockChat == 0)
            surface.drawstring("Block chat messages: @red@<off>", i1 + 3, i2, 1, 0xffffff);
        else
            surface.drawstring("Block chat messages: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
        i2 += 15;
        if (super.settingsBlockPrivate == 0)
            surface.drawstring("Block private messages: @red@<off>", i1 + 3, i2, 1, 0xffffff);
        else
            surface.drawstring("Block private messages: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
        i2 += 15;
        if (super.settingsBlockTrade == 0)
            surface.drawstring("Block trade requests: @red@<off>", i1 + 3, i2, 1, 0xffffff);
        else
            surface.drawstring("Block trade requests: @gre@<on>", i1 + 3, i2, 1, 0xffffff);
        i2 += 15;
        i2 += 15;
        k2 = 0xffffff;
        if (super.mouseX > k1 && super.mouseX < k1 + c1 && super.mouseY > i2 - 12 && super.mouseY < i2 + 4)
            k2 = 0xffff00;
        surface.drawstring("Click here to logout", i1 + 3, i2, 1, k2);
        if (!flag)
            return;
        i1 = super.mouseX - (((Surface) (surface)).width2 - 199);
        j1 = super.mouseY - 36;
        if (i1 >= 0 && j1 >= 0 && i1 < 196 && j1 < 231) {
            int l2 = ((Surface) (surface)).width2 - 199;
            byte byte0 = 36;
            char c2 = '\304';
            int l1 = l2 + 3;
            int j2 = byte0 + 30;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1) {
                optionCameraModeAuto = !optionCameraModeAuto;
                super.clientStream.newPacket(213);
                super.clientStream.writeByte(0);
                super.clientStream.writeByte(optionCameraModeAuto ? 1 : 0);
                super.clientStream.flushPacket();
            }
            j2 += 15;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1) {
                optionMouseButtonOne = !optionMouseButtonOne;
                super.clientStream.newPacket(213);
                super.clientStream.writeByte(2);
                super.clientStream.writeByte(optionMouseButtonOne ? 1 : 0);
                super.clientStream.flushPacket();
            }
            j2 += 15;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1 && et > 0)
                ft = 1;
            j2 += 15;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1) {
                gt = 1;
                super.inputTextCurrent = "";
                super.inputTextFinal = "";
            }
            boolean flag1 = false;
            j2 += 60;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1) {
                super.settingsHideStatus = 1 - super.settingsHideStatus;
                flag1 = true;
            }
            j2 += 15;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1) {
                super.settingsBlockChat = 1 - super.settingsBlockChat;
                flag1 = true;
            }
            j2 += 15;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1) {
                super.settingsBlockPrivate = 1 - super.settingsBlockPrivate;
                flag1 = true;
            }
            j2 += 15;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1) {
                super.settingsBlockTrade = 1 - super.settingsBlockTrade;
                flag1 = true;
            }
            if (flag1)
                sendPrivacySettings(super.settingsHideStatus, super.settingsBlockChat, super.settingsBlockPrivate, super.settingsBlockTrade, 0);
            j2 += 30;
            if (super.mouseX > l1 && super.mouseX < l1 + c2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && mouseButtonClick == 1)
                trylogout();
            mouseButtonClick = 0;
        }
    }

    public void createRightClickMenu() {
        int i1 = -1;
        for (int j1 = 0; j1 < objectCount; j1++)
            objectAlreadyInMenu[j1] = false;

        for (int k1 = 0; k1 < wallObjectCount; k1++)
            wallObjectAlreadyInMenu[k1] = false;

        int l1 = scene.getModelsUnderMouseCount();
        GameModel ah[] = scene.getModelsUnderMouse();
        int ai[] = scene.getPlayersUnderMouse();
        for (int i2 = 0; i2 < l1; i2++) {
            int j2 = ai[i2];
            GameModel h1 = ah[i2];
            if (h1.faceTag[j2] <= 65535 || h1.faceTag[j2] >= 0x30d40 && h1.faceTag[j2] <= 0x493e0)
                if (h1 == scene.currentModel) {
                    int l2 = h1.faceTag[j2] % 10000;
                    int k3 = h1.faceTag[j2] / 10000;
                    if (k3 == 1) {
                        String s = "";
                        int j4 = -1;
                        int l4 = players[l2].attackable;
                        if (l4 == 1) {
                            j4 = 0;
                            if (localPlayer.level > 0 && players[l2].level > 0)
                                j4 = localPlayer.level - players[l2].level;
                            if (j4 < 0)
                                s = "@or1@";
                            if (j4 < -3)
                                s = "@or2@";
                            if (j4 < -6)
                                s = "@or3@";
                            if (j4 < -9)
                                s = "@red@";
                            if (j4 > 0)
                                s = "@gr1@";
                            if (j4 > 3)
                                s = "@gr2@";
                            if (j4 > 6)
                                s = "@gr3@";
                            if (j4 > 9)
                                s = "@gre@";
                            s = " " + s + "(level-" + players[l2].level + ")";
                        }
                        if (selectedSpell >= 0) {
                            if (GameData.spellVar4[selectedSpell] == 1 || GameData.spellVar4[selectedSpell] == 2 && l4 == 1 && localPlayer.attackable == 1) {
                                menuItemText1[menuItemsCount] = textCast + GameData.spellVar1[selectedSpell] + " on";
                                menuItemText2[menuItemsCount] = "@whi@" + players[l2].name;
                                menuItemId[menuItemsCount] = 800;
                                menuItemX[menuItemsCount] = players[l2].currentX;
                                menuItemY[menuItemsCount] = players[l2].currentY;
                                menuSourceType[menuItemsCount] = players[l2].serverIndex;
                                menuSourceIndex[menuItemsCount] = selectedSpell;
                                menuItemsCount++;
                            }
                        } else if (selectedItemInventoryIndex >= 0) {
                            menuItemText1[menuItemsCount] = "Use " + selectedItemName + " with";
                            menuItemText2[menuItemsCount] = "@whi@" + players[l2].name;
                            menuItemId[menuItemsCount] = 810;
                            menuItemX[menuItemsCount] = players[l2].currentX;
                            menuItemY[menuItemsCount] = players[l2].currentY;
                            menuSourceType[menuItemsCount] = players[l2].serverIndex;
                            menuSourceIndex[menuItemsCount] = selectedItemInventoryIndex;
                            menuItemsCount++;
                        } else {
                            if (l4 == 1 && localPlayer.attackable == 1) {
                                menuItemText1[menuItemsCount] = "Attack";
                                menuItemText2[menuItemsCount] = "@whi@" + players[l2].name + s;
                                if (j4 >= 0 && j4 < 5)
                                    menuItemId[menuItemsCount] = 805;
                                else
                                    menuItemId[menuItemsCount] = 2805;
                                menuItemX[menuItemsCount] = players[l2].currentX;
                                menuItemY[menuItemsCount] = players[l2].currentY;
                                menuSourceType[menuItemsCount] = players[l2].serverIndex;
                                menuItemsCount++;
                            }
                            menuItemText1[menuItemsCount] = "Trade with";
                            menuItemText2[menuItemsCount] = "@whi@" + players[l2].name;
                            menuItemId[menuItemsCount] = 2810;
                            menuSourceType[menuItemsCount] = players[l2].serverIndex;
                            menuItemsCount++;
                            menuItemText1[menuItemsCount] = "Follow";
                            menuItemText2[menuItemsCount] = "@whi@" + players[l2].name;
                            menuItemId[menuItemsCount] = 2820;
                            menuSourceType[menuItemsCount] = players[l2].serverIndex;
                            menuItemsCount++;
                        }
                    } else if (k3 == 2) {
                        if (selectedSpell >= 0) {
                            if (GameData.spellVar4[selectedSpell] == 3) {
                                menuItemText1[menuItemsCount] = textCast + GameData.spellVar1[selectedSpell] + " on";
                                menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[groundItemId[l2]][0];
                                menuItemId[menuItemsCount] = 200;
                                menuItemX[menuItemsCount] = groundItemX[l2];
                                menuItemY[menuItemsCount] = groundItemY[l2];
                                menuSourceType[menuItemsCount] = groundItemId[l2];
                                menuSourceIndex[menuItemsCount] = selectedSpell;
                                menuItemsCount++;
                            }
                        } else if (selectedItemInventoryIndex >= 0) {
                            menuItemText1[menuItemsCount] = "Use " + selectedItemName + " with";
                            menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[groundItemId[l2]][0];
                            menuItemId[menuItemsCount] = 210;
                            menuItemX[menuItemsCount] = groundItemX[l2];
                            menuItemY[menuItemsCount] = groundItemY[l2];
                            menuSourceType[menuItemsCount] = groundItemId[l2];
                            menuSourceIndex[menuItemsCount] = selectedItemInventoryIndex;
                            menuItemsCount++;
                        } else {
                            menuItemText1[menuItemsCount] = "Take";
                            menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[groundItemId[l2]][0];
                            menuItemId[menuItemsCount] = 220;
                            menuItemX[menuItemsCount] = groundItemX[l2];
                            menuItemY[menuItemsCount] = groundItemY[l2];
                            menuSourceType[menuItemsCount] = groundItemId[l2];
                            menuItemsCount++;
                            menuItemText1[menuItemsCount] = "Examine";
                            menuItemText2[menuItemsCount] = "@lre@" + GameData.objectVar1[groundItemId[l2]][0];
                            menuItemId[menuItemsCount] = 3200;
                            menuSourceType[menuItemsCount] = groundItemId[l2];
                            menuItemsCount++;
                        }
                    } else if (k3 == 3) {
                        String s1 = "";
                        int k4 = -1;
                        int i5 = npcs[l2].npcId;
                        if (GameData.npcVar7[i5] > 0) {
                            int j5 = (GameData.npcVar3[i5] + GameData.npcVar6[i5] + GameData.npcVar4[i5] + GameData.npcVar5[i5]) / 4;
                            int k5 = (playerStatBase[0] + playerStatBase[1] + playerStatBase[2] + playerStatBase[3] + 27) / 4;
                            k4 = k5 - j5;
                            s1 = "@yel@";
                            if (k4 < 0)
                                s1 = "@or1@";
                            if (k4 < -3)
                                s1 = "@or2@";
                            if (k4 < -6)
                                s1 = "@or3@";
                            if (k4 < -9)
                                s1 = "@red@";
                            if (k4 > 0)
                                s1 = "@gr1@";
                            if (k4 > 3)
                                s1 = "@gr2@";
                            if (k4 > 6)
                                s1 = "@gr3@";
                            if (k4 > 9)
                                s1 = "@gre@";
                            s1 = " " + s1 + "(level-" + j5 + ")";
                        }
                        if (selectedSpell >= 0) {
                            if (GameData.spellVar4[selectedSpell] == 2) {
                                menuItemText1[menuItemsCount] = textCast + GameData.spellVar1[selectedSpell] + " on";
                                menuItemText2[menuItemsCount] = "@yel@" + GameData.npcVar1[npcs[l2].npcId][0];
                                menuItemId[menuItemsCount] = 700;
                                menuItemX[menuItemsCount] = npcs[l2].currentX;
                                menuItemY[menuItemsCount] = npcs[l2].currentY;
                                menuSourceType[menuItemsCount] = npcs[l2].serverIndex;
                                menuSourceIndex[menuItemsCount] = selectedSpell;
                                menuItemsCount++;
                            }
                        } else if (selectedItemInventoryIndex >= 0) {
                            menuItemText1[menuItemsCount] = "Use " + selectedItemName + " with";
                            menuItemText2[menuItemsCount] = "@yel@" + GameData.npcVar1[npcs[l2].npcId][0];
                            menuItemId[menuItemsCount] = 710;
                            menuItemX[menuItemsCount] = npcs[l2].currentX;
                            menuItemY[menuItemsCount] = npcs[l2].currentY;
                            menuSourceType[menuItemsCount] = npcs[l2].serverIndex;
                            menuSourceIndex[menuItemsCount] = selectedItemInventoryIndex;
                            menuItemsCount++;
                        } else {
                            if (GameData.npcVar7[i5] > 0) {
                                menuItemText1[menuItemsCount] = "Attack";
                                menuItemText2[menuItemsCount] = "@yel@" + GameData.npcVar1[npcs[l2].npcId][0] + s1;
                                if (k4 >= 0)
                                    menuItemId[menuItemsCount] = 715;
                                else
                                    menuItemId[menuItemsCount] = 2715;
                                menuItemX[menuItemsCount] = npcs[l2].currentX;
                                menuItemY[menuItemsCount] = npcs[l2].currentY;
                                menuSourceType[menuItemsCount] = npcs[l2].serverIndex;
                                menuItemsCount++;
                            }
                            menuItemText1[menuItemsCount] = "Talk-to";
                            menuItemText2[menuItemsCount] = "@yel@" + GameData.npcVar1[npcs[l2].npcId][0];
                            menuItemId[menuItemsCount] = 720;
                            menuItemX[menuItemsCount] = npcs[l2].currentX;
                            menuItemY[menuItemsCount] = npcs[l2].currentY;
                            menuSourceType[menuItemsCount] = npcs[l2].serverIndex;
                            menuItemsCount++;
                            menuItemText1[menuItemsCount] = "Examine";
                            menuItemText2[menuItemsCount] = "@yel@" + GameData.npcVar1[npcs[l2].npcId][0];
                            menuItemId[menuItemsCount] = 3700;
                            menuSourceType[menuItemsCount] = npcs[l2].npcId;
                            menuItemsCount++;
                        }
                    }
                } else if (h1 != null && h1.key >= 10000) {
                    int i3 = h1.key - 10000;
                    int l3 = wallObjectId[i3];
                    if (!wallObjectAlreadyInMenu[i3]) {
                        if (selectedSpell >= 0) {
                            if (GameData.spellVar4[selectedSpell] == 4) {
                                menuItemText1[menuItemsCount] = textCast + GameData.spellVar1[selectedSpell] + " on";
                                menuItemText2[menuItemsCount] = "@cya@" + GameData.boundaryVar1[l3][0];
                                menuItemId[menuItemsCount] = 300;
                                menuItemX[menuItemsCount] = wallObjectX[i3];
                                menuItemY[menuItemsCount] = wallObjectY[i3];
                                menuSourceType[menuItemsCount] = wallObjectDirection[i3];
                                menuSourceIndex[menuItemsCount] = selectedSpell;
                                menuItemsCount++;
                            }
                        } else if (selectedItemInventoryIndex >= 0) {
                            menuItemText1[menuItemsCount] = "Use " + selectedItemName + " with";
                            menuItemText2[menuItemsCount] = "@cya@" + GameData.boundaryVar1[l3][0];
                            menuItemId[menuItemsCount] = 310;
                            menuItemX[menuItemsCount] = wallObjectX[i3];
                            menuItemY[menuItemsCount] = wallObjectY[i3];
                            menuSourceType[menuItemsCount] = wallObjectDirection[i3];
                            menuSourceIndex[menuItemsCount] = selectedItemInventoryIndex;
                            menuItemsCount++;
                        } else {
                            if (!GameData.boundaryVar9[l3].equalsIgnoreCase("WalkTo")) {
                                menuItemText1[menuItemsCount] = GameData.boundaryVar9[l3];
                                menuItemText2[menuItemsCount] = "@cya@" + GameData.boundaryVar1[l3][0];
                                menuItemId[menuItemsCount] = 320;
                                menuItemX[menuItemsCount] = wallObjectX[i3];
                                menuItemY[menuItemsCount] = wallObjectY[i3];
                                menuSourceType[menuItemsCount] = wallObjectDirection[i3];
                                menuItemsCount++;
                            }
                            if (!GameData.boundaryVar10[l3].equalsIgnoreCase("Examine")) {
                                menuItemText1[menuItemsCount] = GameData.boundaryVar10[l3];
                                menuItemText2[menuItemsCount] = "@cya@" + GameData.boundaryVar1[l3][0];
                                menuItemId[menuItemsCount] = 2300;
                                menuItemX[menuItemsCount] = wallObjectX[i3];
                                menuItemY[menuItemsCount] = wallObjectY[i3];
                                menuSourceType[menuItemsCount] = wallObjectDirection[i3];
                                menuItemsCount++;
                            }
                            menuItemText1[menuItemsCount] = "Examine";
                            menuItemText2[menuItemsCount] = "@cya@" + GameData.boundaryVar1[l3][0];
                            menuItemId[menuItemsCount] = 3300;
                            menuSourceType[menuItemsCount] = l3;
                            menuItemsCount++;
                        }
                        wallObjectAlreadyInMenu[i3] = true;
                    }
                } else if (h1 != null && h1.key >= 0) {
                    int j3 = h1.key;
                    int i4 = objectId[j3];
                    if (!objectAlreadyInMenu[j3]) {
                        if (selectedSpell >= 0) {
                            if (GameData.spellVar4[selectedSpell] == 5) {
                                menuItemText1[menuItemsCount] = textCast + GameData.spellVar1[selectedSpell] + " on";
                                menuItemText2[menuItemsCount] = "@cya@" + GameData.locationVar1[i4][0];
                                menuItemId[menuItemsCount] = 400;
                                menuItemX[menuItemsCount] = objectX[j3];
                                menuItemY[menuItemsCount] = objectY[j3];
                                menuSourceType[menuItemsCount] = objectDirection[j3];
                                menuSourceIndex[menuItemsCount] = objectId[j3];
                                menuTargetIndex[menuItemsCount] = selectedSpell;
                                menuItemsCount++;
                            }
                        } else if (selectedItemInventoryIndex >= 0) {
                            menuItemText1[menuItemsCount] = "Use " + selectedItemName + " with";
                            menuItemText2[menuItemsCount] = "@cya@" + GameData.locationVar1[i4][0];
                            menuItemId[menuItemsCount] = 410;
                            menuItemX[menuItemsCount] = objectX[j3];
                            menuItemY[menuItemsCount] = objectY[j3];
                            menuSourceType[menuItemsCount] = objectDirection[j3];
                            menuSourceIndex[menuItemsCount] = objectId[j3];
                            menuTargetIndex[menuItemsCount] = selectedItemInventoryIndex;
                            menuItemsCount++;
                        } else {
                            if (!GameData.locationVar8[i4].equalsIgnoreCase("WalkTo")) {
                                menuItemText1[menuItemsCount] = GameData.locationVar8[i4];
                                menuItemText2[menuItemsCount] = "@cya@" + GameData.locationVar1[i4][0];
                                menuItemId[menuItemsCount] = 420;
                                menuItemX[menuItemsCount] = objectX[j3];
                                menuItemY[menuItemsCount] = objectY[j3];
                                menuSourceType[menuItemsCount] = objectDirection[j3];
                                menuSourceIndex[menuItemsCount] = objectId[j3];
                                menuItemsCount++;
                            }
                            if (!GameData.locationVar9[i4].equalsIgnoreCase("Examine")) {
                                menuItemText1[menuItemsCount] = GameData.locationVar9[i4];
                                menuItemText2[menuItemsCount] = "@cya@" + GameData.locationVar1[i4][0];
                                menuItemId[menuItemsCount] = 2400;
                                menuItemX[menuItemsCount] = objectX[j3];
                                menuItemY[menuItemsCount] = objectY[j3];
                                menuSourceType[menuItemsCount] = objectDirection[j3];
                                menuSourceIndex[menuItemsCount] = objectId[j3];
                                menuItemsCount++;
                            }
                            menuItemText1[menuItemsCount] = "Examine";
                            menuItemText2[menuItemsCount] = "@cya@" + GameData.locationVar1[i4][0];
                            menuItemId[menuItemsCount] = 3400;
                            menuSourceType[menuItemsCount] = i4;
                            menuItemsCount++;
                        }
                        objectAlreadyInMenu[j3] = true;
                    }
                } else {
                    if (j2 >= 0)
                        j2 = h1.faceTag[j2] - 0x30d40;
                    if (j2 >= 0)
                        i1 = j2;
                }
        }

        if (selectedSpell >= 0 && GameData.spellVar4[selectedSpell] <= 1) {
            menuItemText1[menuItemsCount] = textCast + "on self";
            menuItemText2[menuItemsCount] = "";
            menuItemId[menuItemsCount] = 1000;
            menuSourceType[menuItemsCount] = selectedSpell;
            menuItemsCount++;
        }
        if (i1 != -1) {
            int k2 = i1;
            if (selectedSpell >= 0) {
                if (GameData.spellVar4[selectedSpell] == 6) {
                    menuItemText1[menuItemsCount] = textCast + "on ground";
                    menuItemText2[menuItemsCount] = "";
                    menuItemId[menuItemsCount] = 900;
                    menuItemX[menuItemsCount] = world.mouseX[k2];
                    menuItemY[menuItemsCount] = world.mouseY[k2];
                    menuSourceType[menuItemsCount] = selectedSpell;
                    menuItemsCount++;
                    return;
                }
            } else if (selectedItemInventoryIndex < 0) {
                menuItemText1[menuItemsCount] = "Walk here";
                menuItemText2[menuItemsCount] = "";
                menuItemId[menuItemsCount] = 920;
                menuItemX[menuItemsCount] = world.mouseX[k2];
                menuItemY[menuItemsCount] = world.mouseY[k2];
                menuItemsCount++;
            }
        }
    }

    public void drawRightClickMenu() {
        if (mouseButtonClick != 0) {
            for (int i1 = 0; i1 < menuItemsCount; i1++) {
                int k1 = menuX + 2;
                int i2 = menuY + 27 + i1 * 15;
                if (super.mouseX <= k1 - 2 || super.mouseY <= i2 - 12 || super.mouseY >= i2 + 4 || super.mouseX >= (k1 - 3) + menuWidth)
                    continue;
                menuItemClick(menuIndices[i1]);
                break;
            }

            mouseButtonClick = 0;
            showRightClickMenu = false;
            return;
        }
        if (super.mouseX < menuX - 10 || super.mouseY < menuY - 10 || super.mouseX > menuX + menuWidth + 10 || super.mouseY > menuY + menuHeight + 10) {
            showRightClickMenu = false;
            return;
        }
        surface.drawBoxAlpha(menuX, menuY, menuWidth, menuHeight, 0xd0d0d0, 160);
        surface.drawstring("Choose option", menuX + 2, menuY + 12, 1, 65535);
        for (int j1 = 0; j1 < menuItemsCount; j1++) {
            int l1 = menuX + 2;
            int j2 = menuY + 27 + j1 * 15;
            int k2 = 0xffffff;
            if (super.mouseX > l1 - 2 && super.mouseY > j2 - 12 && super.mouseY < j2 + 4 && super.mouseX < (l1 - 3) + menuWidth)
                k2 = 0xffff00;
            surface.drawstring(menuItemText1[menuIndices[j1]] + " " + menuItemText2[menuIndices[j1]], l1, j2, 1, k2);
        }

    }

    public void createTopMouseMenu() {
        if (selectedSpell >= 0 || selectedItemInventoryIndex >= 0) {
            menuItemText1[menuItemsCount] = "Cancel";
            menuItemText2[menuItemsCount] = "";
            menuItemId[menuItemsCount] = 4000;
            menuItemsCount++;
        }
        for (int i1 = 0; i1 < menuItemsCount; i1++)
            menuIndices[i1] = i1;

        for (boolean flag = false; !flag; ) {
            flag = true;
            for (int j1 = 0; j1 < menuItemsCount - 1; j1++) {
                int l1 = menuIndices[j1];
                int j2 = menuIndices[j1 + 1];
                if (menuItemId[l1] > menuItemId[j2]) {
                    menuIndices[j1] = j2;
                    menuIndices[j1 + 1] = l1;
                    flag = false;
                }
            }

        }

        if (menuItemsCount > 20)
            menuItemsCount = 20;
        if (menuItemsCount > 0) {
            int k1 = -1;
            for (int i2 = 0; i2 < menuItemsCount; i2++) {
                if (menuItemText2[menuIndices[i2]] == null || menuItemText2[menuIndices[i2]].length() <= 0)
                    continue;
                k1 = i2;
                break;
            }

            String s = null;
            if ((selectedItemInventoryIndex >= 0 || selectedSpell >= 0) && menuItemsCount == 1)
                s = "Choose a target";
            else if ((selectedItemInventoryIndex >= 0 || selectedSpell >= 0) && menuItemsCount > 1)
                s = "@whi@" + menuItemText1[menuIndices[0]] + " " + menuItemText2[menuIndices[0]];
            else if (k1 != -1)
                s = menuItemText2[menuIndices[k1]] + ": @whi@" + menuItemText1[menuIndices[0]];
            if (menuItemsCount == 2 && s != null)
                s = s + "@whi@ / 1 more option";
            if (menuItemsCount > 2 && s != null)
                s = s + "@whi@ / " + (menuItemsCount - 1) + " more options";
            if (s != null)
                surface.drawstring(s, 6, 14, 1, 0xffff00);
            if (!optionMouseButtonOne && mouseButtonClick == 1 || optionMouseButtonOne && mouseButtonClick == 1 && menuItemsCount == 1) {
                menuItemClick(menuIndices[0]);
                mouseButtonClick = 0;
                return;
            }
            if (!optionMouseButtonOne && mouseButtonClick == 2 || optionMouseButtonOne && mouseButtonClick == 1) {
                menuHeight = (menuItemsCount + 1) * 15;
                menuWidth = surface.textWidth("Choose option", 1) + 5;
                for (int k2 = 0; k2 < menuItemsCount; k2++) {
                    int l2 = surface.textWidth(menuItemText1[k2] + " " + menuItemText2[k2], 1) + 5;
                    if (l2 > menuWidth)
                        menuWidth = l2;
                }

                menuX = super.mouseX - menuWidth / 2;
                menuY = super.mouseY - 7;
                showRightClickMenu = true;
                if (menuX < 0)
                    menuX = 0;
                if (menuY < 0)
                    menuY = 0;
                if (menuX + menuWidth > 510)
                    menuX = 510 - menuWidth;
                if (menuY + menuHeight > 315)
                    menuY = 315 - menuHeight;
                mouseButtonClick = 0;
            }
        }
    }

    public void menuItemClick(int i1) {
        int j1 = menuItemX[i1];
        int k1 = menuItemY[i1];
        int l1 = menuSourceType[i1];
        int i2 = menuSourceIndex[i1];
        int j2 = menuTargetIndex[i1];
        int k2 = menuItemId[i1];
        if (k2 == 200) {
            walkToGroundItem(regionX, regionY, j1, k1, true);
            super.clientStream.newPacket(224);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 210) {
            walkToGroundItem(regionX, regionY, j1, k1, true);
            super.clientStream.newPacket(250);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedItemInventoryIndex = -1;
        }
        if (k2 == 220) {
            walkToGroundItem(regionX, regionY, j1, k1, true);
            super.clientStream.newPacket(252);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 3200)
            showMessage(GameData.objectVar2[l1], 3);
        if (k2 == 300) {
            walkToWallObject(j1, k1, l1);
            super.clientStream.newPacket(223);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeByte(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 310) {
            walkToWallObject(j1, k1, l1);
            super.clientStream.newPacket(239);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeByte(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedItemInventoryIndex = -1;
        }
        if (k2 == 320) {
            walkToWallObject(j1, k1, l1);
            super.clientStream.newPacket(238);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeByte(l1);
            finalisePacket();
        }
        if (k2 == 2300) {
            walkToWallObject(j1, k1, l1);
            super.clientStream.newPacket(229);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeByte(l1);
            finalisePacket();
        }
        if (k2 == 3300)
            showMessage(GameData.boundaryVar2[l1], 3);
        if (k2 == 400) {
            walkToObject(j1, k1, l1, i2);
            super.clientStream.newPacket(222);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeShort(j2);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 410) {
            walkToObject(j1, k1, l1, i2);
            super.clientStream.newPacket(241);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeShort(j2);
            finalisePacket();
            selectedItemInventoryIndex = -1;
        }
        if (k2 == 420) {
            walkToObject(j1, k1, l1, i2);
            super.clientStream.newPacket(242);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            finalisePacket();
        }
        if (k2 == 2400) {
            walkToObject(j1, k1, l1, i2);
            super.clientStream.newPacket(230);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            finalisePacket();
        }
        if (k2 == 3400)
            showMessage(GameData.locationVar2[l1], 3);
        if (k2 == 600) {
            super.clientStream.newPacket(220);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 610) {
            super.clientStream.newPacket(240);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedItemInventoryIndex = -1;
        }
        if (k2 == 620) {
            super.clientStream.newPacket(248);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 630) {
            super.clientStream.newPacket(249);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 640) {
            super.clientStream.newPacket(246);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 650) {
            selectedItemInventoryIndex = l1;
            showUiTab = 0;
            selectedItemName = GameData.objectVar1[inventoryItemId[selectedItemInventoryIndex]][0];
        }
        if (k2 == 660) {
            super.clientStream.newPacket(251);
            super.clientStream.writeShort(l1);
            finalisePacket();
            selectedItemInventoryIndex = -1;
            showUiTab = 0;
            showMessage("Dropping " + GameData.objectVar1[inventoryItemId[l1]][0], 4);
        }
        if (k2 == 3600)
            showMessage(GameData.objectVar2[l1], 3);
        if (k2 == 700) {
            int l2 = (j1 - 64) / magicLoc;
            int k4 = (k1 - 64) / magicLoc;
            walkToActionSource(regionX, regionY, l2, k4, true);
            super.clientStream.newPacket(225);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 710) {
            int i3 = (j1 - 64) / magicLoc;
            int l4 = (k1 - 64) / magicLoc;
            walkToActionSource(regionX, regionY, i3, l4, true);
            super.clientStream.newPacket(243);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedItemInventoryIndex = -1;
        }
        if (k2 == 720) {
            int j3 = (j1 - 64) / magicLoc;
            int i5 = (k1 - 64) / magicLoc;
            walkToActionSource(regionX, regionY, j3, i5, true);
            super.clientStream.newPacket(245);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 715 || k2 == 2715) {
            int k3 = (j1 - 64) / magicLoc;
            int j5 = (k1 - 64) / magicLoc;
            walkToActionSource(regionX, regionY, k3, j5, true);
            super.clientStream.newPacket(244);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 3700)
            showMessage(GameData.npcVar2[l1], 3);
        if (k2 == 800) {
            int l3 = (j1 - 64) / magicLoc;
            int k5 = (k1 - 64) / magicLoc;
            walkToActionSource(regionX, regionY, l3, k5, true);
            super.clientStream.newPacket(226);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 810) {
            int i4 = (j1 - 64) / magicLoc;
            int l5 = (k1 - 64) / magicLoc;
            walkToActionSource(regionX, regionY, i4, l5, true);
            super.clientStream.newPacket(219);
            super.clientStream.writeShort(l1);
            super.clientStream.writeShort(i2);
            finalisePacket();
            selectedItemInventoryIndex = -1;
        }
        if (k2 == 805 || k2 == 2805) {
            int j4 = (j1 - 64) / magicLoc;
            int i6 = (k1 - 64) / magicLoc;
            walkToActionSource(regionX, regionY, j4, i6, true);
            super.clientStream.newPacket(228);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 2810) {
            super.clientStream.newPacket(235);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 2820) {
            super.clientStream.newPacket(214);
            super.clientStream.writeShort(l1);
            finalisePacket();
        }
        if (k2 == 900) {
            walkToActionSource(regionX, regionY, j1, k1, true);
            super.clientStream.newPacket(221);
            super.clientStream.writeShort(j1 + areaX);
            super.clientStream.writeShort(k1 + areaY);
            super.clientStream.writeShort(l1);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 920) {
            walkToActionSource(regionX, regionY, j1, k1, false);
            if (mouseClickXStep == -24)
                mouseClickXStep = 24;
        }
        if (k2 == 1000) {
            super.clientStream.newPacket(227);
            super.clientStream.writeShort(l1);
            finalisePacket();
            selectedSpell = -1;
        }
        if (k2 == 4000) {
            selectedItemInventoryIndex = -1;
            selectedSpell = -1;
        }
    }
}
